/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { localStorage, sessionStorage } from 'fa-storage';
import * as serialize from 'serialize-javascript';

export abstract class StorageService {
    protected storage: any;

    protected constructor(storage: any) {
        this.storage = storage;
    }

    public load = (key: string) => {
        try {
            const serializedState = this.storage.getItem(key);
            return serializedState == null
                ? undefined
                : JSON.parse(serializedState);
        } catch (ex) {
            return undefined;
        }
    }
    public remove = (key: string) => {
        try {
            this.storage.removeItem(key);
        } catch (ex) {
            // do nothing
        }
    }
    public save = (key: string, state: any) => {
        try {
            const serializedState = serialize(state);
            this.storage.setItem(key, serializedState);
        } catch (ex) {
            // do nothing
        }
    }
}

export class LocalStorageService extends StorageService {
    constructor() {
        super(localStorage);
    }
}

export class SessionStorageService extends StorageService {
    constructor() {
        super(sessionStorage);
    }
}
