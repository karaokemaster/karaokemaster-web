/* tslint:disable:unified-signatures */
// Original source: https://gist.github.com/EvAlex/ad0e43f4087e2e813a8f4cd872b433b8

export const RGB_COLOR_REGEX = /\((\d+),\s*(\d+),\s*(\d+)(,\s*(\d*.\d*))?\)/;

export class Color {
    public r: number = 0;
    public g: number = 0;
    public b: number = 0;
    public a: number = 1;

    constructor()
    constructor(colorStr?: string)
    constructor(r?: string|number, g?: number, b?: number)
    constructor(r?: string|number, g?: number, b?: number, a?: number) {
        if (typeof r === 'string') {
            r = r.trim();
            if (r.indexOf('#') === 0) {
                r = r.substr(r.indexOf('#') + 1);
                const compSize = r.length === 3 ? 1 : 2;
                const multiplier = r.length === 3 ? 16 : 1;
                this.r = parseInt(r.substr(0,            compSize), 16) * multiplier;
                this.g = parseInt(r.substr(1 * compSize, compSize), 16) * multiplier;
                this.b = parseInt(r.substr(2 * compSize, compSize), 16) * multiplier;
            } else if (r.indexOf('rgb') === 0) {
                const res = RGB_COLOR_REGEX.exec(r);
                if (!res) return;

                this.r = parseInt(res[1], 10);
                this.g = parseInt(res[2], 10);
                this.b = parseInt(res[3], 10);
                this.a = res[5] ? parseFloat(res[5]) : 1;
            }
        } else {
            this.r = r ?? this.r;
            this.g = g ?? this.g;
            this.b = b ?? this.b;
            this.a = a ?? this.a;
        }
    }

    public toHex = (backgroundColor?: Color): string => {
        return (backgroundColor
                ? this.applyAlpha(backgroundColor).toHex()
                : '#'
                    + this.r.toString(16).padStart(2, '0')
                    + this.g.toString(16).padStart(2, '0')
                    + this.b.toString(16).padStart(2, '0')
        );
    };

    public toRgb = (backgroundColor?: Color): string => {
        return (backgroundColor
                ? this.applyAlpha(backgroundColor).toRgb()
                : `rgb(${this.r}, ${this.g}, ${this.b})`
        );
    };

    public toRgba = (backgroundColor?: Color): string => {
        return (backgroundColor
                ? this.applyAlpha(backgroundColor).toRgba()
                : `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`
        );
    };

    private applyAlpha = (backgroundColor: Color): Color => {
        return new Color(
            Math.round(this.r * this.a + backgroundColor.r * (1 - this.a)),
            Math.round(this.g * this.a + backgroundColor.g * (1 - this.a)),
            Math.round(this.b * this.a + backgroundColor.b * (1 - this.a))
        );
    }
}