/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

// saga
import { debounce } from 'debounce';
import { applyMiddleware, createStore, Middleware, Reducer, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { registerSelectors } from 'reselect-tools';

import * as emceeSignalR from 'store/emcee/signalR';
import { notificationsMiddleware } from 'store/middleware/notifications';
import { sentryTracingMiddleware } from 'store/middleware/sentryTracing';
import { signalRInvokeMiddleware, signalRRegisterCommands } from 'store/middleware/signalR';
import { EmceeActionTypes } from 'store/emcee';
import { PlayerActionTypes } from 'store/player';
import * as playerSignalR from 'store/player/signalR';
import * as queueSignalR from 'store/queue/signalR';
import { loadState, saveState } from './persistedState';
import { ApplicationState, createRootReducer, rootSaga, selectors } from './store';

const ONE_SECOND = 1000;

export const actionsBlacklist = [
    PlayerActionTypes.SET_STATE,
    PlayerActionTypes.STATE_RECEIVED,
    PlayerActionTypes.BEAT,
    EmceeActionTypes.MICROPHONE_RECEIVED,
];

export default function configureStore(
    routerMiddleware: Middleware,
    routerReducer: Reducer,
    initialState: ApplicationState
): Store<ApplicationState> {
    const composeEnhancers = composeWithDevTools({
        trace: true,
        traceLimit: 25,
        actionsBlacklist,
    });

    const sagaMiddleware = createSagaMiddleware();

    const persistedState: Partial<ApplicationState> = loadState();
    const state = {...initialState, ...persistedState};

    const store = createStore(
        createRootReducer(routerReducer),
        state,
        composeEnhancers(
            applyMiddleware(
                sentryTracingMiddleware,
                routerMiddleware,
                sagaMiddleware,
                signalRInvokeMiddleware,
                playerSignalR.invokeMiddleware,
                queueSignalR.invokeMiddleware,
                emceeSignalR.invokeMiddleware,
                notificationsMiddleware,
            )
        )
    );

    store.subscribe(
        debounce(
            () => saveState(store),
            ONE_SECOND,
        ),
    );

    // tslint:disable-next-line:no-empty
    signalRRegisterCommands(store);
    playerSignalR.registerCommands(store);
    queueSignalR.registerCommands(store);
    emceeSignalR.registerCommands(store);

    sagaMiddleware.run(rootSaga);

    registerSelectors(selectors);

    return store;
}