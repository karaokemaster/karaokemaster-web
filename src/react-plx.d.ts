interface PropertiesItemType {
    startValue: string | number;
    endValue: string | number;
    property: string;
    unit?: string;
}

interface ParallaxDataType {
    start: string | number | HTMLElement;
    startOffset?: string | number;
    duration?: string | number | HTMLElement;
    end?: string | number | HTMLElement;
    endOffset?: string | number;
    properties: PropertiesItemType[];
    // tslint:disable-next-line:ban-types
    easing?: string | string[] | Function;
    name?: string;
}

interface IPlxPros {
    animateWhenNotInViewport?: boolean
    children?: any;
    className?: string;
    disabled?: boolean;
    freeze?: boolean;
    parallaxData: ParallaxDataType[];
    style?: string | number | object;
    tagName?: string;
    // @ts-ignore
    onPlxStart?: PropTypes.func;
    // @ts-ignore
    onPlxEnd?: PropTypes.func;
}

declare class Plx extends React.Component<IPlxPros> {
    constructor(props: IPlxPros)
}

declare module 'react-plx' {
    export default Plx;
}