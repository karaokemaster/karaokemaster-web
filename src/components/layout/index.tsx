/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Theme, Typography } from '@mui/material';
import { createTheme, ThemeProvider, StyledEngineProvider, adaptV4Theme } from '@mui/material/styles';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/react';
import { compose, withEffect } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react'
import { Outlet } from 'react-router';

import { Error } from 'components/error';
import { RequireUser } from 'components/requireUser';
import { notificationsEnabledSelector, notificationSubscriptionSelector } from 'store/app';
import * as appActions from 'store/app/actions';
import { userHasTrackInQueueSelector } from 'store/queue';
import { Connecting } from './connecting';
import { MediaControls } from './media-controls';
import { NavTabs } from './tabs';
import { TrackBanner } from './trackBanner';


declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}


const theme = createTheme(adaptV4Theme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#c91ee2',
        },
        secondary: {
            main: '#4caf50',
        },
    },
}));

const styles = (theme: Theme) => createStyles({
    root: {
        userSelect: 'none',
    },
    hasTrackBanner: {
        paddingTop: '30px',
    },
    hasNotificationPrompt: {
        paddingTop: '90px',
    },
});

interface PropsFromState {
    hasTrackBanner: boolean;
    hasNotificationPrompt: boolean;
}

type LayoutProps = PropsFromState;

class LayoutLayout extends React.Component<LayoutProps> {
    public render = () => {
        const { hasTrackBanner, hasNotificationPrompt } = this.props;
        const { classes } = this.props as any;

        const rootClass = classNames({
            [classes.root]: true,
            'track-banner': hasTrackBanner && !hasNotificationPrompt,
            'notification-prompt': hasNotificationPrompt,
            [classes.hasTrackBanner]: hasTrackBanner && !hasNotificationPrompt,
            [classes.hasNotificationPrompt]: hasNotificationPrompt,
        });

        return (
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <Sentry.ErrorBoundary fallback={<Error />}>
                        <RequireUser>
                            <div className={rootClass}>
                                <TrackBanner />
                                <Typography component="div" style={{ paddingBottom: 50 }}>
                                    <Outlet />
                                </Typography>
                                <NavTabs />
                                <Connecting />
                                <MediaControls />
                            </div>
                        </RequireUser>
                    </Sentry.ErrorBoundary>
                </ThemeProvider>
            </StyledEngineProvider>
        );
    }
}

const Layout = compose(
    withStyles(styles),
    withSelector('hasTrackBanner', userHasTrackInQueueSelector),
    withSelector('notificationsEnabled', notificationsEnabledSelector),
    withSelector('notificationSubscription', notificationSubscriptionSelector),
    withProps({
        hasNotificationPrompt: ({ hasTrackBanner, notificationsEnabled, notificationSubscription }) => (
            hasTrackBanner && (notificationsEnabled === undefined || (notificationsEnabled && notificationSubscription === undefined))
        ),
    }),
    withAction('wake', appActions.wake),
    withEffect(({ wake }) => {
        let lastFired = new Date().getTime();

        const interval = setInterval(() => {
            const now = new Date().getTime();
            if(now - lastFired > 10000) {
                // if it's been more than 10 seconds...
                wake();
            }
            lastFired = now;
        }, 500);

        const focusListener = () => { !document.hidden && wake(); }

        window.addEventListener("focus", focusListener, false);
        window.addEventListener("pageshow", focusListener, false);
        window.addEventListener("visibilitychange", focusListener, false);

        return () => {
            window.removeEventListener("focus", focusListener);
            window.removeEventListener("pageshow", focusListener);
            window.removeEventListener("visibilitychange", focusListener);

            clearInterval(interval);
        }
    }, []),
)(LayoutLayout);

export { Layout }