/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Card, CardContent, Typography } from '@mui/material';
import { compose } from '@truefit/bach';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react'

import { ConnectionStatusIcon } from 'components/connectionStatusIcon';
import { Overlay } from 'components/overlay';
import { playerConnectionStatusSelector } from 'store/player';
import { queueConnectionStatusSelector } from 'store/queue';
import { ConnectionStatus, hubConnectionStatusSelector } from 'store/signalR';

interface PropsFromState {
    connected: boolean;
    hubConnectionStatus: ConnectionStatus;
    playerConnectionStatus: ConnectionStatus;
    queueConnectionStatus: ConnectionStatus;
}

type ConnectingProps = PropsFromState;

const ConnectingLayout: React.FC<ConnectingProps> = ({ hubConnectionStatus, playerConnectionStatus, queueConnectionStatus }) => {
    return (
        <Overlay fullscreen={true}>
            <Card variant="outlined">
                <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                        Status
                    </Typography>
                    <Typography variant="body2" component="p" color="textSecondary">
                        <ConnectionStatusIcon status={hubConnectionStatus} />{" "}
                        <span>Hub: {ConnectionStatus[hubConnectionStatus]}</span>
                    </Typography>
                    <Typography variant="body2" component="p" color="textSecondary">
                        <ConnectionStatusIcon status={playerConnectionStatus} />{" "}
                        <span>Player: {ConnectionStatus[playerConnectionStatus]}</span>
                    </Typography>
                    <Typography variant="body2" component="p" color="textSecondary">
                        <ConnectionStatusIcon status={queueConnectionStatus} />{" "}
                        <span>Queue: {ConnectionStatus[queueConnectionStatus]}</span>
                    </Typography>
                </CardContent>
            </Card>
        </Overlay>
    );
}

const Connecting = compose(
    withSelector('hubConnectionStatus', hubConnectionStatusSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withProps({
        connected: ({ hubConnectionStatus, playerConnectionStatus, queueConnectionStatus }) => (
            hubConnectionStatus === ConnectionStatus.Connected
            && playerConnectionStatus === ConnectionStatus.Connected
            && queueConnectionStatus === ConnectionStatus.Connected
        )
    }),
    renderIf(({ connected }) => connected, compose(renderNothing())(React.Component)),
)(ConnectingLayout);

export { Connecting }