/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Box, createStyles, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { nextTrackForUserSelector } from 'store/queue';

const styles = (theme: Theme) => createStyles({
    blur: {
        zIndex: 1500,
        position: 'fixed',
        top: 0,
        right: 0,
        left: 0,
        height: 'calc(env(safe-area-inset-top))',
        backgroundColor: 'rgba(25,25,25,.25)',
        backdropFilter: 'blur(25px)',

        '.trackBanner &': {
            display: 'none',
        },
    },
});

class BlurLayout extends React.Component {
    public render = () => {
        const { classes } = (this.props as any);

        return (
            <Box className={classes.blur} />
        )
    }
}

const Blur = compose(
    withStyles(styles),
    withSelector('nextTrackForUser', nextTrackForUserSelector),
    renderIf(({ nextTrackForUser }) => nextTrackForUser !== undefined, compose(renderNothing())(React.Component)),
)(BlurLayout);

export { Blur }
