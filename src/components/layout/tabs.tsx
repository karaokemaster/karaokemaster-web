/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faUserCrown } from '@fortawesome/pro-regular-svg-icons'
import { faHeadSideBrain, faListMusic, faSearch } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Badge, BadgeProps, BottomNavigation, BottomNavigationAction, createStyles, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from "@truefit/bach-react-router";
import { withAction, withSelector } from '@truefit/bach-redux';
import { Location } from 'history';
import * as React from 'react';
import { push } from 'redux-first-history';

import { queueEntriesCountSelector } from 'store/queue';
import { userIconSelector } from 'store/userProfile';

const styles = (theme: Theme) => createStyles({
    navigation: {
        position: "fixed",
        left: 0,
        right: 0,
        bottom: 0,
        paddingBottom: "env(safe-area-inset-bottom)",
        paddingLeft: "env(safe-area-inset-left)",
        paddingRight: "env(safe-area-inset-right)",
        zIndex: 1299,
        backgroundColor: 'rgba(25,25,25,.25)',
        backdropFilter: 'blur(25px)',
    },
});

const QueueBadge = compose<BadgeProps>(
    withStyles((theme: Theme) =>
        createStyles({
            badge: {
                top: -13,
                left: -5,
            },
        })
    ),
)(Badge);

interface PropsFromState {
    location: Location;
    queueLength: number;
    userIcon: string;
}

interface PropsFromDispatch {
    push: typeof push;
}

type NavTabsProps = PropsFromState & PropsFromDispatch;

class NavTabsLayout extends React.Component<NavTabsProps> {
    public componentDidMount = () => {
        if (this.isStandalone())
            document.addEventListener("touchstart", this.onTouchStart);
    }

    public componentWillUnmount = () => {
        if (this.isStandalone())
            document.removeEventListener("touchstart", this.onTouchStart);
    }

    public render() {
        const { queueLength, userIcon, location: { pathname } } = this.props;
        const { classes } = (this.props as any);

        const activePath = "/" + pathname.split('/')[1];

        let icon = faUserCrown;
        try {
            icon = findIconDefinition(JSON.parse(userIcon));
        } catch {}

        return (
            <BottomNavigation
                value={activePath}
                onChange={this.onChange}
                className={classes.navigation}
            >
                <BottomNavigationAction
                    label="Queue"
                    icon={
                        <QueueBadge badgeContent={queueLength} color="primary" anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}>
                            <FontAwesomeIcon icon={faListMusic} size="2x" />
                        </QueueBadge>
                    }
                    value="/now-playing"
                />
                <BottomNavigationAction
                    label="For You"
                    icon={<FontAwesomeIcon icon={faHeadSideBrain} size="2x" />}
                    value="/recommendations"
                />
                <BottomNavigationAction
                    label="Search"
                    icon={<FontAwesomeIcon icon={faSearch} size="2x" />}
                    value="/search"
                />
                <BottomNavigationAction
                    label="Profile"
                    icon={<FontAwesomeIcon icon={icon} size="2x" />}
                    value="/user-profile"
                />
            </BottomNavigation>
        );
    }

    private onChange = (event: React.ChangeEvent<{}>, value: any) => {
        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'changeTab', description: value});
            s.setSpan(transaction);

            this.props.push(value);

            transaction.finish();
        });
    }

    private onTouchStart = (e: any) => {
        // is not near edge of view, exit
        if (e.pageX > 30 && e.pageX < window.innerWidth - 30) return;

        // prevent swipe to navigate back gesture
        e.preventDefault();
    }

    private isStandalone = () =>
        (window.matchMedia('(display-mode: standalone)').matches) || ((window.navigator as any).standalone) || document.referrer.includes('android-app://');
}

const NavTabs = compose(
    withStyles(styles),
    withLocation(),
    withSelector('queueLength', queueEntriesCountSelector),
    withSelector('userIcon', userIconSelector),
    withAction('push', push),
)(NavTabsLayout);

export { NavTabs }