/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Box, Button, Theme } from '@mui/material';
import { orange, red } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { Time } from 'components/time';
import { reg } from 'client';
import { runtimeConfig } from 'runtimeConfig';
import { notificationsEnabledSelector, notificationSubscriptionSelector, requestNotifications } from 'store/app';
import { nextTrackForUserSelector } from 'store/queue';
import { subscribePush } from 'store/userProfile';

const bannerHeight = 30;
const promptHeight = 90;

const styles = (theme: Theme) => createStyles({
    trackBanner: {
        zIndex: 1500,
        position: 'fixed',
        top: 0,
        right: 0,
        left: 0,
        paddingTop: 'calc(5px + env(safe-area-inset-top))',
        height: '20px',
        paddingBottom: '5px',
        backgroundColor: theme.palette.secondary.main,
        textAlign: 'center',
    },
    upNow: {
        backgroundColor: red[900],
    },
    upNext: {
        backgroundColor: orange[700],
    },
    hasNotificationPrompt: {
        height: '80px',
    },
    notificationPrompt: {
        marginTop: '0.75em',
    }
});

interface PropsFromState {
    timeUntil: number,
    tracksUntil: number,
    hasNotificationPrompt: boolean,
}

interface PropsFromDispatch {
    requestNotifications: typeof requestNotifications,
    subscribePush: typeof subscribePush,
}

type TrackBannerProps = PropsFromState & PropsFromDispatch;

class TrackBannerLayout extends React.Component<TrackBannerProps> {
    public render = () => {
        const { tracksUntil, timeUntil, hasNotificationPrompt } = this.props;
        const { classes } = (this.props as any);

        const bannerClass = classNames({
            [classes.trackBanner]: true,
            [classes.upNow]: tracksUntil === 0,
            [classes.upNext]: tracksUntil === 1,
            [classes.hasNotificationPrompt]: hasNotificationPrompt,
        });

        return (
            <Box className={bannerClass}>
                {timeUntil < 60
                    ? (tracksUntil === 0 ? <span>You're up now!</span> : <span>You're up next!</span>)
                    : <span>You're up <Time seconds={timeUntil} humanize={true} />!</span>
                }
                {hasNotificationPrompt &&
                    <div className={classes.notificationPrompt}>
                        Get alerts when it's your time to shine!
                        <Button
                            onClick={this.enableNotifications}
                            variant='outlined'
                            size='small'
                        >
                            Enable Notifications
                        </Button>
                    </div>
                }
            </Box>
        )
    }

    private enableNotifications = async () => {
        const { subscribePush } = this.props;

        const result = await Notification.requestPermission();

        if (result === "granted" && reg !== undefined) {
            const convertedVapidKey = urlBase64ToUint8Array(runtimeConfig().client.webPushPublicKey);

            // Subscribe the user
            const sub = await reg.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: convertedVapidKey
            });

            const apiSub = {
                endpoint: sub.endpoint,
                auth: arrayBufferToBase64(sub.getKey('auth')),
                p256dh: arrayBufferToBase64(sub.getKey('p256dh')),
            };

            subscribePush(apiSub);
        }
    }
}

// This function is needed because Chrome doesn't accept a base64 encoded string
// as value for applicationServerKey in pushManager.subscribe yet
// https://bugs.chromium.org/p/chromium/issues/detail?id=802280
const urlBase64ToUint8Array = (base64String: string) => {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

const arrayBufferToBase64 = (buffer: ArrayBuffer | null) => {
    if (buffer === null) return '';

    let binary = '';
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

const TrackBanner = compose(
    withStyles(styles),
    withSelector('nextTrackForUser', nextTrackForUserSelector),
    withSelector('notificationsEnabled', notificationsEnabledSelector),
    withSelector('notificationSubscription', notificationSubscriptionSelector),
    withProps({
        timeUntil: ({ nextTrackForUser }) => nextTrackForUser ? nextTrackForUser.timeUntil : 0,
        tracksUntil: ({ nextTrackForUser }) => nextTrackForUser ? nextTrackForUser.tracksUntil : 0,
        hasNotificationPrompt: ({ notificationsEnabled, notificationSubscription }) => (notificationsEnabled === undefined || (notificationsEnabled && notificationSubscription === undefined)),
    }),
    withAction('subscribePush', subscribePush),
    renderIf(({ nextTrackForUser }) => nextTrackForUser === undefined, compose(renderNothing())(React.Component)),
)(TrackBannerLayout);

export { TrackBanner, bannerHeight, promptHeight }
