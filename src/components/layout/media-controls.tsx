/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as Sentry from '@sentry/browser';
import { compose, withEffect, withRef } from '@truefit/bach';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import { createSilentAudio } from 'create-silent-audio';
import * as React from 'react';

import { PlaybackState, playbackStateSelector, PlayerControl, videoDurationSelector, videoTimeSelector } from 'store/player';
import * as playerActions from 'store/player/actions';
import { nowPlayingEntrySelector, QueueEntry } from 'store/queue';

const HAS_MEDIA_SESSION = window && window.navigator && 'mediaSession' in window.navigator;

interface PropsFromState {
    nowPlaying: QueueEntry,
    videoTime: number,
    videoDuration: number,
    playbackState: PlaybackState,

    silence: React.MutableRefObject<HTMLAudioElement>,
}

interface PropsFromDispatch {
    sendPlayerControl: typeof playerActions.sendPlayerControl,
}

type MediaControlsProps = PropsFromState & PropsFromDispatch;

class MediaControlsLayout extends React.Component<MediaControlsProps> {
    private readonly silence: any = createSilentAudio(1500, 44100);

    public componentDidMount = () => {
        document.addEventListener('click', this.startSilence);

        if (!HAS_MEDIA_SESSION) return;

        navigator.mediaSession.setActionHandler('play', this.play);
        navigator.mediaSession.setActionHandler('pause', this.pause);
    }

    public componentWillUnmount() {
        document.removeEventListener('click', this.startSilence);
    }

    public shouldComponentUpdate = (nextProps: Readonly<MediaControlsProps>, nextState: Readonly<{}>, nextContext: any): boolean => {
        return false;
    }

    public render = () => {
        const { silence } = this.props;

        return (
            <audio ref={silence} loop={true}>
                <source src={this.silence} />
            </audio>
        )
    }

    private startSilence = () => {
        const { silence, playbackState } = this.props;

        try {
            if (silence.current && silence.current.currentTime > 0 && !silence.current.paused && !silence.current.ended && silence.current.readyState > 2)
                return;

            if (playbackState !== PlaybackState.Playing && playbackState !== PlaybackState.Starting && playbackState !== PlaybackState.StoppingAfter)
                return;

            silence.current
                .play()
                .then(() => {
                    // document.removeEventListener('click', this.startSilence);
                });
        }
        catch (ex) {
            Sentry.captureException(ex);
        }
    }

    private play = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'mediaControls:play'});
            s.setSpan(transaction);

            transaction.setData("control", PlayerControl.Play);
            sendPlayerControl(PlayerControl.Play);

            transaction.finish();
        });
    }

    private pause = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'mediaControls:pause'});
            s.setSpan(transaction);

            transaction.setData("control", PlayerControl.Pause);
            sendPlayerControl(PlayerControl.Pause);

            transaction.finish();
        });
    }
}

const MediaControls = compose(
    withSelector('nowPlaying', nowPlayingEntrySelector),
    withSelector('videoTime', videoTimeSelector),
    withSelector('videoDuration', videoDurationSelector),
    withSelector('playbackState', playbackStateSelector),
    withAction('sendPlayerControl', playerActions.sendPlayerControl),
    withRef('silence', undefined),
    withEffect(
        ({ nowPlaying }) => {
            if (!HAS_MEDIA_SESSION || !nowPlaying) return;

            try {
                const { title, user, thumbnailUrl } = nowPlaying;

                navigator.mediaSession.metadata = new MediaMetadata({
                    title,
                    artist: user,
                    album: 'KaraokeMaster',
                    artwork: [
                        // TODO: Supply correct size info
                        { src: thumbnailUrl, sizes: '256x256', type: 'image/jpeg' },
                    ]
                });
            }
            catch (ex) {
                Sentry.captureException(ex);
            }
        },
        [ 'nowPlaying' ]
    ),
    withEffect(
        ({ videoDuration, silence }: MediaControlsProps) => {
            if (!HAS_MEDIA_SESSION || !videoDuration) return;

            try {
                // const clip = createSilentAudio(videoDuration, 44100);
                // silence.current.src = clip;
                // silence.current.load();
                silence.current.play();
            }
            catch (ex) {
                Sentry.captureException(ex);
            }
        },
        [ 'videoDuration' ]
    ),
    withEffect(
        ({ videoTime, videoDuration, silence }: MediaControlsProps) => {
            if (!HAS_MEDIA_SESSION) return;

            try {
                silence.current.currentTime = videoTime;

                navigator.mediaSession.setPositionState({
                    duration: videoDuration,
                    playbackRate: 1,
                    position: videoTime,
                });
            }
            catch (ex) {
                Sentry.captureException(ex);
            }
        },
        [ 'videoTime', 'videoDuration' ]
    ),
    withEffect(
        ({ playbackState, silence }: MediaControlsProps) => {
            if (!HAS_MEDIA_SESSION) return;

            try {
                switch (playbackState)
                {
                    case PlaybackState.Stopped:
                        if (silence.current && silence.current.currentTime > 0 && !silence.current.paused && !silence.current.ended && silence.current.readyState > 2)
                            silence.current.pause();
                        navigator.mediaSession.playbackState = "paused";
                        break;
                    case PlaybackState.Playing:
                    case PlaybackState.Starting:
                    case PlaybackState.StoppingAfter:
                        if (silence.current && silence.current.currentTime > 0 && silence.current.paused && !silence.current.ended && silence.current.readyState > 2)
                            silence.current.play();
                        navigator.mediaSession.playbackState = "playing";
                        break;
                    default:
                        navigator.mediaSession.playbackState = "none";
                        break;
                }
            }
            catch (ex) {
                Sentry.captureException(ex);
            }
        },
        [ 'playbackState' ]
    ),
    renderIf(({ nowPlaying }) => nowPlaying === undefined || !HAS_MEDIA_SESSION, compose(renderNothing())(React.Component)),
)(MediaControlsLayout);

export { MediaControls }
