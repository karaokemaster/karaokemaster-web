/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faUserSecret } from '@fortawesome/pro-light-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, createStyles, Grid, Paper, TextField, Theme, Typography } from '@mui/material';
import { createTheme, ThemeProvider, StyledEngineProvider, adaptV4Theme } from '@mui/material/styles';
import * as Sentry from "@sentry/react";
import { compose, withRef } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { withProps } from '@truefit/bach-recompose';
import { withAction } from '@truefit/bach-redux';
import * as React from 'react';
import { push } from 'redux-first-history';

import { Error } from 'components/error';
import * as userProfileActions from 'store/userProfile/actions';


declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}


const theme = createTheme(adaptV4Theme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#c91ee2',
        },
        secondary: {
            main: '#4caf50',
        },
    },
}));

const styles = (theme: Theme) => createStyles({
    container: {
        position: 'fixed',
        marginTop: '20%',
        textAlign: 'center',
    },
    paper: {
        padding: '1em',
    },
    userInput: {
        '& input': {
            userSelect: 'auto',
        },
    }
});

interface ComposedProps {
    userRef: React.MutableRefObject<HTMLInputElement>
    next?: string
}

interface PropsFromDispatch {
    login: typeof userProfileActions.login
    push: typeof push
}

type LoginProps = ComposedProps & PropsFromDispatch;

class LoginLayout extends React.Component<LoginProps> {
    public render = () => {
        const { userRef } = this.props;
        const { classes } = this.props as any;

        return (
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <Sentry.ErrorBoundary fallback={<Error />}>
                        <Grid
                            container={true}
                            direction="row"
                            justifyContent="center"
                            alignItems="center"
                            className={classes.container}
                        >
                            <Grid item={true}>
                                <Paper className={classes.paper}>
                                    <p>
                                        <FontAwesomeIcon icon={faUserSecret} size="6x" />
                                    </p>
                                    <Typography component="h1">
                                        Hey, stranger! What's your name?
                                    </Typography>
                                    <form action="#">
                                        <TextField
                                            label="Name"
                                            inputRef={userRef}
                                            required={true}
                                            margin="normal"
                                            variant="filled"
                                            fullWidth={true}
                                            onKeyPress={this.userKeyPress}
                                            color="secondary"
                                            className={classes.userInput}
                                        />
                                    </form>
                                    <p>
                                        <Button
                                            variant="contained"
                                            size="large"
                                            color="primary"
                                            onClick={this.login}
                                        >
                                            Go
                                        </Button>
                                    </p>
                                </Paper>
                            </Grid>
                        </Grid>
                    </Sentry.ErrorBoundary>
                </ThemeProvider>
            </StyledEngineProvider>
        );
    }

    public userKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        // e.keyCode doesn't work in keyPress
        if (e.which !== 13) { return; }

        e.preventDefault();

        this.login();
    }

    public userFocus = (e: React.FocusEvent<HTMLInputElement>) => {
        e.currentTarget.setSelectionRange(0, e.currentTarget.value.length);
    }

    private login = () => {
        const { userRef, login, next, push } = this.props;

        if (!userRef.current) { return }

        const user = userRef.current.value;

        login(user);

        push(next ?? '/');
    }
}

const Login = compose(
    withStyles(styles),
    withAction('login', userProfileActions.login),
    withAction('push', push),
    withRef('userRef', undefined),
    withLocation(),
    withProps({
        next: ({ location }) => new URLSearchParams(location.search).get('next'),
    }),
)(LoginLayout);

export { Login };