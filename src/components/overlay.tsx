/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Box, Grid, Paper, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import classNames from 'classnames';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({
    root: {
        minHeight: '20rem',
        border: 'none !important',
    },
    fullscreen: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 5000,
    },
    container: {
        position: 'fixed',
        marginTop: '15%',
        textAlign: 'center',
    },
    item: {
        padding: '15%',
    },
    paper: {
        padding: '1em',
    },
});

interface OtherProps {
    fullscreen?: boolean
    children: JSX.Element
}

class OverlayLayout extends React.Component<OtherProps> {
    public render = () => {
        const { children, fullscreen } = this.props;
        const { classes } = (this.props as any);

        const rootClasses = classNames({
            [classes.root]: true,
            [classes.fullscreen]: fullscreen,
        });

        return (
            <Box className={rootClasses}>
                <Grid
                    container={true}
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    className={classes.container}
                >
                    <Grid item={true} className={classes.item}>
                        <Paper className={classes.paper}>
                            {children}
                        </Paper>
                    </Grid>
                </Grid>
            </Box>
        );
    }
}

const Overlay = compose<OtherProps>(
    withStyles(styles),
)(OverlayLayout);

export { Overlay }
