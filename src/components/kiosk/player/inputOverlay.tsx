/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { compose } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector, withStore } from '@truefit/bach-redux';
import * as React from 'react';
import { Store } from "redux";

import { ApplicationState } from 'store';
import { Cue, CueState, lightingCueThemesSelector, pendingCueSelector } from 'store/cues';
import * as cueActions from 'store/cues/actions';
import { PlaybackState, playbackStateSelector, PlayerControl, videoDurationSelector, videoTimeSelector } from 'store/player';
import * as playerActions from 'store/player/actions';

import { runtimeConfig } from 'runtimeConfig';

interface PropsFromState {
    store: Store<ApplicationState>
    cues: Cue[];
    pendingCue?: Cue;
    playing: boolean;
}

interface PropsFromDispatch {
    sendCue: typeof cueActions.sendCue;
    controlPlayer: typeof playerActions.controlPlayer;
}

interface OtherProps {
    inputAction?(): any;
}

type InputOverlayProps = PropsFromState & PropsFromDispatch;

class InputOverlayLayout extends React.Component<InputOverlayProps & OtherProps> {
    public componentDidMount = () => {
        document.addEventListener("keydown", this.handleKeyDown, false);
    }

    public componentWillUnmount = () => {
        document.removeEventListener("keydown", this.handleKeyDown, false);
    }

    public render() {
        return (
            <div className="input-overlay">
            </div>
        );
    }

    private handleKeyDown = (e: any) => {
        const { inputAction, playing } = this.props;
        const state = this.props.store.getState();
        const toggleCueKey = runtimeConfig().client.player.cueToggleKey || 'Space';
        const stopAfter = videoTimeSelector(state) / videoDurationSelector(state) > runtimeConfig().client.player.stopAfterPercent;

        // TODO: Different actions based on application state (e.g. playing, etc.)
        // TODO: Handle short vs. long press
        switch (e.code) {
            case toggleCueKey:
                if (inputAction)
                    inputAction();
                else if (stopAfter)
                    this.pausePlayer();
                else if (playing)
                    this.toggleCue();
                else
                    this.startPlayer();
                break;
            default:
                return;
        }
        e.preventDefault();
    }

    private toggleCue = () => {
        const { cues, pendingCue, sendCue } = this.props;

        if (!cues || cues.length < 1) return;

        const currentCue = pendingCue ?? cues.find(c => c.state === CueState.On);
        let nextCue = currentCue ? cues.indexOf(currentCue) + 1 : 0;
        if (nextCue >= cues.length) nextCue = 0;
        const cue = cues[nextCue].index;

        sendCue(cue);
    }

    private pausePlayer = () => {
        const { controlPlayer } = this.props;

        controlPlayer(PlayerControl.Pause);
    }

    private startPlayer = () => {
        const { controlPlayer } = this.props;

        controlPlayer(PlayerControl.Play);
    }
}

const InputOverlay = compose<OtherProps>(
    withStore(),
    withSelector('cues', lightingCueThemesSelector),
    withSelector('pendingCue', pendingCueSelector),
    withSelector('playbackState', playbackStateSelector),
    withProps({
        playing: ({ playbackState }) => playbackState === PlaybackState.Playing,
    }),
    withAction('sendCue', cueActions.sendCue),
    withAction('controlPlayer', playerActions.controlPlayer),
)(InputOverlayLayout);

export { InputOverlay };