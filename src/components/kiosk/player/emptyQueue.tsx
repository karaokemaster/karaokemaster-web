/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faMicrophoneLines, faMobile, faStars } from '@fortawesome/pro-duotone-svg-icons'
import { faEquals, faPlus } from '@fortawesome/pro-light-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Typography } from '@mui/material'
import { compose, withState } from '@truefit/bach';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { runtimeConfig } from 'runtimeConfig';
import { apiVersionSelector } from 'store/app';
import { PlaybackState } from 'store/player';
import * as playerActions from 'store/player/actions';
import { InputOverlay } from './inputOverlay';

import gitVersion from "gitversion.json";

interface PropsFromState {
    apiVersion: string,

    timeInterval?: number;
    setTimeInterval: (timeInterval?: number) => void;
}

interface PropsFromDispatch {
    setPlayerState: typeof playerActions.setState;
}

type EmptyQueueProps = PropsFromState & PropsFromDispatch;

class EmptyQueueLayout extends React.Component<EmptyQueueProps> {
    public componentDidMount = () => {
        const timeInterval = window.setInterval(this.updatePlayerState, 1000);
        this.props.setTimeInterval(timeInterval);
    }

    public componentWillUnmount = () => {
        if (this.props.timeInterval) {
            window.clearInterval(this.props.timeInterval);
        }
    }

    public render() {
        const { apiVersion } = this.props;
        const config = runtimeConfig().client;

        return (
            <div className="not-playing">
                <InputOverlay />
                <h1>
                    Get your sing on!
                </h1>
                <p>
                    <FontAwesomeIcon icon={faMobile} size="6x" />
                    <FontAwesomeIcon icon={faPlus} size="3x" style={{padding: "0.5em"}} />
                    <FontAwesomeIcon icon={faMicrophoneLines} size="6x" />
                    <FontAwesomeIcon icon={faEquals} size="3x" style={{padding: "0.5em"}} />
                    <FontAwesomeIcon icon={faStars} size="6x" />
                </p>
                <h2>
                    Visit <u>{config.displayUrl}</u> and add some tracks!
                </h2>
                <div className="guide">
                    <div className="panel">
                        <Typography variant="h3">1. Join the WiFi</Typography>
                        <img src={`${config.apiUrlBase}/v1/Asset/WiFiNetworkQrCode`} />
                    </div>
                    <div className="panel">
                        <Typography variant="h3">2. Add some tracks</Typography>
                        <img src={`${config.apiUrlBase}/v1/Asset/PublicUrlQrCode`} />
                    </div>
                </div>
                <div className="version">
                    v{gitVersion.SemVer} (server v{apiVersion})
                </div>
            </div>
        );
    }

    private updatePlayerState = () => {
        const { setPlayerState } = this.props;

        setPlayerState({ state: PlaybackState.Stopped, videoTime: 0, videoDuration: 0 });
    }
}

const EmptyQueue = compose(
    withSelector('apiVersion', apiVersionSelector),
    withAction('startPlayer', playerActions.startPlayer),
    withAction('setPlayerState', playerActions.setState),
    withState('timeInterval', 'setTimeInterval', undefined),
)(EmptyQueueLayout);

export { EmptyQueue };