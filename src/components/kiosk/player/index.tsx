/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect } from '@truefit/bach';
import { renderIf } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';

import { connectAsPlayer, disconnect } from 'store/app';
import { playerCurrentQueueEntrySelector, playerNextQueueEntrySelector, playerStartedSelector, Reaction } from 'store/player';
import { QueueEntry } from 'store/queue';
import { EmptyQueue } from './emptyQueue';
import { InputOverlay } from './inputOverlay';
import { Lyrics } from './lyrics';
import { NotStarted } from './notStarted';
import { Reactions } from './reactions';
import { TrackInfo } from './trackInfo';
import { VideoPlayer } from './videoPlayer';

import { runtimeConfig } from 'runtimeConfig';

interface PropsFromState {
    started: boolean,
    current: QueueEntry | null,
    next: QueueEntry | null,
    reactions: Reaction[],
}

interface PropsFromDispatch {
    connectAsPlayer: typeof connectAsPlayer
    disconnect: typeof disconnect
}

type PlayerProps = PropsFromState & PropsFromDispatch;

class PlayerLayout extends React.Component<PlayerProps> {
    public render() {
        const { current, next } = this.props;
        const showInfoOverlay = runtimeConfig().client.player.trackInfoOverlay;

        const classes = classNames({
            player: true,
            'has-overlay': showInfoOverlay,
        });

        if (!current) return;

        return (
            <div className={classes}>
                <ErrorBoundary fallback={<div />}>
                    <InputOverlay />
                </ErrorBoundary>
                <TrackInfo className="current-track" track={current} />
                <TrackInfo className="next-track" track={next} next={true} />
                <ErrorBoundary fallback={<div />}>
                    <Lyrics track={current} />
                </ErrorBoundary>
                <ErrorBoundary fallback={<div />}>
                    <Reactions />
                </ErrorBoundary>
                <VideoPlayer track={current} next={next} />
            </div>
        );
    }
}

const Player = compose(
    withSelector('started', playerStartedSelector),
    withSelector('current', playerCurrentQueueEntrySelector),
    withSelector('next', playerNextQueueEntrySelector),
    withAction('connectAsPlayer', connectAsPlayer),
    withAction('disconnect', disconnect),
    withEffect(({ connectAsPlayer, disconnect }) => {
        connectAsPlayer();

        return () => { disconnect(); }
    }, []),
    renderIf(({ current }) => !current, EmptyQueue),
    renderIf(({ started }) => !started, NotStarted),
)(PlayerLayout);

export { Player };