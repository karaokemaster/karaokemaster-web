/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faMicrophoneLines, faMobile, faStars } from '@fortawesome/pro-duotone-svg-icons'
import { faEquals, faPlus } from '@fortawesome/pro-light-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { compose, withState } from '@truefit/bach';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { apiVersionSelector } from 'store/app';
import { PlaybackState } from 'store/player';
import * as playerActions from 'store/player/actions';
import { InputOverlay } from './inputOverlay';

import gitVersion from 'gitversion.json';

interface PropsFromState {
    apiVersion: string;

    countdown: number;
    timeInterval?: number;
    setCountdown: (countdown: number) => void;
    setTimeInterval: (timeInterval?: number) => void;
}

interface PropsFromDispatch {
    startPlayer: typeof playerActions.startPlayer;
    setPlayerState: typeof playerActions.setState;
}

type NotStartedProps = PropsFromState & PropsFromDispatch;

class NotStartedLayout extends React.Component<NotStartedProps> {
    public componentDidMount = () => {
        const timeInterval = window.setInterval(this.updatePlayerState, 100);
        this.props.setTimeInterval(timeInterval);
    }

    public componentWillUnmount = () => {
        if (this.props.timeInterval) {
            window.clearInterval(this.props.timeInterval);
        }
    }

    public render() {
        const { apiVersion, countdown } = this.props;

        if (countdown >= 0) {
            return (
                <div className="not-started">
                    <h1 className="countdown" key={countdown}>{countdown}</h1>
                </div>
            );
        }

        return (
            <div className="not-started">
                <InputOverlay inputAction={this.start} />
                <h1>
                    You ready to sing?
                </h1>
                <p>
                    <FontAwesomeIcon icon={faMobile} size="6x" />
                    <FontAwesomeIcon icon={faPlus} size="3x" style={{padding: "0.5em"}} />
                    <FontAwesomeIcon icon={faMicrophoneLines} size="6x" />
                    <FontAwesomeIcon icon={faEquals} size="3x" style={{padding: "0.5em"}} />
                    <FontAwesomeIcon icon={faStars} size="6x" />
                </p>
                <h2>
                    Tap the switch to start!
                </h2>
                <div className="version">
                    v{gitVersion.SemVer} (server v{apiVersion})
                </div>
            </div>
        );
    }

    private start = async () => {
        const { startPlayer, setCountdown } = this.props;

        for (let i: number = 5; i > 0; i--) {
            setCountdown(i);

            await this.delay(1000);
        }

        setCountdown(0);

        startPlayer();
    }

    private delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    private updatePlayerState = () => {
        const { setPlayerState, countdown } = this.props;

        const state = countdown >= 0
            ? PlaybackState.Starting
            : PlaybackState.Stopped;

        setPlayerState({ state, videoTime: 0, videoDuration: 0 });
    }
}

const NotStarted = compose(
    withSelector('apiVersion', apiVersionSelector),
    withAction('startPlayer', playerActions.startPlayer),
    withAction('setPlayerState', playerActions.setState),
    withState('countdown', 'setCountdown', -1),
    withState('timeInterval', 'setTimeInterval', undefined),
)(NotStartedLayout);

export { NotStarted };