/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faUser, faUserGroup } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import * as he from 'he';
import * as React from 'react';
import { runtimeConfig } from "runtimeConfig";
import { QueueEntry } from 'store/queue';

interface TrackInfoProps {
    track: QueueEntry | null
    className: string
    next?: boolean
}

class TrackInfo extends React.Component<TrackInfoProps> {
    public render() {
        const showInfoOverlay = runtimeConfig().client.player.trackInfoOverlay;

        if (!this.props.track || !showInfoOverlay) { return <div />; }

        const { track: { title, user }, className, next } = this.props;

        return (
            <div className={className}>
                {next ? <span className="intro">Up next: </span> : <span />}
                <span className="title">{he.decode(title)}</span>
                <span className="user">
                    (
                        <FontAwesomeIcon icon={next ? faUserGroup : faUser} />
                        {user}
                    )
                </span>
            </div>
        )
    }
}

export { TrackInfo };