/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { SeverityLevel } from '@sentry/types';
import { compose, withEffect, withRef, withState } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import { EnqueueSnackbar, withSnackbar } from 'enhancers/notistack';
import * as React from 'react';
import YouTube, { YouTubePlayer } from 'react-youtube';

import { activeCueSelector, Cue, currentTrackCueSelector } from 'store/cues';
import * as cueActions from 'store/cues/actions';
import { PlaybackState, playbackStateStartingSelector, PlayerControl, playerPendingControlsSelector } from 'store/player';
import * as playerActions from 'store/player/actions';
import { QueueEntry } from 'store/queue';
import * as queueActions from 'store/queue/actions';

import { runtimeConfig } from 'runtimeConfig';

const styles = (theme: Theme) => createStyles({
    root: {
        height: '100%',
    },
});

interface PropsFromState {
    pendingControls: PlayerControl[];
    starting: boolean;
    cue?: Cue;
    activeCue?: Cue;

    player: YouTubePlayer;
    timeInterval?: number;
    stopAfter: boolean;
    setPlayer: (player: any) => void;
    setTimeInterval: (timeInterval?: number) => void;
    setStopAfter: (stopAfter: boolean) => void;
    cueSaveTimeout: React.MutableRefObject<number | undefined>;
}

interface PropsFromDispatch {
    setEntryPlayState: typeof queueActions.setEntryPlayState;
    setEntryLightingCue: typeof queueActions.setEntryLightingCue;
    completePlayerControl: typeof playerActions.completePlayerControl;
    setPlayerState: typeof playerActions.setState;
    unstartPlayer: typeof playerActions.unstartPlayer;
    sendCue: typeof cueActions.sendCue;
    enqueueSnackbar: EnqueueSnackbar;
}

interface OtherProps {
    track: QueueEntry;
    next: QueueEntry | null;
}

type VideoPlayerProps = PropsFromState & PropsFromDispatch;

class VideoPlayerLayout extends React.Component<VideoPlayerProps & OtherProps> {
    public play = () => {
        this.props.player?.playVideo();
    }

    public pause = async () => {
        if (!this.props.player) { return; }
        const { player, enqueueSnackbar, setStopAfter } = this.props;
        const stopAfterPercent: number = runtimeConfig().client.player.stopAfterPercent;

        const videoTime = await player.getCurrentTime();
        const videoDuration = await player.getDuration();

        if (videoTime / videoDuration > stopAfterPercent) {
            // Ending soon, so pause once done
            setStopAfter(true);
            // TODO: Enhance our toast
            enqueueSnackbar("Stopping after this track", {
                variant: "pauseRequest",
            });
        }
        else {
            player.pauseVideo();
        }
    }

    public componentDidMount = () => {
        const timeInterval = window.setInterval(this.updatePlayerTime, 100);
        this.props.setTimeInterval(timeInterval);
    }

    public componentWillUnmount = () => {
        const { setPlayerState, starting, timeInterval, track: { queueId } } = this.props;

        setPlayerState({ state: (starting ? PlaybackState.Starting : PlaybackState.Stopped), queueId });

        if (timeInterval) {
            window.clearInterval(timeInterval);
        }
    }

    public shouldComponentUpdate = (nextProps: VideoPlayerProps): boolean => {
        if (!this.props.player) { return true; }

        // TODO: This whole shebang would be MUCH better handled with some epic epics
        if (nextProps.pendingControls.length !== this.props.pendingControls.length) {
            if (nextProps.pendingControls.length > 0) {
                const control = nextProps.pendingControls.slice(0, 1)[0];
                const { player, next } = this.props;

                switch (control) {
                    case PlayerControl.Play: {
                        this.play();
                        break;
                    }
                    case PlayerControl.Pause: {
                        this.pause();
                        break;
                    }
                    case PlayerControl.Back: {
                        player.seekTo(0, true);
                        break;
                    }
                    case PlayerControl.Next: {
                        const { track: { queueId }, setEntryPlayState, stopAfter, unstartPlayer } = this.props;

                        setEntryPlayState(queueId, 0, next?.queueId);
                        if (stopAfter) {
                            unstartPlayer();
                        }
                        break;
                    }
                }
                this.props.completePlayerControl(control);
            }
            return false;
        }
            
        return true;
    }

    public render() {
        const { track: { videoId } } = this.props;
        const { classes } = this.props as any;

        const opts: any = {
            height: '100%',
            playerVars: {
                autoplay: 1,
                controls: 0,
                iv_load_policy: 3,
                modestbranding: 1,
                showinfo: 0,
                // start: 0 // seconds
                // end: 0 // seconds
            },
            width: '100%'
        };

        return (
            <YouTube className={classes.root} videoId={videoId} opts={opts} onReady={this.onReady} onStateChange={this.onStateChange} onPlay={this.onPlay} onEnd={this.onEnd} onError={this.onError} />
        );
    }
    
    private onReady = ({ target }: any) => {
        this.props.setPlayer(target);
    }

    private onStateChange = ({ data }: any) => {
        const { setPlayerState, stopAfter, starting, track: { queueId}, cue, activeCue, sendCue, enqueueSnackbar } = this.props;

        // TODO: Evaluate additional possible states
        let state = (data === 1)
            ? PlaybackState.Playing
            : (starting ? PlaybackState.Starting : PlaybackState.Stopped);

        if (data === 1) {
            state = stopAfter ? PlaybackState.StoppingAfter : state;

            if (cue && cue !== activeCue) {
                sendCue(cue.index);
                enqueueSnackbar(cue.name, {
                    variant: "lightingCue",
                });
            }
        }

        // TODO: Grab time/duration from elsewhere, or don't send the message when playing
        setPlayerState({ state, queueId });
    }

    private updatePlayerTime = () => {
        const { player, stopAfter, setPlayerState, starting, track: { queueId } } = this.props;

        if (!player) {
            setPlayerState({ state: (starting ? PlaybackState.Starting : PlaybackState.Stopped), queueId });
            return;
        }

        const videoTime = player.getCurrentTime();
        const videoDuration = player.getDuration();
        let state = PlaybackState.Playing;

        if (player.getPlayerState() === 1) {
            state = stopAfter ? PlaybackState.StoppingAfter : state;
        }
        else {
            state = (starting ? PlaybackState.Starting : PlaybackState.Stopped);
        }

        setPlayerState({ state, queueId, videoTime, videoDuration });
    }

    private onPlay = () => {
        const { track: { queueId }, player, setEntryPlayState } = this.props;

        if (player) {
            // TODO: getAvailableQualityLevels()
            player.setPlaybackQuality("hd720");
        }
        
        setEntryPlayState(queueId, 1);
    }

    private onEnd = () => {
        const { track: { queueId }, track, cue, activeCue, stopAfter, next, setEntryPlayState, setEntryLightingCue, unstartPlayer } = this.props;

        if (activeCue && activeCue?.name !== cue?.name)
            setEntryLightingCue(track, activeCue.name);

        setEntryPlayState(queueId, 0, next?.queueId);

        if (stopAfter) {
            unstartPlayer();
        }
    }

    private onError = () => {
        this.onEnd();

        const { track: { queueId }} = this.props;

        Sentry.withScope(scope => {
            scope.setExtra("queueId", `${queueId}`);
            Sentry.captureMessage("Video playback failed", "error" as SeverityLevel);
        });
    }
}

const VideoPlayer = compose<OtherProps>(
    withStyles(styles),
    withSnackbar(),
    withSelector('cue', currentTrackCueSelector),
    withSelector('activeCue', activeCueSelector),
    withSelector('pendingControls', playerPendingControlsSelector),
    withSelector('starting', playbackStateStartingSelector),
    withAction('completePlayerControl', playerActions.completePlayerControl),
    withAction('setPlayerState', playerActions.setState),
    withAction('unstartPlayer', playerActions.unstartPlayer),
    withAction('setEntryPlayState', queueActions.setEntryPlayState),
    withAction('setEntryLightingCue', queueActions.setEntryLightingCue),
    withAction('sendCue', cueActions.sendCue),
    withState('player', 'setPlayer', undefined),
    withState('timeInterval', 'setTimeInterval', undefined),
    withState('stopAfter', 'setStopAfter', false),
    withProps({
        'queueId': ({ track: { queueId }}) => queueId,
    }),
    withEffect<VideoPlayerProps & { queueId: number }>(
        ({ cue, sendCue, enqueueSnackbar }) => {
            if (!cue) return;

            // Send the cue when the track changes, if the new track has cue set
            sendCue(cue.index);
            enqueueSnackbar(cue.name, {
                variant: 'lightingCue',
            });
        },
        [ 'queueId' ]
    ),
    withEffect<VideoPlayerProps>(
        ({ cue, activeCue, sendCue, enqueueSnackbar }) => {
            // TODO: Track state of pending setEntryLightingCue operation
            if (!cue || cue.index === activeCue?.index) return;

            // TODO: Send the cue if the track's cue doesn't match the active cue
            // sendCue(cue.index);
            enqueueSnackbar(cue.name, {
                variant: 'lightingCue',
            });
        },
        [ 'cue' ]
    ),
    withRef('cueSaveTimeout', undefined),
    withRef('firstUpdate', true),
    withEffect<VideoPlayerProps & OtherProps & { firstUpdate: React.MutableRefObject<boolean> }>(
        ({ cue, track, setEntryLightingCue, enqueueSnackbar, activeCue, cueSaveTimeout, firstUpdate }) => {
            if (firstUpdate.current) {
                firstUpdate.current = false;
                return;
            }
            if (!activeCue || activeCue.index === cue?.index) return;

            if (cueSaveTimeout.current)
                window.clearTimeout(cueSaveTimeout.current)

            enqueueSnackbar(activeCue.name, {
                variant: 'lightingCue',
            });
            cueSaveTimeout.current = window.setTimeout(() => {
                // If the active cue changes from what's set on the track, save it
                setEntryLightingCue(track, activeCue.name);
            }, 5000);
        },
        [ 'activeCue' ]
    ),
)(VideoPlayerLayout);

export { VideoPlayer };