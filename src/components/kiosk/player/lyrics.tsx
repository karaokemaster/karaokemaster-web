/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withMemo } from '@truefit/bach';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import { Lrc, Runner } from 'lrc-kit';
import * as React from 'react';

import { videoTimeSelector } from 'store/player';
import { QueueEntry } from 'store/queue';

// TODO: Improve the style imnplementation here
const styles = (theme: Theme) => createStyles({
    root: {
        position: 'fixed',
        top: 0,
        padding: '5px',
        backgroundColor: '#000000aa',
        cursor: 'none',
        left: 0,
        right: 0,
        bottom: 0,
        paddingTop: '5px',
        zIndex: 99,
        columnCount: 3,
        columnGap: '50px',
        fontSize: '0.85em',
        lineHeight: '1.25em',
    },
    lrc: {
        fontSize: '2.5em',
        textAlign: 'center',
        columnCount: 1,
        paddingTop: '25%',

        '& div': {
            position: 'relative',
            animation: '$slide 3s ease-out',

            '&::last-child': {
                animation: '$fade 3s ease-out',
            }
        },
    },
    prevNextLine: {
        color: '#ffffff66',
    },
    '@keyframes slide': {
        '0%': {
            top: '1em',
        },
        '100%': {
            top: 0,
        },
    },
    '@keyframes fade': {
        '0%': {
            opacity: '0%',
        },
        '100%': {
            opacity: '100%',
        },
    },
});

interface PropsFromState {
    lyrics: string;
    nextLine: string;
    prevLine: string;
    runner: typeof Runner;
    curIndex: number;
}

interface OtherProps {
    track: QueueEntry | null
}

class LyricsLayout extends React.Component<PropsFromState & OtherProps> {
    public shouldComponentUpdate(nextProps: Readonly<PropsFromState & OtherProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        try {
            if (this.props.lyrics !== nextProps.lyrics) return true;
            if (this.props.curIndex !== nextProps.curIndex) return true;
        } catch {}

        return false;
    }

    public render() {
        const { lyrics, prevLine, nextLine, runner } = this.props;
        const { classes } = (this.props as any);

        const isLrc = !!runner;

        const lyricsClass = classNames({
            [classes.root]: true,
            [classes.lrc]: isLrc,
        });

        // \u2028 -> line separator; \u2029 -> paragraph separator
        // https://stackoverflow.com/a/58116999
        const lines = lyrics
            .replace(/\r/g, "")
            .split(/[\n\u2028\u2029]/)
            .map(line => line.trim().length < 1 ? <React.Fragment>&nbsp;</React.Fragment> : line)
            .map((line, i) => <div key={i}>{line}</div>);

        return (
            <div className={lyricsClass}>
                {isLrc && <div className={classNames(classes.prevNextLine)}>{prevLine}&nbsp;</div>}
                {lines}
                {isLrc && <div className={classNames(classes.prevNextLine)}>{nextLine}&nbsp;</div>}
            </div>
        )
    }
}

const Lyrics = compose<OtherProps>(
    withStyles(styles),
    withSelector('videoTime', videoTimeSelector),
    withProps({
        'runner': ({ track }) => {
            try {
                const { lyrics } = track;
                if (!lyrics) return '';

                const lrc = Lrc.parse(lyrics);

                if (!lrc || !lrc.lyrics || lrc.lyrics.length < 1) return null;

                const runner = new Runner(lrc);

                return runner;
            }
            catch { return ''; }
        },
    }),
    withProps({
        'curIndex': ({ runner, videoTime }) => {
            try {
                if (!runner) return -1;

                runner.timeUpdate(videoTime);
                return runner.curIndex();
            }
            catch { return -1; }
        },
    }),
    withMemo('lyrics',
        ({ track, runner, curIndex }) => {
            try {
                if (!runner) return track ? track.lyrics : '';
                if (curIndex < 0) return '';

                const l = runner.getLyric(curIndex);
                return l.content;
            }
            catch {
                return '';
            }
        },
        ['track', 'runner', 'curIndex'],
    ),
    withMemo('prevLine',
        ({ track, runner, curIndex }) => {
            try {
                if (!runner) return track ? track.lyrics : '';
                if (curIndex < 1) return '';

                const l = runner.getLyric(curIndex - 1);
                return l.content;
            }
            catch {
                return '';
            }
        },
        ['track', 'runner', 'curIndex'],
    ),
    withMemo('nextLine',
        ({ track, runner, curIndex }) => {
            try {
                if (!runner) return track ? track.lyrics : '';
                if (curIndex < 0 || curIndex > (runner.lrc.length - 2)) return '';

                const l = runner.getLyric(curIndex + 1);
                return l.content;
            }
            catch {
                return '';
            }
        },
        ['track', 'runner', 'curIndex'],
    ),
    renderIf(({ track }) => !track || !track.lyrics, compose(renderNothing())(React.Component)),
)(LyricsLayout);

export { Lyrics };