/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Avatar, AvatarProps, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withEffect } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { FloatingItem, floatSeconds, FloatTrack } from 'components/floating-emoji';
import { playerReactionsSelector, Reaction } from 'store/player';
import * as playerActions from 'store/player/actions';

const avatarStyles = (theme: Theme) => createStyles({
    root: {
        backgroundColor: 'transparent', // 'rgba(0, 0, 0, 0.45)',
        width: '80px',
        height: '80px',
    },
});

const ReactionAvatar = compose<AvatarProps>(
    withStyles(avatarStyles),
)(Avatar);

const FloatingReaction = compose<{ reaction: Reaction }>(
    withProps({
        width: 500,
        key: ({ reaction }) => reaction.key,
        children: ({ reaction }) => (<ReactionAvatar>{reaction.reaction}</ReactionAvatar>),
    }),
    withAction('reactionComplete', playerActions.reactionComplete),
    withEffect(
        ({ reactionComplete, reaction }: any) => setTimeout(() => reactionComplete(reaction), floatSeconds * 1000),
        [ 'key' ],
    ),
)(FloatingItem)


interface PropsFromState {
    reactions: Reaction[]
}

type ReactionProps = PropsFromState;

class ReactionsLayout extends React.Component<ReactionProps> {
    public render() {
        const { reactions } = this.props;

        return (
            <FloatTrack width={600} height="70vh">
                {reactions.map(r => <FloatingReaction reaction={r} key={r.key} />)}
            </FloatTrack>
        );
    }
}

const Reactions = compose(
    withSelector('reactions', playerReactionsSelector),
)(ReactionsLayout);

export { Reactions };