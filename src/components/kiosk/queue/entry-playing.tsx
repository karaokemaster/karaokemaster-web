/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { humanize, toTitleCase, transform } from '@alduino/humanizer/string';
import { faPlay } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Chip, CircularProgress, CircularProgressProps, ListItemAvatar, Theme } from '@mui/material';
import { green, orange, red } from '@mui/material/colors'
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { Time } from 'components/time';
import { PlaybackState, playbackStateSelector, videoDurationSelector, videoTimeSelector } from 'store/player';

const progressSize = 75;
const progressPadding = 2.5;
const progressOffset = progressSize / 2;
const wrapperSize = progressSize + 2 * progressPadding;
const progressRadius = wrapperSize / 2;

const styles = (theme: Theme) => createStyles({
    progressWrapper: {
        margin: theme.spacing(),
        position: 'relative',
        textAlign: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        width: wrapperSize,
        height: wrapperSize,
        borderRadius: progressRadius,
        lineHeight: theme.typography.pxToRem(progressRadius),
    },
    icon: {
        marginLeft: theme.spacing(1),
    },
});

const progressStyles = (theme: Theme) => createStyles({
    root: {
        left: progressRadius,
        position: 'absolute',
        top: progressRadius,
        zIndex: 1,
        marginLeft: -progressOffset,
        marginTop: -progressOffset,
    },
    colorPrimary: {
        color: green[500],
    },
    colorSecondary: {
        color: red[500],
    },
    circleDeterminate: {
        transition: 'stroke-dashoffset 1s linear',
    },
    barColorPrimary: {
        backgroundColor: green[500],
    },
    barColorSecondary: {
        backgroundColor: orange[900],
    },
});

const TrackProgress = compose<CircularProgressProps>(
    withStyles(progressStyles),
    withSelector('videoTime', videoTimeSelector),
    withSelector('videoDuration', videoDurationSelector),
    withProps({
        variant: 'determinate',
        size: progressSize,
        thickness: 6,
        value: ({ videoTime, videoDuration }) => videoDuration > 0 ? 100 * videoTime / videoDuration : 0,
        color: ({ videoTime, videoDuration }) => videoDuration > 0 && (videoDuration - videoTime) <= 30 ? 'secondary' : 'primary',
    }),
)(CircularProgress);

class AvatarLayout extends React.Component{
    public render = () => {
        const { classes } = (this.props as any);

        return (
            <ListItemAvatar>
                <div className={classNames(classes.progressWrapper)}>
                    <FontAwesomeIcon icon={faPlay} className={classes.icon} />
                    <TrackProgress />
                </div>
            </ListItemAvatar>
        );
    }
}

const NowPlayingAvatar = compose(
    withStyles(styles),
)(AvatarLayout);


const chipStyles = (theme: Theme) => createStyles({
    root: {
        borderRadius: 5,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    colorPrimary: {

    },
    colorSecondary: {
        color: theme.palette.text.primary,
        border: '2px solid rgba(0, 0, 0, 0.5)',
        backgroundColor: 'rgb(255, 0, 0)',
        boxSizing: 'content-box',
        animation: '$pulse 2s ease-in-out infinite'
    },

    '@keyframes pulse': {
        '0%, 100%': {
            backgroundColor: 'rgba(255, 0, 0, 1)',
        },
        '50%': {
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
        },
    },
});

const NowPlayingChips = compose(
    withStyles(chipStyles),
    withSelector('playbackState', playbackStateSelector),
    withSelector('videoTime', videoTimeSelector),
    withSelector('videoDuration', videoDurationSelector),
    withProps({
        remaining: ({ videoTime, videoDuration }) => videoDuration - videoTime,
        ending: ({ videoTime, videoDuration }) => videoDuration > 0 ? (videoDuration - videoTime) <= 30 : false,
        state: ({ playbackState }) => transform(humanize(PlaybackState[playbackState]), toTitleCase),
        showTime: ({ playbackState }) => playbackState === PlaybackState.Playing || playbackState === PlaybackState.StoppingAfter,
        label: ({ state, showTime, remaining }) => <>{state}{showTime && <> - <Time seconds={remaining} /> remaining</>}</>,
        color: ({ ending }) => ending ? 'secondary' : 'primary',
    }),
)(Chip);

export { NowPlayingAvatar, NowPlayingChips }