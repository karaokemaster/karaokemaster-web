/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Avatar, Chip, ListItemAvatar, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { Time } from 'components/time';
import { videoTimeLoResSelector } from 'store/player';
import { TimedQueueEntry } from 'store/queue';

interface OtherProps {
    entry: TimedQueueEntry;
}

class EntryAvatar extends React.Component<OtherProps> {
    public render = () => {
        const { entry } = this.props;

        return (
            <ListItemAvatar>
                <Avatar src={entry.thumbnailUrl} variant={"square"} />
            </ListItemAvatar>
        );
    }
}


const chipStyles = (theme: Theme) => createStyles({
    root: {
        borderRadius: 5,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});

const EntryChips = compose<OtherProps>(
    withStyles(chipStyles),
    withSelector('videoTime', videoTimeLoResSelector),
    withProps({
        timeUntil: ({ entry: { timeUntil }, videoTime }) => timeUntil - (videoTime ?? 0),
        label: ({ timeUntil }) => <Time seconds={timeUntil} humanize={true} />,
    }),
    renderIf(({ timeUntil }) => !timeUntil, compose(renderNothing())(React.Component)),
)(Chip);

export { EntryAvatar, EntryChips }