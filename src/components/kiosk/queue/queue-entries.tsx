/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { List, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from "@truefit/bach-recompose";
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { runtimeConfig } from 'runtimeConfig';
import { sortedTimedQueueSelector, TimedQueueEntry } from 'store/queue';
import { Entry } from './entry';

const styles = (theme: Theme) => createStyles({
    queue: {
        paddingTop: 0,
        position: 'relative',
    },
});

interface PropsFromState {
    entries: TimedQueueEntry[],
}

type QueueEntriesProps = PropsFromState;

class QueueEntriesLayout extends React.Component<QueueEntriesProps> {
    public render() {
        const { entries } = this.props;
        const { classes } = this.props as any;

        return (
            <List className={classes.queue}>
                {entries.map((e, i) => <Entry entry={e} index={i} key={e.queueId} />)}
            </List>
        )
    }
}

// TODO: Move away from timed queue to avoid constant re-renders
//       Use sorted queue to render list; add selector to get timed entry where needed
const QueueEntries = compose(
    withStyles(styles),
    withSelector('queue', sortedTimedQueueSelector),
    withProps({
        entries: ({ queue }: QueueEntriesProps & { queue: TimedQueueEntry[] }) => {
            const entriesLimit = runtimeConfig().client.queue.maxEntries;
            const entries = queue.filter(e => e.order >= 0).slice(0, entriesLimit);

            return entries;
        },
    }),
)(QueueEntriesLayout);

export { QueueEntries };