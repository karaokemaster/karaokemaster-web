/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Grid, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { ConnectionStatusIcon } from 'components/connectionStatusIcon';
import { Error } from 'components/error';
import { apiVersionSelector, connectAsKiosk, disconnect } from 'store/app';
import { playerConnectionStatusSelector } from 'store/player';
import { queueConnectionStatusSelector } from 'store/queue';
import { ConnectionStatus } from 'store/signalR';
import { Reactions } from '../player/reactions';
import { Clock } from './clock';
import { QueueEntries } from './queue-entries';

import { runtimeConfig } from 'runtimeConfig';

import gitVersion from 'gitversion.json';

const qrCodeSize = 150;
const guidePadding = 10;

const styles = (theme: Theme) => createStyles({
    queueContainer: {
        maxWidth: '100%',
    },
    queue: {
        paddingTop: 0,
        position: 'relative',
    },
    guides: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        minWidth: '67%',
        paddingTop: '30px',
        background: 'linear-gradient(180deg, rgba(0,0,0,0) 0, rgba(0,0,0,1) 50px)',
    },
    guidePanel: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexGrow: 1,
        backgroundColor: '#fff',
        color: '#000',
        borderRadius: guidePadding,
        padding: guidePadding,
        margin: guidePadding,
    },
    guideText: {
        paddingLeft: '1em',
        fontSize: '2rem',
    },
    guideCode: {
        width: qrCodeSize,
        height: qrCodeSize,
    },
    clock: {
        textAlign: 'center',
        paddingTop: '2em',
    },
    version: {
        position: 'fixed',
        bottom: 0,
        right: 0,
        padding: '5px',
        color: '#999',
        fontSize: '1rem',
        opacity: 0.5,
    },
});

interface PropsFromState {
    apiVersion: string,
    playerConnectionStatus: ConnectionStatus,
    queueConnectionStatus: ConnectionStatus,
}

interface PropsFromDispatch {
    connectAsKiosk: typeof connectAsKiosk
    disconnect: typeof disconnect
}

type QueueProps = PropsFromState & PropsFromDispatch;

class QueueLayout extends React.Component<QueueProps> {
    public render() {
        const { apiVersion, queueConnectionStatus, playerConnectionStatus } = this.props;
        const { classes } = this.props as any;
        const config = runtimeConfig().client;

        return (
            <div className="kiosk">
                <Grid container={true} spacing={3}>
                    <Grid item={true} xs={8}>
                        <Grid container={true} direction="column">
                            <Grid item={true} className={classes.queueContainer}>
                                <ErrorBoundary fallback={<Error />}>
                                    <QueueEntries />
                                </ErrorBoundary>
                            </Grid>
                            <Grid item={true} className={classes.guides}>
                                <Grid container={true} direction="row">
                                    <div className={classes.guidePanel}>
                                        <Typography variant="h3" className={classes.guideText}>1. Join the WiFi</Typography>
                                        <img src={`${config.apiUrlBase}/v1/Asset/WiFiNetworkQrCode`} className={classes.guideCode} />
                                    </div>
                                    <div className={classes.guidePanel}>
                                        <Typography variant="h3" className={classes.guideText}>2. Add some tracks</Typography>
                                        <img src={`${config.apiUrlBase}/v1/Asset/PublicUrlQrCode`} className={classes.guideCode} />
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item={true} xs={4}>
                        <div className={classes.clock}>
                            <ErrorBoundary fallback={<Error />}>
                                <Clock />
                            </ErrorBoundary>
                        </div>

                        <div className={classes.version}>
                            v{gitVersion.SemVer} (server v{apiVersion}){" "}
                            <ConnectionStatusIcon status={queueConnectionStatus} />{" "}
                            <ConnectionStatusIcon status={playerConnectionStatus} />
                        </div>

                        <Reactions />
                    </Grid>
                </Grid>

            </div>
        )
    }
}

const Queue = compose(
    withStyles(styles),
    withSelector('apiVersion', apiVersionSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withAction('connectAsKiosk', connectAsKiosk),
    withAction('disconnect', disconnect),
    withEffect(({ connectAsKiosk, disconnect }) => {
        connectAsKiosk();

        return () => { disconnect(); }
    }, []),
)(QueueLayout);

export { Queue };