/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Theme } from '@mui/material';
import { grey } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import { compose, withRef } from '@truefit/bach';
import { withSelector, withStore } from "@truefit/bach-redux";
import { axisRadialInner } from 'd3-radial-axis';
import { scaleLinear } from 'd3-scale';
import { select } from 'd3-selection';
import { Arc, arc, DefaultArcObject } from 'd3-shape';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import { Store } from 'redux';

import { ApplicationState } from 'store';
import { playbackStatePlayingSelector, queueIdSelector, videoDurationSelector, videoTimeHiResSelector } from 'store/player';
import { queueTimestampSelector, sortedTimedQueueSelector, TimedQueueEntry } from 'store/queue';

const oneHour = 3600;

const styles = (theme: Theme) => createStyles({
    clock: {
        '& .background': {
            strokeWidth: 0,
            fill: 'DarkSlateGrey',
            fillOpacity: 0.3,
        },
        '& .axis .domain': {
            strokeWidth: 0,
        },
        '& .axis .tick line': {
            strokeWidth: '2px',
            stroke: 'White',
        },
        '& .axis .tick:first-of-type': {
            display: 'none', // hide 0 tick
        },
        '& .axis .tick text': {
            display: 'none',
            fontSize: '16px',
            fill: grey[500],

            '&.cardinal': {
                display: 'unset',
                fontSize: '28px',
                fontWeight: 500,
                fill: 'White',
            },
        },
        '& .minor-ticks text, & .minor-ticks path': {
            display: 'none',  // Hide all but ticks
        },
        '& .minor-ticks line': {
            stroke: 'rgba(255, 255, 255, 0.25)',
        },
        '& .pointers .hour, & .ending .caret, & .queue .duration': {
            fill: 'grey',
            stroke: 'white',
            strokeWidth: 0.005,
        },
        '& .pointers .min': {
            fill: 'black',
            stroke: 'white',
            strokeWidth: 0.005,
        },
        '& .pointers .sec': {
            fill: 'red',
        },
        '& .pointers .center': {
            fill: 'darkgrey',
        },
        '& .track': {
            // stroke: 'White',
            // strokeWidth: '1px',
        },
    },
});

interface PropsFromState {
    timedQueue: TimedQueueEntry[]
    queueTimestamp: number
    playing: boolean
    store: Store<ApplicationState>
}

interface ComposedProps {
    svgNode: React.MutableRefObject<SVGSVGElement>
}

type ClockProps = PropsFromState & ComposedProps;

class ClockLayout extends React.Component<ClockProps> {
    private hourScale = scaleLinear().domain([0, 12]).range([0, 360]);
    private secMinScale = scaleLinear().domain([0, 60]).range([0, 360]);
    private trackSecScale = scaleLinear().domain([0, 3600]).range([0, Math.PI * 2]);
    private durationHourScale = scaleLinear().domain([0, 12]).range([0, Math.PI * 2]);

    private r: number;
    private trackArc: Arc<any, DefaultArcObject>;
    private queueArc: Arc<any, DefaultArcObject>;

    public componentDidMount = () => {
        this.createClock();
        this.renderTracks();

        // Kick-off clock
        this.framed();
    }

    public shouldComponentUpdate(nextProps: Readonly<ClockProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        return this.props.queueTimestamp !== nextProps.queueTimestamp;
    }

    public componentDidUpdate = (prevProps: Readonly<ClockProps>, prevState: Readonly<{}>, snapshot?: any) => {
        if (prevProps.queueTimestamp === this.props.queueTimestamp) return;

        this.renderTracks();
    }

    public render = () => {
        const { svgNode } = this.props;
        const { classes } = this.props as any;

        return <svg ref={svgNode} className={classes.clock} width="75%" />;
    }

    private createClock = () => {
        const { svgNode } = this.props;

        if (!svgNode.current) return;

        this.r = svgNode.current.clientWidth / 2;
        const r = this.r;

        this.trackArc = arc()
            .innerRadius(r * 0.5)
            .outerRadius(r * 0.8)
            .padAngle(this.trackSecScale(10));

        this.queueArc = arc()
            .innerRadius(r * 0.05)
            .outerRadius(r * 0.48);

        const pointersRelDimensions = [
            { class: 'hour', width: 0.03, height: 0.55 },
            { class: 'min', width: 0.03, height: 0.85 },
            { class: 'sec', width: 0.01, height: 0.85 }
        ];

        // Size canvas
        const svg = select(svgNode.current)
            .attr('width', r * 2)
            .attr('height', r * 2)
            .attr('viewBox', `${-r} ${-r} ${r*2} ${r*2}`);

        // Add background
        svg.append('circle').classed('background', true)
            .attr('cx', 0)
            .attr('cy', 0)
            .attr('r', r);

        svg.append('g').classed('queue', true);

        svg.append('g').classed('tracks', true);

        // Add axis
        svg.append('g').classed('axis', true)
            .call(axisRadialInner(
                this.hourScale.copy().range([0, 2 * Math.PI]),
                r - 1
                )
                    .ticks(12)
                    .tickSize(12)
            );

        svg.append('g').classed('ending', true);

        svg.selectAll('.axis text')
            .each((x: number, i, nodes: SVGTextElement[]) => {
                if (i % 3 === 0)
                    select(nodes[i]).attr('class', 'cardinal');
            });

        svg.append('g').classed('minor-ticks', true)
            .call(axisRadialInner(
                this.secMinScale.copy().range([0, 2 * Math.PI]),
                r - 1
                )
                    .ticks(60)
                    .tickSize(6)
            );

        // Add pointers
        svg.append('g').classed('pointers', true)
            .attr('transform', `scale(${r})`)
            .selectAll('rect')
            .data(pointersRelDimensions)
            .enter()
            .append('rect')
            .attr('class', d=> d.class)
            .attr('x', d => -d.width/2)
            .attr('y', d => -d.height + d.width/2)
            .attr('width', d => d.width)
            .attr('height', d => d.height)
            .attr('rx', 0.02)
            .attr('ry', 0.03);

        // Add center
        svg.select('.pointers')
            .append('circle').classed('center', true)
            .attr('cx', 0)
            .attr('cy', 0)
            .attr('r', 0.02);
    }

    private renderTracks = () => {
        const { svgNode } = this.props;

        if (!svgNode.current || !this.trackArc) return;

        const { timedQueue } = this.props;
        const filteredQueue = timedQueue.filter(e => e.timeUntil < oneHour).reverse();

        const durationHours = timedQueue.reduce((a, b) => a + (b.duration ?? 0), 0) / 3600;

        const svg = select(svgNode.current);

        svg.selectAll('.track').remove();
        svg.selectAll('.caret').remove();
        svg.selectAll('.duration').remove();

        svg.select('.tracks')
            .selectAll('.track')
            .data(filteredQueue)
            .enter()
            .append('path')
            .attr('class', d => d.order === 0 ? 'playing' : '')
            .classed('track', true)
            .attr('d', d => {
                return this.trackArc({
                    innerRadius: 0,
                    outerRadius: 0,
                    startAngle: this.trackSecScale(d.timeUntil),
                    // Cap this at one hour to avoid overlap
                    // TODO: Fade out fill color at one hour
                    endAngle: this.trackSecScale(Math.min(oneHour, d.timeUntil + d.duration)),
                });
            })
            .attr('fill', d => d.userColor)
            .attr('opacity', d => Math.min(1.0, 1.15 * (oneHour - d.timeUntil) / oneHour))
        ;

        if (durationHours > 0) {
            svg.select('.ending')
                .append('path').classed('caret', true)
                .attr('d', `M  0 -${this.r - 25} l -4 -25 l 8 0 z`)
                // Note that rotate takes an argument in degrees, not radians
                .attr('transform', `rotate(${this.hourScale(durationHours)})`)
            ;

            svg.select('.queue')
                .append('path')
                .classed('duration', true)
                .attr('d', d => this.queueArc({
                    innerRadius: 0,
                    outerRadius: 0,
                    startAngle: 0,
                    endAngle: this.durationHourScale(durationHours),
                }))
                .attr('fill', '#ff0000');
        }
    }

    private framed = () => {
        const { svgNode } = this.props;

        if (!svgNode.current)
            requestAnimationFrame(this.framed);

        const dt = new Date();
        const { timedQueue } = this.props;
        const state = this.props.store.getState();
        const queueId = queueIdSelector(state);
        const videoDuration = videoDurationSelector(state);
        const videoTime = timedQueue.length && timedQueue[0].queueId === queueId ? videoTimeHiResSelector(state) : 0;

        const ms = dt.getMilliseconds();
        const secs = dt.getSeconds() + ms/1000;
        const mins = dt.getMinutes() + secs/60;
        const hours = dt.getHours()%12 + mins/60;
        // TODO: Latch video time when time - duration reaches zero-ish until queue timestamp changes
        const trackOffset = dt.getMinutes() + dt.getSeconds() / 60 - videoTime / 60;
        // This offset is in degrees, not radians like above
        const hoursOffset = this.hourScale(hours - videoTime / 3600);

        const svg = select(svgNode.current);

        svg.select('.pointers .hour').attr('transform', `rotate(${this.hourScale(hours)})`);
        svg.select('.pointers .min').attr('transform', `rotate(${this.secMinScale(mins)})`);
        svg.select('.pointers .sec').attr('transform', `rotate(${this.secMinScale(secs)})`);
        svg.selectAll('.track').attr('transform', `rotate(${this.secMinScale(trackOffset)})`);
        svg.select('.ending').attr('transform', `rotate(${hoursOffset})`);
        svg.select('.queue').attr('transform', `rotate(${hoursOffset})`);

        const opacity = videoDuration ? Math.min(1, (videoDuration - videoTime) / 5) : 1;
        svg.select('.track.playing').attr('opacity', opacity);

        requestAnimationFrame(this.framed);
    }
}

const Clock = compose(
    withStyles(styles),
    withRef('svgNode', null),
    withStore(),
    withSelector<PropsFromState, TimedQueueEntry[]>('timedQueue', sortedTimedQueueSelector),
    withSelector<PropsFromState, number>('queueTimestamp', queueTimestampSelector),
    withSelector<PropsFromState, boolean>('playing', playbackStatePlayingSelector),
)(ClockLayout);

export { Clock }