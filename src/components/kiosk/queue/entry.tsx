/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListItem, ListItemAvatar, ListItemText, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as he from 'he';
import * as React from 'react';
import { Textfit } from 'react-textfit';

import { runtimeConfig } from 'runtimeConfig';
import { PlayingQueueEntry, TimedQueueEntry } from 'store/queue';
import { EntryAvatar, EntryChips } from './entry-next';
import { NowPlayingAvatar, NowPlayingChips } from './entry-playing';

const maxEntries = runtimeConfig().client.queue.maxEntries;
const bgAngle = 90 + Math.atan(4.1 / maxEntries) * 180 / Math.PI;

const styles = (theme: Theme) => createStyles({
    entry: {
        position: 'absolute',
        top: 0,
        left: 0,
        transition: 'all 0.5s ease-out',
        zIndex: 0,
        textShadow: '-1px 1px 0 #000, ' +
            '1px 1px 0 #000, ' +
            '1px -1px 0 #000, ' +
            '-1px -1px 0 #000',
        // backgroundColor: ({ entry: { userColor }, index}: OtherProps) => fade(userColor, Math.max((maxEntries - index) / maxEntries, 0)),
        backgroundColor: ({ entry: { userColor } }: OtherProps) => userColor,
        transform: ({ index }: OtherProps) => `translate3d(0%, ${(index) * 100}%, 0)`,

        '& > *': {
            zIndex: 2,
        },

        '&::after': {
            zIndex: 1,
            position: 'absolute',
            left: '-1px',
            right: '-1px',
            top: '-1px',
            bottom: '-1px',
            content: '""',
            background: `linear-gradient(${bgAngle}deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0) calc(55% - var(--index) / ${maxEntries} * 35%), rgba(0,0,0,1) calc(99% - var(--index) / ${maxEntries} * 49%))`,
            '--index': ({ index }: OtherProps) => index + 1,
        },
    },
    title: {
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        fontSize: '1.1em',
    },
    userName: {
        width: '86px',
        textAlign: 'center',
    },
    userIcon: {
        // '& path': {
        //     stroke: 'black',
        //     strokeWidth: '10px',
        // },
    },
});

interface OtherProps {
    index: number;
    entry: PlayingQueueEntry | TimedQueueEntry;
}

const isPlaying = (entry: TimedQueueEntry) => entry.order === 0;

class EntryLayout extends React.Component<OtherProps> {
    public render = () => {
        const { entry } = this.props;
        const { classes } = (this.props as any);

        const icon = findIconDefinition(JSON.parse(entry.userIcon || ""));
        const avatar = isPlaying(entry) ? <NowPlayingAvatar key={entry.queueId} /> : <EntryAvatar entry={entry} />;
        const chips = isPlaying(entry) ? <NowPlayingChips /> : <EntryChips entry={entry} />;

        // TODO: Use chips for the secondary
        return (
            <ListItem className={classes.entry}>
                <ListItemAvatar>
                    <span>
                        <FontAwesomeIcon icon={icon} fixedWidth={true} size="2x" className={classes.userIcon} />
                        <div className={classes.userName}><Textfit mode="single" min={6} max={30}>{entry.user}</Textfit></div>
                    </span>
                </ListItemAvatar>
                {avatar}
                <ListItemText disableTypography={true} primary={<Typography noWrap={true} className={classes.title}>{he.decode(entry.title)}</Typography>} secondary={chips} />
            </ListItem>
        );
    }
}

const Entry = compose<OtherProps>(
    withStyles(styles),
)(EntryLayout);

export { Entry }