/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faLightbulbOn } from '@fortawesome/pro-duotone-svg-icons';
import { faForwardStep } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Typography } from '@mui/material';
import { adaptV4Theme, createTheme, styled, StyledEngineProvider, Theme, ThemeProvider } from '@mui/material/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose } from '@truefit/bach';
import { withAction } from '@truefit/bach-redux';
import { MaterialDesignContent, SnackbarProvider } from 'notistack';
import * as React from 'react'
import { Outlet } from 'react-router';

import { Error } from 'components/error';
import * as playerActions from 'store/player/actions';

import 'src/styles/player.css';


declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

const theme = createTheme(adaptV4Theme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#c91ee2',
        },
        secondary: {
            main: '#4caf50',
        },
    },
    typography: {
        fontSize: 30,
    },
    overrides: {
        MuiListItemAvatar: {
            root: { 
                minWidth: 115
            }
        },
        MuiAvatar: {
            root: {
                width: 100,
                height: 100,
            }
        }
    }
}));

declare module 'notistack' {
    interface VariantOverrides {
        // removes the `warning` variant
        // warning: false;
        // adds `myCustomVariant` variant
        lightingCue: true;
        pauseRequest: true;
        // adds `reportComplete` variant and specifies the
        // "extra" props it takes in options of `enqueueSnackbar`
        // reportComplete: {
        //     allowDownload: boolean
        // }
    }
}

const StyledMaterialDesignContent = styled(MaterialDesignContent)((styles) => ({
    '&.notistack-MuiContent': {
        border: '1px solid',
        borderColor: styles.theme.palette.text.primary,
        color: styles.theme.palette.text.primary,
        backgroundColor: styles.theme.palette.background.paper,
        fontSize: styles.theme.typography.h4.fontSize,
    },

    '&.notistack-MuiContent-lightingCue': {
        borderColor: styles.theme.palette.info.light,
        color: styles.theme.palette.info.light,
    },
    '&.notistack-MuiContent-pauseRequest': {
        borderColor: styles.theme.palette.warning.light,
        color: styles.theme.palette.warning.light,
    },
}));

interface PropsFromDispatch {
    sendKeyDown: typeof playerActions.sendKeyDown;
}

type KioskProps = PropsFromDispatch;

class KioskLayout extends React.Component<KioskProps> {
    public componentDidMount = () => {
        window.addEventListener("keydown", this.handleKeyDown, false);
    }

    public componentWillUnmount = () => {
        window.removeEventListener("keydown", this.handleKeyDown, false);
    }

    public render() {
        return (
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <ErrorBoundary fallback={<Error />}>
                        <SnackbarProvider
                            maxSnack={1}
                            anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
                            Components={{
                                lightingCue: StyledMaterialDesignContent,
                                pauseRequest: StyledMaterialDesignContent,
                            }}
                            iconVariant={{
                                lightingCue: <FontAwesomeIcon icon={faLightbulbOn} size="2x" />,
                                pauseRequest: <FontAwesomeIcon icon={faForwardStep} size="2x" />,
                            }}
                        >
                            <div>
                                <Typography component="div">
                                    <Outlet />
                                </Typography>
                            </div>
                        </SnackbarProvider>
                    </ErrorBoundary>
                </ThemeProvider>
            </StyledEngineProvider>
        );
    }

    private handleKeyDown = (e: KeyboardEvent) => {
        const { sendKeyDown } = this.props;

        sendKeyDown(e.code);
    }
}

const Kiosk = compose(
    withAction('sendKeyDown', playerActions.sendKeyDown),
)(KioskLayout);

export { Kiosk as KioskLayout }