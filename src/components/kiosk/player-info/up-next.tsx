/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Avatar, createStyles, Theme, Typography } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { TimedQueueEntry, upNextSelector } from 'store/queue';

export const height = 135;
const margin = 110; // TODO: 100px + whatever

const styles = (theme: Theme) => createStyles({
    upNext: {
        position: 'relative',
        maxWidth: '100%',
        height: height - theme.spacing(2),
        padding: theme.spacing(1),
    },
    heading: {
        marginLeft: margin,
        fontSize: '1.5rem',
        fontStyle: 'italic',
    },
    count: {
        paddingLeft: theme.spacing(2),
        fontSize: '1.25rem',
    },
    thumbnail: {
        float: 'left',
    },
    title: {
        marginLeft: margin,
    },
    user: {
        marginLeft: margin,
        fontSize: '2rem',
    },
});

interface PropsFromState {
    nextTrack: TimedQueueEntry,
    upNextCount: number,
}

type UpNextProps = PropsFromState;

const UpNextLayout: React.FC<UpNextProps> = (props) => {
    const { nextTrack, upNextCount } = props;
    const { classes } = props as any;

    const icon = findIconDefinition(JSON.parse(nextTrack.userIcon || ""));

    let upNext: any = compose(renderNothing())(React.Component);

    // TODO: Replace w/ react-intl
    if (upNextCount > 1)
        upNext = <span className={classes.count}>plus {upNextCount} more tracks</span>;
    else if (upNextCount === 1)
        upNext = <span className={classes.count}>plus 1 more track</span>;

    return (
        <div className={classes.upNext}>
            <Typography variant="h6" className={classes.heading}>
                Up Next
                {upNext}
            </Typography>
            <Avatar src={nextTrack.thumbnailUrl} variant={"square"} className={classes.thumbnail} />
            <Typography noWrap={true} className={classes.title}>{nextTrack.title}</Typography>
            <div className={classes.user}>
                <FontAwesomeIcon icon={icon} fixedWidth={true} />{" "}
                {nextTrack.user}
            </div>
        </div>
    );
};

const UpNext = compose(
    withStyles(styles),
    withSelector('upNext', upNextSelector),
    withProps({
        nextTrack: ({ upNext }) => upNext.length > 0 ? upNext[0] : undefined,
        upNextCount: ({ upNext }) => upNext.length - 1,
    }),
    renderIf(({ nextTrack }) => nextTrack === undefined, compose(renderNothing())(React.Component)),
)(UpNextLayout);

export { UpNext };