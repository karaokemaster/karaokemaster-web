/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Grid, List, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { ConnectionStatusIcon } from 'components/connectionStatusIcon';
import { Error } from 'components/error';
import { apiVersionSelector, connectAsKiosk, disconnect } from 'store/app';
import { Cue, lightingCuesSelector } from 'store/cues';
import { playerConnectionStatusSelector } from 'store/player';
import { queueConnectionStatusSelector } from 'store/queue';
import { ConnectionStatus } from 'store/signalR';
import { LightingCue } from './lighting-cue';
import { height as nowPlayingHeight, NowPlaying } from './now-playing';
import { height as upNextHeight, UpNext } from './up-next';
import { FootSwitch } from './widgets/footswitch';
import { PlayPause } from './widgets/play-pause';
import { Tempo } from './widgets/tempo';

import gitVersion from 'gitversion.json';

const styles = (theme: Theme) => createStyles({
    playerInfo: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: nowPlayingHeight + upNextHeight,
    },
    queue: {
        maxWidth: '100%',

        '& > *': {
            overflowX: 'hidden',
            overflowY: 'hidden',
            position: 'unset !important',
        },
    },
    widgets: {
        padding: theme.spacing(1),

        '& > .MuiGrid-item': {
            paddingBottom: 0,
            paddingLeft: 0,
        }
    },
    cues: {
        position: 'absolute',
        left: 0,
        top: nowPlayingHeight + upNextHeight,
        right: 0,
        bottom: 0,
        padding: theme.spacing(1),
        paddingTop: theme.spacing(3),
        overflowX: 'hidden',
        overflowY: 'hidden',
    },
    cueList: {
        paddingRight: '8px',
        columnCount: 4,
        columnGap: '1rem',
    },
    version: {
        position: 'fixed',
        bottom: 0,
        right: 0,
        padding: '5px',
        color: '#999',
        fontSize: '1rem',
        opacity: 0.5,
    },
});

interface PropsFromState {
    cues: Cue[],
    apiVersion: string,
    playerConnectionStatus: ConnectionStatus,
    queueConnectionStatus: ConnectionStatus,
}

interface PropsFromDispatch {
    connectAsKiosk: typeof connectAsKiosk
    disconnect: typeof disconnect
}

type InfoProps = PropsFromState & PropsFromDispatch;

class InfoLayout extends React.Component<InfoProps> {
    public render() {
        const { cues, apiVersion, playerConnectionStatus, queueConnectionStatus } = this.props;
        const { classes } = this.props as any;

        return (
            <div className="kiosk">
                <div className={classes.playerInfo}>
                    <Grid container={true} spacing={3}>
                        <Grid item={true} xs={8}>
                            <Grid container={true} direction="column">
                                <ErrorBoundary fallback={<Error />}>
                                    <NowPlaying />
                                    <UpNext />
                                </ErrorBoundary>
                            </Grid>
                        </Grid>
                        <Grid item={true} xs={4}>
                            <ErrorBoundary fallback={<Error />}>
                                <Grid container={true} spacing={3} className={classes.widgets}>
                                    <Grid item={true} xs={6}>
                                        <Tempo />
                                    </Grid>
                                    <Grid item={true} xs={6}>
                                        <PlayPause />
                                    </Grid>
                                    <Grid item={true} xs={12}>
                                        <FootSwitch />
                                    </Grid>
                                </Grid>
                            </ErrorBoundary>
                        </Grid>
                    </Grid>
                </div>
                <div className={classes.cues}>
                    <Typography variant="h6">Lighting Cue</Typography>
                    <List className={classes.cueList}>
                        <ErrorBoundary fallback={<Error />}>
                            {cues.map(c => <LightingCue key={c.index} cue={c} />)}
                        </ErrorBoundary>
                    </List>
                </div>

                <div className={classes.version}>
                    v{gitVersion.SemVer} (server v{apiVersion}){" "}
                    <ConnectionStatusIcon status={queueConnectionStatus} />{" "}
                    <ConnectionStatusIcon status={playerConnectionStatus} />
                </div>
            </div>
        )
    }
}

const PlayerInfo = compose(
    withStyles(styles),
    withSelector('cues', lightingCuesSelector),
    withSelector('apiVersion', apiVersionSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withAction('connectAsKiosk', connectAsKiosk),
    withAction('disconnect', disconnect),
    withEffect(({ connectAsKiosk, disconnect }) => {
        connectAsKiosk();

        return () => { disconnect(); }
    }, []),
)(InfoLayout);

export { PlayerInfo };