/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { darken, LinearProgress, LinearProgressProps, Theme } from '@mui/material';
import { green, orange, pink } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { mapProps, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';

import { runtimeConfig } from 'runtimeConfig';
import { nowPlayingSelector } from 'store/queue';

export const progressHeight = 30;

const progressStyles = (theme: Theme) => createStyles({
    root: {
        height: 30,
        borderRadius: 5,
        border: '2px solid #000',
        // transition: 'background 0.25s ease-in',
    },
    colorPrimary: {
        background: `linear-gradient(90deg, ` +
            `${theme.palette.background.default} 0%, ` +
            `${theme.palette.background.default} ${runtimeConfig().client.player.stopAfterPercent * 100}%, ` +
            `${theme.palette.background.paper} ${runtimeConfig().client.player.stopAfterPercent * 100}%, ` +
            `${theme.palette.background.paper} 100%)`,
    },
    colorSecondary: {
        backgroundColor: darken(pink.A400, 0.5),
        backgroundImage: ({endingPercent}: any) =>
            `linear-gradient(90deg, ` +
            `${darken(pink.A400, 0.7)} 0%, ` +
            `${darken(pink.A400, 0.7)} ${endingPercent}%, ` +
            `transparent ${endingPercent}%, ` +
            `transparent 100%), ` +
            `url("data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg ` +
            `fill='${encodeURIComponent(darken(pink.A400, 0.6))}' ` +
            `fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E")`,
        // TODO: Only animate when value > endingPercent
        // animation: '$barberPole 1s linear infinite',
    },
    '@keyframes barberPole': {
        '0%': {
            backgroundPositionY: 0,
        },
        '100%': {
            backgroundPositionY: 40,
        },
    },
    bar: {
        borderRadius: 5,
        transition: 'transform 1s linear, background-color 0.25s ease-in',
    },
    barColorPrimary: {
        backgroundColor: green[500],
    },
    barColorSecondary: {
        backgroundColor: orange[900],
    },
});

export const TrackProgress = compose<LinearProgressProps>(
    withSelector('nowPlaying', nowPlayingSelector),
    withProps({
        value: ({nowPlaying}) => nowPlaying?.videoDuration > 0 ? 100 * nowPlaying?.videoTime / nowPlaying?.videoDuration : 0,
        color: ({value}) => value / 100 > runtimeConfig().client.player.stopAfterPercent ? 'secondary' : 'primary',
        endingPercent: ({nowPlaying}) => nowPlaying ? (nowPlaying.videoDuration - 30) / nowPlaying.videoDuration * 100 : 95,
    }),
    withStyles(progressStyles),
    mapProps(({classes, value, color, endingPercent, nowPlaying, ...rest}) => ({
        classes,
        value,
        color,
        variant: 'determinate',
        ...rest
    })),
)(LinearProgress);