/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faCircle, faCircleCheck, faCirclePlay } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { blue, green } from '@mui/material/colors';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';

import { Cue, CueState, isCuePendingSelector, isCueSavedToCurrentTrack } from 'store/cues';
import * as cueActions from 'store/cues/actions';

const styles = (theme: Theme) => createStyles({
    cue: {
        borderRadius: '1rem',
        marginBottom: '0.25rem',
        '-webkit-column-break-inside': 'avoid',
        pageBreakInside: 'avoid',
        breakInside: 'avoid',
    },
    icon: {
        color: 'inherit',
    },
    cueName: {
        fontSize: '0.85em',
    },
    activeCue: {
        backgroundColor: green[500],
        color: '#000',
    },
    selection: {
        fontSize: '22px',
    },
    waiting: {
        animationDuration: '1s',
        animationFillMode: 'both',
        animationIterationCount: 'infinite',
        animationName: '$waitingShimmer',
        animationTimingFunction: 'easeInOut',
        animationDirection: 'alternate',
        backgroundColor: blue[900],
        background: `linear-gradient(to right, ${blue[900]} 8%, ${blue[500]} 18%, ${blue[900]} 33%)`,
    },
    '@keyframes waitingShimmer': {
        '0%': {
            backgroundPosition: '-72px 0',
        },
        '100%': {
            backgroundPosition: '328px 0',
        },
    },
});

interface PropsFromState {
    isCueSaved: boolean
    pending: boolean
}

interface PropsFromDispatch {
    sendCue: typeof cueActions.sendCue
}

interface OtherProps {
    cue: Cue,
}

type LightingCueProps = PropsFromState & PropsFromDispatch;

// TODO: Find a way to eliminate while preserving derive state
const initialState = { waiting: false }
type LightingCueState = Readonly<typeof initialState>

class LightingCueLayout extends React.Component<LightingCueProps & OtherProps, LightingCueState> {
    public readonly state: LightingCueState = initialState;

    public static getDerivedStateFromProps = (props: LightingCueProps & OtherProps, state: LightingCueState) => {
        // TODO: Replace w/ effect
        if (state.waiting) {
            if (props.cue && props.cue.state === CueState.On) {
                return { waiting: false }
            }
        }

        return {};
    }

    public render() {
        const { cue, isCueSaved, pending } = this.props;
        const { classes } = this.props as any;
        const { waiting } = this.state;

        const itemClasses = classNames({
            [classes.cue]: true,
            [classes.activeCue]: cue.state === CueState.On,
            [classes.waiting]: waiting || pending,
        });

        return (
            <ListItem className={itemClasses} onClick={this.selectCue}>
                <ListItemIcon className={classes.icon}><FontAwesomeIcon icon={cue.state === CueState.On ? faCirclePlay : faCircle} /></ListItemIcon>
                <ListItemText primaryTypographyProps={{ className: classes.cueName, noWrap: true }}>{cue.name}</ListItemText>
                <ListItemSecondaryAction className={classes.selection}>
                    {isCueSaved && <FontAwesomeIcon icon={faCircleCheck}/>}
                </ListItemSecondaryAction>
            </ListItem>
        )
    }

    public selectCue = () => {
        const { sendCue, cue } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({ name: 'kiosk/playerInfo:selectCue' });
            s.setSpan(transaction);

            this.setState({ waiting: true });

            sendCue(cue.index);

            transaction.finish();
        });
    }
}

const LightingCue = compose<OtherProps>(
    withStyles(styles),
    withSelector('isCueSaved', (state, props: OtherProps) => isCueSavedToCurrentTrack(state, props.cue)),
    withSelector('pending', (state, props: OtherProps) => isCuePendingSelector(state, props.cue)),
    withAction('sendCue', cueActions.sendCue),
)(LightingCueLayout);

export { LightingCue };