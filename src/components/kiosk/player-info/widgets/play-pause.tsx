/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faHourglassStart } from '@fortawesome/pro-duotone-svg-icons';
import { faForwardStep, faPause, faPlay } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { createStyles, darken, Theme, Typography } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';

import { PlaybackState, playbackStateSelector, playerConnectionStatusSelector, PlayerControl } from 'store/player';
import * as playerActions from 'store/player/actions';
import { nowPlayingSelector, upNextSelector } from 'store/queue';
import { ConnectionStatus } from 'store/signalR';

import { runtimeConfig } from 'runtimeConfig';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        borderRadius: '1rem',
        padding: '0.75rem',
        backgroundColor: theme.palette.background.default,
        borderColor: theme.palette.background.default,
        borderWidth: '0.2rem',
        borderStyle: 'solid',
        height: '50px',
    },
    disabled: {
        backgroundColor: darken(theme.palette.background.default, 0.35),
        borderColor: darken(theme.palette.background.default, 0.35),
        color: theme.palette.background.paper,

        '& $icon': {
            color: theme.palette.background.default,
        },
    },
    icon: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '0.85em',
        maxWidth: '50px',
        maxHeight: '50px',
        width: '50px',
        height: '50px',
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary,
        borderRadius: '50%',
    },
    label: {
        marginLeft: `calc(${theme.spacing(2)} + 50px)`,
        fontSize: '28px',
        textAlign: 'left',
    },
});

interface PropsFromState {
    playbackState: PlaybackState,
    enabled: boolean,
    playing: boolean;
    stoppingAfter: boolean;
    stopAfter: boolean;
}

interface PropsFromDispatch {
    sendPlayerControl: typeof playerActions.sendPlayerControl;
}

type PlayPauseProps = PropsFromState & PropsFromDispatch;

class PlayPauseLayout extends React.Component<PlayPauseProps> {
    public render() {
        const { enabled, playing, stoppingAfter, stopAfter, playbackState } = this.props;
        const { classes } = this.props as any;

        const root = classNames({
            [classes.root]: true,
            [classes.disabled]: !enabled,
        });

        let icon: IconDefinition = faPlay;
        let text: string = "Play";

        if (playbackState === PlaybackState.Starting) {
            icon = faHourglassStart;
            text = "Starting";
        }
        else if (playing && stoppingAfter) {
            icon = faForwardStep;
            text = "Stopping after";
        }
        else if (playing && stopAfter) {
            icon = faForwardStep;
            text = "Stop after";
        }
        else if (playing) {
            icon = faPause;
            text = "Pause";
        }

        return (
            <div className={root} onClick={this.playPause}>
                <div className={classes.icon}>
                    <FontAwesomeIcon icon={icon} />
                </div>
                <Typography variant="h3" className={classes.label}>{text}</Typography>
            </div>
        )
    }

    private playPause = () => {
        const { playbackState, sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'playerInfo:playPause'});
            s.setSpan(transaction);

            const control = playbackState === PlaybackState.Playing ? PlayerControl.Pause : PlayerControl.Play;
            transaction.setData("control", control);
            sendPlayerControl(control);

            transaction.finish();
        });
    }
}

const PlayPause = compose(
    withStyles(styles),
    withSelector('nowPlaying', nowPlayingSelector),
    withSelector('upNext', upNextSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('playbackState', playbackStateSelector),
    withAction('sendPlayerControl', playerActions.sendPlayerControl),
    withProps({
        enabled: ({ nowPlaying, upNext, playbackState, playerConnectionStatus }) => (nowPlaying || upNext.length > 0)
            && playbackState !== PlaybackState.Disconnected
            && playerConnectionStatus === ConnectionStatus.Connected,
        stoppingAfter: ({ playbackState }) => playbackState === PlaybackState.StoppingAfter,
        playing: ({ playbackState, stoppingAfter }) => playbackState === PlaybackState.Playing || stoppingAfter,
        stopAfter: ({ playing, videoTime, videoDuration }) => playing && (videoTime / videoDuration > runtimeConfig().client.player.stopAfterPercent),
    }),
)(PlayPauseLayout);

export { PlayPause };