/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faWavePulse } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { darken, Theme, Typography } from '@mui/material';
import { green } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import { compose, withEffect, withRef } from '@truefit/bach';
import { withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { playbackStatePlayingSelector, playerTempoBeatSelector, playerTempoSelector } from 'store/player';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'relative',
        borderRadius: '1rem',
        padding: '0.75rem',
        backgroundColor: darken(theme.palette.background.default, 0.35),
        borderColor: theme.palette.background.default,
        borderWidth: '0.2rem',
        borderStyle: 'solid',
        height: '50px',
    },
    notPlaying: {
        '& $tempo': {
            display: 'none',
        },
        '& $beat': {
            animation: 'none',
        },
    },
    icon: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '0.85em',
        maxWidth: '50px',
        maxHeight: '50px',
        width: '50px',
        height: '50px',
        textAlign: 'center',
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary,
        borderRadius: '50%',
    },
    tempo: {
        fontSize: '44px',
        textAlign: 'right',
    },
    beat: {
        animation: '$pulse 0.15s ease-out forwards',
    },
    '@keyframes pulse': {
        '0%': {
            backgroundColor: theme.palette.background.paper,
        },
        '5%': {
            backgroundColor: green[700],
        },
        '25%': {
            backgroundColor: green[700],
        },
        '100%': {
            backgroundColor: theme.palette.background.paper,
        },
    },
});

interface PropsFromState {
    bpm: number,
    beat: React.MutableRefObject<HTMLDivElement>;
    playing: boolean;
}

type TempoProps = PropsFromState;

class TempoLayout extends React.Component<TempoProps> {
    public render() {
        const { bpm, beat, playing } = this.props;
        const { classes } = this.props as any;

        const root = classNames({
            [classes.root]: true,
            [classes.notPlaying]: !playing,
        });

        return (
            <div className={root}>
                <div ref={beat} className={classes.icon}>
                    <FontAwesomeIcon icon={faWavePulse} />
                </div>
                <Typography variant="h3" className={classes.tempo}>{bpm}</Typography>
            </div>
        )
    }
}

let timeout: number = 0;

const Tempo = compose(
    withStyles(styles),
    withSelector('bpm', playerTempoSelector),
    withSelector('bpmTimestamp', playerTempoBeatSelector),
    withSelector('playing', playbackStatePlayingSelector),
    withRef('beat', undefined),
    withEffect(({ beat, classes }: TempoProps & any) => {
        if (!beat.current) return;

        if (timeout) window.clearTimeout(timeout);

        beat.current.classList.add(classes.beat);
        timeout = window.setTimeout(() => {
            beat.current.classList.remove(classes.beat);
            timeout = 0;
        }, 200);
    }, [ 'bpmTimestamp' ]),
)(TempoLayout);

export { Tempo };