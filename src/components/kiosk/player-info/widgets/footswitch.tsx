/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faHourglassStart, faMusicNoteSlash, faShoePrints, faTransporterEmpty } from '@fortawesome/pro-duotone-svg-icons';
import { faArrowTurnDown, faForwardStep, faPlay } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { darken, Theme, Typography } from '@mui/material';
import { green } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import { compose, withRef } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { runtimeConfig } from 'runtimeConfig';

import { PlaybackState, playbackStateSelector, playerCurrentQueueEntrySelector, videoDurationSelector, videoTimeSelector } from 'store/player';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        borderRadius: '1rem',
        padding: '0.75rem',
        backgroundColor: darken(theme.palette.background.default, 0.35),
        borderColor: theme.palette.background.default,
        borderWidth: '0.2rem',
        borderStyle: 'solid',
        height: '50px',
    },
    icon: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '0.85em',
        maxWidth: '50px',
        maxHeight: '50px',
        width: '50px',
        height: '50px',
        textAlign: 'center',
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary,
        borderRadius: '50%',
    },
    action: {
        fontSize: '20px',
        textAlign: 'left',
    },
    text: {
        marginLeft: `calc(${theme.spacing(2)} + 50px)`,
        fontSize: '24px',
        textAlign: 'left',
    },
    pulse: {
        animation: '$pulse 0.35s ease-out forwards',
    },
    '@keyframes pulse': {
        '0%': {
            backgroundColor: theme.palette.background.paper,
        },
        '5%': {
            backgroundColor: green[700],
        },
        '25%': {
            backgroundColor: green[700],
        },
        '100%': {
            backgroundColor: theme.palette.background.paper,
        },
    },
});

interface PropsFromState {
    widget: React.MutableRefObject<HTMLDivElement>;
    playbackState: PlaybackState;
    playing: boolean;
    stoppingAfter: boolean;
    hasTrack: boolean;
    stopAfter: boolean;
}

type FootSwitchProps = PropsFromState;

class FootSwitchLayout extends React.Component<FootSwitchProps> {
    private timeout: number;

    public componentDidMount = () => {
        document.addEventListener("keydown", this.handleKeyDown, false);
        document.addEventListener("km_keydown", this.handleKeyDown, false);
    }

    public componentWillUnmount = () => {
        document.removeEventListener("keydown", this.handleKeyDown, false);
        document.removeEventListener("km_keydown", this.handleKeyDown, false);
    }

    public render() {
        const { widget, playbackState, hasTrack, playing, stoppingAfter, stopAfter } = this.props;
        const { classes } = this.props as any;

        let icon: IconDefinition = faTransporterEmpty;
        let text: string = "Add some tracks!";
        let action: boolean = false;

        if (playbackState === PlaybackState.Starting) {
            icon = faHourglassStart;
            text = "Starting";
        }
        else if (playbackState === PlaybackState.Disconnected || playbackState === PlaybackState.Failed) {
            icon = faMusicNoteSlash;
            text = "Offline";
        }
        else if (!playing && hasTrack) {
            icon = faPlay;
            text = "Start playback";
            action = true;
        }
        else if (playing && !stopAfter) {
            icon = faArrowTurnDown;
            text = "Toggle lighting cue";
            action = true;
        }
        else if (playing && stoppingAfter) {
            icon = faForwardStep;
            text = "Stopping after this track";
        }
        else if (playing && stopAfter) {
            icon = faForwardStep;
            text = "Stop after this track";
            action = true;
        }

        return (
            <div ref={widget} className={classes.root}>
                <div className={classes.icon}>
                    <FontAwesomeIcon icon={faShoePrints} rotation={270} />
                </div>
                <Typography variant="h3" className={classes.text}>
                    <Typography variant="h5" className={classes.action} hidden={!action}>
                        Tap the foot switch to
                    </Typography>
                    <FontAwesomeIcon icon={icon} />{" "}
                    {text}
                </Typography>
            </div>
        )
    }

    private handleKeyDown = (e: any) => {
        const toggleCueKey = runtimeConfig().client.player.cueToggleKey || 'Space';
        if (e.code !== toggleCueKey) return;

        // TODO: Also intercept player control messages
        if (this.timeout) window.clearTimeout(this.timeout);

        const { widget } = this.props;
        const { classes } = this.props as any;

        widget.current.classList.add(classes.pulse);

        this.timeout = window.setTimeout(() => {
            widget.current.classList.remove(classes.pulse);
            this.timeout = 0;
        }, 400);
    }
}

const FootSwitch = compose(
    withStyles(styles),
    withSelector('playbackState', playbackStateSelector),
    withSelector('videoTime', videoTimeSelector),
    withSelector('videoDuration', videoDurationSelector),
    withSelector('current', playerCurrentQueueEntrySelector),
    withProps({
        hasTrack: ({ current }) => !!current,
        stoppingAfter: ({ playbackState }) => playbackState === PlaybackState.StoppingAfter,
        playing: ({ playbackState, stoppingAfter }) => playbackState === PlaybackState.Playing || stoppingAfter,
        stopAfter: ({ playing, videoTime, videoDuration }) => playing && (videoTime / videoDuration > runtimeConfig().client.player.stopAfterPercent),
    }),
    withRef('widget', undefined),
)(FootSwitchLayout);

export { FootSwitch };