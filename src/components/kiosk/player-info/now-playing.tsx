/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { createStyles, Theme, Typography } from '@mui/material';
import { lightBlue, red } from '@mui/material/colors';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';

import { Time } from 'components/time';
import { nextTrackForUsernameSelector, nowPlayingSelector, PlayingQueueEntry, TimedQueueEntry } from 'store/queue';
import { progressHeight, TrackProgress } from './track-progress';

export const height = 135;

const styles = (theme: Theme) => createStyles({
    nowPlaying: {
        position: 'relative',
        maxWidth: '100%',
        height: `calc(${height}px - ${theme.spacing(2)})`, // height - theme.spacing(2),
        padding: theme.spacing(1),
    },
    user: {
        position: 'absolute',
        bottom: `calc(${progressHeight}px + ${theme.spacing(1)})`,
        left: theme.spacing(1),
        fontSize: '2rem',
    },
    time: {
        position: 'absolute',
        bottom: `calc(${progressHeight}px + ${theme.spacing(1)})`,
        right: theme.spacing(1),
        fontSize: '1.5rem',
    },
    progress: {
        position: 'absolute',
        height: progressHeight,
        bottom: theme.spacing(1),
        left: theme.spacing(1),
        right: theme.spacing(1),
    },

    container: {
        position: 'relative',
        height: `calc(${height}px - ${theme.spacing(1)} - 20px)`,
        paddingBottom: `calc(${theme.spacing(1)} + 20px)`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '0.25em',

        textShadow: '-1px 1px 0 #000, ' +
            '1px 1px 0 #000, ' +
            '1px -1px 0 #000, ' +
            '-1px -1px 0 #000',
    },
    // TODO: Ensure track progress opacity is unaffected
    nextForUser: {
        backgroundColor: lightBlue[900],
        // animation: '$fadeIn 0.5s ease-in forwards',
    },
    userUpNext: {
        backgroundColor: red[900],
        // animation: '$fadeIn 0.5s ease-in forwards',
    },
    noMoreForUser: {
        backgroundColor: theme.palette.primary.main,
        textAlign: 'center',
        // animation: '$fadeIn 0.5s ease-in forwards',
    },
    '@keyframes fadeIn': {
        '0%': {
            opacity: 0,
        },
        '100%': {
            opacity: 1,
        },
    },
});

interface PropsFromState {
    nowPlaying: PlayingQueueEntry,
    nextTrackForUser: TimedQueueEntry,
    percent: number,
    ending: boolean,

    user?: string,
}

type NowPlayingProps = PropsFromState;

class NextForUserLayout extends React.Component<NowPlayingProps> {
    public render() {
        const { nextTrackForUser, user } = this.props;
        const { classes } = this.props as any;

        return (
            <div className={classNames(classes.container, classes.nextForUser)}>
                <div>Stay tuned, {user}! You're back <Time seconds={nextTrackForUser.timeUntil} humanize={true} />!</div>
                <TrackProgress className={classes.progress} />
            </div>
        )
    }
}

class UserUpNextLayout extends React.Component<NowPlayingProps> {
    public render() {
        const { user } = this.props;
        const { classes } = this.props as any;

        return (
            <div className={classNames(classes.container, classes.userUpNext)}>
                <div>Don't go anywhere, {user}! You're up next!</div>
                <TrackProgress className={classes.progress} />
            </div>
        )
    }
}

class NoMoreForUserLayout extends React.Component {
    public render() {
        const { classes } = this.props as any;

        return (
            <div className={classNames(classes.container, classes.noMoreForUser)}>
                <div>This is your last track, for now.</div>
                <div>Be sure to add some more!</div>
                <TrackProgress className={classes.progress} />
            </div>
        )
    }
}

const NowPlayingLayout: React.FC<NowPlayingProps> = (props) => {
    const { nowPlaying } = props;
    const { classes } = props as any;

    const icon = findIconDefinition(JSON.parse(nowPlaying.userIcon || ""));

    return (
        <div className={classes.nowPlaying}>
            <Typography noWrap={true} className={classes.title}>{nowPlaying.title}</Typography>
            <div className={classes.user}>
                <FontAwesomeIcon icon={icon} fixedWidth={true} />{" "}
                {nowPlaying.user}
            </div>
            <div className={classes.time}>
                <Time seconds={nowPlaying.videoTime} /> / <Time seconds={nowPlaying.videoDuration} />
            </div>
            <TrackProgress className={classes.progress} />
        </div>
    );
};

const NowPlaying = compose(
    withStyles(styles),
    withSelector('nowPlaying', nowPlayingSelector),
    withProps({
        user: ({ nowPlaying }) => nowPlaying?.user,
        ending: ({ nowPlaying }) => nowPlaying?.videoDuration > 0 ? (nowPlaying?.videoDuration - nowPlaying?.videoTime) <= 30 : false,
    }),
    withSelector('nextTrackForUser', (state, { user }) => nextTrackForUsernameSelector(user)(state)),
    renderIf(({ nowPlaying }) => nowPlaying === undefined, compose(renderNothing())(React.Component)),
    renderIf(({ ending, nextTrackForUser }) => ending && nextTrackForUser === undefined, NoMoreForUserLayout),
    renderIf(({ ending, nextTrackForUser }) => ending && nextTrackForUser.order === 1, UserUpNextLayout),
    renderIf(({ ending }) => ending, NextForUserLayout),
)(NowPlayingLayout);

export { NowPlaying };