/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { compose, withEffect } from '@truefit/bach';
import { withLocation } from '@truefit/bach-react-router';
import { renderIf, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import { withAction } from '@truefit/bach-redux';
import * as React from 'react';
import { Navigate } from 'react-router-dom';

import { connectAsUser, disconnect } from 'store/app';
import { authenticatingSelector, tokenSelector, usernameSelector } from 'store/userProfile';
import * as userProfileActions from 'store/userProfile/actions';
import { Loading } from './loading';

const RedirectToLogin = compose(
    withLocation(),
    withProps({
        to: ({ location }) => `/login?next=${location.pathname}`,
    })
)(Navigate);

interface PropsFromState {
    authenticating: boolean
    username: string
    token: string
}

interface PropsFromDispatch {
    login: typeof userProfileActions.login
    connectAsUser: typeof connectAsUser
    disconnect: typeof disconnect
}

interface OtherProps {
    children: JSX.Element
}

type RequireUserProps = PropsFromState & PropsFromDispatch;

class RequireUserLayout extends React.Component<RequireUserProps & OtherProps> {
    public componentDidMount = () => {
        const { connectAsUser } = this.props;

        connectAsUser();
    }

    public componentWillUnmount = () => {
        const { disconnect } = this.props;

        disconnect();
    }

    public render = () => {
        const { children } = this.props;

        return children;
    }
}

const RequireUser = compose(
    withLocation(),
    withSelector('authenticating', authenticatingSelector),
    withSelector('username', usernameSelector),
    withSelector('token', tokenSelector),
    withAction('login', userProfileActions.login),
    withAction('connectAsUser', connectAsUser),
    withAction('disconnect', disconnect),
    withEffect(
        ({ authenticating, username, token, login }: RequireUserProps) => {
            // tslint:disable-next-line:no-console
            console.log('effect!')
            if (!authenticating && username && !token)
                login(username);
        },
        [ 'username', 'token' ],
    ),
    renderIf(({ username, authenticating }) => !authenticating && !username, RedirectToLogin),
    renderIf(({ authenticating, token }) => authenticating || !token, Loading),
)(RequireUserLayout);

export { RequireUser };