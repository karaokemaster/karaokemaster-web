/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as moment from "moment";
import 'moment-duration-format';
import * as React from 'react';


interface OtherProps {
    seconds?: number | null;
    nullIfNegative?: boolean;
    humanize?: boolean;
}

type TimeProps = OtherProps;

class TimeLayout extends React.Component<TimeProps> {
    public render = () => {
        const { seconds, nullIfNegative, humanize } = this.props;

        if (seconds === null || seconds === undefined || (nullIfNegative && (seconds < 1))) {
            return <>--:--</>;
        }

        const duration = moment.duration({ seconds: Math.max(0, seconds) });
        const timeDisplay = humanize
            ? duration.humanize(true)
            : duration.format('mm:ss', { trim: false })
        ;

        return <>{timeDisplay}</>;
    }
}

const Time = TimeLayout;

export { Time };