/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCircle as falCircle, faCircleNotch } from '@fortawesome/pro-light-svg-icons';
import { faCircle as fasCircle, faCircleExclamation } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { green, grey, orange, red } from '@mui/material/colors';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { ConnectionStatus } from 'store/signalR';

const styles = (theme: Theme) => createStyles({
    connected: {
        color: green[900],
    },
    disconnected: {
        color: red[900],
    },
    connecting: {
        color: orange[500],
    },
    default: {
        color: grey[800],
    },
});

interface OtherProps {
    status: ConnectionStatus
}

class ConnectionStatusIconLayout extends React.Component<OtherProps> {
    public render = () => {
        const { status } = this.props;
        const { classes } = this.props as any;

        switch (status) {
            case ConnectionStatus.Connected:
                return <FontAwesomeIcon icon={fasCircle} className={classes.connected} />
            case ConnectionStatus.Disconnected:
                return <FontAwesomeIcon icon={faCircleExclamation} className={classes.disconnected} />
            case ConnectionStatus.Connecting:
            case ConnectionStatus.Reconnecting:
                return <FontAwesomeIcon icon={faCircleNotch} className={classes.connecting} spin />
            default:
                return <FontAwesomeIcon icon={falCircle} className={classes.default} />
        }
    }
}

const ConnectionStatusIcon = compose<OtherProps>(
    withStyles(styles),
)(ConnectionStatusIconLayout);

export { ConnectionStatusIcon }
