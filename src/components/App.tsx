/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import * as Sentry from '@sentry/browser';
import * as SentryReact from '@sentry/react';
import { History } from 'history';
import * as React from 'react';
import { HistoryRouter as Router } from 'redux-first-history/rr6';

import { AppRoutes } from 'components/routes';

import 'styles/index.css';

interface OwnProps {
    history: History
}

class App extends React.Component<OwnProps> {
    constructor(props: any) {
        super(props);
    }

    public componentDidCatch(error: any, info: any) {
        Sentry.withScope(scope => {
            Object.keys(info).forEach(key => {
                scope.setExtra(key, info[key]);
            });
            Sentry.captureException(error);
        });
    }

    public render() {
        const { history } = this.props;

        return (
            <Router history={history}>
                <AppRoutes />
            </Router>
        );
    }
}

export default SentryReact.withProfiler(App);
