/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withRef } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import Smarquee from 'smarquee';

const styles = (theme: Theme) => createStyles({
    root: {
        userSelect: 'none',

        '&::after': {
            zIndex: 1,
            position: 'absolute',
            left: '-1px',
            right: '-1px',
            top: '-1px',
            bottom: '-1px',
            content: '""',
            backgroundColor: 'transparent',
            background: 'linear-gradient(90deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0) 80%, rgba(0,0,0,1) 100%)',
        },
    },
});

interface PropsFromState {
    marquee: React.MutableRefObject<HTMLSpanElement>,
}

type MarqueeProps = PropsFromState;

interface OtherProps {
    text: string,
}

class MarqueeLayout extends React.Component<MarqueeProps & OtherProps> {
    private smarquee: any;

    public componentDidMount = () => {
        const { marquee } = this.props;

        this.smarquee = new Smarquee({
            element: marquee.current,
            styleOptions: {
                scrollingTitleMargin: 100,
                delay: '3s',
            }
        });
        this.smarquee.init();
    }

    public componentWillUnmount() {
        this.smarquee.destroy();
    }

    public componentDidUpdate(prevProps: Readonly<MarqueeProps & OtherProps>, prevState: Readonly<{}>, snapshot?: any) {
        const { text } = this.props;

        if (prevProps.text === text) return;

        this.smarquee.updateText(text);
    }

    public render = () => {
        const { text, marquee } = this.props;
        const { classes } = (this.props as any);

        return (
            <Typography ref={marquee} className={classes.root}>
                {text}
            </Typography>
        )
    }
}

const Marquee = compose<OtherProps>(
    withStyles(styles),
    withRef('marquee', undefined),
)(MarqueeLayout);

export { Marquee }
