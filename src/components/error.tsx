/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faBomb } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import { Overlay } from "./overlay";

const styles = (theme: Theme) => createStyles({
    icon: {
        color: theme.palette.primary.main,
        padding: '1.5rem',
    },
    caption: {
    },
});

const ErrorLayout = ({ classes }: any) => (
    <Overlay>
        <>
            <FontAwesomeIcon icon={faBomb} size="5x" className={classes.icon} />
            <Typography variant="h6" className={classes.caption}>
                Oops!
            </Typography>
        </>
    </Overlay>
);

const Error = compose(
    withStyles(styles),
)(ErrorLayout);

export { Error }
