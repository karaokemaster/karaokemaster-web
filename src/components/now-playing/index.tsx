/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withLayoutEffect, withRef, withState } from '@truefit/bach';
import { withLocation } from '@truefit/bach-react-router';
import { renderIf, withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import Plx from 'react-plx';
import { animateScroll } from 'react-scroll';
import { ItemInterface, ReactSortable } from 'react-sortablejs';
import { SwipeableList } from 'react-swipeable-list';

import { Error } from 'components/error';
import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { EditLyricsDialog } from 'components/queue/editLyricsDialog';
import { Entry } from 'components/queue/entry';
import { SetUserDialog } from 'components/queue/setUserDialog';
import { nowPlayingSelector, PlayingQueueEntry, queueConnectionStatusSelector, QueueEntry, queueTimestampSelector, upNextSelector, userHasTrackInQueueSelector } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { scrollPositionSelector } from 'store/user';
import * as userActions from 'store/user/actions';
import { ConnectionStatus } from "../../store/signalR";
import { EmptyQueue } from './emptyQueue';
import { initialHeight, miniHeight, MiniPlayer, parallaxEnd } from './miniPlayer';
import { Start } from './start';

import 'react-swipeable-list/dist/styles.css';

const headingHeight = 54;
const headingPadding = 10;

const styles = (theme: Theme) => createStyles({
    container: {
        paddingTop: initialHeight + headingHeight + (headingPadding * 2),
        userSelect: 'none',

        '&.notPlaying': {
            paddingTop: headingHeight + (headingPadding * 2),

            '.track-banner &': {
                paddingTop: headingHeight + (headingPadding * 2) - bannerHeight,
            },
            '.notification-prompt &': {
                paddingTop: headingHeight + (headingPadding * 2),
            },
        },
        '.track-banner &': {
            paddingTop: initialHeight + headingHeight + (headingPadding * 2) - bannerHeight,
        },
        '.notification-prompt &': {
            paddingTop: initialHeight + headingHeight + (headingPadding * 2),
        },
    },
    heading: {
        '--headingPadding': `${headingPadding}px`,
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        height: initialHeight + headingHeight,
        // backgroundColor: 'rgba(25,25,25,.25)',
        background: "linear-gradient(180deg, rgba(25,25,25,.25) 0%, rgba(25,25,25,.25) 95%, rgba(25,25,25,0) 100%)",
        backdropFilter: 'blur(25px)',
        zIndex: 1000,
        margin: 0,
        paddingTop: 'calc(var(--headingPadding) + env(safe-area-inset-top))',
        paddingBottom: headingPadding,
        userSelect: 'none',

        '.notPlaying &': {
            height: headingHeight,

            '.track-banner &': {
                '--bannerHeight': `${bannerHeight}px`,
                top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
                paddingTop: headingPadding,
                height: headingHeight - 20,
            },

            '.notification-prompt &': {
                '--promptHeight': `${promptHeight}px`,
                top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
                paddingTop: headingPadding,
                height: headingHeight - 20,
            },
        },
        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
            paddingTop: headingPadding,
            height: initialHeight + headingHeight - 20,
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
            paddingTop: headingPadding,
            height: initialHeight + headingHeight - 20,
        },
    },
    headingText: {
        position: "absolute",
        bottom: "10px",
        left: "10px",
        right: "10px",
        margin: 0,
        fontSize: "24px",
        fontWeight: "bold",
        borderTop: "1px solid #ffff",
        userSelect: 'none',

        '.notPlaying &': {
            borderTop: 0,
        },
    },
    listRoot: {
        '& > div': {
            borderBottom: '1px solid #333',
        },
        '& > div:last-child': {
            borderBottom: 0,
        },
    },
    sortableGhost: {
        opacity: 0.3,
    },
    sortableDrag: {
        backgroundColor: 'rgba(90, 90, 90, 0.5)',
        borderBottom: '0 !important',
    },
});

const getParallax = (parallaxOffset: number, headingOffset: number) => ({
    heading: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "height",
                    startValue: initialHeight + headingHeight + headingOffset,
                    endValue: miniHeight + headingHeight,
                    unit: "px",
                },
            ]
        },
    ],
    headingText: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "borderTopColor",
                    startValue: "rgba(255, 255, 255, 1)",
                    endValue: "rgba(0, 0, 0, 0)",
                },
            ]
        }
    ]
})

interface PropsFromState {
    queueConnectionStatus: ConnectionStatus;
    nowPlaying?: PlayingQueueEntry;
    upNext: QueueEntry[];
    queueTimestamp: number;
    locationKey?: string | undefined;
    scrollPosition: number;
    hasTrackBanner: boolean;

    makeupRef: React.MutableRefObject<HTMLDivElement>;
    listRef: React.MutableRefObject<ReactSortable<QueueItem>>
    scrolling: boolean;
    scrollTimer?: number;
    editLyricsEntry?: QueueEntry;
    setUserEntry?: QueueEntry;
    setScrolling: (scrolling: boolean) => void,
    setScrollTimer: (scrolling?: number) => void,
    setEditLyricsEntry: (entry?: QueueEntry) => void,
    setSetUserEntry: (entry?: QueueEntry) => void,

    upNextList: QueueItem[];
    setUpNextList: (upNextList: QueueItem[]) => void,
}

interface PropsFromDispatch {
    moveQueueEntry: typeof queueActions.move;
    setScrollPosition: typeof userActions.setScrollPosition;
}

type NowPlayingProps = PropsFromState & PropsFromDispatch;

type QueueItem = QueueEntry & ItemInterface;


class NowPlayingLayout extends React.Component<NowPlayingProps> {
    public componentDidMount() {
        const { scrollPosition } = this.props;

        window.scrollTo(0, scrollPosition);
        window.addEventListener("scroll", this.onScroll, true);
    }

    public componentWillUnmount() {
        const { setScrollPosition } = this.props;

        window.removeEventListener("scroll", this.onScroll, true);
        setScrollPosition("now-playing", window.scrollY);
    }

    public render = () => {
        const { makeupRef, listRef, queueConnectionStatus, nowPlaying, upNext, hasTrackBanner, upNextList, editLyricsEntry, setUserEntry } = this.props;
        const { classes } = (this.props as any);

        const containerClasses = classNames({
            [classes.container]: true,
            'notPlaying': nowPlaying === undefined,
        });
        const parallax = getParallax(hasTrackBanner ? -(headingPadding * 2) : 0,hasTrackBanner ? -(headingPadding * 2) : 0);

        const queueDisabled = (queueConnectionStatus !== ConnectionStatus.Connected);

        return (
            <div className={containerClasses}>
                {nowPlaying &&
                    <ErrorBoundary fallback={<Error />}>
                        <MiniPlayer entry={nowPlaying} />
                    </ErrorBoundary>
                }

                <ErrorBoundary fallback={<Error />}>
                    <Start />

                    <Plx className={classes.heading} parallaxData={nowPlaying === undefined ? [] : parallax.heading}>
                        <Plx className={classes.headingText} parallaxData={parallax.headingText}>Up Next</Plx>
                    </Plx>

                    <SwipeableList>
                        <ReactSortable
                            disabled={queueDisabled}
                            list={upNextList}
                            setList={this.setList}
                            delay={100}
                            ghostClass={classes.sortableGhost}
                            dragClass={classes.sortableDrag}
                            className={classes.listRoot}
                            ref={listRef}
                        >
                            {upNext.map(e => <Entry entry={e} key={e.queueId} editLyricsDialog={this.editLyricsDialog} setUserDialog={this.setUserDialog} />)}
                        </ReactSortable>
                    </SwipeableList>

                    <div ref={makeupRef} />
                </ErrorBoundary>

                {editLyricsEntry &&
                    <ErrorBoundary fallback={<Error />}>
                        <EditLyricsDialog entry={editLyricsEntry} onClose={this.closeEditLyricsDialog} />
                    </ErrorBoundary>
                }
                {setUserEntry &&
                    <ErrorBoundary fallback={<Error />}>
                        <SetUserDialog entry={setUserEntry} onClose={this.closeSetUserDialog} />
                    </ErrorBoundary>
                }
            </div>
        )
    }

    private setList = (list: QueueItem[]) => {
        const { setUpNextList, moveQueueEntry, upNextList } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'queue:setList'});
            s.setSpan(transaction);

            const currentList = [...upNextList];
            setUpNextList(list);

            try {
                const moves = list
                    .map((l, i) => ({
                        item: l,
                        from: currentList.findIndex(c => c.id === l.id),
                        to: i,
                    }))
                    .filter(i => i.from !== i.to)
                ;

                if (moves.length === 0) {
                    transaction.finish();
                    return;
                }

                let move: any;

                if (moves.length > 2 && Math.abs(moves[0].from - moves[0].to) === 1) {
                    move = moves[moves.length - 1];
                } else {
                    move = moves[0];
                }

                const maxIndex = Math.min(list.length, currentList.length) - 1;
                const movedItem: QueueItem = move.item;
                const prevItemId = move.to > 0 ? list[move.to - 1].queueId : undefined;
                const nextItemId = move.to < maxIndex ? list[move.to + 1].queueId : undefined;

                moveQueueEntry(movedItem, prevItemId, nextItemId);
            } catch (ex) {
                // Something went wrong, so we should reverse our state change...
                setUpNextList(currentList);
                Sentry.captureException(ex);
            }

            transaction.finish();
        });
    }

    private editLyricsDialog = (entry: QueueEntry) => {
        this.props.setEditLyricsEntry(entry);
    }

    private closeEditLyricsDialog = () => {
        this.props.setEditLyricsEntry(undefined);
    }

    private setUserDialog = (entry: QueueEntry) => {
        this.props.setSetUserEntry(entry);
    }

    private closeSetUserDialog = () => {
        this.props.setSetUserEntry(undefined);
    }

    private onScroll = () => {
        if (this.props.nowPlaying === undefined) return;

        if (this.props.scrollTimer) {
            window.clearTimeout(this.props.scrollTimer);
        }
        else {
            this.onScrollStart();
        }

        const scrollTimer = window.setTimeout(this.onScrollEnd, 66);
        this.props.setScrollTimer(scrollTimer);
    }

    private onScrollStart = () => {
        if (this.props.nowPlaying === undefined) return;

        this.props.setScrolling(true);
    }

    private onScrollEnd = () => {
        if (this.props.nowPlaying === undefined) return;

        const { hasTrackBanner } = this.props;

        // TODO: Listen for touchEnd event instead of using timeout
        // const scrollTimer = window.setTimeout(() => { this.setState({ scrolling: false, scrollTimer: undefined }); }, 150);
        // this.setState({ scrollTimer });
        this.props.setScrolling(false);
        this.props.setScrollTimer(undefined);

        const snapPoint = parallaxEnd + (hasTrackBanner ? -(headingPadding * 2) : 0);

        if (window.scrollY >= snapPoint) return;

        if (window.scrollY > snapPoint / 2) {
            // Scroll to mini
            animateScroll.scrollTo(snapPoint);
        }
        else {
            // Scroll to max (i.e. top)
            animateScroll.scrollToTop();
        }
    }
}

const getUpNextList = (upNext: QueueEntry[]) =>
    upNext.map(e => ({
        id: e.queueId,
        ...e,
    }));

const NowPlaying = compose(
    withStyles(styles),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withSelector('queueTimestamp', queueTimestampSelector),
    withSelector('nowPlaying', nowPlayingSelector),
    withSelector('upNext', upNextSelector),
    withSelector('scrollPosition', scrollPositionSelector('now-playing')),
    withSelector('hasTrackBanner', userHasTrackInQueueSelector),
    withAction('moveQueueEntry', queueActions.move),
    withAction('setScrollPosition', userActions.setScrollPosition),

    withState('scrolling', 'setScrolling', false),
    withState('scrollTimer', 'setScrollTimer', undefined),
    withState('editLyricsEntry', 'setEditLyricsEntry', undefined),
    withState('setUserEntry', 'setSetUserEntry', undefined),
    withState('upNextList', 'setUpNextList', ({ upNext }: NowPlayingProps) => getUpNextList(upNext)),
    withEffect(
        ({ upNext, setUpNextList }: NowPlayingProps) => setUpNextList(getUpNextList(upNext)),
        [ 'queueTimestamp' ],
    ),
    withRef('firstUpdate', true),
    withEffect(
        ({ firstUpdate }: any) => { if (firstUpdate.current) { firstUpdate.current = false; return; } animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
    withRef('makeupRef', undefined),
    withRef('listRef', undefined),
    withLayoutEffect(
        ({ makeupRef, listRef }: NowPlayingProps) => {
            if (!makeupRef.current || !listRef.current) return;

            const minHeight = window.innerHeight - bannerHeight - initialHeight - headingHeight - 50;
            const scrollHeight = window.innerHeight - bannerHeight - miniHeight - headingHeight - 50;
            const listHeight = (listRef.current as any).ref.current.clientHeight;
            const makeupHeight = listHeight > minHeight
                ? Math.max(scrollHeight, listHeight) - listHeight
                : 0;

            makeupRef.current.style.minHeight = `${makeupHeight}px`;
            makeupRef.current.style.height = `${makeupHeight}px`;
        },
    ),
    renderIf(({ nowPlaying, upNext, upNextList }) => (!nowPlaying && upNext.length < 1 || (upNext.length !== upNextList.length)), EmptyQueue)
)(NowPlayingLayout);

export { NowPlaying };