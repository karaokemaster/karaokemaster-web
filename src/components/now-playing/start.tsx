/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faPlay } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, createStyles, Theme } from '@mui/material';
import * as Sentry from "@sentry/browser";
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from "@truefit/bach-redux";
import * as React from 'react';

import { PlaybackState, playbackStateSelector, PlayerControl } from 'store/player';
import * as playerActions from 'store/player/actions';
import { nowPlayingSelector, upNextSelector } from 'store/queue';

// TODO: Consolidate w/ error, emptyQueue overlays
const styles = (theme: Theme) => createStyles({
    root: {
        fontSize: '2em',
        textAlign: 'center',
    },
});

interface PropsFromDispatch {
    sendPlayerControl: typeof playerActions.sendPlayerControl;
}

type StartProps = PropsFromDispatch;

class StartLayout extends React.Component<StartProps> {
    public render = () => {
        const { classes } = (this.props as any);

        return (
            <div className={classes.root}>
                <Button color="secondary" variant="contained" size="large" onClick={this.start}>
                    <FontAwesomeIcon icon={faPlay} />{" "}
                    Play
                </Button>
            </div>
        )
    }

    private start = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'start:play'});
            s.setSpan(transaction);

            transaction.setData("control", PlayerControl.Play);
            sendPlayerControl(PlayerControl.Play);

            transaction.finish();
        });
    }
}

const Start = compose(
    withStyles(styles),
    withSelector('nowPlaying', nowPlayingSelector),
    withSelector('upNext', upNextSelector),
    withSelector('playbackState', playbackStateSelector),
    withAction('sendPlayerControl', playerActions.sendPlayerControl),
    withProps({
        canStart: ({ nowPlaying, upNext, playbackState }) => !nowPlaying && upNext.length > 0 && playbackState === PlaybackState.Stopped,
    }),
    renderIf(({ canStart }) => !canStart, compose(renderNothing())(React.Component)),
)(StartLayout);

export { Start }
