/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCirclePause, faForwardFast, faForwardStep, faPause, faPlay, faRedo, faTrashCan } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Grid, IconButton, Theme } from '@mui/material';
import { grey } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as he from 'he';
import * as React from 'react';
import Plx from 'react-plx';

import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { Marquee } from 'components/marquee';
import { Thumb } from 'components/thumb';
import { Time } from 'components/time';
import { PlaybackState, playbackStateSelector, playerConnectionStatusSelector, PlayerControl } from 'store/player';
import * as playerActions from 'store/player/actions';
import { PlayingQueueEntry, queueConnectionStatusSelector, userHasTrackInQueueSelector } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus, hubConnectionStatusSelector } from 'store/signalR';
import { isCurrentUserSelector } from 'store/userProfile';
import { runtimeConfig } from '../../runtimeConfig';
import { notificationsEnabledSelector, notificationSubscriptionSelector } from '../../store/app';
import { ReactionsButton } from './reactions';

const styles = (theme: Theme) => createStyles({
    player: {
        position: 'fixed',
        top: 'env(safe-area-inset-top)',
        left: 0,
        right: 0,
        height: "370px",
        backgroundColor: "black",
        backdropFilter: 'blur(25px)',
        zIndex: 1100,
        padding: theme.spacing(),

        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
        },
    },
    heading: {
        position: "absolute",
        left: "10px",
        right: "10px",
        top: "25px",
        fontSize: "24px",
        fontWeight: "bold",
        margin: 0,

        // TODO: Calculate dimension
        '.track-banner &': {
            top: '5px',
        },

        // TODO: Calculate dimension
        '.notification-prompt &': {
            top: '5px',
        },
    },
    thumbnail: {
        position: "absolute",
        top: "80px",
        width: "180px",
        height: "180px",
        left: "50%",
        marginLeft: "-90px",
        '& img': {
            objectFit: "cover",
            width: "180px",
            height: "180px",
        },

        // TODO: Calculate dimension
        '.track-banner &': {
            top: '70px',
        },

        // TODO: Calculate dimension
        '.notification-prompt &': {
            top: '70px',
        },
    },
    trackInfo: {
        position: "absolute",
        left: "10px",
        top: "275px",
        right: "10px",

        // TODO: Calculate dimension
        '.track-banner &': {
            top: '255px',
        },

        // TODO: Calculate dimension
        '.notification-prompt &': {
            top: '255px',
        },
    },
    trackTitle: {
        fontWeight: "bold",
        height: "24px",
        overflowY: "hidden",
        textOverflow: "ellipsis",
        margin: 0,
    },
    trackUser: {
        color: grey[500],
        float: "left",
        margin: 0,
    },
    trackTime: {
        color: grey[500],
        float: "right",
        margin: 0,
    },
    playerControls: {
        position: "absolute",
        left: "10px",
        top: "325px",
        right: "10px",

        // TODO: Calculate dimension
        '.track-banner &': {
            top: '305px',
        },

        // TODO: Calculate dimension
        '.notification-prompt &': {
            top: '365px',
        },

        '& svg': {
            maxWidth: '32px',
            maxHeight: '32px',
        },
    },
    rated: {
        backgroundColor: '#fff !important',
        color: '#000',
    }
});

const initialHeight = 370;
const miniHeight = 93;
const parallaxEnd = initialHeight - miniHeight - 5;

const getParallax = (parallaxOffset: number, topOffset: number, playerOffset: number) => ({
    player: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "backgroundColor",
                    startValue: "rgba(0, 0, 0, 0)",
                    endValue: "rgba(33, 33, 33, 0.5)",
                },
                {
                    property: "top",
                    startValue: 0 + topOffset,
                    endValue: 10 + topOffset,
                    unit: "px",
                },
                {
                    property: "right",
                    startValue: 0,
                    endValue: 10,
                    unit: "px",
                },
                {
                    property: "left",
                    startValue: 0,
                    endValue: 10,
                    unit: "px",
                },
                {
                    property: "height",
                    startValue: initialHeight + playerOffset,
                    endValue: miniHeight,
                    unit: "px",
                },
                {
                    property: "borderRadius",
                    startValue: 0,
                    endValue: 5,
                    unit: "px",
                },
            ]
        }
    ],
    heading: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "opacity",
                    startValue: 1,
                    endValue: 0,
                },
                {
                    property: "fontSize",
                    startValue: 24,
                    endValue: 16,
                    unit: "px",
                },
                {
                    property: "top",
                    startValue: 25 + playerOffset,
                    endValue: 8,
                    unit: "px",
                },
            ]
        }
    ],
    thumbnail: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "scale",
                    startValue: 1,
                    endValue: 0.5,
                },
                {
                    property: "top",
                    startValue: 80 + playerOffset,
                    endValue: -35,
                    unit: "px",
                },
                {
                    property: "left",
                    startValue: 50,
                    endValue: 0,
                    unit: "%",
                },
                {
                    property: "marginLeft",
                    startValue: -90,
                    endValue: -35,
                    unit: "px",
                },
            ]
        }
    ],
    trackInfo: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "top",
                    startValue: 275 + playerOffset,
                    endValue: 5,
                    unit: "px",
                },
                {
                    property: "left",
                    startValue: 10,
                    endValue: 110,
                    unit: "px",
                },
                {
                    property: "fontSize",
                    startValue: 16,
                    endValue: 14,
                    unit: "px",
                },
                {
                    property: "height",
                    startValue: 24,
                    endValue: 20,
                    unit: "px",
                },
            ]
        }
    ],
    trackDetail: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "fontSize",
                    startValue: 16,
                    endValue: 14,
                    unit: "px",
                },
                {
                    property: "height",
                    startValue: 24,
                    endValue: 22,
                    unit: "px",
                },
            ]
        }
    ],
    playerControls: [
        {
            start: 0,
            end: parallaxEnd + parallaxOffset,
            properties: [
                {
                    property: "top",
                    startValue: 325 + playerOffset,
                    endValue: 55,
                    unit: "px",
                },
                {
                    property: "left",
                    startValue: 10,
                    endValue: 110,
                    unit: "px",
                },
            ]
        }
    ],
});

interface PropsFromState {
    hubConnectionStatus: ConnectionStatus,
    playerConnectionStatus: ConnectionStatus,
    queueConnectionStatus: ConnectionStatus,
    playbackState: PlaybackState,
    isCurrentUser: boolean,
    hasTrackBanner: boolean,
    hasNotificationPrompt: boolean,
    willStopAfter: boolean,

    animate: string | undefined;
    setAnimate: (animate: string | undefined) => void;
}

interface PropsFromDispatch {
    sendPlayerControl: typeof playerActions.sendPlayerControl;
    dequeue: typeof queueActions.dequeue;
    setRating: typeof queueActions.setRating;
}

type MiniPlayerProps = PropsFromState & PropsFromDispatch;

interface OtherProps {
    entry: PlayingQueueEntry;
}

class MiniPlayerLayout extends React.Component<MiniPlayerProps & OtherProps> {
    public render = () => {
        const { entry, playbackState, animate, hubConnectionStatus, playerConnectionStatus, queueConnectionStatus, hasTrackBanner, hasNotificationPrompt, isCurrentUser, willStopAfter } = this.props;
        const { classes } = (this.props as any);

        const notificationOffset = hasNotificationPrompt ? promptHeight : 0;
        const parallaxOffset = (hasTrackBanner ? bannerHeight : 0) - notificationOffset;
        const topOffset = getComputedStyle(document.documentElement).getPropertyValue("--sat") + bannerHeight;
        const playerOffset = (hasTrackBanner ? 20 : 0);
        const parallax = getParallax(-parallaxOffset, -topOffset, -playerOffset);

        const playing: boolean = playbackState === PlaybackState.Playing || playbackState === PlaybackState.StoppingAfter;
        const stopping: boolean = playbackState === PlaybackState.StoppingAfter;
        const playerDisabled = (playerConnectionStatus !== ConnectionStatus.Connected) || playbackState === PlaybackState.Disconnected;
        const ratingDisabled = (hubConnectionStatus !== ConnectionStatus.Connected || !isCurrentUser);
        const queueDisabled = (queueConnectionStatus !== ConnectionStatus.Connected);
        const reactionsVisible = playerConnectionStatus === ConnectionStatus.Connected && !isCurrentUser && playing;

        let playPauseIcon = faPlay;
        if (stopping)
            playPauseIcon = faCirclePause;
        else if (playing && willStopAfter)
            playPauseIcon = faForwardStep;
        else if (playing)
            playPauseIcon = faPause;

        return (
            <Plx className={classes.player} parallaxData={parallax.player}>
                <Plx className={classes.heading} parallaxData={parallax.heading}>Now Playing</Plx>
                <Plx className={classes.thumbnail} parallaxData={parallax.thumbnail}>
                    <img src={entry.thumbnailUrl} />
                </Plx>
                <Plx className={classes.trackInfo} parallaxData={parallax.trackInfo}>
                    <Plx className={classes.trackTitle} parallaxData={parallax.trackDetail}>
                        <Marquee text={he.decode(entry.title)} />
                    </Plx>
                    <Plx className={classes.trackUser} parallaxData={parallax.trackDetail}>{entry.user}</Plx>
                    <Plx className={classes.trackTime} parallaxData={parallax.trackDetail}>
                        <Time seconds={entry.videoTime} nullIfNegative={true} /> / <Time seconds={entry.videoDuration} nullIfNegative={true} />
                    </Plx>
                </Plx>
                <Plx className={classes.playerControls} parallaxData={parallax.playerControls}>
                    <Grid container={true} direction="row" justifyContent="space-around" alignItems="center">
                        <Grid item={true} hidden={!reactionsVisible}>
                            <ReactionsButton />
                        </Grid>
                        <Grid item={true} hidden={reactionsVisible}>
                            <IconButton
                                onClick={this.downRateTrack}
                                disabled={ratingDisabled}
                                className={entry.rating < 0 ? classes.rated : ''}
                                size="large"
                            >
                                <Thumb down={true} animate={(animate === 'down' && entry.rating < 0)} disabled={ratingDisabled} selected={entry.rating < 0} />
                            </IconButton>
                        </Grid>
                        <Grid item={true}>
                            {playing
                                ? <IconButton onClick={this.restartTrack} disabled={playerDisabled} size="large">
                                    <FontAwesomeIcon icon={faRedo} flip="horizontal"/>
                                </IconButton>
                                : <IconButton onClick={this.deleteTrack} disabled={queueDisabled} size="large">
                                    <FontAwesomeIcon icon={faTrashCan}/>
                                </IconButton>
                            }
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={this.playPause} disabled={playerDisabled} size="large">
                                <FontAwesomeIcon icon={playPauseIcon} />
                            </IconButton>
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={this.nextTrack} disabled={playerDisabled} size="large">
                                <FontAwesomeIcon icon={faForwardFast} />
                            </IconButton>
                        </Grid>
                        <Grid item={true} hidden={reactionsVisible}>
                            <IconButton
                                onClick={this.upRateTrack}
                                disabled={ratingDisabled}
                                className={entry.rating > 0 ? classes.rated : ''}
                                size="large">
                                <Thumb animate={(animate === 'up' && entry.rating > 0)} disabled={ratingDisabled} selected={entry.rating > 0} />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Plx>
            </Plx>
        );
    }

    private playPause = () => {
        const { playbackState, sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:playPause'});
            s.setSpan(transaction);

            const control = playbackState === PlaybackState.Playing ? PlayerControl.Pause : PlayerControl.Play;
            transaction.setData("control", control);
            sendPlayerControl(control);

            transaction.finish();
        });
    }

    private nextTrack = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:nextTrack'});
            s.setSpan(transaction);

            sendPlayerControl(PlayerControl.Next);

            transaction.finish();
        });
    }

    private restartTrack = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:restartTrack'});
            s.setSpan(transaction);

            sendPlayerControl(PlayerControl.Back);

            transaction.finish();
        });
    }

    private deleteTrack = () => {
        const { entry, dequeue } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:deleteTrack'});
            s.setSpan(transaction);

            dequeue(entry);

            transaction.finish();
        });
    }

    private downRateTrack = () => {
        const { entry, setRating, setAnimate, playerConnectionStatus } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:downRateTrack'});
            s.setSpan(transaction);

            // Main hub
            setAnimate('down');
            setRating(entry, entry.rating === -1 ? 0 : -1);

            if (entry.rating !== -1 && playerConnectionStatus === ConnectionStatus.Connected)
                // Player hub
                this.nextTrack();

            transaction.finish();
        });
    }

    private upRateTrack = () => {
        const { entry, setRating, setAnimate } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:upRateTrack'});
            s.setSpan(transaction);

            setAnimate('up');
            setRating(entry, entry.rating === 1 ? 0 : 1);

            transaction.finish();
        });
    }
}

const MiniPlayer = compose<OtherProps>(
    withStyles(styles),
    withSelector('hubConnectionStatus', hubConnectionStatusSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withSelector('playbackState', playbackStateSelector),
    withSelector('hasTrackBanner', userHasTrackInQueueSelector),
    withSelector('notificationsEnabled', notificationsEnabledSelector),
    withSelector('notificationSubscription', notificationSubscriptionSelector),
    withSelector('isCurrentUser', (state, { entry: { user }}: OtherProps) => (isCurrentUserSelector(user)(state))),
    withProps({
        stopAfterPercent: () => runtimeConfig().client.player.stopAfterPercent,
        willStopAfter: ({ stopAfterPercent, entry: { videoTime, videoDuration }}) => videoTime / videoDuration > stopAfterPercent,
        hasNotificationPrompt: ({ notificationsEnabled, notificationSubscription }) => (notificationsEnabled === undefined || (notificationsEnabled && notificationSubscription === undefined)),
    }),
    withAction('sendPlayerControl', playerActions.sendPlayerControl),
    withAction('dequeue', queueActions.dequeue),
    withAction('setRating', queueActions.setRating),
    withState('animate', 'setAnimate', undefined),
    // withEffect(
    //     ({ setAnimate }: MiniPlayerProps & OtherProps) => {
    //         setAnimate(undefined);
    //     },
    //     [ 'entry' ]
    // ),
)(MiniPlayerLayout);

export { MiniPlayer, initialHeight, miniHeight, parallaxEnd };