/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faFaceSadTear } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { createStyles, Theme, Typography } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { Overlay } from 'components/overlay';

// TODO: Consolidate w/ error, emptyQueue overlays
const styles = (theme: Theme) => createStyles({
    icon: {
        color: theme.palette.primary.main,
        padding: '1.5rem',
    },
    caption: {
        fontSize: '0.9em',
    },
});

class EmptyQueueLayout extends React.Component {
    public render = () => {
        const { classes } = (this.props as any);

        return (
            <Overlay>
                <>
                    <FontAwesomeIcon icon={faFaceSadTear} size="5x" className={classes.icon} />
                    <Typography variant="h6" className={classes.caption}>
                        Nothing in the queue yet; add some tracks and get rocking!
                    </Typography>
                </>
            </Overlay>
        )
    }
}

const EmptyQueue = compose(
    withStyles(styles),
)(EmptyQueueLayout);

export { EmptyQueue }
