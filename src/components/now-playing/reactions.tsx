/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Avatar, Box, createStyles, SpeedDial, SpeedDialAction, SpeedDialIcon, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withAction } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';

import { FloatingItem, FloatTrack } from 'components/floating-emoji';
import * as React from 'react';
import * as playerActions from 'store/player/actions';
import { initialHeight } from './miniPlayer';

const floatSeconds: number = 3;

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'relative',
        display: 'inline',
    },
    track: {
        bottom: '40px',
        left: '-7px',
    },
    reaction: {
        backgroundColor: 'transparent',
    },
});

interface PropsFromState {
    items: JSX.Element[];
    setItems: (items: JSX.Element[]) => void;
}

interface PropsFromDispatch {
    sendReaction: typeof playerActions.sendReaction;
}

type ReactionsProps = PropsFromState & PropsFromDispatch;

class ReactionsLayout extends React.Component<ReactionsProps>{
    public render = () => {
        const { items} = this.props;
        const { classes } = this.props as any;

        // const disabled = playerConnectionStatus !== ConnectionStatus.Connected;
        const reactions = ['🥰', '👏', '❤️‍🔥'];
        return (
            <Box className={classes.root}>
                <SpeedDial
                    ariaLabel="Reactions"
                    icon={<SpeedDialIcon icon={'🥰'} />}
                    direction="right"
                    sx={{
                        position: 'relative',
                        '& .MuiSpeedDial-fab': {
                            backgroundColor: '#000',
                        },
                        '& .MuiSpeedDial-actions': {
                            position: 'absolute',
                            bottom: '3em',
                            left: '-1em',
                        }
                    }}
                >
                    {reactions.map((rxn) => (
                        <SpeedDialAction
                            key={rxn}
                            onClick={this.sendReaction(rxn)}
                            icon={rxn}
                        />
                    ))}
                </SpeedDial>
                <FloatTrack width={120} height={initialHeight - 80} className={classes.track}>
                    {items}
                </FloatTrack>
            </Box>
        );
    }

    private sendReaction = (reaction: string) => () => {
        const { sendReaction, items, setItems } = this.props;
        const { classes } = this.props as any;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'reactionsButton:sendReaction'});
            s.setSpan(transaction);

            const key = Date.now();
            const newItems = [
                ...items,
                <FloatingItem width={20} key={key} duration={floatSeconds}><Avatar className={classes.reaction}>{reaction}</Avatar></FloatingItem>,
            ];

            sendReaction(reaction);
            setItems(newItems);
            setTimeout(this.removeItem, floatSeconds * 1000);

            transaction.finish();
        });
    }

    private removeItem = () => {
        const { items, setItems } = this.props;

        const newItems = items.length > 1 ? items.slice(1) : [];

        setItems(newItems);
    }
}

export const ReactionsButton = compose(
    withStyles(styles),
    withAction('sendReaction', playerActions.sendReaction),
    withState('items', 'setItems', []),
)(ReactionsLayout);