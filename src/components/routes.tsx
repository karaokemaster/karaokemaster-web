/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import * as React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import { Layout } from 'components/layout';
import { Login } from 'components/login';
import { NowPlaying } from 'components/now-playing';
import { Recommendations } from 'components/recommendations';
import { Search } from 'components/search';
import { UserProfile } from 'components/user-profile';

import { KioskLayout } from 'components/kiosk/layout'
import { Player } from 'components/kiosk/player';
import { PlayerInfo } from 'components/kiosk/player-info';
import { Queue } from 'components/kiosk/queue';

import { EmceeLayout } from 'components/emcee/layout';

// If your app is big + you have routes with a lot of components, you should consider
// code-splitting your routes! If you bundle stuff up with Webpack, I recommend `react-loadable`.
//
// $ yarn add react-loadable
// $ yarn add --dev @types/react-loadable
//
// The given `pages/` directory provides an example of a directory structure that's easily
// code-splittable.

const NotFound: React.FunctionComponent = () => (
    <div>Not Found</div>
)

export const AppRoutes: React.FunctionComponent = () => (
    <Routes>
        <Route path="login" element={<Login />} />
        <Route path="/*" element={<Layout />}>
            <Route path="search" element={<Search />} />
            <Route path="recommendations" element={<Recommendations />} />
            <Route path="now-playing" element={<NowPlaying />} />
            <Route path="user-profile/*" element={<UserProfile />} />
            <Route index element={<Navigate replace to="recommendations" />} />
            <Route path="*" element={<NotFound />} />
        </Route>
        <Route path="kiosk" element={<KioskLayout />}>
            <Route path="queue" element={<Queue />} />
            <Route path="player" element={<Player />} />
            <Route path="player-info" element={<PlayerInfo />} />
        </Route>
        <Route path="emcee/*" element={<EmceeLayout />} />
    </Routes>
)
