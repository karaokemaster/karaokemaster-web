/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { createStyles, List, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { Loading } from 'components/loading';
import { searchResultsLoadingSelector } from 'store/search';
import { Video } from 'store/videos';
import { SearchResult } from './searchResult';

const styles = (theme: Theme) => createStyles({
    root: {
        '& > div': {
            borderBottom: '1px solid #333',
        },
        '& > div:last-child': {
            borderBottom: 0,
        },
    },
})

interface OtherProps {
    results: Video[];
}

class ResultsLayout extends React.Component<OtherProps> {
    public render() {
        const { results } = this.props;
        const { classes } = (this.props as any);

        return (
            <List className={classes.root}>
                {results.map(r => <SearchResult video={r} key={r.videoId} />)}
            </List>
        );
    }
}

const Results = compose<OtherProps>(
    withStyles(styles),
    withSelector('searchLoading', searchResultsLoadingSelector),
    renderIf(({ searchLoading }) => searchLoading, Loading),
    renderIf(({ results }) => results?.length < 1, compose(renderNothing())(React.Component)),
)(ResultsLayout);

export { Results }
