/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faPlus } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Avatar, CircularProgress, createStyles, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, Theme } from '@mui/material';
import { blue, green } from '@mui/material/colors'
import Fab from '@mui/material/Fab'
import * as Sentry from '@sentry/browser';
import { compose, withEffect, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as he from 'he';
import * as React from 'react';

import { Checkmark } from 'components/checkmark';
import { RatingBadge } from "components/search/ratingBadge";
import { hasTrackBeenPlayedSelector, isTrackInQueueSelector, isTrackPlayingSelector } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus, hubConnectionStatusSelector } from 'store/signalR';
import { Video } from 'store/videos';
import { SearchResultChips } from './searchResultChips';

const styles = (theme: Theme) => createStyles({
    root: {
        width: '100%',
        userSelect: 'none',
        '-webkit-touch-callout': 'none',
        flexGrow: 1,
    },
    button: {
        '&:hover': {
            backgroundColor: blue[700],
        },
        backgroundColor: blue[500],
        color: "#fff",
        padding: 4,
    },
    buttonSuccess: {
        '&:hover': {
            backgroundColor: green[700],
        },
        backgroundColor: green[500],
        color: "#fff",
        padding: 4,
    },
    fabProgress: {
        color: green[500],
        left: -3,
        position: 'absolute',
        top: -3,
        zIndex: 1,
    },
    wrapper: {
        display: 'inline-block',
        margin: theme.spacing(),
        position: 'relative',
    },
});

interface PropsFromState {
    connectionStatus: ConnectionStatus,
    isInQueue: boolean,
    playing: boolean,
    hasBeenPlayed: boolean,

    waiting: boolean;
    animate: boolean;
    setWaiting: (waiting: boolean) => void;
    setAnimate: (animate: boolean) => void;
}

interface PropsFromDispatch {
    enqueue: typeof queueActions.enqueue;
}

interface OtherProps {
    video: Video;
    hideIfPlayed?: boolean;
}

type SearchResultProps = PropsFromState & PropsFromDispatch;

class SearchResultLayout extends React.Component<SearchResultProps & OtherProps> {
    public render = () => {
        const { isInQueue, playing, waiting, animate, connectionStatus, video, video: { thumbnailUrl, title, rating } } = this.props;
        const { classes } = (this.props as any);

        const success = (isInQueue || playing);
        const disabled = (connectionStatus !== ConnectionStatus.Connected);

        // TODO: Use chips for the secondary
        return (
            <ListItem
                className={isInQueue ? "in-queue" : ""}
                ContainerComponent="div"
                ContainerProps={{ className: classes.root }}
            >
                <ListItemAvatar>
                    <RatingBadge rating={rating}>
                        <Avatar src={thumbnailUrl} variant={"square"} />
                    </RatingBadge>
                </ListItemAvatar>
                <ListItemText disableTypography={true} primary={<div>{he.decode(title)}</div>} secondary={<SearchResultChips video={video} />} />
                <ListItemSecondaryAction>
                    <div className={classes.wrapper}>
                        {success
                            ? <Checkmark animate={animate} />
                            : <Fab size="small" color="primary" className={classes.button} onClick={this.addToQueue} disabled={disabled}>
                                <FontAwesomeIcon icon={faPlus}/>
                            </Fab>
                        }
                        {waiting && <CircularProgress size={46} className={classes.fabProgress} />}
                    </div>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }

    private addToQueue = async () => {
        const { enqueue, video, setWaiting } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({ name: 'searchResult:addToQueue' });
            s.setSpan(transaction);

            setWaiting(true);
            enqueue(video);

            transaction.finish();
        })
    }
}

const SearchResult = compose<OtherProps>(
    withStyles(styles),
    withSelector('connectionStatus', hubConnectionStatusSelector),
    withSelector('playing', (state, { video }: OtherProps) => isTrackPlayingSelector(state, video.videoId)),
    withSelector('isInQueue', (state, { video }: OtherProps) => isTrackInQueueSelector(state, video.videoId)),
    withSelector('hasBeenPlayed', (state, { video: { videoId } }: OtherProps) => hasTrackBeenPlayedSelector(state, videoId)),
    withAction('enqueue', queueActions.enqueue),
    withState('waiting', 'setWaiting', false),
    withState('animate', 'setAnimate', false),
    withEffect(({ waiting, playing, isInQueue, setWaiting, setAnimate }: SearchResultProps) => {
            if (!waiting || !(isInQueue || playing)) return;

            setWaiting(false);
            setAnimate(true);
        },
        [ 'isInQueue', 'playing' ]),
    renderIf(({ hasBeenPlayed, hideIfPlayed }) => hasBeenPlayed && hideIfPlayed, compose(renderNothing())(React.Component)),
)(SearchResultLayout);

export { SearchResult }
