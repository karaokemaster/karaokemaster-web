/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faThumbsDown, faThumbsUp } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Badge, createStyles, Theme } from '@mui/material';
import { green, red } from '@mui/material/colors';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({
    badge: {
        height: '26px',
        maxWidth: '26px',
        bottom: '4px',
        fontSize: '0.85em',
        borderRadius: '13px',
        backgroundColor: '#fff',
        color: '#000',
        border: '1.5px solid #000',
    },
    colorPrimary: {
        backgroundColor: green[500],
        color: '#fff',
    },
    colorSecondary: {
        backgroundColor: red[500],
        color: '#fff',
    },
});

interface OtherProps {
    rating?: number;
    children: JSX.Element;
}

type RatingBadgeProps = OtherProps;

class RatingBadgeLayout extends React.Component<RatingBadgeProps> {
    public render = () => {
        const { rating: ratingValue, children } = this.props;
        const { classes } = (this.props as any);
        const rating = ratingValue || 0;

        const color = rating > 0
            ? "primary"
            : (rating < 0 ? "secondary" : undefined);

        let ratingBadge = <></>;
        if (rating > 0)
            ratingBadge = <FontAwesomeIcon icon={faThumbsUp}/>;
        else if (rating < 0)
            ratingBadge = <FontAwesomeIcon icon={faThumbsDown}/>;

        return (
            <Badge
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                }}
                classes={classes}
                badgeContent={ratingBadge}
                invisible={rating === 0}
                color={color}
            >
                {children}
            </Badge>
        );
    }
}

const RatingBadge = compose<OtherProps>(
    withStyles(styles),
)(RatingBadgeLayout);

export { RatingBadge };