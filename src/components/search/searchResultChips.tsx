/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faClockRotateLeft, faRectangleList } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Chip } from '@mui/material';
import { compose } from '@truefit/bach';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { hasTrackBeenPlayedSelector, isTrackInQueueSelector, isTrackPlayingSelector } from 'store/queue';
import { Video } from 'store/videos';

interface OtherProps {
    video: Video;
}

interface ComposedChipsProps {
    playing: boolean,
    isInQueue: boolean;
    hasBeenPlayed: boolean;
}

class SearchResultChipsLayout extends React.Component<OtherProps & ComposedChipsProps> {
    public render = () => {
        const { isInQueue, playing, hasBeenPlayed, video: { hasLyrics }} = this.props;

        return (
            <React.Fragment>
                {playing && <Chip label="Playing" color="primary" />}
                {isInQueue && !playing && <Chip label="In Queue" variant="outlined" />}
                {hasBeenPlayed && <Chip label={<FontAwesomeIcon icon={faClockRotateLeft} />} color="default" />}
                {hasLyrics && <Chip label={<FontAwesomeIcon icon={faRectangleList} />} color="primary" />}
            </React.Fragment>
        )
    }
}

const SearchResultChips = compose<OtherProps>(
    withSelector('playing', (state, { video: { videoId } }: OtherProps) => isTrackPlayingSelector(state, videoId)),
    withSelector('isInQueue', (state, { video: { videoId } }: OtherProps) => isTrackInQueueSelector(state, videoId)),
    withSelector('hasBeenPlayed', (state, { video: { videoId } }: OtherProps) => hasTrackBeenPlayedSelector(state, videoId))
)(SearchResultChipsLayout);

export { SearchResultChips }