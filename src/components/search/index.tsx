/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faCircleXmark } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { createStyles, FormControl, IconButton, Input, InputAdornment, Theme, Typography } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withRef } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';
import { animateScroll } from 'react-scroll';

import { Error } from 'components/error';
import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { autocompleteResultsSelector, searchActions, searchResultsSelector } from 'store/search';
import { scrollPositionSelector } from 'store/user';
import * as userActions from 'store/user/actions';
import { Video } from 'store/videos';
import { AutocompleteResults } from './autocompleteResults';
import { Results } from './results';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: "36px",
        paddingBottom: "56px",
    },
    searchBar: {
        position: "fixed",
        top: 0,
        paddingTop: 'env(safe-area-inset-top)',
        zIndex: 2000,
        backgroundColor: 'rgba(25,25,25,.25)',
        backdropFilter: 'blur(25px)',

        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
            paddingTop: 0,
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
            paddingTop: 0,
        },
    },
    searchInput: {
        '& input': {
            userSelect: 'auto',
        },
        padding: 5,
    },
});

interface PropsFromState {
    results: Video[];
    autocomplete: Video[];
    locationKey?: string | undefined;
    scrollPosition: number;
}

interface PropsFromDispatch {
    fetchRequest: typeof searchActions.fetchRequest;
    autocompleteQuery: typeof searchActions.autocompleteQuery;
    autocompleteClear: typeof searchActions.autocompleteClear;
    setScrollPosition: typeof userActions.setScrollPosition;
}

interface ComposedProps {
    searchRef: React.MutableRefObject<HTMLInputElement>
}

type SearchProps = PropsFromState & PropsFromDispatch & ComposedProps;

class SearchLayout extends React.Component<SearchProps> {
    public componentDidMount = () => {
        const { scrollPosition, searchRef } = this.props;

        window.scrollTo(0, scrollPosition);

        searchRef.current?.focus();
    }

    public componentWillUnmount() {
        const { setScrollPosition } = this.props;

        setScrollPosition("search", window.scrollY);
    }

    public render = () => {
        const { results, autocomplete, searchRef } = this.props;
        const { classes } = (this.props as any);

        return (
            <Typography component="div" className={classes.root}>
                <ErrorBoundary fallback={<Error />}>
                    <form action="#">
                        <FormControl
                            variant="standard"
                            fullWidth={true}
                            margin="none"
                            className={classes.searchBar}>
                            <Input
                                type="text"
                                placeholder="Search"
                                inputRef={searchRef}
                                onKeyPress={this.searchKeyPress}
                                onChange={this.searchChange}
                                className={classes.searchInput}
                                color="secondary"
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton onClick={this.clearSearch} size="large">
                                            <FontAwesomeIcon icon={faCircleXmark} />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </form>
                    <AutocompleteResults results={autocomplete} />
                    <Results results={results} />
                </ErrorBoundary>
            </Typography>
        );
    }

    private searchKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        // e.keyCode doesn't work in keyPress
        if (e.which !== 13) { return; }

        e.preventDefault();

        if (!this.props.searchRef.current) { return }

        const query = this.props.searchRef.current.value;

        this.search(query);
    }

    private searchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.autocomplete();
    }

    private clearSearch = () => {
        const { searchRef, autocompleteClear } = this.props;

        if (searchRef.current)
            searchRef.current.value = "";
        searchRef.current?.focus();

        autocompleteClear();
    }

    private search = async (query: string) => {
        const { fetchRequest, searchRef } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'search:go'});
            s.setSpan(transaction);

            searchRef.current?.blur();

            fetchRequest(query);

            transaction.finish();
        });
    }

    private autocomplete = async () => {
        const { autocompleteQuery, searchRef } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'search:autocomplete'});
            s.setSpan(transaction);

            const query = searchRef.current.value;

            autocompleteQuery(query);

            transaction.finish();
        });
    }
}

const Search = compose(
    withStyles(styles),
    withRef('searchRef', null),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withSelector('results', searchResultsSelector),
    withSelector('autocomplete', autocompleteResultsSelector),
    withSelector('scrollPosition', scrollPositionSelector('search-tab')),
    withAction('fetchRequest', searchActions.fetchRequest),
    withAction('autocompleteQuery', searchActions.autocompleteQuery),
    withAction('autocompleteClear', searchActions.autocompleteClear),
    withAction('setScrollPosition', userActions.setScrollPosition),
    withRef('firstUpdate', true),
    withEffect(
        ({ firstUpdate }: any) => { if (firstUpdate.current) { firstUpdate.current = false; return; } animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
    withEffect(
        (props: SearchProps) => {
            props.searchRef.current?.focus();
            props.searchRef.current?.setSelectionRange(0, props.searchRef.current.value.length);
        },
        [ 'locationKey' ],
    ),
)(SearchLayout);

export { Search };
