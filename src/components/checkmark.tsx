/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Theme } from '@mui/material';
import { createStyles, withStyles } from '@mui/styles';
import classNames from 'classnames';
import * as React from 'react';

const green = '#388e3c';
const white = '#fff';
const curve = 'cubic-bezier(0.650, 0.000, 0.450, 1.000)';

const styles = (theme: Theme) => createStyles({
    root: {
        width: '40px',
        height: '40px',
        borderRadius: '50%',
        display: 'block',
        strokeWidth: 2,
        stroke: white,
        strokeMiterlimit: 10,
        boxShadow: `inset 0px 0px 0px 30px ${green}`,
    },
    animate: {
        boxShadow: `inset 0px 0px 0px ${green}`,
        animation: '$fill .4s ease-in-out .4s forwards, $scale .3s ease-in-out .9s both',
    },

    circle: {
        strokeDasharray: 166,
        strokeDashoffset: 0,
        strokeWidth: 2,
        strokeMiterlimit: 10,
        stroke: green,
        fill: 'none',

        '$animate &': {
            strokeDashoffset: 166,
            animation: `$stroke .6s ${curve} forwards`,
        }
    },

    check: {
        transformOrigin: '50% 50%',
        strokeDasharray: 48,
        strokeDashoffset: 0,

        '$animate &': {
            strokeDashoffset: 48,
            animation: `$stroke .3s ${curve} .8s forwards`,
        },
    },

    '@keyframes stroke': {
        '100%': {
            strokeDashoffset: 0,
        },
    },

    '@keyframes scale': {
        '0%, 100%': {
            transform: 'none',
        },
        '50%': {
            transform: 'scale3d(1.1, 1.1, 1)',
        },
    },

    '@keyframes fill': {
        '100%': {
            boxShadow: `inset 0px 0px 0px 30px ${green}`,
        },
    },
});

interface OtherProps {
    animate?: boolean;
    className?: string;
}

type CheckmarkProps = OtherProps;

class CheckmarkLayout extends React.Component<CheckmarkProps> {
    public render = () => {
        const { animate, className } = this.props;
        const { classes } = this.props as any;

        const checkClasses = classNames({
            [classes.root]: true,
            [classes.animate]: animate,
        }, className);

        return (
            <svg className={checkClasses} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                <circle className={classes.circle} cx="26" cy="26" r="25" fill="none"/>
                <path className={classes.check} fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
            </svg>
        );
    }
}

const Checkmark = withStyles(styles)(CheckmarkLayout);

export { Checkmark };