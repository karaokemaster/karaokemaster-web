/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCowbellCirclePlus } from '@fortawesome/pro-duotone-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, createStyles, Theme, Typography } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withAction } from '@truefit/bach-redux';
import * as React from 'react';

import { Overlay } from 'components/overlay';
import { defaultRecommendationsFilter } from 'store/recommendations';
import * as userProfileActions from 'store/userProfile/actions';

const styles = (theme: Theme) => createStyles({
    icon: {
        padding: '1.25rem',
    },
    caption: {
        fontSize: '0.9em',
        marginBottom: '1em',
    },
});

interface PropsFromDispatch {
    setFilter: typeof userProfileActions.setRecommendationsFilter;
}

type EmptyFilterProps = PropsFromDispatch;

interface OtherProps {
    show: boolean;
}

class EmptyFilterLayout extends React.Component<EmptyFilterProps & OtherProps> {
    public render = () => {
        const { classes } = (this.props as any);

        return (
            <Overlay>
                <>
                    <FontAwesomeIcon icon={faCowbellCirclePlus} size="5x" className={classes.icon} />
                    <Typography variant="h6" className={classes.caption}>
                        Just one recommendation:<br/>more cowbell!
                    </Typography>
                    <Button variant="contained" color="primary" onClick={this.resetFilter}>Reset filter</Button>
                </>
            </Overlay>
        )
    }

    private resetFilter = () => {
        const { setFilter } = this.props;

        window.setTimeout(() => {
            setFilter(defaultRecommendationsFilter);
        }, 100);
    }
}

const EmptyFilter = compose<OtherProps>(
    withStyles(styles),
    withAction('setFilter', userProfileActions.setRecommendationsFilter),
    renderIf(({ show }) => !show, compose(renderNothing())(React.Component)),
)(EmptyFilterLayout);

export { EmptyFilter }
