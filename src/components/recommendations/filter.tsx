/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faSquare, faSquareCheck } from '@fortawesome/pro-light-svg-icons'
import { faFilter } from '@fortawesome/pro-light-svg-icons';
import { faFilter as faFilterSolid } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Checkbox, createStyles, Divider, ListItemIcon, Menu, MenuItem, Theme, Typography } from '@mui/material';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from "@truefit/bach-recompose";
import { withAction, withSelector } from '@truefit/bach-redux';
import isEqual from "lodash.isequal";
import * as React from 'react';

import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { defaultRecommendationsFilter, RecommendationsFilter } from 'store/recommendations';
import { recommendationsFilterSelector } from 'store/userProfile';
import * as userProfileActions from 'store/userProfile/actions';

const styles = (theme: Theme) => createStyles({
    filter: {
        position: 'fixed',
        zIndex: 1200,
        top: 'env(safe-area-inset-top)',
        right: 'env(safe-area-inset-right)',
        width: '48px',
        height: '48px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
        },
    },
    filterButton: {
        padding: 0,
        width: '75%',
        height: '75%',
        minWidth: 'unset',
    },
    filterMenu: {
        paddingTop: 0,
        paddingBottom: 0,
    },
    filterItem: {
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: theme.spacing(1),
    },
    filterCheck: {
        paddingLeft: 0,
        minWidth: 'unset',
    },
});

interface PropsFromState {
    filter: RecommendationsFilter;
    hasFilter: boolean;

    menuAnchor: HTMLElement | null;
    setMenuAnchor: (menuAnchor: HTMLElement | null) => void;
}

interface PropsFromDispatch {
    setFilter: typeof userProfileActions.setRecommendationsFilter;
}

type FilterProps = PropsFromState & PropsFromDispatch;

class FilterLayout extends React.Component<FilterProps> {
    public render = () => {
        const { menuAnchor, filter, hasFilter } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.filter}>
                <Button onClick={this.toggleMenu} className={classes.filterButton}>
                    <FontAwesomeIcon icon={(hasFilter ? faFilterSolid : faFilter)} />
                </Button>
                <Menu
                    anchorEl={menuAnchor}
                    getContentAnchorEl={null}
                    open={Boolean(menuAnchor)}
                    onClose={this.menuClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    MenuListProps={{ className: classes.filterMenu }}
                >
                    <MenuItem onClick={this.toggleFilterRecommendations} className={classes.filterItem}>
                        <ListItemIcon className={classes.filterCheck}>
                            <Checkbox checked={filter.recommendations} icon={<FontAwesomeIcon icon={faSquare} />} checkedIcon={<FontAwesomeIcon icon={faSquareCheck} />} />
                        </ListItemIcon>
                        <Typography variant="inherit">Recommendations</Typography>
                    </MenuItem>
                    <MenuItem onClick={this.toggleFilterPreviouslyPlayed} className={classes.filterItem}>
                        <ListItemIcon className={classes.filterCheck}>
                            <Checkbox checked={filter.previouslyPlayed} icon={<FontAwesomeIcon icon={faSquare} />} checkedIcon={<FontAwesomeIcon icon={faSquareCheck} />} />
                        </ListItemIcon>
                        <Typography variant="inherit">Previously Played</Typography>
                    </MenuItem>
                    <MenuItem onClick={this.toggleFilterTopVideos} className={classes.filterItem}>
                        <ListItemIcon className={classes.filterCheck}>
                            <Checkbox checked={filter.topVideos} icon={<FontAwesomeIcon icon={faSquare} />} checkedIcon={<FontAwesomeIcon icon={faSquareCheck} />} />
                        </ListItemIcon>
                        <Typography variant="inherit">Top Videos</Typography>
                    </MenuItem>
                    <Divider />
                    <MenuItem onClick={this.toggleFilterHidePlayed} className={classes.filterItem}>
                        <ListItemIcon className={classes.filterCheck}>
                            <Checkbox checked={filter.hidePlayed} icon={<FontAwesomeIcon icon={faSquare} />} checkedIcon={<FontAwesomeIcon icon={faSquareCheck} />} />
                        </ListItemIcon>
                        <Typography variant="inherit">Hide Played</Typography>
                    </MenuItem>
                </Menu>
            </div>
        );
    }

    private toggleMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        const { setMenuAnchor } = this.props;

        setMenuAnchor(event.currentTarget);
    }

    private toggleFilterRecommendations = () => {
        const { filter, setFilter } = this.props;

        setFilter({ ...filter, recommendations: !filter.recommendations });
    }

    private toggleFilterPreviouslyPlayed = () => {
        const { filter, setFilter } = this.props;

        setFilter({ ...filter, previouslyPlayed: !filter.previouslyPlayed });
    }

    private toggleFilterTopVideos = () => {
        const { filter, setFilter } = this.props;

        setFilter({ ...filter, topVideos: !filter.topVideos });
    }

    private toggleFilterHidePlayed = () => {
        const { filter, setFilter } = this.props;

        setFilter({ ...filter, hidePlayed: !filter.hidePlayed });
    }

    private menuClose = () => {
        const { setMenuAnchor } = this.props;

        setMenuAnchor(null);
    }
}

const Filter = compose(
    withStyles(styles),
    withSelector('filter', recommendationsFilterSelector),
    withProps({
        hasFilter: ({ filter }) => !isEqual(filter, defaultRecommendationsFilter),
    }),
    withAction('setFilter', userProfileActions.setRecommendationsFilter),
    withState('menuAnchor', 'setMenuAnchor', null),
)(FilterLayout);

export { Filter };
