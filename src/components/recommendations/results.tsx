/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { createStyles, ListSubheader, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { Loading } from 'components/loading';
import { SearchResult } from 'components/search/searchResult';
import { Video } from 'store/videos';
import { recommendationsFilterSelector } from 'store/userProfile';

const styles = (theme: Theme) => createStyles({
    headerSticky: {
        top: 'env(safe-area-inset-top)',
        backgroundColor: 'rgba(25,25,25,.25)',
        backdropFilter: 'blur(25px)',
        zIndex: 1100,

        '&:first-child': {
            top: 0,
            paddingTop: 'env(safe-area-inset-top)',
            marginTop: 'calc(0px - env(safe-area-inset-top))',
        },

        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
            paddingTop: 0,
            marginTop: 0,
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
            paddingTop: 0,
            marginTop: 0,
        },
    },
});

interface PropsFromState {
    hidePlayed: boolean;
}

type ResultsProps = PropsFromState;

interface OtherProps {
    title: React.ReactElement | string;
    results: Video[];
    loading?: boolean;
    show: boolean;
}

class ResultsLayout extends React.Component<ResultsProps & OtherProps> {
    public render() {
        const { title, results, hidePlayed } = this.props;
        const { classes } = (this.props as any);

        return (
            <>
                <ListSubheader className={classes.headerSticky}>{title}</ListSubheader>
                {results.map(r => <SearchResult video={r} key={r.videoId} hideIfPlayed={hidePlayed} />)}
            </>
        );
    }
}

const Results = compose<OtherProps>(
    withStyles(styles),
    withSelector('filter', recommendationsFilterSelector),
    withProps({
        hidePlayed: ({ filter }) => filter.hidePlayed,
    }),
    renderIf(({ loading }) => loading === true, Loading),
    renderIf(({ results, show }) => !show || results?.length < 1, compose(renderNothing())(React.Component)),
)(ResultsLayout);

export { Results }
