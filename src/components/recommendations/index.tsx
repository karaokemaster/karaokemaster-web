/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { createStyles, List, Theme, Typography } from '@mui/material';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withRef, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';
import { animateScroll } from 'react-scroll';

import { Error } from 'components/error';
import {
    previouslyPlayedSelector,
    recommendationsActions,
    RecommendationsFilter,
    recommendationsLoadingSelector,
    recommendationsSelector,
    topVideosSelector,
} from 'store/recommendations';
import { scrollPositionSelector } from 'store/user';
import * as userActions from 'store/user/actions';
import { recommendationsFilterSelector } from 'store/userProfile';
import { Video } from 'store/videos';
import { EmptyFilter } from './emptyFilter';
import { Filter } from './filter';
import { Results } from './results';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 0,
        paddingBottom: '56px',

        '& > div': {
            borderBottom: '1px solid #333',
        },
        '& > div:last-child': {
            borderBottom: 0,
        },
    },
});

interface PropsFromState {
    recommendations: Video[];
    previouslyPlayed: Video[];
    topVideos: Video[];
    loading: boolean;
    locationKey?: string | undefined;
    scrollPosition: number;
    filter: RecommendationsFilter;

    menuAnchor: HTMLElement | null;
    setMenuAnchor: (menuAnchor: HTMLElement | null) => void;
}

interface PropsFromDispatch {
    fetchRecommendations: typeof recommendationsActions.fetchRequest;
    fetchTopVideos: typeof recommendationsActions.fetchTopVideosRequest;
    setScrollPosition: typeof userActions.setScrollPosition;
}

type RecommendationsProps = PropsFromState & PropsFromDispatch;

class RecommendationsLayout extends React.Component<RecommendationsProps> {
    public componentDidMount = () => {
        const { fetchRecommendations, fetchTopVideos, scrollPosition, topVideos } = this.props;

        window.scrollTo(0, scrollPosition);

        // TODO: Decide if this is even necessary...
        // TODO: Maybe pull-to-refresh instead?
        fetchRecommendations();
        if (topVideos.length < 1)
            fetchTopVideos();
    }

    public componentWillUnmount() {
        const { setScrollPosition } = this.props;

        setScrollPosition("recommendations-tab", window.scrollY);
    }

    public componentDidUpdate = (prevProps: Readonly<RecommendationsProps>, prevState: Readonly<{}>, snapshot?: any) => {
        if (this.props.locationKey === prevProps.locationKey) { return; }
        animateScroll.scrollToTop();
    }

    public render = () => {
        const { loading, recommendations, previouslyPlayed, topVideos, filter } = this.props;
        const { classes } = (this.props as any);

        // TODO: Different (or no) loading indicator...
        // TODO: Style filter menu
        const emptyFilter = !filter.recommendations && !filter.previouslyPlayed && !filter.topVideos;

        return (
            <ErrorBoundary fallback={<Error />}>
                <Typography component="div">
                    <Filter />
                    <EmptyFilter show={emptyFilter} />
                    <List className={classes.root}>
                        <Results title="Recommendations" results={recommendations} loading={loading} show={filter.recommendations} />
                        <Results title="Previously Played" results={previouslyPlayed} show={filter.previouslyPlayed} />
                        <Results title="Top Videos" results={topVideos} show={filter.topVideos} />
                    </List>
                </Typography>
            </ErrorBoundary>
        );
    }
}

const Recommendations = compose(
    withStyles(styles),
    withLocation(),
    withSelector('loading', recommendationsLoadingSelector),
    withSelector('recommendations', recommendationsSelector),
    withSelector('previouslyPlayed', previouslyPlayedSelector),
    withSelector('topVideos', topVideosSelector),
    withSelector('scrollPosition', scrollPositionSelector('recommendations-tab')),
    withSelector('filter', recommendationsFilterSelector),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withAction('fetchRecommendations', recommendationsActions.fetchRequest),
    withAction('fetchTopVideos', recommendationsActions.fetchTopVideosRequest),
    withAction('setScrollPosition', userActions.setScrollPosition),
    withRef('firstUpdate', true),
    withState('menuAnchor', 'setMenuAnchor', null),
    withEffect(
        ({ firstUpdate }: any) => { if (firstUpdate.current) { firstUpdate.current = false; return; } animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
)(RecommendationsLayout);

export { Recommendations };
