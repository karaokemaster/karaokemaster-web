/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Box, Theme } from '@mui/material';
import { createStyles, styled } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withStyles } from 'enhancers/bach-mui';
import React from 'react';

export const floatSeconds: number = 10;

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'absolute',
        pointerEvents: 'none',
        width: '40px',
        height: '40px',
        bottom: '-100px',
        animation: `$floating ${floatSeconds}s linear`,
    },

    '@keyframes floating': {
        '0%': {
            bottom: '-40px',
            opacity: 1,
        },
        '60%': {
            bottom: '75%',
            opacity: 1,
        },
        '100%': {
            bottom: '95%',
            opacity: 0,
        },
    },
});

interface ItemProps {
    width: number
    duration?: number
    children: JSX.Element
}

class FloatingItemLayout extends React.Component<ItemProps>{
    private readonly xPos: number;

    constructor(props: any) {
        super(props);

        this.xPos = Math.floor(Math.random() * this.props.width);
    }

    public render = () => {
        const { children, duration } = this.props;
        const { classes } = this.props as any;

        const left = `${this.xPos}px`;
        const animationDuration = duration ? `${duration}s` : `${floatSeconds}s`;

        return (
            <Box className={classes.root} style={{ left, animationDuration }}>
                {children}
            </Box>
        )
    }
}

export const FloatingItem = compose<ItemProps>(
    withStyles(styles),
)(FloatingItemLayout);

interface TrackProps {
    width: number
    height: number | string
    className?: string
}

const TrackRoot = styled('div')(({theme}) => ({
    position: 'absolute',
    right: 0,
    bottom: 0,
    zIndex: 100,
    pointerEvents: 'none',
}));

export const FloatTrack = compose<TrackProps>(
    withProps({
        style: ({ width, height }) => ({ width, height }),
    }),
)(TrackRoot);

