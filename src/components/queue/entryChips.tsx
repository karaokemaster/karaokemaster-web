/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faListOl, faRectangleList } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Chip } from '@mui/material';
import { compose } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import { Lrc } from 'lrc-kit';
import * as React from 'react';

import { isTrackPlayingSelector, QueueEntry } from 'store/queue';

interface OtherProps {
    entry: QueueEntry;
}

interface ComposedChipsProps {
    playing: boolean,
    hasLyrics: boolean,
    hasLrc: boolean,
}

class EntryChipsLayout extends React.Component<ComposedChipsProps & OtherProps> {
    public render = () => {
        const { entry: { user, userIcon, userColor }, playing, hasLyrics, hasLrc } = this.props;

        const icon = findIconDefinition(JSON.parse(userIcon || ""));

        return (
            <React.Fragment>
                {playing ? <Chip label="Playing" color="primary" /> : null}
                <Chip icon={<FontAwesomeIcon icon={icon} fixedWidth={true} />} label={user} variant="outlined" style={{ backgroundColor: userColor }} />
                {hasLrc ? <Chip label={<FontAwesomeIcon icon={faListOl} />} color="secondary" /> : null}
                {hasLyrics && !hasLrc ? <Chip label={<FontAwesomeIcon icon={faRectangleList} />} color="secondary" /> : null}
            </React.Fragment>
        )
    }
}

const EntryChips = compose<OtherProps>(
    withSelector('playing', (state, { entry }: OtherProps) => isTrackPlayingSelector(state, entry.videoId)),
    withProps({
        hasLyrics: ({ entry: { lyrics }}) => !(lyrics === undefined || lyrics === null || lyrics.trim().length === 0),
        hasLrc: ({ entry: { lyrics }}) => !(lyrics === undefined || lyrics === null || Lrc.parse(lyrics).lyrics.length < 1),
    }),
)(EntryChipsLayout);

export { EntryChips }