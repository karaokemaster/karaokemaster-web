/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Autocomplete, Button, createFilterOptions, createStyles, Dialog, DialogActions, DialogContent, DialogTitle, List, ListItem, ListItemText, TextField, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';

import { QueueEntry } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { activeUsersSelector, allUsersSelector, User, usersLoadingSelector } from 'store/user';
import * as userActions from 'store/user/actions';

const styles = (theme: Theme) => createStyles({
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    user: {

    },
    selected: {
        backgroundColor: theme.palette.secondary.main,
        '&:hover': {
            backgroundColor: theme.palette.secondary.main,
        }
    },
});

interface PropsFromState {
    activeUsers: User[];
    allUsers: User[];
    users: UserInputValue[];
    loading: boolean;

    user?: UserInputValue;
    setUser: (user: UserInputValue | null) => void;
}

interface PropsFromDispatch {
    setEntryUser: typeof queueActions.setEntryUser;
    fetchActiveUsersRequest: typeof userActions.fetchActiveUsersRequest;
}

interface OtherProps {
    entry: QueueEntry;
    onClose: () => void;
}

type SetUserProps = PropsFromState & PropsFromDispatch;

interface UserInputValue {
    userId?: number;
    username: string;
    title: string;
}

class SetUserLayout extends React.Component<SetUserProps & OtherProps> {
    public componentDidMount = () => {
        const { fetchActiveUsersRequest } = this.props;

        fetchActiveUsersRequest();
    }

    public render = () => {
        const { entry, activeUsers, users, user } = this.props;
        const { classes } = (this.props as any);

        return (
            <Dialog open={true} scroll="paper" onClose={this.handleClose} aria-labelledby="setUser-title" className={classes.modal}>
                <DialogTitle id="setUser-title">Set User</DialogTitle>
                <DialogContent>
                    <p id="setUser-description">Setting user for {entry.title}</p>
                    <Autocomplete
                        value={user}
                        onChange={this.autocompleteOnChange}
                        filterOptions={this.autocompleteFilterOptions}
                        selectOnFocus
                        clearOnBlur
                        handleHomeEndKeys
                        id="find-or-add-user"
                        options={users}
                        getOptionLabel={this.autocompleteOptionLabel}
                        renderOption={(option) => option && option.title}
                        style={{ width: 300 }}
                        freeSolo
                        renderInput={(params) => <TextField {...params} label="Find or add a user..." variant="outlined" color="secondary" />}
                    />
                    <List className={classNames(classes.user)}>
                        {activeUsers.map(u =>
                            <ListItem button key={u.userId} className={classNames({ [classes.user]: true, [classes.selected]: user && (u.userId === user.userId) })}>
                                <ListItemText primary={u.username} onClick={() => this.selectUser(u)} />
                            </ListItem>
                        )}
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button onClick={this.setUser}>
                        Set User
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    private handleClose = () => {
        const { onClose } = this.props;

        onClose();
    }

    private setUser = async () => {
        const { entry, onClose, user, setEntryUser } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'queue:setUserBegin'});
            s.setSpan(transaction);

            if (user?.username)
                setEntryUser(entry, user.username.trim());

            transaction.finish();
        });

        onClose();
    }

    private selectUser = async (user: User) => {
        const value = { ...user, title: user.username };

        this.props.setUser(value);
    }

    private autocompleteOnChange = async (event: any, newValue: UserInputValue) => {
        let value = newValue;

        if (typeof newValue === 'string') {
            value = { username: newValue, title: newValue };
        }

        this.props.setUser(value);
    }

    private autocompleteFilterOptions = (options: UserInputValue[], params: any) => {
        const filtered = createFilterOptions<UserInputValue>()(options, params);

        // Suggest the creation of a new value
        if (params.inputValue !== '') {
            filtered.push({
                username: params.inputValue,
                title: `Add "${params.inputValue}"`,
            });
        }

        return filtered;
    }

    private autocompleteOptionLabel = (option: UserInputValue) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
            return option;
        }
        // Regular option
        return option.username;
    }
}

const SetUserDialog = compose<OtherProps>(
    withStyles(styles),
    withSelector('loading', usersLoadingSelector),
    withSelector('allUsers', allUsersSelector),
    withSelector('activeUsers', activeUsersSelector),
    withProps({
        users: ({ allUsers }: SetUserProps) => allUsers.map(u => ({ userId: u.userId, username: u.username, title: u.username }))
    }),
    withState<SetUserProps & OtherProps, UserInputValue | undefined>('user', 'setUser', ({ entry, users }) => users.find(u => u.username === entry.user)),
    withAction('setEntryUser', queueActions.setEntryUser),
    withAction('fetchActiveUsersRequest', userActions.fetchActiveUsersRequest),
)(SetUserLayout);

export { SetUserDialog }
