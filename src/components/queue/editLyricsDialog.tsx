/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Button, createStyles, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction } from '@truefit/bach-redux';
import * as React from 'react';

import { QueueEntry } from 'store/queue';
import * as queueActions from 'store/queue/actions';

const styles = (theme: Theme) => createStyles({
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
});

interface PropsFromDispatch {
    setEntryLyrics: typeof queueActions.setEntryLyrics;

    lyrics?: string;
    setLyrics: (lyrics?: string) => void;
}

interface OtherProps {
    entry: QueueEntry;
    onClose: () => void;
}

type EditLyricsProps = PropsFromDispatch;

class EditLyricsLayout extends React.Component<EditLyricsProps & OtherProps> {
    public render = () => {
        const { entry } = this.props;
        const { classes } = (this.props as any);

        return (
            <Dialog open={true} scroll="paper" onClose={this.handleClose} aria-labelledby="editLyrics-title" className={classes.modal}>
                <DialogTitle id="editLyrics-title">Edit Lyrics</DialogTitle>
                <DialogContent>
                    <p id="editLyrics-description">Edit lyrics for {entry.title}</p>
                    <TextField
                        variant="standard"
                        fullWidth={true}
                        multiline={true}
                        rows={5}
                        maxRows={10}
                        color="secondary"
                        defaultValue={entry.lyrics}
                        onChange={this.onLyricsChange} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button onClick={this.setLyrics}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onLyricsChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const lyrics = e.target.value;

        this.props.setLyrics(lyrics);
    }

    private handleClose = () => {
        const { onClose } = this.props;

        onClose();
    }

    private setLyrics = async () => {
        const { entry, onClose, lyrics, setEntryLyrics } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'queue:setLyrics'});
            s.setSpan(transaction);

            setEntryLyrics(entry, lyrics || "");

            transaction.finish();
        });

        onClose();
    }
}

const EditLyricsDialog = compose<OtherProps>(
    withStyles(styles),
    withAction('setEntryLyrics', queueActions.setEntryLyrics),
    withState<EditLyricsProps & OtherProps, string | undefined>('lyrics', 'setLyrics', (props) => props.entry.lyrics),
)(EditLyricsLayout);

export { EditLyricsDialog }
