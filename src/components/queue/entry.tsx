/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faPeopleArrowsLeftRight, faRectangleList, faTrash } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Avatar, Box, createStyles, ListItem, ListItemAvatar, ListItemText, Theme, Typography } from '@mui/material';
import { blue, orange, red } from '@mui/material/colors';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as he from 'he';
import * as React from 'react';
import { LeadingActions, SwipeableListItem, SwipeAction, TrailingActions, Type as ListType } from 'react-swipeable-list';

import { RatingBadge } from 'components/search/ratingBadge';
import { queueConnectionStatusSelector, QueueEntry } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus } from 'store/signalR';
import { EntryChips } from './entryChips';

const styles = (theme: Theme) => createStyles({
    root: {
        userSelect: 'none',
        '-webkit-touch-callout': 'none',
    },
    action: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        userSelect: "none",
        padding: "8px",
        fontSize: "10px",
        fontWeight: 500,
        boxSizing: "border-box",
        color: "#eee",
        whiteSpace: "nowrap",
    },
    actionIcon: {
        display: "block",
        fontSize: "2em",
    },
    deleteAction: {
        backgroundColor: red[500],
    },
    editLyricsAction: {
        backgroundColor: blue[500],
    },
    setUserAction: {
        backgroundColor: orange[500],
    },
});

interface PropsFromState {
    queueConnectionStatus: ConnectionStatus;
}

interface PropsFromDispatch {
    dequeue: typeof queueActions.dequeue;
}

interface OtherProps {
    entry: QueueEntry;
    editLyricsDialog: (entry: QueueEntry) => void;
    setUserDialog: (entry: QueueEntry) => void;
}

type EntryProps = PropsFromState & PropsFromDispatch;

class EntryLayout extends React.Component<EntryProps & OtherProps> {
    public render = () => {
        const { entry, queueConnectionStatus } = this.props;
        const { classes } = (this.props as any);
        const { rating } = entry;

        const queueDisabled = (queueConnectionStatus !== ConnectionStatus.Connected);

        const leadingActions = (
            <LeadingActions>
                <SwipeAction onClick={this.editLyrics}>
                    <Box className={classNames([classes.action, classes.editLyricsAction])}>
                        <FontAwesomeIcon icon={faRectangleList} className={classes.actionIcon} />
                        <Typography>Edit Lyrics</Typography>
                    </Box>
                </SwipeAction>
                <SwipeAction onClick={this.setUser}>
                    <Box className={classNames([classes.action, classes.setUserAction])}>
                        <FontAwesomeIcon icon={faPeopleArrowsLeftRight} className={classes.actionIcon} />
                        <Typography>Set User</Typography>
                    </Box>
                </SwipeAction>
            </LeadingActions>
        );

        const trailingActions = (
            <TrailingActions>
                <SwipeAction
                    destructive={true}
                    onClick={this.removeFromQueue}
                >
                    <Box className={classNames([classes.action, classes.deleteAction])}>
                        <FontAwesomeIcon icon={faTrash} className={classes.actionIcon} />
                        <Typography>Delete</Typography>
                    </Box>
                </SwipeAction>
            </TrailingActions>
        );

        // TODO: Use chips for the secondary
        return (
            <SwipeableListItem
                leadingActions={leadingActions}
                trailingActions={trailingActions}
                listType={ListType.IOS}
                fullSwipe={true}
                blockSwipe={queueDisabled}
            >
                <ListItem
                    className={classes.root}
                >
                    <ListItemAvatar>
                        <RatingBadge rating={rating}>
                            <Avatar src={entry.thumbnailUrl} variant={"square"} />
                        </RatingBadge>
                    </ListItemAvatar>
                    <ListItemText
                        disableTypography={true}
                        primary={<div>{he.decode(entry.title)}</div>}
                        secondary={<EntryChips entry={entry} />}
                    />
                </ListItem>
            </SwipeableListItem>
        );
    }

    private removeFromQueue = async () => {
        const { dequeue, entry } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'queue:remove'});
            s.setSpan(transaction);

            dequeue(entry);

            transaction.finish();
        });
    }

    private editLyrics = async () => {
        const { entry, editLyricsDialog } = this.props;

        editLyricsDialog(entry);
    }

    private setUser = async () => {
        const { entry, setUserDialog } = this.props;

        setUserDialog(entry);
    }
}

const Entry = compose<OtherProps>(
    withStyles(styles),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withAction('dequeue', queueActions.dequeue),
)(EntryLayout);

export { Entry }