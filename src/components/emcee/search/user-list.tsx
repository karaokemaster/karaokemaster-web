/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Autocomplete, createFilterOptions, List, TextField, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withState } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { usersSelector } from 'store/emcee';
import { User } from 'store/user';
import { UserListItem } from './user-list-item';

const styles = (theme: Theme) => createStyles({
    root: {
    },
    searchBar: {
        // position: "fixed",
        // top: 0,
        // paddingTop: 0,
        // zIndex: 2000,
        // backgroundColor: 'rgba(25,25,25,.25)',
        // backdropFilter: 'blur(25px)',
    },
    searchInput: {
        '& input': {
            userSelect: 'auto',
        },
    },
    results: {
        position: 'absolute',
        top: theme.spacing(10),
        bottom: 0,
        left: theme.spacing(1.5),
        right: theme.spacing(1.5),
        overflowY: 'auto',
        paddingBottom: '1.5em',

        '&::-webkit-scrollbar': {
            width: '0 !important',
        },
    },
});

const newUser: User = {
    username: '',
    icon: '{"prefix":"fad","iconName":"face-smile-plus"}',
    color: '#888888',
    userId: -1,
    isActive: false,
};

interface PropsFromState {
    activeUsers: UserInputValue[];
    inactiveUsers: UserInputValue[];
    users: UserInputValue[];
    loading: boolean;

    user?: UserInputValue;
    selectUser: (user: UserInputValue | null) => void;
}

type UserListProps = PropsFromState;

interface OtherProps {
    setUser: (user: UserInputValue | null) => void;
    disableAdd?: boolean;
}

interface UserInputValueBase {
    userId?: number;
    username: string;
    title: string;
}
type UserInputValue = UserInputValueBase & User;

class UserListLayout extends React.Component<UserListProps & OtherProps> {
    public render = () => {
        const { activeUsers, inactiveUsers, users, user, disableAdd } = this.props;
        const { classes } = (this.props as any);

        const isNewUser = user && !users.some(u => u.username === user.username);
        const inputLabel = !disableAdd ? "Find or add a user..." : "Find a user...";

        // TODO: Add user icon
        return (
            <Typography component="div" className={classes.root}>
                <Autocomplete
                    className={classes.searchBar}
                    value={user}
                    onChange={this.autocompleteUserOnChange}
                    filterOptions={this.autocompleteUserFilterOptions}
                    selectOnFocus
                    clearOnBlur
                    handleHomeEndKeys
                    id="find-or-add-user"
                    options={users}
                    getOptionLabel={this.autocompleteUserOptionLabel}
                    renderOption={(option) => option && option.title}
                    freeSolo
                    fullWidth
                    renderInput={(params) => <TextField {...params} label={inputLabel} variant="outlined" color="secondary" />}
                />
                <List className={classNames(classes.user, classes.results)}>
                    {isNewUser && <UserListItem key="new" user={user} selected={true} inactive={true} onClick={this.selectUser} />}
                    {activeUsers.map(u =>
                        <UserListItem key={u.userId} user={u} selected={!!(user && (u.userId === user.userId))} inactive={false} onClick={this.selectUser} />
                    )}
                    {inactiveUsers.map(u =>
                        <UserListItem key={u.userId} user={u} selected={!!(user && (u.userId === user.userId))} inactive={true} onClick={this.selectUser} />
                    )}
                </List>
            </Typography>
        );
    }

    private selectUser = async (user: UserInputValue) => {
        const value = { ...user, title: user.username };

        this.props.selectUser(value);
        this.props.setUser(value);
    }

    private autocompleteUserOnChange = async (event: any, newValue: UserInputValue) => {
        const { disableAdd } = this.props;

        let value = newValue;

        if (typeof newValue === 'string') {
            if (disableAdd) return;

            const { users } = this.props;
            const user = users.find(u => u.userId === newValue) || newUser;
            value = { ...user, username: newValue, title: newValue };
        }

        this.props.selectUser(value);
        this.props.setUser(value);
    }

    private autocompleteUserFilterOptions = (options: UserInputValue[], params: any) => {
        const { disableAdd } = this.props;

        const filtered = createFilterOptions<UserInputValue>()(options, params);

        // Suggest the creation of a new value
        if (!disableAdd && params.inputValue !== '') {
            filtered.push({
                ...newUser,
                username: params.inputValue,
                title: `Add "${params.inputValue}"`,
            });
        }

        return filtered;
    }

    private autocompleteUserOptionLabel = (option: UserInputValue) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
            return option;
        }
        // Regular option
        return option.username;
    }
}

const UserList = compose<OtherProps>(
    withStyles(styles),
    withSelector('users', usersSelector),
    withProps({
        users: ({ users }: UserListProps) => users.map(u => ({ ...u, title: u.username })),
        activeUsers: ({ users }: UserListProps) => users.filter(u => u.isActive),
        inactiveUsers: ({ users }: UserListProps) => users.filter(u => !u.isActive),
    }),
    withState<UserListProps, UserInputValue | null>('user', 'selectUser', null),
)(UserListLayout);

export { UserList, UserInputValue };
