/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Avatar, ListItem, ListItemIcon, ListItemText, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { UserInputValue } from './user-list';

const styles = (theme: Theme) => createStyles({
    root: {
        borderRadius: '0.25em',
    },
    icon: {
        color: theme.palette.text.primary,
        borderColor: '#00000066',
        borderWidth: '2px',
        borderStyle: 'solid',
        width: 48,
        height: 48,
    },
    inactive: {
        color: theme.palette.text.disabled,

        '& $icon': {
            opacity: 0.5,
        },

        '&$selected': {
            color: theme.palette.text.primary,

            '& $icon': {
                opacity: 1,
            },
        }
    },
    selected: {
        backgroundColor: theme.palette.secondary.main,
        '&:hover': {
            backgroundColor: theme.palette.secondary.main,
        }
    },
});

interface OtherProps {
    user: UserInputValue;
    selected: boolean;
    inactive: boolean;

    onClick: (user: UserInputValue | null) => void;
}

class UserListItemLayout extends React.Component<OtherProps> {
    public render = () => {
        const { user, selected, inactive } = this.props;
        const { classes } = (this.props as any);

        const icon = findIconDefinition(JSON.parse(user.icon || ""));

        // TODO: Add user icon
        return (
            <ListItem button className={classNames({ [classes.root]: true, [classes.selected]: selected, [classes.inactive]: inactive })} onClick={this.selectUser}>
                <ListItemIcon>
                    <Avatar className={classNames(classes.icon)} style={{ backgroundColor: user.color }}>
                        <FontAwesomeIcon icon={icon} fixedWidth={true} className={classes.userIcon} />
                    </Avatar>
                </ListItemIcon>
                <ListItemText primary={user.username} />
            </ListItem>
        );
    }

    private selectUser = () => {
        const { user, onClick } = this.props;

        onClick(user);
    }
}

const UserListItem = compose<OtherProps>(
    withStyles(styles),
)(UserListItemLayout);

export { UserListItem };
