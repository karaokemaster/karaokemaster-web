/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faCircleXmark } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { FormControl, Grid, IconButton, InputAdornment, OutlinedInput, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withRef, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';
import { animateScroll } from 'react-scroll';

import { Error } from 'components/error';
import { recommendationsActions, topVideosSelector } from 'store/recommendations';
import { autocompleteResultsSelector, searchActions, searchResultsSelector } from 'store/search';
import { Video } from 'store/videos';
import { AutocompleteResults } from './autocompleteResults';
import { Results } from './results';
import { UserInputValue, UserList } from './user-list';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: '120px', // TODO: Source this value from layout/tabs
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(3),
        overflow: 'hidden',
    },
    main: {
        minHeight: '100%'
    },
    container: {
        position: 'relative',
    },
    searchBar: {
        // position: "fixed",
        // top: 0,
        // paddingTop: 0,
        // zIndex: 2000,
        // backgroundColor: 'rgba(25,25,25,.25)',
        // backdropFilter: 'blur(25px)',
    },
    searchInput: {
        '& input': {
            userSelect: 'auto',
        },
    },
    results: {
        position: 'absolute',
        top: theme.spacing(10),
        bottom: 0,
        left: 0,
        right: 0,
        overflowY: 'auto',
        paddingBottom: '1.5em',

        '&::-webkit-scrollbar': {
            width: '0 !important',
        },
    },
});

interface PropsFromState {
    user?: UserInputValue;
    setUser: (user: UserInputValue | null) => void;

    results: Video[];
    autocomplete: Video[];
    topVideos: Video[];
    locationKey?: string | undefined;
}

interface PropsFromDispatch {
    fetchRequest: typeof searchActions.fetchRequest;
    fetchTopVideos: typeof recommendationsActions.fetchTopVideosRequest;
    autocompleteQuery: typeof searchActions.autocompleteQuery;
    autocompleteClear: typeof searchActions.autocompleteClear;
}

interface ComposedProps {
    searchRef: React.MutableRefObject<HTMLInputElement>
}

type SearchProps = PropsFromState & PropsFromDispatch & ComposedProps;

class SearchLayout extends React.Component<SearchProps> {
    public componentDidMount = () => {
        const { searchRef, topVideos, fetchTopVideos } = this.props;

        searchRef.current?.focus();

        if (topVideos.length < 1)
            fetchTopVideos();
    }

    public render = () => {
        const { user, results, topVideos, autocomplete, searchRef } = this.props;
        const { classes } = (this.props as any);

        // TODO: Refactor into separate components
        return (
            <Typography component="div" className={classes.root}>
                <ErrorBoundary fallback={<Error />}>
                    <Grid container={true} direction="row" spacing={3} className={classes.main}>
                        <Grid item={true} xs={3} className={classes.container}>
                            <UserList setUser={this.selectUser} />
                        </Grid>
                        <Grid item={true} xs={9} className={classes.container}>
                            <form action="#">
                                <FormControl
                                    variant="standard"
                                    fullWidth={true}
                                    margin="none"
                                    className={classes.searchBar}>
                                    <OutlinedInput
                                        type="text"
                                        placeholder="Search"
                                        inputRef={searchRef}
                                        onKeyPress={this.searchKeyPress}
                                        onChange={this.searchChange}
                                        className={classes.searchInput}
                                        color="secondary"
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton onClick={this.clearSearch} size="large">
                                                    <FontAwesomeIcon icon={faCircleXmark} />
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                            </form>
                            <div className={classNames(classes.results)}>
                                {
                                    autocomplete.length === 0 && results.length === 0
                                        ? <Results results={topVideos} user={user?.username} />
                                        : <>
                                            <AutocompleteResults results={autocomplete} user={user?.username} />
                                            <Results results={results} user={user?.username} />
                                        </>
                                }
                            </div>
                        </Grid>
                    </Grid>
                </ErrorBoundary>
            </Typography>
        );
    }

    private selectUser = (user: UserInputValue) => {
        const { searchRef, setUser } = this.props;

        setUser(user);
        searchRef.current?.focus();
    }

    private searchKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        // e.keyCode doesn't work in keyPress
        if (e.which !== 13) { return; }

        e.preventDefault();

        if (!this.props.searchRef.current) { return }

        const query = this.props.searchRef.current.value;

        this.search(query);
    }

    private searchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.autocomplete();
    }

    private clearSearch = () => {
        const { searchRef, autocompleteClear } = this.props;

        if (searchRef.current)
            searchRef.current.value = "";
        searchRef.current?.focus();

        autocompleteClear();
    }

    private search = async (query: string) => {
        const { fetchRequest, searchRef } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:search:go'});
            s.setSpan(transaction);

            searchRef.current?.blur();

            fetchRequest(query);

            transaction.finish();
        });
    }

    private autocomplete = async () => {
        const { autocompleteQuery, searchRef } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:search:autocomplete'});
            s.setSpan(transaction);

            const query = searchRef.current.value;

            autocompleteQuery(query);

            transaction.finish();
        });
    }
}

const Search = compose(
    withStyles(styles),
    withRef('searchRef', null),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withSelector('results', searchResultsSelector),
    withSelector('autocomplete', autocompleteResultsSelector),
    withSelector('topVideos', topVideosSelector),
    withState<SearchProps, UserInputValue | null>('user', 'setUser', null),
    withAction('fetchRequest', searchActions.fetchRequest),
    withAction('fetchTopVideos', recommendationsActions.fetchTopVideosRequest),
    withAction('autocompleteQuery', searchActions.autocompleteQuery),
    withAction('autocompleteClear', searchActions.autocompleteClear),
    withRef('firstUpdate', true),
    withEffect(
        ({ firstUpdate }: any) => { if (firstUpdate.current) { firstUpdate.current = false; return; } animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
    withEffect(
        (props: SearchProps) => {
            props.searchRef.current?.focus();
            props.searchRef.current?.setSelectionRange(0, props.searchRef.current.value.length);
        },
        [ 'locationKey' ],
    ),
)(SearchLayout);

export { Search };
