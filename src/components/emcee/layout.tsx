/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { adaptV4Theme, createTheme, CssBaseline, StyledEngineProvider, Theme, ThemeProvider, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect } from '@truefit/bach';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom';

import { NavTabs } from 'components/emcee/tabs';
import { Error } from 'components/error';

import { ConnectionStatusIcon } from 'components/connectionStatusIcon';
import gitVersion from 'gitversion.json';
import { apiVersionSelector, connectAsEmcee, disconnect } from 'store/app';
import { emceeConnectionStatusSelector, loginEmcee } from 'store/emcee';
import { playerConnectionStatusSelector } from 'store/player';
import { queueConnectionStatusSelector } from 'store/queue';
import { ConnectionStatus } from 'store/signalR';
import { tokenSelector } from 'store/userProfile';
import { Codes } from './codes';
import { History } from './history';
import { Lighting } from './lighting';
import { Microphones } from './microphones';
import { Queue } from './queue';
import { Search } from './search';
import { Users } from './users';

import 'src/styles/player.css';


declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}


const theme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#c91ee2',
        },
        secondary: {
            main: '#4caf50',
        },
        background: {
            default: '#000',
        },
    },
    typography: {
        fontSize: 16,
    },
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                '@global': {
                    body: {
                        '&::-webkit-scrollbar': {
                            width: '0 !important',
                        },
                    },
                },
            },
        },
        MuiListItemAvatar: {
            styleOverrides: {
                root: {
                    minWidth: 115
                },
            },
        },
        MuiAvatar: {
            styleOverrides: {
                root: {
                    width: 100,
                    height: 100,
                },
            },
        },
        MuiTab: {
            styleOverrides: {
                root: {
                    '&.Mui-selected': {
                        color: '#fff',
                    },
                },
            },
        },
    }
});

const styles = (theme: Theme) => createStyles({
    root: {
        userSelect: 'none',
    },
    content: {
        paddingLeft: '120px',
    },
    version: {
        zIndex: 1600,
        position: 'fixed',
        bottom: 0,
        right: 0,
        padding: '5px',
        color: '#000',
        fontSize: '1rem',
        opacity: 0.5,
    },
    blur: {
        zIndex: 1500,
        position: 'fixed',
        bottom: 0,
        right: 0,
        left: '100px',
        height: '2em',
        backgroundColor: 'rgba(25,25,25,.25)',
        background: 'linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(25,25,25,0) 100%)',
        backdropFilter: 'blur(25px)',
    },
});

// TODO: Move version/status display to discrete component
interface PropsFromState {
    apiVersion: string,
    playerConnectionStatus: ConnectionStatus,
    queueConnectionStatus: ConnectionStatus,
    emceeConnectionStatus: ConnectionStatus,
}

type EmceeLayoutProps = PropsFromState;

class EmceeLayout extends React.Component<EmceeLayoutProps> {
    public render() {
        const { classes, apiVersion, playerConnectionStatus, queueConnectionStatus, emceeConnectionStatus } = this.props as any;

        const rootClass = classNames({
            [classes.root]: true,
        });

        return (
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <ErrorBoundary fallback={<Error />}>
                        <div className={rootClass}>
                            <NavTabs />
                            <ErrorBoundary fallback={<Error />}>
                                <Typography component="div" className={classes.content}>
                                    <Routes>
                                        <Route path="queue" element={<Queue />} />
                                        <Route path="search" element={<Search />} />
                                        <Route path="history" element={<History />} />
                                        <Route path="lighting" element={<Lighting />} />
                                        <Route path="microphones" element={<Microphones />} />
                                        <Route path="codes" element={<Codes />} />
                                        <Route path="users" element={<Users />} />
                                        <Route index element={<Navigate replace to="queue" />} />
                                    </Routes>
                                </Typography>
                            </ErrorBoundary>
                            <div className={classes.version}>
                                v{gitVersion.SemVer} (server v{apiVersion}){" "}
                                <ConnectionStatusIcon status={queueConnectionStatus} />{" "}
                                <ConnectionStatusIcon status={playerConnectionStatus} />{" "}
                                <ConnectionStatusIcon status={emceeConnectionStatus} />
                            </div>
                            <div className={classes.blur}></div>
                        </div>
                    </ErrorBoundary>
                </ThemeProvider>
            </StyledEngineProvider>
        );
    }
}

const Emcee = compose(
    withStyles(styles),
    withSelector('token', tokenSelector),
    withSelector('apiVersion', apiVersionSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withSelector('emceeConnectionStatus', emceeConnectionStatusSelector),
    withAction('connectAsEmcee', connectAsEmcee),
    withAction('disconnect', disconnect),
    withAction('loginEmcee', loginEmcee),
    withEffect(({ loginEmcee, connectAsEmcee, disconnect }) => {
        loginEmcee();

        connectAsEmcee();

        return () => { disconnect(); }
    }, []),
    renderIf(({ token }) => !token, renderNothing),
)(EmceeLayout);

export { Emcee as EmceeLayout }