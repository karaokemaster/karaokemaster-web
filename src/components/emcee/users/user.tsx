/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faUserCrown } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AppBar, Avatar, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';

import { selectedUserSelector } from 'store/emcee';
import { User } from 'store/user';
import { UserEditor } from './editor';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'absolute',
        top: theme.spacing(1),
        bottom: 0,
        left: theme.spacing(1.5),
        right: theme.spacing(1.5),
        overflowY: 'auto',
        paddingBottom: '1.5em',

        '&::-webkit-scrollbar': {
            width: '0 !important',
        },
    },
    bar: {
        display: 'flex',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        padding: theme.spacing(1),
        backgroundColor: theme.palette.background.default,
    },
    icon: {
        color: theme.palette.text.primary,
        borderColor: '#00000066',
        borderWidth: '2px',
        borderStyle: 'solid',
        width: 48,
        height: 48,
        marginLeft: theme.spacing(1),
    },
    username: {

    },
});

interface PropsFromState {
    user?: User,
}

type UserProps = PropsFromState;

class UserLayout extends React.Component<UserProps> {
    public render = () => {
        const { user } = this.props;
        const { classes } = (this.props as any);

        if (!user) return null; // renderIf below should handle; this is just here to suppress errors

        let icon = faUserCrown;
        try {
            icon = findIconDefinition(JSON.parse(user.icon || ""));
        } catch {}

        return (
            <div className={classes.root}>
                <AppBar position="static" className={classes.bar}>
                    <Avatar className={classNames(classes.icon)} style={{ backgroundColor: user.color }}>
                        <FontAwesomeIcon icon={icon} fixedWidth={true} className={classes.userIcon} />
                    </Avatar>
                    <Typography variant="h6" className={classNames(classes.username)}>
                        {user.username}
                    </Typography>
                </AppBar>
                <UserEditor user={user} />
            </div>
        )
    }
}

const UserComponent = compose(
    withStyles(styles),
    withSelector('user', selectedUserSelector),
    renderIf(({ user }) => !user, compose(renderNothing())(React.Component)),
)(UserLayout);

export { UserComponent as User }
