/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Grid, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withRef } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { withProps } from '@truefit/bach-recompose';
import { withAction } from '@truefit/bach-redux';
import * as React from 'react';
import { animateScroll } from 'react-scroll';

import { Error } from 'components/error';
import { emceeActions } from 'store/emcee';
import { UserInputValue, UserList } from '../search/user-list';
import { User } from './user';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: '120px', // TODO: Source this value from layout/tabs
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(3),
        overflow: 'hidden',
    },
    main: {
        minHeight: '100%'
    },
    container: {
        position: 'relative',
    },
});

interface PropsFromState {
    locationKey?: string | undefined;
}

interface PropsFromDispatch {
    selectUser: typeof emceeActions.selectUser;
}

type UsersProps = PropsFromState & PropsFromDispatch;

class UsersLayout extends React.Component<UsersProps> {
    public render = () => {
        const { classes } = (this.props as any);

        // TODO: Refactor into separate components
        return (
            <Typography component="div" className={classes.root}>
                <ErrorBoundary fallback={<Error />}>
                    <Grid container={true} direction="row" spacing={3} className={classes.main}>
                        <Grid item={true} xs={3} className={classes.container}>
                            <UserList setUser={this.selectUser} disableAdd={true} />
                        </Grid>
                        <Grid item={true} xs={9} className={classes.container}>
                            <User />
                        </Grid>
                    </Grid>
                </ErrorBoundary>
            </Typography>
        );
    }

    private selectUser = (user: UserInputValue) => {
        const { selectUser } = this.props;

        selectUser(user.userId);
    }
}

const Users = compose(
    withStyles(styles),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withAction('selectUser', emceeActions.selectUser),
    withRef('firstUpdate', true),
    withEffect(
        ({ firstUpdate }: any) => { if (firstUpdate.current) { firstUpdate.current = false; return; } animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
)(UsersLayout);

export { Users };
