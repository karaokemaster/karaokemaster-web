/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { IconLookup } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { IconResult } from 'store/emcee';

const styles = (theme: Theme) => createStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        '-webkit-column-break-inside': 'avoid',
        pageBreakInside: 'avoid',
        breakInside: 'avoid',
    },

    // TODO: Combine with styles from iconPicker
    customIcon: {
        backgroundColor: 'darkgrey',
        padding: theme.spacing(0.5),
        margin: theme.spacing(0.5),
        borderRadius: theme.spacing(0.5),
    },
    selected: {
        backgroundColor: theme.palette.primary.main,
    },
});

interface OtherProps {
    result: IconResult,
    selectedIcon: IconLookup,
    setIcon: (icon: IconLookup) => void,
}

class FindIconResultLayout extends React.Component<OtherProps> {
    public render = () => {
        const { result, selectedIcon } = this.props;
        const { classes } = (this.props as any);

        const isSelected = selectedIcon.prefix === result.icon.prefix && selectedIcon.iconName === result.icon.iconName;
        const iconClasses = classNames({
            [classes.customIcon]: true,
            [classes.selected]: isSelected,
        });

        return (
            <div className={classes.root}>
                <FontAwesomeIcon icon={result.icon} className={classNames(iconClasses)} size="2x" fixedWidth={true} onClick={this.selectIcon} />{" "}
                {result.label}
            </div>
        )
    }

    private selectIcon = () => {
        const { setIcon, result } = this.props;

        setIcon(result.icon);
    }
}

const FindIconResult = compose<OtherProps>(
    withStyles(styles),
)(FindIconResultLayout);

export { FindIconResult }
