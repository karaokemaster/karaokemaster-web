/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { IconLookup } from '@fortawesome/fontawesome-svg-core';
import { faMagnifyingGlassArrowRight } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { TextField, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withRef, withState } from '@truefit/bach';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { icons } from 'components/user-profile/settings/iconPicker';
import { emceeActions, findIconResults, IconResult } from 'store/emcee';
import { FindIconResult } from './find-icon-result';

const styles = (theme: Theme) => createStyles({
    root: {
        width: '100%',
        textAlign: 'left',
    },
    findIcon: {
        marginLeft: theme.spacing(0.5),
        marginTop: theme.spacing(0.5),
    },
    results: {
        columnCount: 3,
    },

    // TODO: Combine with styles from iconPicker
    customIcon: {
        backgroundColor: 'darkgrey',
        padding: theme.spacing(0.5),
        margin: theme.spacing(0.5),
        borderRadius: theme.spacing(0.5),
    },
    selected: {
        backgroundColor: theme.palette.primary.main,
    },
    iconResult: {
        display: 'flex',
        alignItems: 'center',
    }
});

interface PropsFromState {
    findIconResults: IconResult[],

    findIconOpen: boolean;
    setFindIconOpen: (open: boolean) => void;
}

interface PropsFromDispatch {
    findIcon: typeof emceeActions.findIcon,
}

interface ComposedProps {
    findIconRef: React.MutableRefObject<HTMLInputElement>
}

type FindIconProps = PropsFromState & PropsFromDispatch & ComposedProps;

interface OtherProps {
    selectedIcon: IconLookup;
    setIcon: (icon: IconLookup) => void;
}

class FindIconLayout extends React.Component<FindIconProps & OtherProps> {
    public render = () => {
        const { selectedIcon, setIcon, findIconOpen, findIconResults, findIconRef } = this.props;
        const { classes } = (this.props as any);

        const isCustomIcon = findIconOpen || icons.find(i => i.prefix === selectedIcon.prefix && i.iconName === selectedIcon.iconName) === undefined;
        const customIcon = isCustomIcon && !findIconOpen ? selectedIcon : faMagnifyingGlassArrowRight;
        const customIconClasses = classNames({
            [classes.customIcon]: true,
            [classes.selected]: isCustomIcon,
        });
        // TODO: Only show the save button when there's been a change!

        return (
            <div className={classes.root}>
                <FontAwesomeIcon icon={customIcon} className={customIconClasses} size="2x" fixedWidth={true} onClick={this.toggleFindIcon} />
                {findIconOpen &&
                    <TextField
                        inputRef={findIconRef}
                        onKeyPress={this.findIconKeyPress}
                        className={classes.findIcon}
                        label="Find icon"
                        variant="outlined"
                        color="primary"
                    />
                }
                {findIconOpen &&
                    <div className={classNames(classes.results)}>
                        {findIconResults.map(r => <FindIconResult key={r.icon.iconName} result={r} selectedIcon={selectedIcon} setIcon={setIcon} />)}
                    </div>
                }
            </div>
        )
    }

    private toggleFindIcon = () => {
        const { findIconOpen, setFindIconOpen } = this.props;

        setFindIconOpen(!findIconOpen);
    }

    private findIconKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        // e.keyCode doesn't work in keyPress
        if (e.which !== 13) { return; }

        e.preventDefault();

        if (!this.props.findIconRef.current) { return }

        const query = this.props.findIconRef.current.value;

        this.findIcon(query);
    }

    private findIcon = (query: string) => {
        const { findIcon } = this.props;

        findIcon(query);
    }
}

const FindIcon = compose<OtherProps>(
    withStyles(styles),
    withRef('findIconRef', null),
    withState<FindIconProps & OtherProps, boolean>('findIconOpen', 'setFindIconOpen', false),
    withAction('findIcon', emceeActions.findIcon),
    withSelector('findIconResults', findIconResults),
)(FindIconLayout);

export { FindIcon }
