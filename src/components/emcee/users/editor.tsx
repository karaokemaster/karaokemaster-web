/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { findIconDefinition, IconLookup } from '@fortawesome/fontawesome-svg-core';
import { faUserCrown } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Avatar, Button, Card, CardContent, Theme, Typography } from '@mui/material';
import { red } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import { compose, withEffect, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withAction } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';
import * as serialize from 'serialize-javascript';

import { ColorPicker } from 'components/user-profile/settings/colorPicker';
import { IconPicker } from 'components/user-profile/settings/iconPicker';
import { emceeActions, IconResult } from 'store/emcee';
import { User } from 'store/user';
import { FindIcon } from './find-icon';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
    card: {
        width: '100%',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    title: {
        fontSize: 14,
    },
    error: {
        color: red[500],
    },
    icons: {
        maxWidth: '97.4%', // TODO: Less awful
        paddingRight: '6em',
        paddingLeft: '6em',
        textAlign: 'center',
    },
    colors: {
        textAlign: 'center',
    },
    preview: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        color: theme.palette.text.primary,
        borderColor: '#00000066',
        borderWidth: '2px',
        borderStyle: 'solid',
        width: 48,
        height: 48,
        marginRight: theme.spacing(1),
    },
    username: {

    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
});

interface PropsFromState {
    changeIconOpen: boolean,
    setChangeIconOpen: (value: boolean) => void,
    findIconResults: IconResult[],

    icon: IconLookup;
    setIcon: (icon: IconLookup) => void;
    color: string;
    setColor: (color: string) => void;
    findIconOpen: boolean;
    setFindIconOpen: (open: boolean) => void;
}

interface PropsFromDispatch {
    saveUser: typeof emceeActions.saveUser,
    findIcon: typeof emceeActions.findIcon,
}

interface ComposedProps {
    findIconRef: React.MutableRefObject<HTMLInputElement>
}

type UserEditorProps = PropsFromState & PropsFromDispatch & ComposedProps;

interface OtherProps {
    user?: User,
}

class UserEditorLayout extends React.Component<UserEditorProps & OtherProps> {
    public render = () => {
        const { user, icon, setIcon, color, setColor } = this.props;
        const { classes } = (this.props as any);

        if (!user) return null; // renderIf below should handle; this is just here to suppress errors

        // TODO: Only show the save button when there's been a change!

        return (
            <div className={classes.root}>
                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Icon
                        </Typography>
                        <Typography variant="h6" component="h2" className={classes.icons}>
                            <IconPicker icon={icon} onChange={setIcon} />
                            <FindIcon selectedIcon={icon} setIcon={setIcon} key={user.userId} />
                        </Typography>
                    </CardContent>
                </Card>

                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Color
                        </Typography>
                        <Typography variant="h6" component="h2" className={classes.colors}>
                            <ColorPicker color={color} savedColor={user.color} onChange={setColor} />
                        </Typography>
                    </CardContent>
                </Card>

                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Preview
                        </Typography>
                        <Typography variant="h6" component="h2" className={classes.preview}>
                            <Avatar className={classNames(classes.icon)} style={{ backgroundColor: color }}>
                                <FontAwesomeIcon icon={icon} fixedWidth={true} className={classes.userIcon} />
                            </Avatar>
                            <Typography variant="h6" className={classNames(classes.username)}>
                                {user.username}
                            </Typography>
                        </Typography>
                    </CardContent>
                </Card>

                <Button onClick={this.save}>Save</Button>
            </div>
        )
    }

    private save = () => {
        const { user, icon, color, saveUser } = this.props;

        const newUser = {
            ...user,
            icon: serialize(icon),
            color,
        };

        saveUser(newUser);
    }
}

const UserEditor = compose<OtherProps>(
    withStyles(styles),
    withState<UserEditorProps & OtherProps, string>('color', 'setColor', ({ user }: UserEditorProps & OtherProps) => user ? user.color : ""),
    withState<UserEditorProps & OtherProps, IconLookup>('icon', 'setIcon', faUserCrown),
    withAction('saveUser', emceeActions.saveUser),
    withEffect(
        ({ user, setColor, setIcon }: UserEditorProps & OtherProps) => {
            let icon = faUserCrown;
            try {
                icon = findIconDefinition(JSON.parse(user && user.icon || ""));
            } catch {}
            setIcon(icon);
            setColor(user ? user.color : "");
        },
        [ 'user' ],
    ),
    renderIf(({ user }) => !user, compose(renderNothing())(React.Component)),
)(UserEditorLayout);

export { UserEditor }
