/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withRef } from '@truefit/bach';
import { withStore } from "@truefit/bach-redux";
import { scaleLinear } from 'd3-scale';
import { select } from 'd3-selection';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import { Store } from 'redux';

import { ApplicationState } from 'store';
import { microphoneLevelSelector } from 'store/emcee';

const styles = (theme: Theme) => createStyles({
    meter: {
        '& .background': {
            strokeWidth: 0,
            fill: 'DarkSlateGrey',
            fillOpacity: 0.3,
        },
        '& .rfLevel': {
            strokeWidth: 0,
            fill: theme.palette.primary.dark,
        },
        '& .afLevel': {
            strokeWidth: 0,
            fill: theme.palette.secondary.dark,
        },
        '& .afPeak': {
            strokeWidth: 0,
            fill: theme.palette.secondary.light,
        },
    },
});

interface PropsFromState {
    store: Store<ApplicationState>
}

interface ComposedProps {
    svgNode: React.MutableRefObject<SVGSVGElement>
}

type SignalLevelProps = PropsFromState & ComposedProps;

interface OtherProps {
    name: string
}

class SignalLevelLayout extends React.Component<SignalLevelProps & OtherProps> {
    private levelYScale = scaleLinear().domain([0, 120]).range([100, 0]);
    private levelHeightScale = scaleLinear().domain([0, 120]).range([0, 100]);
    private peakScale = scaleLinear().domain([0, 120]).range([100, 0]);
    private animationRequest: number;

    public componentDidMount = () => {
        this.createMeter();

        // Kick-off clock
        this.animationRequest = requestAnimationFrame(this.framed);
    }

    public shouldComponentUpdate(nextProps: Readonly<SignalLevelProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        return false;
    }

    public componentWillUnmount() {
        cancelAnimationFrame(this.animationRequest);
    }

    public render = () => {
        const { svgNode } = this.props;
        const { classes } = this.props as any;

        return <svg ref={svgNode} className={classes.meter} />;
    }

    private createMeter = () => {
        const { svgNode } = this.props;

        if (!svgNode.current) return;

        // Size canvas
        const fullWidth = svgNode.current.clientWidth;
        const barWidth = fullWidth / 2.2;
        const fullHeight = svgNode.current.clientHeight;

        const svg = select(svgNode.current)
            .attr('width', svgNode.current.clientWidth)
            .attr('height', svgNode.current.clientHeight)
            .attr('viewBox', `0 0 ${fullWidth} ${fullHeight}`)
        ;

        this.levelYScale = scaleLinear().domain([0, 120]).range([fullHeight, 0]);
        this.levelHeightScale = scaleLinear().domain([0, 120]).range([0, fullHeight]);
        this.peakScale = scaleLinear().domain([0, 120]).range([fullHeight, 0]);


        // Add background
        svg.append('rect').classed('background', true)
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', barWidth)
            .attr('height', fullHeight);
        svg.append('rect').classed('background', true)
            .attr('x', fullWidth - barWidth)
            .attr('y', 0)
            .attr('width', barWidth)
            .attr('height', fullHeight);

        svg.append('rect').classed('rfLevel', true)
            .attr('x', 0)
            .attr('y', fullHeight)
            .attr('width', barWidth)
            .attr('height', 0);

        svg.append('rect').classed('afLevel', true)
            .attr('x', fullWidth - barWidth)
            .attr('y', fullHeight)
            .attr('width', barWidth)
            .attr('height', 0);

        svg.append('rect').classed('afPeak', true)
            .attr('x', fullWidth - barWidth)
            .attr('y', fullHeight)
            .attr('width', barWidth)
            .attr('height', 1);
    }

    private framed = () => {
        const { svgNode } = this.props;

        if (!svgNode.current) {
            this.animationRequest = requestAnimationFrame(this.framed);
            return;
        }

        const { name } = this.props;
        const state = this.props.store.getState();

        const microphone = state.emcee.microphoneLevels.find(m => m.name === name);

        if (!microphone) {
            this.animationRequest = requestAnimationFrame(this.framed);
            return;
        }

        const svg = select(svgNode.current);

        if (microphone.rfLevel) {
            const rfLevelY = this.levelYScale(microphone.rfLevel || 0);
            const rfLevelHeight = this.levelHeightScale(microphone.rfLevel || 0);

            svg.select('.rfLevel')
                .attr('y', rfLevelY)
                .attr('height', rfLevelHeight);
        }

        if (microphone.afLevel) {
            const afLevelY = this.levelYScale(microphone.afLevel || 0);
            const afLevelHeight = this.levelHeightScale(microphone.afLevel || 0);

            svg.select('.afLevel')
                .attr('y', afLevelY)
                .attr('height', afLevelHeight);
        }

        if (microphone.afPeak) {
            const peak = this.peakScale(microphone.afPeak || 0);

            svg.select('.afPeak')
                .attr('y', peak);
        }

        this.animationRequest = requestAnimationFrame(this.framed);
    }
}

const SignalLevel = compose<OtherProps>(
    withStyles(styles),
    withRef('svgNode', null),
    withStore(),
)(SignalLevelLayout);

export { SignalLevel }