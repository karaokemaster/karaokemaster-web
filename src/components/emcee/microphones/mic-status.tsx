/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCircle } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBatteryEmpty, faBatteryExclamation, faBatteryFull, faBatteryQuarter, faBatteryThreeQuarters } from '@fortawesome/sharp-regular-svg-icons'
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose } from '@truefit/bach';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { Microphone, microphonesSelector } from 'store/emcee';

const styles = (theme: Theme) => createStyles({
    container: {
        position: 'absolute',
        bottom: 0,
        width: '100px',
        padding: theme.spacing(1),
        textAlign: 'center',
    },
    micBadge: {
        border: '2px solid #000',
        borderRadius: '1em',
    },
});

export const batteryIcon = (battery?: number | undefined) => {
    if (battery === undefined || battery === null)
        return faBatteryExclamation;
    else if (battery > 85)
        return faBatteryFull;
    else if (battery > 50)
        return faBatteryThreeQuarters;
    else if (battery > 15)
        return faBatteryQuarter;
    else
        return faBatteryEmpty;
}

interface PropsFromState {
    microphones: Microphone[];
}

type MicStatsProps = PropsFromState;

class MicStatusLayout extends React.Component<MicStatsProps> {
    public render = () => {
        const { microphones } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.container}>
                <ErrorBoundary fallback={<div />}>
                    {microphones.map(m => <Mic key={m.name} microphone={m} /> )}
                </ErrorBoundary>
            </div>
        )
    }
}

class MicLayout extends React.Component<{ microphone: Microphone }> {
    public render = () => {
        const { microphone: { color, battery }} = this.props;
        const { classes } = (this.props as any);

        const alert = (battery && battery < 15 ? true : false);

        return (
            <div>
                <FontAwesomeIcon icon={faCircle} color={color} size="xs" className={classes.micBadge} />
                {" "}&nbsp;{" "}
                <FontAwesomeIcon icon={batteryIcon(battery)} size="xl" fade={alert} />
            </div>
        )
    }
}

const Mic = compose<{ microphone: Microphone }>(
    withStyles(styles),
)(MicLayout);

const MicStatus = compose(
    withStyles(styles),
    withSelector('microphones', microphonesSelector),
)(MicStatusLayout);

export { MicStatus };