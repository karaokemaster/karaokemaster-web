/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { List, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose } from '@truefit/bach';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { Error } from 'components/error';
import { Microphone, microphonesSelector } from 'store/emcee';
import { Microphone as Mic } from './microphone';

const styles = (theme: Theme) => createStyles({
    root: {
        height: '100vh',
    },
    micList: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        gap: theme.spacing(2),
    },
});

interface PropsFromState {
    microphones: Microphone[],
}

type MicrophonesProps = PropsFromState;

class MicrophonesLayout extends React.Component<MicrophonesProps> {
    public render() {
        const { microphones } = this.props;
        const { classes } = this.props as any;

        return (
            <div className={classes.root}>
                <Typography variant="h6">Microphones</Typography>
                <List className={classes.micList}>
                    <ErrorBoundary fallback={<Error />}>
                        {microphones.map(m => <Mic key={m.name} microphone={m} />)}
                    </ErrorBoundary>
                </List>
            </div>
        )
    }
}

const Microphones = compose(
    withStyles(styles),
    withSelector('microphones', microphonesSelector),
)(MicrophonesLayout);

export { Microphones };