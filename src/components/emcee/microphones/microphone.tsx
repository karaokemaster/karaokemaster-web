/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCircle } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrophone, faMicrophoneSlash, faTowerBroadcast } from '@fortawesome/sharp-regular-svg-icons';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';

import { Microphone } from 'store/emcee';
import { batteryIcon } from './mic-status';
import { SignalLevel } from './signal-level';

const styles = (theme: Theme) => createStyles({
    root: {
        display: 'block',
        width: '150px',
        userSelect: 'none',
        '-webkit-touch-callout': 'none',
        border: '1px solid #fff',
        borderRadius: theme.spacing(1),
        padding: theme.spacing(2),
    },
    name: {
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
    },
    battery: {
        paddingLeft: theme.spacing(2),
    },
    antenna: {
    },
    active: {
        paddingLeft: theme.spacing(1),
    },
    inactive: {
        paddingLeft: theme.spacing(1),
        color: '#999',
    },
    levels: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'stretch',
        height: '75vh',
    },
});

interface OtherProps {
    microphone: Microphone;
}

class MicrophoneLayout extends React.Component<OtherProps> {
    public render = () => {
        const { microphone: { name, color, battery, rfAntenna, rfPilot, mute } } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.root}>
                <div>
                    <FontAwesomeIcon icon={faCircle} color={color} />
                    <span className={classes.name}>{name}</span>
                    <FontAwesomeIcon icon={mute ? faMicrophoneSlash : faMicrophone} />
                </div>
                <div>
                    <FontAwesomeIcon icon={faTowerBroadcast} className={classes.antenna} />
                    <span className={rfPilot && rfAntenna === 1 ? classes.active : classes.inactive}>A</span>
                    <span className={rfPilot && rfAntenna === 2 ? classes.active : classes.inactive}>B</span>
                    <FontAwesomeIcon icon={batteryIcon(battery)} size="xl" className={classes.battery} />
                </div>
                <div className={classes.levels}>
                    <SignalLevel name={name} />
                </div>
            </div>
        );
    }
}

const Mic = compose<OtherProps>(
    withStyles(styles),
)(MicrophoneLayout);

export { Mic as Microphone }
