/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { List, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { Error } from 'components/error';
import { LightingCue } from 'components/kiosk/player-info/lighting-cue';
import { Cue, lightingCuesSelector } from 'store/cues';

const styles = (theme: Theme) => createStyles({
    cues: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: '120px', // TODO: Source this value from layout/tabs
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(3),
        overflowX: 'hidden',
        overflowY: 'hidden',
    },
    cueList: {
        paddingRight: '8px',
        columnCount: 4,
        columnGap: '1rem',
    },
});

interface PropsFromState {
    cues: Cue[],
}

type LightingProps = PropsFromState;

class LightingLayout extends React.Component<LightingProps> {
    public render() {
        const { cues } = this.props;
        const { classes } = this.props as any;

        return (
            <div>
                <div className={classes.cues}>
                    <Typography variant="h6">Lighting Cue</Typography>
                    <List className={classes.cueList}>
                        <ErrorBoundary fallback={<Error />}>
                            {cues.map(c => <LightingCue key={c.index} cue={c} />)}
                        </ErrorBoundary>
                    </List>
                </div>
            </div>
        )
    }
}

const Lighting = compose(
    withStyles(styles),
    withSelector('cues', lightingCuesSelector),
)(LightingLayout);

export { Lighting };