/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCirclePause, faForwardFast, faForwardStep, faPause, faPlay, faRectangleList, faRedo, faTrashCan } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Grid, IconButton, Theme, Typography } from '@mui/material';
import { grey } from '@mui/material/colors';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from "@truefit/bach-recompose";
import { withAction, withSelector } from '@truefit/bach-redux';
import * as he from 'he';
import * as React from 'react';

import { Time } from 'components/time';
import { PlaybackState, playbackStateSelector, playerConnectionStatusSelector, PlayerControl } from 'store/player';
import * as playerActions from 'store/player/actions';
import { PlayingQueueEntry, queueConnectionStatusSelector } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus } from 'store/signalR';
import { LightingCue } from './widgets/lighting-cue';
import { Tempo } from './widgets/tempo';

import { runtimeConfig } from 'runtimeConfig';

const styles = (theme: Theme) => createStyles({
    player: {
        position: 'fixed',
        top: 0,
        left: '120px',
        right: '450px',
        height: '260px',
        backgroundColor: 'black',
        zIndex: 1100,
        padding: theme.spacing(),
    },
    heading: {
        position: 'absolute',
        left: '10px',
        right: '10px',
        top: '25px',
        fontSize: '24px',
        fontWeight: 'bold',
        margin: 0,
    },
    thumbnail: {
        position: 'absolute',
        top: '80px',
        left: '10px',
        width: '180px',
        height: '180px',
        '& img': {
            objectFit: 'cover',
            width: '180px',
            height: '180px',
        },
    },
    trackInfo: {
        position: 'absolute',
        left: '210px',
        top: '80px',
        right: '10px',
    },
    trackTitle: {
        fontWeight: 'bold',
        height: '24px',
        overflowY: 'hidden',
        textOverflow: 'ellipsis',
        margin: 0,
    },
    trackUser: {
        color: grey[500],
        float: 'left',
        margin: 0,
    },
    trackTime: {
        color: grey[500],
        float: 'right',
        margin: 0,
    },
    playerControls: {
        position: 'absolute',
        left: '210px',
        top: '180px',
        bottom: '260px',
        right: '10px',

        '& svg': {
            maxWidth: '48px',
            maxHeight: '48px',
        },
    },
    rated: {
        backgroundColor: '#fff !important',
        color: '#000',
    },
    widgets: {
        position: 'absolute',
        top: 0,
        right: '-450px',
        width: '450px',
        padding: '10px',
    }
});

const initialHeight = 260;

interface PropsFromState {
    playerConnectionStatus: ConnectionStatus,
    queueConnectionStatus: ConnectionStatus,
    playbackState: PlaybackState,
    willStopAfter: boolean,
}

interface PropsFromDispatch {
    sendPlayerControl: typeof playerActions.sendPlayerControl;
    dequeue: typeof queueActions.dequeue;
}

type MiniPlayerProps = PropsFromState & PropsFromDispatch;

interface OtherProps {
    entry: PlayingQueueEntry;
}

class MiniPlayerLayout extends React.Component<MiniPlayerProps & OtherProps> {
    public render = () => {
        const { entry, playbackState, playerConnectionStatus, queueConnectionStatus, willStopAfter } = this.props;
        const { classes } = (this.props as any);

        const playing: boolean = playbackState === PlaybackState.Playing || playbackState === PlaybackState.StoppingAfter;
        const stopping: boolean = playbackState === PlaybackState.StoppingAfter;
        const playerDisabled = (playerConnectionStatus !== ConnectionStatus.Connected) || playbackState === PlaybackState.Disconnected;
        const queueDisabled = (queueConnectionStatus !== ConnectionStatus.Connected);

        let playPauseIcon = faPlay;
        if (stopping)
            playPauseIcon = faCirclePause;
        else if (playing && willStopAfter)
            playPauseIcon = faForwardStep;
        else if (playing)
            playPauseIcon = faPause;

        return (
            <Typography component="div" className={classes.player}>
                <Typography component="h2" className={classes.heading}>Now Playing</Typography>
                <Typography component="div" className={classes.thumbnail}>
                    <img src={entry.thumbnailUrl} />
                </Typography>
                <Typography component="div" className={classes.trackInfo}>
                    <Typography component="h3" className={classes.trackTitle}>
                        <Typography noWrap={true}>{he.decode(entry.title)}</Typography>
                    </Typography>
                    <Typography component="h4" className={classes.trackUser}>{entry.user}</Typography>
                    <Typography component="h4" className={classes.trackTime}>
                        <Time seconds={entry.videoTime} nullIfNegative={true} /> / <Time seconds={entry.videoDuration} nullIfNegative={true} />
                    </Typography>
                </Typography>
                <Typography component="div" className={classes.playerControls}>
                    <Grid container={true} direction="row" justifyContent="space-around" alignItems="center">
                        <Grid item={true}>
                            {playing
                                ? <IconButton onClick={this.restartTrack} disabled={playerDisabled} size="large">
                                    <FontAwesomeIcon icon={faRedo} flip="horizontal"  size="2x" />
                                </IconButton>
                                : <IconButton onClick={this.deleteTrack} disabled={queueDisabled} size="large">
                                    <FontAwesomeIcon icon={faTrashCan} size="2x" />
                                </IconButton>
                            }
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={this.playPause} disabled={playerDisabled} size="large">
                                <FontAwesomeIcon icon={playPauseIcon} size="2x" />
                            </IconButton>
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={this.nextTrack} disabled={playerDisabled} size="large">
                                <FontAwesomeIcon icon={faForwardFast} size="2x" />
                            </IconButton>
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={this.editLyrics} size="large">
                                <FontAwesomeIcon icon={faRectangleList} size="2x" />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Typography>
                <Typography component="div" className={classes.widgets}>
                    <Tempo />
                    <LightingCue />
                </Typography>
            </Typography>
        );
    }

    private playPause = () => {
        const { playbackState, sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:miniPlayer:playPause'});
            s.setSpan(transaction);

            const control = playbackState === PlaybackState.Playing ? PlayerControl.Pause : PlayerControl.Play;
            transaction.setData("control", control);
            sendPlayerControl(control);

            transaction.finish();
        });
    }

    private nextTrack = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:miniPlayer:nextTrack'});
            s.setSpan(transaction);

            sendPlayerControl(PlayerControl.Next);

            transaction.finish();
        });
    }

    private restartTrack = () => {
        const { sendPlayerControl } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'miniPlayer:restartTrack'});
            s.setSpan(transaction);

            sendPlayerControl(PlayerControl.Back);

            transaction.finish();
        });
    }

    private deleteTrack = () => {
        const { entry, dequeue } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:miniPlayer:deleteTrack'});
            s.setSpan(transaction);

            dequeue(entry);

            transaction.finish();
        });
    }

    private editLyrics = () => {
        // const { entry, setRating, setAnimate } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:miniPlayer:editLyrics'});
            s.setSpan(transaction);


            transaction.finish();
        });
    }
}

const MiniPlayer = compose<OtherProps>(
    withStyles(styles),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withSelector('playbackState', playbackStateSelector),
    withProps({
        stopAfterPercent: () => runtimeConfig().client.player.stopAfterPercent,
        willStopAfter: ({ stopAfterPercent, entry: { videoTime, videoDuration }}) => videoTime / videoDuration > stopAfterPercent,
    }),
    withAction('sendPlayerControl', playerActions.sendPlayerControl),
    withAction('dequeue', queueActions.dequeue),
)(MiniPlayerLayout);

export { MiniPlayer, initialHeight };