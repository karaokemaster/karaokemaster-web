/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faPeopleArrowsLeftRight, faRectangleList, faTrash, faTrashCheck, faUpToLine } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Avatar, ListItem, ListItemAvatar, ListItemIcon, ListItemSecondaryAction, ListItemText, Theme, Typography } from '@mui/material';
import { blue, orange, red } from '@mui/material/colors';
import Fab from '@mui/material/Fab';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as he from 'he';
import * as React from 'react';

import { queueConnectionStatusSelector, QueueEntry, TimedQueueEntry } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus } from 'store/signalR';
import { EntryChips } from './entryChips';

const styles = (theme: Theme) => createStyles({
    root: {
        userSelect: 'none',
        '-webkit-touch-callout': 'none',
    },
    user: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '86px',
        paddingRight: theme.spacing(2),
    },
    icon: {
        color: theme.palette.text.primary,
        borderColor: '#00000066',
        borderWidth: '2px',
        borderStyle: 'solid',
        width: 48,
        height: 48,
    },
    username: {

    },
    button: {
        '&:hover': {
            backgroundColor: blue[700],
        },
        backgroundColor: blue[500],
        color: "#fff",
        padding: 4,
        marginLeft: theme.spacing(1),
    },
    confirmDelete: {
        backgroundColor: red[700],

        '&:hover': {
            backgroundColor: red[900],
        }
    },

    action: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        userSelect: "none",
        padding: "8px",
        fontSize: "10px",
        fontWeight: 500,
        boxSizing: "border-box",
        color: "#eee",
        whiteSpace: "nowrap",
    },
    actionIcon: {
        display: "block",
        fontSize: "2em",
    },
    deleteAction: {
        backgroundColor: red[500],
    },
    editLyricsAction: {
        backgroundColor: blue[500],
    },
    setUserAction: {
        backgroundColor: orange[500],
    },
});

interface PropsFromState {
    queueConnectionStatus: ConnectionStatus;

    confirmDelete?: NodeJS.Timeout;
    setConfirmDelete: (confirmDelete?: NodeJS.Timeout) => void;
}

interface PropsFromDispatch {
    dequeue: typeof queueActions.dequeue;
}

interface OtherProps {
    entry: TimedQueueEntry;
    editLyricsDialog: (entry: QueueEntry) => void;
    setUserDialog: (entry: QueueEntry) => void;
    moveNext: (entry: QueueEntry) => void;
}

type EntryProps = PropsFromState & PropsFromDispatch;

class EntryLayout extends React.Component<EntryProps & OtherProps> {
    public render = () => {
        const { entry, queueConnectionStatus, confirmDelete } = this.props;
        const { classes } = (this.props as any);

        const queueDisabled = (queueConnectionStatus !== ConnectionStatus.Connected);
        const icon = findIconDefinition(JSON.parse(entry.userIcon || ""));

        // TODO: Use chips for the secondary
        return (
            <ListItem className={classes.root}>
                <ListItemIcon className={classes.user}>
                    <Avatar className={classNames(classes.icon)} style={{ backgroundColor: entry.userColor }}>
                        <FontAwesomeIcon icon={icon} fixedWidth={true} className={classes.userIcon} />
                    </Avatar>
                    <Typography component="div" className={classes.username}>{entry.user}</Typography>
                </ListItemIcon>
                <ListItemAvatar>
                    <Avatar src={entry.thumbnailUrl} variant={"square"} />
                </ListItemAvatar>
                <ListItemText
                    disableTypography={true}
                    primary={<div>{he.decode(entry.title)}</div>}
                    secondary={<EntryChips entry={entry} />}
                />
                <ListItemSecondaryAction>
                    <Fab size="small" color="primary" className={classes.button} onClick={this.moveNext} disabled={queueDisabled || entry.order === 1}>
                        <FontAwesomeIcon icon={faUpToLine}/>
                    </Fab>
                    <Fab size="small" color="primary" className={classes.button} onClick={this.editLyrics} disabled={queueDisabled}>
                        <FontAwesomeIcon icon={faRectangleList}/>
                    </Fab>
                    <Fab size="small" color="primary" className={classes.button} onClick={this.setUser} disabled={queueDisabled}>
                        <FontAwesomeIcon icon={faPeopleArrowsLeftRight}/>
                    </Fab>
                    <Fab
                        size="small"
                        color="primary"
                        className={classNames({ [classes.button]: true, [classes.confirmDelete]: !!confirmDelete })}
                        onClick={this.removeFromQueue}
                        disabled={queueDisabled}
                    >
                        <FontAwesomeIcon icon={confirmDelete ? faTrashCheck : faTrash}/>
                    </Fab>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }

    private removeFromQueue = async () => {
        const { confirmDelete, setConfirmDelete, dequeue, entry } = this.props;

        if (!confirmDelete) {
            const timeout = setTimeout(this.resetConfirmDelete, 5000);
            setConfirmDelete(timeout);

            return;
        }

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:queue:remove'});
            s.setSpan(transaction);

            if (confirmDelete) {
                clearTimeout(confirmDelete);
                setConfirmDelete(undefined);
            }

            dequeue(entry);

            transaction.finish();
        });
    }

    private resetConfirmDelete = () => {
        const { confirmDelete, setConfirmDelete } = this.props;

        if (!confirmDelete) return;

        clearTimeout(confirmDelete);
        setConfirmDelete(undefined);
    }

    private editLyrics = async () => {
        const { entry, editLyricsDialog } = this.props;

        editLyricsDialog(entry);
    }

    private setUser = async () => {
        const { entry, setUserDialog } = this.props;

        setUserDialog(entry);
    }

    private moveNext = async () => {
        const { entry, moveNext } = this.props;

        moveNext(entry);
    }
}

const Entry = compose<OtherProps>(
    withStyles(styles),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withAction('dequeue', queueActions.dequeue),
    withState('confirmDelete', 'setConfirmDelete', undefined),
)(EntryLayout);

export { Entry }