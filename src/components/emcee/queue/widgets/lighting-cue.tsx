/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCircleCheck, faLightbulbOn } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { darken, lighten, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { activeCueSelector, Cue, isCueSavedToCurrentTrack } from 'store/cues';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        borderRadius: '1rem',
        padding: '0.75rem',
        backgroundColor: darken(theme.palette.background.default, 0.35),
        borderColor: lighten(theme.palette.background.default, 0.35),
        borderWidth: '0.2rem',
        borderStyle: 'solid',
        height: '50px',
    },
    icon: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '0.85em',
        maxWidth: '50px',
        maxHeight: '50px',
        width: '50px',
        height: '50px',
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary,
        borderRadius: '50%',
    },
    label: {
        marginLeft: `calc(${theme.spacing(2)} + 50px)`,
        fontSize: '28px',
        textAlign: 'left',
    },
});

interface PropsFromState {
    cue: Cue,
    isCueSaved: boolean,
}

type LightingCueProps = PropsFromState;

class LightingCueLayout extends React.Component<LightingCueProps> {
    public render() {
        const { cue, isCueSaved } = this.props;
        const { classes } = this.props as any;

        return (
            <div className={classes.root}>
                <div className={classes.icon}>
                    <FontAwesomeIcon icon={faLightbulbOn} />
                </div>
                <Typography variant="h3" className={classes.label}>{cue.name}</Typography>
                {isCueSaved && <FontAwesomeIcon icon={faCircleCheck}/>}
            </div>
        )
    }
}

const LightingCue = compose(
    withStyles(styles),
    withSelector('cue', activeCueSelector),
    withSelector('isCueSaved', isCueSavedToCurrentTrack),
    renderIf(({ cue }) => cue === undefined, compose(renderNothing())(React.Component)),
)(LightingCueLayout);

export { LightingCue };