/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { darken, lighten, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withEffect, withRef } from '@truefit/bach';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import Cardiogram from 'react-cardiogram';
import { ManualBangHandle } from 'react-cardiogram/dist/lib/types';

import { playerTempoBeatSelector } from 'store/player';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'relative',
        borderRadius: '1rem',
        // padding: '0.75rem',
        backgroundColor: darken(theme.palette.background.default, 0.35),
        borderColor: lighten(theme.palette.background.default, 0.35),
        borderWidth: '0.2rem',
        borderStyle: 'solid',
        // height: '50px',
    },
});

interface PropsFromState {
    cardiogram: React.MutableRefObject<ManualBangHandle>;
}

type TempoProps = PropsFromState;

class TempoLayout extends React.Component<TempoProps> {

    public render() {
        const { cardiogram } = this.props;
        const { classes } = this.props as any;

        const opts = {
            height: 75, // canvas height in px
            scale: 55, // % of canvas height used for graph
            // paintInterval: 30, // interval between drawing points in ms
            // density: 2, // horizontal size of drawing points in px
            // 400px wide canvas / 2 (density) = 200 points * 30 ms (interval) = 6000 ms = 6 s window
        };

        return (
            <div className={classes.root}>
                <Cardiogram ref={cardiogram} {...opts} />
            </div>
        )
    }
}

const Tempo = compose(
    withStyles(styles),
    withSelector('bpmTimestamp', playerTempoBeatSelector),
    withRef('cardiogram', undefined),
    withEffect(({ cardiogram }: TempoProps & any) => {
        cardiogram.current?.bang();
    }, [ 'bpmTimestamp' ]),
)(TempoLayout);

export { Tempo };