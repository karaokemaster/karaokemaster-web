/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose, withRef, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { scaleLinear } from 'd3-scale';
import { select } from 'd3-selection';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({
    clock: {
        width: '36px',
        height: '36px',

        '& .background': {
            strokeWidth: '4px',
            stroke: '#fff',
            clipPath: 'circle()',
            fill: '#000',
        },
        '& .axis .domain': {
            strokeWidth: 0,
        },
        '& .pointers .hour, & .pointers .min': {
            fill: 'white',
            stroke: 'white',
            strokeWidth: 0.005,
        },
        '& .pointers .center': {
            fill: 'darkgrey',
        },
    },
});

interface ComposedProps {
    svgNode: React.MutableRefObject<SVGSVGElement>

    clockTick?: number;
    setClockTick: (tick?: number) => void;
}

interface OtherProps {
    timeOffset: number
}

type ClockProps = ComposedProps;

class ClockLayout extends React.Component<ClockProps & OtherProps> {
    private hourScale = scaleLinear().domain([0, 12]).range([0, 360]);
    private secMinScale = scaleLinear().domain([0, 60]).range([0, 360]);

    private r: number;

    public componentDidMount = () => {
        this.createClock();

        const tick = window.setInterval(this.framed, 1000);
        this.props.setClockTick(tick);

        this.framed();
    }

    public componentWillUnmount = () => {
        const { setClockTick, clockTick } = this.props;

        if (clockTick)
            window.clearInterval(clockTick);
        setClockTick(undefined);
    }

    public render = () => {
        const { svgNode } = this.props;
        const { classes } = this.props as any;

        return <svg ref={svgNode} className={classes.clock} width="100px" />;
    }

    private createClock = () => {
        const { svgNode } = this.props;

        if (!svgNode.current) return;

        this.r = svgNode.current.clientWidth / 2;
        const r = this.r;

        const pointersRelDimensions = [
            { class: 'hour', width: 0.1, height: 0.55 },
            { class: 'min', width: 0.1, height: 0.90 },
        ];

        // Size canvas
        const svg = select(svgNode.current)
            .attr('width', r * 2)
            .attr('height', r * 2)
            .attr('viewBox', `${-r} ${-r} ${r*2} ${r*2}`);

        // Add background
        svg.append('circle').classed('background', true)
            .attr('cx', 0)
            .attr('cy', 0)
            .attr('r', r);

        // Add pointers
        svg.append('g').classed('pointers', true)
            .attr('transform', `scale(${r})`)
            .selectAll('rect')
            .data(pointersRelDimensions)
            .enter()
            .append('rect')
            .attr('class', d=> d.class)
            .attr('x', d => -d.width/2)
            .attr('y', d => -d.height + d.width/2)
            .attr('width', d => d.width)
            .attr('height', d => d.height)
        ;
    }

    private framed = () => {
        const { svgNode } = this.props;

        if (!svgNode.current)
            return;

        const dt = new Date();
        const { timeOffset } = this.props;

        const ms = dt.getMilliseconds();
        const secs = dt.getSeconds() + ms/1000 + timeOffset%60;
        const mins = dt.getMinutes() + secs/60 + (timeOffset/60)%60;
        const hours = dt.getHours()%12 + mins/60 + timeOffset/3600;

        const svg = select(svgNode.current);

        svg.select('.pointers .hour').attr('transform', `rotate(${this.hourScale(hours)})`);
        svg.select('.pointers .min').attr('transform', `rotate(${this.secMinScale(mins)})`);
    }
}

const Clock = compose<OtherProps>(
    withStyles(styles),
    withRef('svgNode', null),
    withState('clockTick', 'setClockTick', undefined),
)(ClockLayout);

export { Clock }