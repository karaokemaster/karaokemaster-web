/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faLightbulbOn, faListOl, faRectangleList } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Chip, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import { Lrc } from 'lrc-kit';
import * as React from 'react';

import { isTrackPlayingSelector, TimedQueueEntry } from 'store/queue';
import { Clock } from './clock';

const styles = (theme: Theme) => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'row',
        gap: theme.spacing(1),
        alignItems: 'center',
    },
    chip: {
        borderRadius: theme.spacing(1),
    },
});

interface OtherProps {
    entry: TimedQueueEntry;
}

interface ComposedChipsProps {
    timeUntil: number,
    hasLyrics: boolean,
    hasLrc: boolean,
    lightingCue: string,
}

class EntryChipsLayout extends React.Component<ComposedChipsProps & OtherProps> {
    public render = () => {
        const { timeUntil, hasLyrics, hasLrc, lightingCue } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.root}>
                <Clock timeOffset={timeUntil} />
                {hasLrc ? <Chip label={<FontAwesomeIcon icon={faListOl} />} color="secondary" className={classes.chip} /> : null}
                {hasLyrics && !hasLrc ? <Chip label={<FontAwesomeIcon icon={faRectangleList} />} color="secondary" className={classes.chip} /> : null}
                {lightingCue && <Chip icon={<FontAwesomeIcon icon={faLightbulbOn} />} label={lightingCue} className={classes.chip} color="secondary" />}
            </div>
        )
    }
}

const EntryChips = compose<OtherProps>(
    withStyles(styles),
    withSelector('playing', (state, { entry }: OtherProps) => isTrackPlayingSelector(state, entry.videoId)),
    withProps({
        hasLyrics: ({ entry: { lyrics }}) => !(lyrics === undefined || lyrics === null || lyrics.trim().length === 0),
        hasLrc: ({ entry: { lyrics }}) => !(lyrics === undefined || lyrics === null || Lrc.parse(lyrics).lyrics.length < 1),
        lightingCue: ({ entry: { lightingCue }}) => lightingCue,
        timeUntil: ({ entry: { timeUntil }}) => timeUntil,
    }),
)(EntryChipsLayout);

export { EntryChips }