/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { List, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withRef, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { renderIf, withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as React from 'react';
import { animateScroll } from 'react-scroll';
import { ItemInterface, ReactSortable } from 'react-sortablejs';

import { Error } from 'components/error';
import { EmptyQueue } from 'components/now-playing/emptyQueue';
import { EditLyricsDialog } from 'components/queue/editLyricsDialog';
import { SetUserDialog } from 'components/queue/setUserDialog';
import { nowPlayingSelector, PlayingQueueEntry, queueConnectionStatusSelector, QueueEntry, queueTimestampSelector, TimedQueueEntry, timedQueueSelector } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus } from 'store/signalR';
import { scrollPositionSelector } from 'store/user';
import * as userActions from 'store/user/actions';
import { Entry } from './entry';
import { initialHeight, MiniPlayer } from './miniPlayer';

import 'react-swipeable-list/dist/styles.css';

const headingHeight = 54;
const headingPadding = 10;

const styles = (theme: Theme) => createStyles({
    container: {
        paddingTop: initialHeight + headingHeight + (headingPadding * 2),
        userSelect: 'none',
        paddingBottom: '1.5em',

        '&.notPlaying': {
            paddingTop: headingHeight + (headingPadding * 2),
        },
    },
    heading: {
        '--headingPadding': `${headingPadding}px`,
        position: 'fixed',
        top: 0,
        left: '120px',
        right: 0,
        height: initialHeight + headingHeight,
        backgroundColor: '#000',
        zIndex: 1000,
        margin: 0,
        paddingTop: 'calc(var(--headingPadding) + env(safe-area-inset-top))',
        paddingBottom: headingPadding,
        userSelect: 'none',

        '.notPlaying &': {
            height: headingHeight,
        },
    },
    headingText: {
        position: "absolute",
        bottom: "10px",
        left: "10px",
        right: "10px",
        margin: 0,
        fontSize: "24px",
        fontWeight: "bold",
        borderTop: "1px solid #ffff",
        userSelect: 'none',

        '.notPlaying &': {
            borderTop: 0,
        },
    },
    listRoot: {
        '& > div': {
            borderBottom: '1px solid #333',
        },
        '& > div:last-child': {
            borderBottom: 0,
        },
    },
    sortableGhost: {
        opacity: 0.3,
    },
    sortableDrag: {
        backgroundColor: 'rgba(90, 90, 90, 0.5)',
        borderBottom: '0 !important',
    },
});

interface PropsFromState {
    queueConnectionStatus: ConnectionStatus;
    nowPlaying?: PlayingQueueEntry;
    timedQueue: TimedQueueEntry[];
    upNext: TimedQueueEntry[];
    queueTimestamp: number;
    locationKey?: string | undefined;

    editLyricsEntry?: QueueEntry;
    setUserEntry?: QueueEntry;
    setEditLyricsEntry: (entry?: QueueEntry) => void,
    setSetUserEntry: (entry?: QueueEntry) => void,

    upNextList: QueueItem[];
    setUpNextList: (upNextList: QueueItem[]) => void,
}

interface PropsFromDispatch {
    moveQueueEntry: typeof queueActions.move;
}

type QueueProps = PropsFromState & PropsFromDispatch;

type QueueItem = QueueEntry & ItemInterface;

class QueueLayout extends React.Component<QueueProps> {
    public render = () => {
        const { queueConnectionStatus, nowPlaying, upNext, upNextList, editLyricsEntry, setUserEntry } = this.props;
        const { classes } = (this.props as any);

        const containerClasses = classNames({
            [classes.container]: true,
            'notPlaying': nowPlaying === undefined,
        });

        const queueDisabled = (queueConnectionStatus !== ConnectionStatus.Connected);

        return (
            <div className={containerClasses}>
                {nowPlaying &&
                    <ErrorBoundary fallback={<Error />}>
                        <MiniPlayer entry={nowPlaying} />
                    </ErrorBoundary>
                }

                <ErrorBoundary fallback={<Error />}>
                    <Typography component="div" className={classes.heading}>
                        <Typography component="h2" className={classes.headingText}>Up Next</Typography>
                    </Typography>

                    <List>
                        <ReactSortable
                            disabled={queueDisabled}
                            list={upNextList}
                            setList={this.setList}
                            delay={100}
                            ghostClass={classes.sortableGhost}
                            dragClass={classes.sortableDrag}
                            className={classes.listRoot}
                        >
                            {upNext.map(e => <Entry entry={e} key={e.queueId} editLyricsDialog={this.editLyricsDialog} setUserDialog={this.setUserDialog} moveNext={this.moveNext} />)}
                        </ReactSortable>
                    </List>
                </ErrorBoundary>

                {editLyricsEntry &&
                    <ErrorBoundary fallback={<Error />}>
                        <EditLyricsDialog entry={editLyricsEntry} onClose={this.closeEditLyricsDialog} />
                    </ErrorBoundary>
                }
                {setUserEntry &&
                    <ErrorBoundary fallback={<Error />}>
                        <SetUserDialog entry={setUserEntry} onClose={this.closeSetUserDialog} />
                    </ErrorBoundary>
                }
            </div>
        )
    }

    private setList = (list: QueueItem[]) => {
        const { setUpNextList, moveQueueEntry, upNextList } = this.props;

        // TODO: Consolidate w/ compoents/now-playing/index
        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'emcee:queue:setList'});
            s.setSpan(transaction);

            const currentList = [...upNextList];
            setUpNextList(list);

            try {
                const moves = list
                    .map((l, i) => ({
                        item: l,
                        from: currentList.findIndex(c => c.id === l.id),
                        to: i,
                    }))
                    .filter(i => i.from !== i.to)
                ;

                if (moves.length === 0) {
                    transaction.finish();
                    return;
                }

                let move: any;

                if (moves.length > 2 && Math.abs(moves[0].from - moves[0].to) === 1) {
                    move = moves[moves.length - 1];
                } else {
                    move = moves[0];
                }

                const maxIndex = Math.min(list.length, currentList.length) - 1;
                const movedItem: QueueItem = move.item;
                const prevItemId = move.to > 0 ? list[move.to - 1].queueId : undefined;
                const nextItemId = move.to < maxIndex ? list[move.to + 1].queueId : undefined;

                moveQueueEntry(movedItem, prevItemId, nextItemId);
            } catch (ex) {
                // Something went wrong, so we should reverse our state change...
                setUpNextList(currentList);
                Sentry.captureException(ex);
            }

            transaction.finish();
        });
    }

    private moveNext = (entry: QueueEntry) => {
        const { upNextList } = this.props;

        const entryItem = upNextList.find(e => e.queueId === entry.queueId);
        if (!entryItem) return;

        const newQueue = [
            entryItem,
            ...upNextList.filter(e => e.queueId !== entry.queueId),
        ];

        this.setList(newQueue);
    }

    private editLyricsDialog = (entry: QueueEntry) => {
        this.props.setEditLyricsEntry(entry);
    }

    private closeEditLyricsDialog = () => {
        this.props.setEditLyricsEntry(undefined);
    }

    private setUserDialog = (entry: QueueEntry) => {
        this.props.setSetUserEntry(entry);
    }

    private closeSetUserDialog = () => {
        this.props.setSetUserEntry(undefined);
    }
}

const getUpNextList = (upNext: TimedQueueEntry[]) =>
    upNext
        .map(e => ({
            id: e.queueId,
            ...e,
        }));

const Queue = compose(
    withStyles(styles),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withSelector('queueTimestamp', queueTimestampSelector),
    withSelector('nowPlaying', nowPlayingSelector),
    withSelector('timedQueue', timedQueueSelector),
    withAction('moveQueueEntry', queueActions.move),
    withProps({
        upNext: ({ timedQueue }: QueueProps) => timedQueue.filter(e => e.order !== 0),
    }),

    withState('editLyricsEntry', 'setEditLyricsEntry', undefined),
    withState('setUserEntry', 'setSetUserEntry', undefined),
    withState('upNextList', 'setUpNextList', ({ upNext }: QueueProps) => getUpNextList(upNext)),
    withEffect(
        ({ upNext, setUpNextList }: QueueProps) => setUpNextList(getUpNextList(upNext)),
        [ 'queueTimestamp' ],
    ),
    withRef('firstUpdate', true),
    withEffect(
        ({ firstUpdate }: any) => { if (firstUpdate.current) { firstUpdate.current = false; return; } animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
    renderIf(({ nowPlaying, upNext, upNextList }) => (!nowPlaying && upNext.length < 1 || (upNext.length !== upNextList.length)), EmptyQueue)
)(QueueLayout);

export { Queue };