/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import classNames from 'classnames';
import * as React from 'react';

import { runtimeConfig } from 'runtimeConfig';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: '120px', // TODO: Source this value from layout/tabs
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(3),
        overflowX: 'hidden',
        overflowY: 'hidden',
    },
    codes: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        minHeight: '100%',
        textAlign: 'center',
    },
    panel: {
        width: '50%',
        height: '25%',
        backgroundColor: '#fff',
        color: '#000',
        borderRadius: '1em',

        '& img': {
            width: '300px',
            height: '300px',
        },
    },
});

class CodesLayout extends React.Component {
    public render() {
        const { classes } = this.props as any;
        const config = runtimeConfig().client;

        return (
            <div className={classNames(classes.root)}>
                <div className={classNames(classes.codes)}>
                    <div className={classNames(classes.panel)}>
                        <Typography variant="h3">1. Join the WiFi</Typography>
                        <img src={`${config.apiUrlBase}/v1/Asset/WiFiNetworkQrCode`} />
                    </div>
                    <div className={classNames(classes.panel)}>
                        <Typography variant="h3">2. Add some tracks</Typography>
                        <img src={`${config.apiUrlBase}/v1/Asset/PublicUrlQrCode`} />
                    </div>
                </div>
            </div>
        )
    }
}

const Codes = compose(
    withStyles(styles),
)(CodesLayout);

export { Codes };