/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faClockRotateLeft, faLightbulbOn, faListMusic, faMagnifyingGlass, faMicrophoneStand, faQrcode, faUsersGear } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Tab, Tabs, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withLocation } from "@truefit/bach-react-router";
import { withAction } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import { Location } from 'history';
import * as React from 'react';
import { push } from 'redux-first-history';

import { MicStatus } from './microphones/mic-status';

const emceeBase = '/emcee';

const styles = (theme: Theme) => createStyles({
    navigation: {
        position: "fixed",
        left: 0,
        top: 0,
        bottom: 0,
        width: '100px',
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.primary.main,
    },
    tab: {
        padding: theme.spacing(3),
        minWidth: 'auto',
    }
});

interface PropsFromState {
    location: Location;
}

interface PropsFromDispatch {
    push: typeof push;
}

type NavTabsProps = PropsFromState & PropsFromDispatch;

class NavTabsLayout extends React.Component<NavTabsProps> {
    public render() {
        const { classes } = (this.props as any);

        return (
            <Tabs
                value={this.props.location.pathname}
                onChange={this.onChange}
                className={classes.navigation}
                orientation="vertical"
            >
                <Tab
                    icon={<FontAwesomeIcon icon={faListMusic} size="2x" />}
                    value={`${emceeBase}/queue`}
                    className={classes.tab}
                />
                <Tab
                    icon={<FontAwesomeIcon icon={faMagnifyingGlass} size="2x" />}
                    value={`${emceeBase}/search`}
                    className={classes.tab}
                />
                <Tab
                    icon={<FontAwesomeIcon icon={faClockRotateLeft} size="2x" />}
                    value={`${emceeBase}/history`}
                    className={classes.tab}
                />
                <Tab
                    icon={<FontAwesomeIcon icon={faLightbulbOn} size="2x" />}
                    value={`${emceeBase}/lighting`}
                    className={classes.tab}
                />
                <Tab
                    icon={<FontAwesomeIcon icon={faMicrophoneStand} size="2x" />}
                    value={`${emceeBase}/microphones`}
                    className={classes.tab}
                />
                <Tab
                    icon={<FontAwesomeIcon icon={faUsersGear} size="2x" />}
                    value={`${emceeBase}/users`}
                    className={classes.tab}
                />
                <Tab
                    icon={<FontAwesomeIcon icon={faQrcode} size="2x" />}
                    value={`${emceeBase}/codes`}
                    className={classes.tab}
                />
                <MicStatus />
            </Tabs>
        );
    }

    private onChange = (event: React.ChangeEvent<{}>, value: any) => {
        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'changeTab', description: value});
            s.setSpan(transaction);

            this.props.push(value);

            transaction.finish();
        });
    }
}

const NavTabs = compose(
    withStyles(styles),
    withLocation(),
    withAction('push', push),
)(NavTabsLayout);

export { NavTabs }