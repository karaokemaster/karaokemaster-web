/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faLightbulbOn } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Chip, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { isTrackInQueueSelector, isTrackPlayingSelector } from 'store/queue';
import { QueueEntry } from 'store/queue';

const styles = (theme: Theme) => createStyles({
    chip: {
        borderRadius: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
});

interface OtherProps {
    entry: QueueEntry;
}

interface ComposedChipsProps {
    playing: boolean,
    isInQueue: boolean;
}

class HistoryItemChipsLayout extends React.Component<OtherProps & ComposedChipsProps> {
    public render = () => {
        const { isInQueue, playing, entry: { lightingCue, user, userIcon, userColor }} = this.props;
        const { classes } = (this.props as any);

        const icon = findIconDefinition(JSON.parse(userIcon || ""));

        return (
            <React.Fragment>
                {playing && <Chip label="Playing" color="primary" className={classes.chip} />}
                <Chip icon={<FontAwesomeIcon icon={icon} fixedWidth={true} />} label={user} variant="outlined" className={classes.chip} style={{ backgroundColor: userColor }} />
                {isInQueue && !playing && <Chip label="In Queue" variant="outlined" className={classes.chip} />}
                {lightingCue && <Chip icon={<FontAwesomeIcon icon={faLightbulbOn} />} label={lightingCue} className={classes.chip} color="secondary" />}
            </React.Fragment>
        )
    }
}

const HistoryItemChips = compose<OtherProps>(
    withStyles(styles),
    withSelector('playing', (state, { entry: { videoId } }: OtherProps) => isTrackPlayingSelector(state, videoId)),
    withSelector('isInQueue', (state, { entry: { videoId } }: OtherProps) => isTrackInQueueSelector(state, videoId)),
)(HistoryItemChipsLayout);

export { HistoryItemChips }