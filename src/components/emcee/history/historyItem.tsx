/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faListOl, faPlus, faRectangleList } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Avatar, CircularProgress, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, Theme } from '@mui/material';
import { blue, green } from '@mui/material/colors'
import Fab from '@mui/material/Fab'
import { createStyles } from '@mui/styles';
import * as Sentry from '@sentry/browser';
import { compose, withEffect, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import classNames from 'classnames';
import * as he from 'he';
import { Lrc } from 'lrc-kit';
import * as React from 'react';

import { Checkmark } from 'components/checkmark';
import { hasTrackBeenPlayedSelector, isTrackInQueueSelector, isTrackPlayingSelector, QueueEntry } from 'store/queue';
import * as queueActions from 'store/queue/actions';
import { ConnectionStatus, hubConnectionStatusSelector } from 'store/signalR';
import { Video } from 'store/videos';
import { HistoryItemChips } from './historyItemChips';

const styles = (theme: Theme) => createStyles({
    root: {
        width: '100%',
        userSelect: 'none',
        '-webkit-touch-callout': 'none',
        flexGrow: 1,
    },

    action: {
        height: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        userSelect: "none",
        padding: "8px",
        fontSize: "10px",
        fontWeight: 500,
        boxSizing: "border-box",
        color: "#eee",
        whiteSpace: "nowrap",
    },
    button: {
        '&:hover': {
            backgroundColor: blue[700],
        },
        backgroundColor: blue[500],
        color: "#fff",
        padding: 4,
    },
    buttonSuccess: {
        '&:hover': {
            backgroundColor: green[700],
        },
        backgroundColor: green[500],
        color: "#fff",
        padding: 4,
    },
    fabProgress: {
        color: green[500],
        left: -3,
        position: 'absolute',
        top: -3,
        zIndex: 1,
    },
    wrapper: {
        display: 'inline-block',
        margin: theme.spacing(),
        position: 'relative',
        whiteSpace: 'nowrap',
        flexDirection: 'column',
    },
});

interface PropsFromState {
    connectionStatus: ConnectionStatus,
    entry: QueueEntry,
    video: Video,
    isInQueue: boolean,
    playing: boolean,
    hasBeenPlayed: boolean,
    hasLyrics: boolean,
    hasLrc: boolean,

    waiting: boolean;
    animate: boolean;
    setWaiting: (waiting: boolean) => void;
    setAnimate: (animate: boolean) => void;
}

interface PropsFromDispatch {
    enqueueForUser: typeof queueActions.enqueueForUser;
}

interface OtherProps {
    entry: QueueEntry;
    editLyricsDialog: (entry: QueueEntry) => void;
}

type HistoryItemProps = PropsFromState & PropsFromDispatch;

class HistoryItemLayout extends React.Component<HistoryItemProps & OtherProps> {
    public render = () => {
        const { isInQueue, playing, waiting, hasLyrics, hasLrc, animate, connectionStatus, entry, entry: { thumbnailUrl, title } } = this.props;
        const { classes } = (this.props as any);

        const success = (isInQueue || playing);
        const disabled = (connectionStatus !== ConnectionStatus.Connected);

        // TODO: Use chips for the secondary
        return (
            <ListItem
                className={isInQueue ? "in-queue" : ""}
                ContainerComponent="div"
                ContainerProps={{ className: classes.root }}
            >
                <ListItemAvatar>
                    <Avatar src={thumbnailUrl} variant={"square"} />
                </ListItemAvatar>
                <ListItemText disableTypography={true} primary={<div>{he.decode(title)}</div>} secondary={<HistoryItemChips entry={entry} />} />
                <ListItemSecondaryAction className={classes.action}>
                    <Fab size="small" color="primary" className={classNames({ [classes.button]: true, [classes.buttonSuccess]: hasLyrics })} onClick={this.editLyrics} disabled={disabled}>
                        <FontAwesomeIcon icon={hasLrc ? faListOl : faRectangleList}/>
                    </Fab>
                    <div className={classes.wrapper}>
                        {success
                            ? <Checkmark animate={animate} className={classes.button} />
                            : <Fab size="small" color="primary" className={classes.button} onClick={this.addToQueue} disabled={disabled}>
                                <FontAwesomeIcon icon={faPlus}/>
                            </Fab>
                        }
                        {waiting && <CircularProgress size={46} className={classes.fabProgress} />}
                    </div>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }

    private addToQueue = async () => {
        const { enqueueForUser, entry, video, setWaiting } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({ name: 'emcee:searchResult:addToQueue' });
            s.setSpan(transaction);

            setWaiting(true);
            enqueueForUser(entry.user, video);

            transaction.finish();
        })
    }

    private editLyrics = async () => {
        const { entry, editLyricsDialog } = this.props;

        editLyricsDialog(entry);
    }
}

const HistoryItem = compose<OtherProps>(
    withStyles(styles),
    withSelector('connectionStatus', hubConnectionStatusSelector),
    withSelector('playing', (state, { entry }: OtherProps) => isTrackPlayingSelector(state, entry.videoId)),
    withSelector('isInQueue', (state, { entry }: OtherProps) => isTrackInQueueSelector(state, entry.videoId)),
    withSelector('hasBeenPlayed', (state, { entry: { videoId } }: OtherProps) => hasTrackBeenPlayedSelector(state, videoId)),
    withAction('enqueueForUser', queueActions.enqueueForUser),
    withState('waiting', 'setWaiting', false),
    withState('animate', 'setAnimate', false),
    withProps({
        video: ({ entry }: HistoryItemProps) => ({ ...entry, hasLyrics: !!entry.lyrics, playCount: 0 }),
        hasLyrics: ({ entry: { lyrics }}) => !(lyrics === undefined || lyrics === null || lyrics.trim().length === 0),
        hasLrc: ({ entry: { lyrics }}) => !(lyrics === undefined || lyrics === null || Lrc.parse(lyrics).lyrics.length < 1),
    }),
    withEffect(({ waiting, playing, isInQueue, setWaiting, setAnimate }: HistoryItemProps) => {
            if (!waiting || !(isInQueue || playing)) return;

            setWaiting(false);
            setAnimate(true);
        },
        [ 'isInQueue', 'playing' ]),
    renderIf(({ hasBeenPlayed, hideIfPlayed }) => hasBeenPlayed && hideIfPlayed, compose(renderNothing())(React.Component)),
)(HistoryItemLayout);

export { HistoryItem }
