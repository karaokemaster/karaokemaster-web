/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { List, Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { ErrorBoundary } from '@sentry/react';
import { compose, withEffect, withState } from '@truefit/bach';
import { withLocation } from '@truefit/bach-react-router';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import { withStyles } from 'enhancers/bach-mui';
import * as React from 'react';
import { animateScroll } from 'react-scroll';

import { Error } from 'components/error';
import { EditLyricsDialog } from 'components/queue/editLyricsDialog';
import { playedVideosSelector } from 'store/emcee';
import { QueueEntry } from 'store/queue';
import * as userActions from 'store/user/actions';
import { HistoryItem } from './historyItem';

import 'react-swipeable-list/dist/styles.css';

const styles = (theme: Theme) => createStyles({
    container: {
        paddingTop: 0,
        userSelect: 'none',
        paddingBottom: '1.5em',
    },
});

interface PropsFromState {
    played: QueueEntry[];
    locationKey?: string | undefined;
    scrollPosition: number;

    editLyricsEntry?: QueueEntry;
    setEditLyricsEntry: (entry?: QueueEntry) => void,
}

interface PropsFromDispatch {
    setScrollPosition: typeof userActions.setScrollPosition;
}

type HistoryProps = PropsFromState & PropsFromDispatch;

class HistoryLayout extends React.Component<HistoryProps> {
    public render = () => {
        const { played, editLyricsEntry } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.container}>
                <ErrorBoundary fallback={<Error />}>
                    <List>
                        {played.map(p => <HistoryItem key={p.videoId} entry={p} editLyricsDialog={this.editLyricsDialog} /> )}
                    </List>
                </ErrorBoundary>

                {editLyricsEntry &&
                    <ErrorBoundary fallback={<Error />}>
                        <EditLyricsDialog entry={editLyricsEntry} onClose={this.closeEditLyricsDialog} />
                    </ErrorBoundary>
                }
            </div>
        )
    }

    private editLyricsDialog = (entry: QueueEntry) => {
        this.props.setEditLyricsEntry(entry);
    }

    private closeEditLyricsDialog = () => {
        this.props.setEditLyricsEntry(undefined);
    }
}

const History = compose(
    withStyles(styles),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withSelector('played', playedVideosSelector),

    withState('editLyricsEntry', 'setEditLyricsEntry', undefined),
    withEffect(
        () => { animateScroll.scrollToTop(); },
        [ 'locationKey' ],
    ),
)(HistoryLayout);

export { History };