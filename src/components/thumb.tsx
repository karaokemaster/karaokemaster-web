/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles, withTheme } from 'enhancers/bach-mui';
import classNames from 'classnames';
import { replaceColor } from 'lottie-colorify';
import { useLottie } from 'lottie-react';
import * as React from 'react';

import { Color } from 'services/Color';

import thumbAnimation from 'styles/thumbUp.json';

const styles = (theme: Theme) => createStyles({
    thumb: {
        transformOrigin: '50% 50%',
        margin: '-7px -4px',
    },
    down: {
        transform: 'scaleY(-1)',
    },
});

interface PropsFromState {
    theme: Theme;
}

type ThumbProps = PropsFromState;

interface OtherProps {
    animate?: boolean;
    selected?: boolean;
    disabled?: boolean;
    down?: boolean;
}

const ThumbLayout: React.FC<ThumbProps & OtherProps> = (props) => {
    const { animate, selected, disabled, down, theme } = props;
    const { classes } = props as any;

    const thumbClasses = classNames({
        [classes.thumb]: true,
        [classes.selected]: selected,
        [classes.down]: down,
    });

    const bgColor = new Color(theme.palette.common.black);
    const color = selected
        ? bgColor.toHex()
        : (disabled
            ? new Color(theme.palette.action.disabled).toHex(bgColor)
            : new Color(theme.palette.text.primary).toHex(bgColor));

    const data = replaceColor('#000000', color, thumbAnimation);

    let options: any = {
        animationData: data,
        className: thumbClasses,
    };

    if (selected && animate)
        options = { ...options, autoplay: true, loop: false }
    else if (selected)
        options = { ...options, autoplay: false, loop: false }
    else
        options = { ...options, autoplay: false, loop: false }

    const lottie = useLottie(options);

    if (selected && !animate)
        lottie.goToAndStop((lottie.getDuration(true) ?? 1) - 1, true);

    return lottie.View;
}

const Thumb = React.memo(compose<OtherProps>(
    withStyles(styles),
    withTheme(),
)(ThumbLayout));

export { Thumb };