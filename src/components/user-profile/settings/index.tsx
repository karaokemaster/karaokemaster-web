/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faUserCrown } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Card, CardActions, CardContent, createStyles, Theme, Typography } from '@mui/material';
import { red } from '@mui/material/colors';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';
import { push } from 'redux-first-history';

import { updateErrorSelector, userProfileSelector, UserProfileState } from 'store/userProfile';
import * as userProfileActions from 'store/userProfile/actions';
import { Color } from './colorPicker';
import { EditColorDialog } from './editColorDialog';
import { EditIconDialog } from './editIconDialog';
import { EditUsernameDialog } from './editUsernameDialog';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
    card: {
        width: '100%',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    title: {
        fontSize: 14,
    },
    error: {
        color: red[500],
    },
    icon: {
        textAlign: "center",
    },
    color: {
        textAlign: "center",
    },
    actions: {
        flexDirection: "row",
        justifyContent: "flex-end",
    },
});

interface PropsFromState {
    user: UserProfileState,
    usernameError?: string,

    changeUsernameOpen: boolean,
    changeIconOpen: boolean,
    changeColorOpen: boolean
    setChangeUsernameOpen: (value: boolean) => void,
    setChangeIconOpen: (value: boolean) => void,
    setChangeColorOpen: (value: boolean) => void
}

interface PropsFromDispatch {
    logout: typeof userProfileActions.logout;
    push: typeof push;
}

type UserSettingsProps = PropsFromState & PropsFromDispatch;

class UserSettingsLayout extends React.Component<UserSettingsProps> {
    public render = () => {
        const { user, changeUsernameOpen, changeIconOpen, changeColorOpen, usernameError } = this.props;
        const { classes } = (this.props as any);

        let icon = faUserCrown;
        try {
            icon = findIconDefinition(JSON.parse(user.icon || ""));
        } catch {}

        // TODO: Only show the save button when there's been a change!
        
        return (
            <div className={classes.root}>
                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Username
                        </Typography>
                        {usernameError &&
                            <Typography className={classes.error} variant="caption" component="p">
                                {usernameError}
                            </Typography>
                        }
                        <Typography variant="h6" component="h2">
                            {user.username}
                        </Typography>
                        <Typography variant="caption" component="p">
                            This is the name we'll use for your requested tracks. We'll also track your history under
                            this name to give you better recommendations, so be sure to use it in the future.
                        </Typography>
                    </CardContent>
                    <CardActions className={classes.actions}>
                        <Button size="small" onClick={this.logout}>Log Out</Button>
                        <Button size="small" onClick={this.openChangeUsername}>Change Name</Button>
                    </CardActions>
                </Card>

                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Icon
                        </Typography>
                        <Typography variant="h6" component="h2" className={classes.icon}>
                            <FontAwesomeIcon icon={icon} size="2x" />
                        </Typography>
                    </CardContent>
                    <CardActions className={classes.actions}>
                        <Button size="small" onClick={this.openChangeIcon}>Change Icon</Button>
                    </CardActions>
                </Card>

                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Color
                        </Typography>
                        <Typography variant="h6" component="h2" className={classes.color}>
                            {user.color && <Color color={user.color} selectedColor={undefined} onClick={_ => { this.openChangeColor(); }} />}
                        </Typography>
                    </CardContent>
                    <CardActions className={classes.actions}>
                        <Button size="small" onClick={this.openChangeColor}>Change Color</Button>
                    </CardActions>
                </Card>

                <EditUsernameDialog open={changeUsernameOpen} onClose={this.closeChangeUsername} />
                <EditIconDialog open={changeIconOpen} onClose={this.closeChangeIcon} />
                <EditColorDialog open={changeColorOpen} onClose={this.closeChangeColor} />
            </div>
        )
    }

    private openChangeUsername = () => {
        this.props.setChangeUsernameOpen(true);
    }

    private closeChangeUsername = () => {
        this.props.setChangeUsernameOpen(false);
    }

    private openChangeIcon = () => {
        this.props.setChangeIconOpen(true);
    }

    private closeChangeIcon = () => {
        this.props.setChangeIconOpen(false);
    }

    private openChangeColor = () => {
        this.props.setChangeColorOpen(true);
    }

    private closeChangeColor = () => {
        this.props.setChangeColorOpen(false);
    }

    private logout = () => {
        // TODO: Confirm
        this.props.logout();
        this.props.push('/');
    }
}

const UserSettings = compose(
    withStyles(styles),
    withSelector('user', userProfileSelector),
    withSelector('usernameError', updateErrorSelector('username')),
    withAction('logout', userProfileActions.logout),
    withAction('push', push),
    withState('changeUsernameOpen', 'setChangeUsernameOpen', false),
    withState('changeIconOpen', 'setChangeIconOpen', false),
    withState('changeColorOpen', 'setChangeColorOpen', false),
)(UserSettingsLayout);

export { UserSettings }
