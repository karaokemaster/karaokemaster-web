/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Button, createStyles, Dialog, DialogActions, DialogContent, DialogTitle, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { userColorSelector, usernameSelector } from 'store/userProfile';
import * as userProfileActions from 'store/userProfile/actions';
import { ColorPicker } from './colorPicker';

const styles = (theme: Theme) => createStyles({
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
});

interface PropsFromState {
    username: string;
    color?: string;

    selectedColor?: string;
    setSelectedColor: (color: string | undefined) => void;
}

interface PropsFromDispatch {
    setUserColor: typeof userProfileActions.setUserColor;
}

interface OtherProps {
    open: boolean;
    onClose: () => void;
}

type EditColorProps = PropsFromState & PropsFromDispatch;

class EditColorLayout extends React.Component<EditColorProps & OtherProps> {
    public render = () => {
        const { open, username, selectedColor } = this.props;
        const { classes } = (this.props as any);

        return (
            <Dialog open={open} scroll="paper" onClose={this.handleClose} aria-labelledby="editLyrics-title" className={classes.modal}>
                <DialogTitle id="editLyrics-title">Change Color</DialogTitle>
                <DialogContent>
                    <p id="editLyrics-description">Change color for {username}</p>
                    <ColorPicker color={selectedColor} onChange={this.onColorChange} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button onClick={this.setColor}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onColorChange = (color: string) => {
        this.props.setSelectedColor(color);
    }

    private handleClose = () => {
        const { onClose } = this.props;

        onClose();
    }

    private setColor = async () => {
        const { onClose, selectedColor, setUserColor } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'user:setColor'});
            s.setSpan(transaction);

            setUserColor(selectedColor);

            transaction.finish();
        });

        onClose();
    }
}

const EditColorDialog = compose<OtherProps>(
    withStyles(styles),
    withSelector('username', usernameSelector),
    withSelector('color', userColorSelector),
    withAction('setUserColor', userProfileActions.setUserColor),
    withState<EditColorProps, string | undefined>('selectedColor', 'setSelectedColor', (props) => props.color),
)(EditColorLayout);

export { EditColorDialog }
