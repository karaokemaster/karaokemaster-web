/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Button, createStyles, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { usernameSelector } from 'store/userProfile';
import * as userProfileActions from 'store/userProfile/actions';

const styles = (theme: Theme) => createStyles({
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    userInput: {
        '& input': {
            userSelect: 'auto',
        },
    },
});

interface PropsFromState {
    username?: string;

    newUsername?: string;
    setNewUsername: (username: string | undefined) => void;
}

interface PropsFromDispatch {
    changeUsername: typeof userProfileActions.changeUsername;
}

interface OtherProps {
    open: boolean;
    onClose: () => void;
}

type EditUsernameProps = PropsFromState & PropsFromDispatch;

class EditUsernameLayout extends React.Component<EditUsernameProps & OtherProps> {
    public render = () => {
        const { open, username } = this.props;
        const { classes } = (this.props as any);

        return (
            <Dialog open={open} scroll="paper" onClose={this.handleClose} aria-labelledby="editLyrics-title" className={classes.modal}>
                <DialogTitle id="editLyrics-title">Change Username</DialogTitle>
                <DialogContent>
                    <p id="editLyrics-description">Change username for {username}</p>
                    <TextField
                        variant="standard"
                        fullWidth={true}
                        defaultValue={username}
                        onChange={this.onUsernameChange}
                        onKeyPress={this.usernameKeyPress}
                        color="secondary"
                        className={classes.userInput} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button onClick={this.setUsername}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onUsernameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const username = e.target.value;

        this.props.setNewUsername(username);
    }

    public usernameKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        // e.keyCode doesn't work in keyPress
        if (e.which !== 13) { return; }

        e.preventDefault();

        this.setUsername();
    }

    private handleClose = () => {
        const { onClose } = this.props;

        onClose();
    }

    private setUsername = async () => {
        const { onClose, newUsername, changeUsername } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'user:changeUsername'});
            s.setSpan(transaction);

            if (newUsername && newUsername.trim().length > 1)
                changeUsername(newUsername.trim());

            transaction.finish();
        });

        onClose();
    }
}

const EditUsernameDialog = compose<OtherProps>(
    withStyles(styles),
    withSelector('username', usernameSelector),
    withAction('changeUsername', userProfileActions.changeUsername),
    withState<EditUsernameProps, string | undefined>('newUsername', 'setNewUsername', (props) => props.username),
)(EditUsernameLayout);

export { EditUsernameDialog }
