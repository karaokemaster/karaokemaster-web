/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { IconLookup } from '@fortawesome/fontawesome-svg-core';
import { Button, createStyles, Dialog, DialogActions, DialogContent, DialogTitle, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { compose, withState } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { userIconSelector, usernameSelector } from 'store/userProfile';
import * as userProfileActions from 'store/userProfile/actions';
import { IconPicker } from './iconPicker';

const styles = (theme: Theme) => createStyles({
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
});

interface PropsFromState {
    username: string;
    icon?: string;

    selectedIcon?: IconLookup;
    setSelectedIcon: (icon: IconLookup | undefined) => void;
}

interface PropsFromDispatch {
    setUserIcon: typeof userProfileActions.setUserIcon;
}

interface OtherProps {
    open: boolean;
    onClose: () => void;
}

type EditIconProps = PropsFromState & PropsFromDispatch;

class EditIconLayout extends React.Component<EditIconProps & OtherProps> {
    public render = () => {
        const { open, username, selectedIcon } = this.props;
        const { classes } = (this.props as any);

        return (
            <Dialog open={open} scroll="paper" onClose={this.handleClose} aria-labelledby="editLyrics-title" className={classes.modal}>
                <DialogTitle id="editLyrics-title">Change Icon</DialogTitle>
                <DialogContent>
                    <p id="editLyrics-description">Change icon for {username}</p>
                    <IconPicker icon={selectedIcon} onChange={this.onIconChange} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button onClick={this.setIcon}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    private onIconChange = (icon: IconLookup) => {
        const { setSelectedIcon } = this.props;
        setSelectedIcon(icon);
    }

    private handleClose = () => {
        const { onClose } = this.props;

        onClose();
    }

    private setIcon = async () => {
        const { onClose, selectedIcon, setUserIcon } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'user:setIcon'});
            s.setSpan(transaction);

            setUserIcon(selectedIcon);

            transaction.finish();
        });

        onClose();
    }
}

const EditIconDialog = compose<OtherProps>(
    withStyles(styles),
    withSelector('username', usernameSelector),
    withSelector('icon', userIconSelector),
    withAction('setUserIcon', userProfileActions.setUserIcon),
    withState<EditIconProps, IconLookup | undefined>('selectedIcon', 'setSelectedIcon', (props) => (JSON.parse(props.icon || "{}") as IconLookup)),
)(EditIconLayout);

export { EditIconDialog }
