/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { faCheck } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { createStyles, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import classNames from 'classnames';
import * as React from 'react';
import stringHash from 'string-hash';

const styles = (theme: Theme) => createStyles({
    root: {
        lineHeight: 0,
    },
    color: {
        padding: 0,
        margin: 0,
        width: "50px",
        height: "50px",
        lineHeight: "50px",
        verticalAlign: "top",
        display: "inline-block",
        borderStyle: "solid",
        borderWidth: "2px",
        borderColor: "transparent",
    },
    selected: {
        borderColor: "#fff",
    },
});

const colors: string[] = [
    "#f44336",
    "#e91e63",
    "#9c27b0",
    "#673ab7",

    "#3f51b5",
    "#2196f3",
    "#03a9f4",
    "#00bcd4",

    "#009688",
    "#4caf50",
    "#8bc34a",
    "#cddc39",

    "#ffeb3b",
    "#ffc107",
    "#ff9800",
    "#ff5722",
];

export const defaultColor = (username: string): string => {
    const hash = stringHash(username);
    const ix = hash % colors.length;

    return colors[ix];
}

interface OtherProps {
    color?: string;
    savedColor?: string;
    onChange: (color: string) => void;
}

class ColorPickerLayout extends React.Component<OtherProps> {
    public render = () => {
        const { color, savedColor, onChange } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.root}>
                {colors.map((c, x) => <Color color={c} key={x} selectedColor={color} savedColor={savedColor} onClick={onChange} />)}
            </div>
        )
    }
}

interface ColorProps {
    color: string;
    selectedColor?: string;
    savedColor?: string;
    onClick: (color: string) => void
}

class ColorLayout extends React.Component<ColorProps> {
    public render = () => {
        const { color, selectedColor, savedColor } = this.props;
        const { classes } = (this.props as any);

        const selected = !!(selectedColor &&
            selectedColor === color);
        const saved = !!(savedColor &&
            savedColor === color);

        const colorClasses = classNames({
            [classes.color]: true,
            [classes.selected]: selected,
        });

        return (
            <div className={colorClasses} style={{ backgroundColor: color }} onClick={this.onClick}>
                {saved && <FontAwesomeIcon icon={faCheck} />}
            </div>
        )
    }

    private onClick = () => {
        const { color, onClick } = this.props;

        onClick(color);
    }
}

const Color = compose<ColorProps>(
    withStyles(styles),
)(ColorLayout);

const ColorPicker = compose<OtherProps>(
    withStyles(styles),
)(ColorPickerLayout);

export { ColorPicker, Color };

