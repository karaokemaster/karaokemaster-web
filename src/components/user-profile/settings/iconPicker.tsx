/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { findIconDefinition, IconDefinition, IconLookup, library } from '@fortawesome/fontawesome-svg-core'
import { fad } from '@fortawesome/pro-duotone-svg-icons'
import { fal } from '@fortawesome/pro-light-svg-icons'
import { far } from '@fortawesome/pro-regular-svg-icons'
import { fas } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { createStyles, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import classNames from 'classnames';
import * as React from 'react';
import stringHash from 'string-hash';

library.add(fad)
library.add(fal)
library.add(far)
library.add(fas)

const styles = (theme: Theme) => createStyles({
    root: {
    },
    icon: {
        backgroundColor: 'darkgrey',
        padding: theme.spacing(0.5),
        margin: theme.spacing(0.5),
        borderRadius: theme.spacing(0.5),
    },
    selected: {
        backgroundColor: theme.palette.primary.main,
    },
});

// TODO: Add display order to allow re-sorting w/o affecting defaultIcon results
export const icons: IconLookup[] = [
    { prefix: "fad", iconName: "user-tie" },
    { prefix: "fad", iconName: "user-secret" },
    { prefix: "fad", iconName: "user-nurse" },
    { prefix: "fad", iconName: "user-ninja" },

    { prefix: "fad", iconName: "user-doctor" },
    { prefix: "fad", iconName: "user-injured" },
    { prefix: "fad", iconName: "user-graduate" },
    { prefix: "fad", iconName: "user-visor" },

    { prefix: "fas", iconName: "user-robot" },
    { prefix: "fad", iconName: "user-headset" },
    { prefix: "fad", iconName: "user-helmet-safety" },
    { prefix: "fad", iconName: "user-crown" },

    { prefix: "fad", iconName: "user-cowboy" },
    { prefix: "fad", iconName: "angel" },
    { prefix: "fad", iconName: "baby" },
    { prefix: "fas", iconName: "head-vr" },

    { prefix: "fad", iconName: "luchador" },
    { prefix: "fas", iconName: "snowboarding" },
    { prefix: "fad", iconName: "poo" },
    { prefix: "fad", iconName: "hamsa" },

    { prefix: "far", iconName: "ankh" },
    { prefix: "fad", iconName: "banana" },
    { prefix: "fad", iconName: "beer-mug" },
    { prefix: "fad", iconName: "bowling-ball-pin" },

    { prefix: "fad", iconName: "coffee-beans" },
    { prefix: "far", iconName: "crab" },
    { prefix: "fad", iconName: "donut" },
    { prefix: "fad", iconName: "eye-evil" },

    { prefix: "fad", iconName: "eyes" },
    { prefix: "far", iconName: "anchor" },
    { prefix: "fad", iconName: "award-simple" },
    { prefix: "fad", iconName: "brain-circuit" },

    { prefix: "fad", iconName: "yin-yang" },
    { prefix: "fad", iconName: "chess-knight" },
    { prefix: "fad", iconName: "banjo" },
    { prefix: "fad", iconName: "boombox" },

    { prefix: "fad", iconName: "trophy" },
    { prefix: "fad", iconName: "user-astronaut" },
    { prefix: "fad", iconName: "alien-8bit" },
    { prefix: "fad", iconName: "rocket-launch" },

    { prefix: "fad", iconName: "space-station-moon-alt" },
    { prefix: "fad", iconName: "starship" },
    { prefix: "fad", iconName: "starship-freighter" },
    { prefix: "fad", iconName: "ufo" },

    { prefix: "fad", iconName: "spaghetti-monster-flying" },
    { prefix: "fas", iconName: "badger-honey" },
    { prefix: "fad", iconName: "cat" },
    { prefix: "fad", iconName: "cat-space" },

    { prefix: "fad", iconName: "dog" },
    { prefix: "fad", iconName: "dragon" },
    { prefix: "fad", iconName: "hippo" },
    { prefix: "fad", iconName: "monkey" },

    { prefix: "fad", iconName: "narwhal" },
    { prefix: "fad", iconName: "otter" },
    { prefix: "fad", iconName: "pig" },
    { prefix: "fad", iconName: "snake" },

    { prefix: "fad", iconName: "dove" },
    { prefix: "fad", iconName: "duck" },
    { prefix: "fad", iconName: "elephant" },
    { prefix: "fad", iconName: "frog" },

    { prefix: "fad", iconName: "rabbit" },
    { prefix: "fad", iconName: "alicorn" },
    { prefix: "fad", iconName: "squirrel" },
    { prefix: "fad", iconName: "turtle" },

    { prefix: "fad", iconName: "spider" },
];

export const defaultIcon = (username: string): IconLookup => {
    const hash = stringHash(username);
    const ix = hash % icons.length;

    return icons[ix];
}

interface OtherProps {
    icon?: IconLookup;
    onChange: (icon: IconLookup) => void;
}

class IconPickerLayout extends React.Component<OtherProps> {
    public render = () => {
        const { icon, onChange } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.root}>
                {icons.map((i, x) => <Icon icon={i} key={x} selectedIcon={icon} onClick={onChange} />)}
            </div>
        )
    }
}

interface IconProps {
    icon: IconLookup;
    selectedIcon?: IconLookup;
    onClick: (icon: IconLookup) => void
}

class IconLayout extends React.Component<IconProps> {
    public render = () => {
        const { icon: iconLookup, selectedIcon } = this.props;
        const { classes } = (this.props as any);

        const selected = !!(selectedIcon &&
            selectedIcon.iconName === iconLookup.iconName);

        const icon: IconDefinition = findIconDefinition(iconLookup);

        const iconClasses = classNames({
            [classes.icon]: true,
            [classes.selected]: selected,
        });

        return (
            <FontAwesomeIcon icon={icon} className={iconClasses} size="2x" fixedWidth={true} onClick={this.onClick} />
        )
    }

    private onClick = () => {
        const { icon, onClick } = this.props;

        onClick(icon);
    }
}

const Icon = compose<IconProps>(
    withStyles(styles),
)(IconLayout);

const IconPicker = compose<OtherProps>(
    withStyles(styles),
)(IconPickerLayout);

export { IconPicker };

