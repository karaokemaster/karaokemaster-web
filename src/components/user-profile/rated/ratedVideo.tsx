/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { faTrash } from '@fortawesome/pro-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Box, createStyles, Theme, Typography } from '@mui/material';
import { red } from '@mui/material/colors';
import * as Sentry from '@sentry/browser';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import classNames from 'classnames';
import * as React from 'react';
import { LeadingActions, SwipeableListItem, SwipeAction, TrailingActions, Type as ListType } from 'react-swipeable-list';

import { SearchResult } from 'components/search/searchResult';
import { Video } from 'store/videos';

const styles = (theme: Theme) => createStyles({
    action: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        userSelect: "none",
        padding: "8px",
        fontSize: "10px",
        fontWeight: 500,
        boxSizing: "border-box",
        color: "#eee",
        whiteSpace: "nowrap",
    },
    actionIcon: {
        display: "block",
        fontSize: "2em",
    },
    deleteAction: {
        backgroundColor: red[500],
    },
});

interface OtherProps {
    video: Video;
}

class RatedVideoLayout extends React.Component<OtherProps> {
    public render = () => {
        const { video } = this.props;
        const { classes } = (this.props as any);

        const leadingActions = (
            <LeadingActions>
            </LeadingActions>
        );

        const trailingActions = (
            <TrailingActions>
                <SwipeAction
                    destructive={true}
                    onClick={this.removeRating}
                >
                    <Box className={classNames([classes.action, classes.deleteAction])}>
                        <FontAwesomeIcon icon={faTrash} className={classes.actionIcon} />
                        <Typography>Delete</Typography>
                    </Box>
                </SwipeAction>
            </TrailingActions>
        );

        // TODO: Use chips for the secondary
        return (
            <SwipeableListItem
                leadingActions={leadingActions}
                trailingActions={trailingActions}
                listType={ListType.IOS}
                fullSwipe={true}
            >
                <SearchResult video={video} />
            </SwipeableListItem>
        );
    }

    private removeRating = async () => {
        // const { dequeue, video } = this.props;

        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'ratedVideos:removeRating'});
            s.setSpan(transaction);

            // dequeue(video);

            transaction.finish();
        });
    }
}

const RatedVideo = compose<OtherProps>(
    withStyles(styles),
)(RatedVideoLayout);

export { RatedVideo }