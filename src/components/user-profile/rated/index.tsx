/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { createStyles, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';
import { SwipeableList } from "react-swipeable-list";

import { Loading } from 'components/loading';
import { ratedVideosLoadingSelector, ratedVideosSelector } from 'store/user';
import * as userActions from 'store/user/actions';
import { Video } from 'store/videos';
import { RatedVideo } from './ratedVideo';

// TODO: Consolidate w/ userSettings.tsx
const styles = (theme: Theme) => createStyles({
    root: {
        // width: '100%',
    },
});

interface PropsFromState {
    videos: Video[],
    loading: boolean,
}

interface PropsFromDispatch {
    fetchRatedVideosRequest: typeof userActions.fetchRatedVideosRequest;
}

type RatedVideosProps = PropsFromState & PropsFromDispatch;

class RatedVideosLayout extends React.Component<RatedVideosProps> {
    public componentDidMount() {
        const { fetchRatedVideosRequest } = this.props;

        fetchRatedVideosRequest();
    }

    public render = () => {
        const { videos, loading } = this.props;

        if (loading)
            return <Loading />;

        return (
            <SwipeableList>
                {videos.map(v => (<RatedVideo key={v.videoId} video={v} />))}
            </SwipeableList>
        )
    }
}

const RatedVideos = compose(
    withStyles(styles),
    withSelector('videos', ratedVideosSelector),
    withSelector('loading', ratedVideosLoadingSelector),
    withAction('fetchRatedVideosRequest', userActions.fetchRatedVideosRequest)
)(RatedVideosLayout);

export { RatedVideos }
