/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { List } from '@mui/material';
import { compose, withEffect } from '@truefit/bach';
import { renderIf, withProps } from "@truefit/bach-recompose";
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { Loading } from 'components/loading';
import { EmptyQueue } from 'components/now-playing/emptyQueue';
import { SearchResult } from 'components/search/searchResult';
import { Session, sessionLoadingSelector, todaySessionKey } from 'store/user';
import * as userActions from 'store/user/actions';
import { Video } from 'store/videos';

interface PropsFromState {
    loading: boolean;
    videos: Video[];
}

interface PropsFromDispatch {
    selectSession: typeof userActions.selectSession;
}

interface OtherProps {
    session: Session;
}

type SessionProps = PropsFromState & PropsFromDispatch;

class SessionLayout extends React.Component<SessionProps & OtherProps> {
    public state = { history: [] }

    public render = () => {
        const { videos } = this.props;

        return (
            <List>
                {videos.map(v => <SearchResult key={v.videoId} video={v} />)}
            </List>
        )
    }
}

const HistorySession = compose<OtherProps>(
    withSelector('loading', (state, { session: { date } }: OtherProps) => sessionLoadingSelector(state, date)),
    withProps({
        videos: (props: SessionProps & OtherProps) => props.session.videos || [],
    }),
    withAction('selectSession', userActions.selectSession),
    withEffect(
        ({ loading, videos, selectSession, session }: SessionProps & OtherProps) => {
            if (videos.length === 0 && !loading && session.date !== todaySessionKey)
                selectSession(session.date);
        },
        [ 'videos', 'loading' ],
    ),
    renderIf(({ loading, videos, session: { date } }) => loading || (videos.length === 0 && date !== todaySessionKey), Loading),
    // TODO: Switch to a customized version of the empty queue component
    renderIf(({ videos, session: { date } }) => videos.length === 0 && date === todaySessionKey, EmptyQueue),
)(SessionLayout);

export { HistorySession };
