/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { createStyles, FormControl, InputLabel, Select, Theme } from '@mui/material';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { renderIf, renderNothing, withProps } from "@truefit/bach-recompose";
import { withAction, withSelector } from '@truefit/bach-redux';
import * as React from 'react';

import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { selectedSessionSelector, Session, sessionsSelector, todaySessionKey } from 'store/user';
import * as userActions from 'store/user/actions';
import { tabsHeight } from '../';
import { HistorySession } from './session';

const styles = (theme: Theme) => createStyles({
    root: {
        width: '100%',
        paddingTop: '50px',
    },
    sessionSelect: {
        '--tabsHeight': `${tabsHeight}px`,

        position: 'fixed',
        top: 'calc(var(--tabsHeight) + env(safe-area-inset-top) - 2px)',
        backgroundColor: 'rgba(25,25,25,.25)',
        backdropFilter: 'blur(25px)',
        zIndex: 1100,

        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + var(--tabsHeight) + env(safe-area-inset-top) - 2px)',
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + var(--tabsHeight) + env(safe-area-inset-top) - 2px)',
        },
    },
});

interface SessionEntry {
    name: string,
    date: string,
    key: number,
}

interface PropsFromState {
    selectedSession: Session | undefined,
    sessions: SessionEntry[],
}

interface PropsFromDispatch {
    selectSession: typeof userActions.selectSession;
}

type HistoryProps = PropsFromState & PropsFromDispatch;

class HistoryLayout extends React.Component<HistoryProps> {
    public render = () => {
        const { selectedSession, sessions } = this.props;
        const { classes } = (this.props as any);

        return (
            <div className={classes.root}>
                <FormControl
                    variant="filled"
                    fullWidth={true}
                    color="secondary"
                    className={classes.sessionSelect}
                >
                    <InputLabel>Session</InputLabel>
                    <Select
                        variant="standard"
                        native={true}
                        value={selectedSession?.date}
                        onChange={this.onChange}>
                        {sessions.map(s => <option value={s.date} key={s.key}>{s.name}</option>)}
                    </Select>
                </FormControl>
                {selectedSession && <HistorySession session={selectedSession} key={new Date(selectedSession.date).getTime()} />}
            </div>
        );
    }

    private onChange = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
        const { selectSession } = this.props;

        const value = new Date(event.target.value as string);

        selectSession(value);
    }
}

const History = compose(
    withStyles(styles),
    withSelector('selectedSession', selectedSessionSelector),
    withSelector('_sessions', sessionsSelector),
    withProps({
        sessions: ({ _sessions }) => _sessions.map((s: Session) => ({
            name: s.date === todaySessionKey ? 'Today (all users\' videos)' : `${new Date(s.date).toDateString()} (${s.videoCount} videos)`,
            date: s.date.toString(),
            key: new Date(s.date).getTime()
        })),
    }),
    withAction('selectSession', userActions.selectSession),
    renderIf(({ sessions }) => sessions === undefined, compose(renderNothing())(React.Component)),
)(HistoryLayout);

export { History }
