/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { Card, CardContent, Theme, Typography } from '@mui/material';
import { createStyles } from '@mui/styles';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withProps } from '@truefit/bach-recompose';
import { withSelector } from '@truefit/bach-redux';
import * as moment from 'moment';
import * as React from 'react';

import { ConnectionStatusIcon } from 'components/connectionStatusIcon';
import { notificationsEnabledSelector, ServerInfo, serverInfoSelector } from 'store/app';
import { playerConnectionStatusSelector } from 'store/player';
import { queueConnectionStatusSelector } from 'store/queue';
import { ConnectionStatus, hubConnectionStatusSelector } from 'store/signalR';
import { userProfileSelector, UserProfileState } from 'store/userProfile';

import gitVersion from "gitversion.json";

const styles = (theme: Theme) => createStyles({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
    card: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    title: {
        fontSize: 14,
    },
    release: {
        fontSize: 16,
    },
    info: {
        marginLeft: theme.spacing(2),
    },
});

interface PropsFromState {
    user: UserProfileState;
    serverInfo: ServerInfo;
    hubConnectionStatus: ConnectionStatus;
    playerConnectionStatus: ConnectionStatus;
    queueConnectionStatus: ConnectionStatus;

    notificationsEnabled: boolean | undefined;
    isStandalone: boolean;
    hasNotificationPrompt: boolean;
    supportsAppBadge: boolean;
}

type InfoProps = PropsFromState;

class InfoLayout extends React.Component<InfoProps> {
    public render = () => {
        const { user, serverInfo, hubConnectionStatus, playerConnectionStatus, queueConnectionStatus, notificationsEnabled, isStandalone, hasNotificationPrompt, supportsAppBadge } = this.props;
        const { classes } = (this.props as any);

        const apiDate = moment(serverInfo.apiDate).format('YYYY-MM-DD');

        return (
            <div className={classes.root}>
                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            User
                        </Typography>
                        <Typography variant="h5" component="h2" className={classes.release}>
                            {user.username}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            ID: {user.userId || "unset"}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            Color: {user.color || "unset"}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            Icon: {user.icon || "unset"}
                        </Typography>
                    </CardContent>
                </Card>
                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Status
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            <ConnectionStatusIcon status={hubConnectionStatus} />{" "}
                            <span>Hub: {ConnectionStatus[hubConnectionStatus]}</span>
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            <ConnectionStatusIcon status={playerConnectionStatus} />{" "}
                            <span>Player: {ConnectionStatus[playerConnectionStatus]}</span>
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            <ConnectionStatusIcon status={queueConnectionStatus} />{" "}
                            <span>Queue: {ConnectionStatus[queueConnectionStatus]}</span>
                        </Typography>
                    </CardContent>
                </Card>
                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Client
                        </Typography>
                        <Typography variant="h5" component="h2" className={classes.release}>
                            {`kmweb-${gitVersion.SemVer}`}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            v{gitVersion.SemVer}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            {gitVersion.CommitDate}
                        </Typography>

                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            notificationsEnabled: {notificationsEnabled === undefined ? 'undefined' : (notificationsEnabled ? 'true' : 'false')}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            isStandalone: {isStandalone ? 'true' : 'false'}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            hasNotificationPrompt: {hasNotificationPrompt ? 'true' : 'false'}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            supportsAppBadge: {supportsAppBadge ? 'true' : 'false'}
                        </Typography>
                    </CardContent>
                </Card>
                <Card variant="outlined" className={classes.card}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Server
                        </Typography>
                        <Typography variant="h5" component="h2" className={classes.release}>
                            {serverInfo.apiRelease}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            v{serverInfo.apiVersion}
                        </Typography>
                        <Typography className={classes.info} variant="body2" component="p" color="textSecondary">
                            {apiDate}
                        </Typography>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

const Info = compose(
    withStyles(styles),
    withSelector('user', userProfileSelector),
    withSelector('serverInfo', serverInfoSelector),
    withSelector('hubConnectionStatus', hubConnectionStatusSelector),
    withSelector('playerConnectionStatus', playerConnectionStatusSelector),
    withSelector('queueConnectionStatus', queueConnectionStatusSelector),
    withSelector('notificationsEnabled', notificationsEnabledSelector),
    withProps({
        isStandalone: () => (window.matchMedia('(display-mode: standalone)').matches),
        hasNotificationPrompt: ({ notificationsEnabled }) => (notificationsEnabled === undefined),
        supportsAppBadge: () => 'setAppBadge' in navigator,
    }),
)(InfoLayout);

export { Info }
