/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { AppBar, Box, createStyles, Tab, Tabs, Theme } from '@mui/material';
import * as Sentry from '@sentry/browser';
import { ErrorBoundary } from '@sentry/react';
import { compose } from '@truefit/bach';
import { withStyles } from 'enhancers/bach-mui';
import { withLocation } from '@truefit/bach-react-router';
import { withLifecycle, withProps } from '@truefit/bach-recompose';
import { withAction, withSelector } from '@truefit/bach-redux';
import { Location } from 'history';
import * as React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { animateScroll } from 'react-scroll';
import { push } from 'redux-first-history';

import { Error } from 'components/error';
import { Blur } from 'components/layout/blur';
import { bannerHeight, promptHeight } from 'components/layout/trackBanner';
import { scrollPositionSelector, selectedTabSelector } from 'store/user';
import * as userActions from 'store/user/actions';
import { History } from './history';
import { Info } from './info';
import { RatedVideos } from './rated';
import { UserSettings } from './settings';

const profileBase = '/user-profile';

export const tabsHeight = 50;

const styles = (theme: Theme) => createStyles({
    container: {
        paddingTop: tabsHeight,
        paddingBottom: '56px', // TODO: Source from wherever
    },
    item: {
        width: '100%',
    },
    tabs: {
        top: 'env(safe-area-inset-top)',

        '.track-banner &': {
            '--bannerHeight': `${bannerHeight}px`,
            top: 'calc(var(--bannerHeight) + env(safe-area-inset-top))',
        },

        '.notification-prompt &': {
            '--promptHeight': `${promptHeight}px`,
            top: 'calc(var(--promptHeight) + env(safe-area-inset-top))',
        },
    },
    hidden: {
        display: 'none',
    },
});

const NotFound: React.FunctionComponent = () => (
    <div>Not Found</div>
)

interface PropsFromState {
    location: Location;
    locationKey?: string | undefined;
    scrollPosition: number;
    tab: string;
}

interface PropsFromDispatch {
    push: typeof push;
    setScrollPosition: typeof userActions.setScrollPosition;
    setAreaTab: typeof userActions.setAreaTab;
}

type UserProfileProps = PropsFromState & PropsFromDispatch;

class UserProfileLayout extends React.Component<UserProfileProps> {
    public render = () => {
        const { classes } = (this.props as any);

        return (
            <Box component="div" className={classes.container}>
                <Blur />
                <AppBar position="fixed" className={classes.tabs}>
                    <Tabs
                        value={this.props.location.pathname}
                        variant="scrollable"
                        onChange={this.handleChange}
                    >
                        <Tab value={profileBase} hidden={true} className={classes.hidden} />
                        <Tab label="Profile" value={`${profileBase}/profile`} />
                        {/*<Tab label="Favorites" />*/}
                        <Tab label="Ratings" value={`${profileBase}/ratings`} />
                        <Tab label="History" value={`${profileBase}/history`} />
                        <Tab label="Info" value={`${profileBase}/info`} />
                    </Tabs>
                </AppBar>
                <ErrorBoundary fallback={<Error />}>
                    <Routes>
                        <Route path="profile" element={<UserSettings />} />
                        <Route path="ratings" element={<RatedVideos />} />
                        <Route path="history" element={<History />} />
                        <Route path="info" element={<Info />} />
                        <Route index element={<Navigate replace to="profile" />} />
                        <Route path="*" element={<NotFound />} />
                    </Routes>
                </ErrorBoundary>
            </Box>
        )
    }

    private handleChange = (event: React.ChangeEvent<{}>, value: any) => {
        Sentry.withScope(s => {
            const transaction = Sentry.startTransaction({name: 'changeUserTab', description: value});
            s.setSpan(transaction);

            this.props.setAreaTab("user-profile", value);
            this.props.push(value);

            transaction.finish();
        });
    }
}

const UserProfile = compose(
    withStyles(styles),
    withLocation(),
    withProps({
        locationKey: ({ location: { key }}) => key,
    }),
    withSelector('scrollPosition', scrollPositionSelector('user-profile')),
    withSelector('tab', selectedTabSelector('user-profile')),
    withAction('push', push),
    withAction('setScrollPosition', userActions.setScrollPosition),
    withAction('setAreaTab', userActions.setAreaTab),
    withLifecycle<UserProfileProps>({
        componentDidMount: (props) => {
            const { scrollPosition, tab } = props;

            if (tab)
                props.push(tab);
            window.scrollTo(0, scrollPosition);
        },
        componentDidUpdate: (props, prevProps) => {
            if (props.locationKey === prevProps.locationKey) return;
            animateScroll.scrollToTop();
        },
        componentWillUnmount: (props) => {
            const { setScrollPosition } = props;

            setScrollPosition("user-profile", window.scrollY);
        },
    }),
)(UserProfileLayout);

export { UserProfile };