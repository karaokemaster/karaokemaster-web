/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as Sentry from "@sentry/browser";
import { Store } from 'redux';
import { SemVer } from "semver";
import * as semver from 'semver';

import { LocalStorageService } from 'services/StorageService';
import { ApplicationState } from 'store';
import { initialState as appInitialState } from 'store/app';
import { initialState as userInitialState } from 'store/userProfile';

import gitVersion from 'gitversion.json';

type PersistedState = Partial<ApplicationState> & { '$version'?: string };
type StateLoader = (state: PersistedState) => Partial<ApplicationState>;

const storage = new LocalStorageService();

const loadState = () => {
    let state: Partial<ApplicationState> = {};

    Sentry.withScope(scope => {
        try {
            const persistedState: PersistedState = storage.load('state');
            scope.setExtra('persistedState', persistedState);

            if (!persistedState)
                return;

            const version = semver.parse(persistedState.$version) ?? new SemVer("0.1");

            const stateLoader = loaderFactory(version);

            if (stateLoader === undefined) return;

            state = stateLoader(persistedState);
        }
        catch (error) {
            Sentry.captureException(error);
        }

        scope.setExtra('state', state);
    });

    return state;
}

const saveState = (store: Store<ApplicationState>) => {
    const state = store.getState();

    const { userProfile: { authenticating, token, ...userProfile } } = state;
    const { app } = state;

    const savedState: PersistedState = {
        userProfile,
        app,
        '$version': gitVersion.SemVer,
    };

    storage.save('state', savedState);
}

const loaderFactory = (version: SemVer) => {
    const v1 = (state: PersistedState) => {
        const { user } = state;

        return {
            userProfile: {
                ...userInitialState,
                username: (user as any)?.user,
            },
            user: undefined,
        }
    }

    const v2 = (state: PersistedState) => {
        const { userProfile } = state;

        return {
            userProfile: {
                ...userInitialState,
                ...userProfile,
                username: userProfile?.username ?? (userProfile as any)?.user,
            },
        }
    }

    const v2dot3 = (state: PersistedState) => {
        const { userProfile, app } = state;

        return {
            userProfile: {
                ...userInitialState,
                ...userProfile,
                username: userProfile?.username ?? (userProfile as any)?.user,
            },
            app: {
                ...appInitialState,
                ...app,
            },
        }
    }

    const loaders: Record<string, StateLoader> = {
        ">=2.3 <3.0": v2dot3,
        ">=1.9 <2.3": v2,
        "<1.9": v1,
    };

    for (const range in loaders) {
        if (semver.satisfies(version, range, { includePrerelease: true })) {
            return loaders[range];
        }
    }

    return v1;
}

export { loadState, saveState }
