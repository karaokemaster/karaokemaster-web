import { ModuleClient, Twin } from 'azure-iot-device';
import { Amqp } from 'azure-iot-device-amqp';

import gitVersion from "gitversion.json";
import { runtimeConfig, updateConfig } from './runtimeConfig';

let client: ModuleClient | null = null;
let twin: Twin | null = null;


export const loadConfig = async () => {
    /* tslint:disable:no-console */

    if (process.env.IOTEDGE_DEVICEID) {
        console.log("📡 Loading module twin config...");
        client = await ModuleClient.fromEnvironment(Amqp);
    }
    else if (process.env.IOT_CONNECTION) {
        console.log("☁️ Loading config from connection string...");
        client = await ModuleClient.fromConnectionString(process.env.IOT_CONNECTION, Amqp);
    }

    if (!client) return runtimeConfig();

    twin = await client.getTwin();

    twin.on("properties.desired", (desiredProps: any) => {
        if (desiredProps.$version === runtimeConfig().$version)
            return;

        updateConfig(desiredProps);

        console.log("⚙️ Configuration update!");
        console.log(runtimeConfig());
    });

    const props = twin.properties.desired;
    updateConfig(props);
    console.log("⚙️ Configuration");
    console.log(runtimeConfig());

    twin.properties.reported.update({ version: gitVersion.SemVer }, (err: any) => {
        if (err)
            console.error(`Error updating twin properties: ${err}`);
    })

    /* tslint:enable:no-console */
    return runtimeConfig();
}
