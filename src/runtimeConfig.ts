const env = typeof window !== "undefined"
    ? (window as any).env // client
    : process.env; // server

let config = {
    client: {
        apiUrlBase: env.RAZZLE_API_BASE || env.apiUrlBase || "",
        displayUrl: env.RAZZLE_DISPLAY_URL || env.displayUrl || "",
        publicUrl: env.RAZZLE_PUBLIC_URL || env.publicUrl || "",
        player: {
            cueToggleKey: env.RAZZLE_CUE_TOGGLE_KEY || env.cueToggleKey || "Space",
            trackInfoOverlay: !!(env.RAZZLE_PLAYER_TRACK_OVERLAY || env.playerTrackOverlay),
            stopAfterPercent: env.stopAfterPercent || 0.67,
        },
        queue: {
            maxEntries: 7,
        },
        sentryDsn: env.RAZZLE_SENTRY_DSN || env.sentryDsn,
        sentryTracingOrigins: [],
        webPushPublicKey: env.RAZZLE_PUSH_KEY || env.webPushPublicKey,
    },
    environment: env.NODE_ENV || env.environment,
    port: env.PORT || env.port || 3000,
    $version: 0,
};

export const updateConfig = (update: any) => {
    config = { ...config, ...update };
}

export const runtimeConfig = () => (config);