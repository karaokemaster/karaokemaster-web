/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as express from 'express';
import * as serialize from 'serialize-javascript'; // Safer stringify, prevents XSS attacks

import { runtimeConfig } from 'runtimeConfig';

// tslint:disable-next-line:no-var-requires
const assets = require(process.env.RAZZLE_ASSETS_MANIFEST || '');

const title = 'KaraokeMaster';

const server = express();
server
    .disable('x-powered-by')
    .use(express.static(process.env.RAZZLE_PUBLIC_DIR || ''))
    .get('/*', (req, res) => {
        const context: any = {};
        const config = runtimeConfig().client;

        const markup = '';

        if (req.url === "/") {
            res.redirect("/recommendations");
        } else if (context.url) {
            res.redirect(context.url);
        } else {
            res.status(200).send(`<!doctype html>
<html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, viewport-fit=cover, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=no">
        <meta name="theme-color" content="#000000">
        <link rel="manifest" href="${config.publicUrl}/manifest.json">
        <link rel="shortcut icon" href="${config.publicUrl}/favicon.ico">
        <meta name="apple-mobile-web-app-title" content="${title}" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"></meta>
        <link rel="apple-touch-icon" sizes="57x57" href="${config.publicUrl}/touch-icon-iphone.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="${config.publicUrl}/touch-icon-ipad.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="${config.publicUrl}/touch-icon-iphone-retina.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="${config.publicUrl}/touch-icon-ipad-retina.png" />
        <link rel="apple-touch-icon" sizes="167x167" href="${config.publicUrl}/touch-icon-ipad-pro.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="${config.publicUrl}/touch-icon-iphone-6-plus.png" />
    
        <link rel="icon" sizes="192x192" href="${config.publicUrl}/icon-hd.png" />
        <link rel="icon" sizes="128x128" href="${config.publicUrl}/icon.png" />

        <title>${title}</title>

        ${
          assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ''
        }
        <script>window.env = ${serialize(config)};</script>
        ${
            runtimeConfig().environment === 'production'
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
    </head>
    <body>
        <div id="root">${markup}</div>
    </body>
</html>`
            );
        }
    });

export default server;
