/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

/// <reference lib="webworker" />
declare const self: ServiceWorkerGlobalScope;

// Incrementing OFFLINE_VERSION will kick off the install event and force
//  previously cached resources to be updated from the network.
// This variable is intentionally declared and unused.
const OFFLINE_VERSION = 1;
const CACHE_NAME = "offline";
const OFFLINE_URL = "offline.html";

self.addEventListener("install", (event) => {
    // tslint:disable-next-line:no-console
    console.debug("Install service worker");

    event.waitUntil(
        (async () => {
            const cache = await caches.open(CACHE_NAME);
            await cache.add(new Request(OFFLINE_URL, {cache: "reload"}));

            // tslint:disable-next-line:no-console
            console.debug("Service worker installed");
        })()
    );
    void self.skipWaiting();
});

self.addEventListener("activate", (event) => {
    // tslint:disable-next-line:no-console
    console.debug("Activate service worker");

    event.waitUntil(
        (async () => {
            // Enable navigation preload if it's supported.
            // See https://developers.google.com/web/updates/2017/02/navigation-preload
            if ("navigationPreload" in self.registration) {
                // @ts-ignore this is only called if the browser supports it
                // await self.registration.navigationPreload.enable();

                // tslint:disable-next-line:no-console
                console.debug("Service worker enabled navigationPreload");
            }
        })()
    );

    void self.clients.claim();
});

self.addEventListener("fetch", (event) => {
    if (event.request.mode === "navigate") {
        event.respondWith(
            (async () => {
                try {
                    // @ts-ignore
                    const preloadResponse = await event.preloadResponse;
                    if (preloadResponse) {
                        // tslint:disable-next-line:no-console
                        console.debug("Returning preloaded response for fetch request");

                        return preloadResponse;
                    }

                    const networkResponse = await fetch(event.request);

                    // tslint:disable-next-line:no-console
                    console.debug("Returning network response for fetch request");

                    return networkResponse;
                } catch (error) {
                    // catch is only triggered if an exception is thrown, which is likely
                    // due to a network error.
                    // If fetch() returns a valid HTTP response with a response code in
                    // the 4xx or 5xx range, the catch() will NOT be called.
                    // tslint:disable-next-line:no-console
                    console.debug(`Fetch failed; returning offline page instead: ${error}`);

                    const cache = await caches.open(CACHE_NAME);
                    const cachedResponse = await cache.match(OFFLINE_URL);
                    return cachedResponse;
                }
            })()
        );
    } else {
        // tslint:disable-next-line:no-console
        console.debug("Fetch request not handled by service worker");
    }
});

// self.onmessage = ({ data: { question } }) => {
//     self.postMessage({
//         answer: 42,
//     });
// };

self.addEventListener('push', (event) => {
    const data = event.data?.json() ?? {};
    const title = "It's your time to shine!";
    const message = data.title || "You're up in a minute!";
    // const icon = "icon-hd.png";

    // interface NotificationOptions {
    //     actions?: NotificationAction[];
    //     badge?: string;
    //     body?: string;
    //     data?: any;
    //     dir?: NotificationDirection;
    //     icon?: string;
    //     image?: string;
    //     lang?: string;
    //     renotify?: boolean;
    //     requireInteraction?: boolean;
    //     silent?: boolean;
    //     tag?: string;
    //     timestamp?: EpochTimeStamp;
    //     vibrate?: VibratePattern;
    // }
    event.waitUntil(self.registration.showNotification(title, {
        body: message,
        // tag: (data.queueId || -1).toString(),
        // icon,
        vibrate: [100, 50, 100],
        actions: [
            {
                action: "explore", title: "Go interact with this!",
                // icon: "images/checkmark.png"
            },
            {
                action: "close", title: "Ignore",
                // icon: "images/red_x.png"
            },
        ]
    }));
});

self.addEventListener('notificationclick', (event) => {
    const notification = event.notification;
    const action = event.action;

    if (action === 'close') {
        notification.close();
    } else {
        // Some actions
        // clients.openWindow('http://www.example.com');
        notification.close();
    }
});

export default null;
