/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

export enum AppActionTypes {
    SERVER_INFO = '@@app/SERVER_INFO',
    WAKE = '@@app/WAKE',
    CONNECT_AS_PLAYER = '@@app/CONNECT_AS_PLAYER',
    CONNECT_AS_USER = '@@app/CONNECT_AS_USER',
    CONNECT_AS_KIOSK = '@@app/CONNECT_AS_KIOSK',
    CONNECT_AS_EMCEE = '@@app/CONNECT_AS_EMCEE',
    DISCONNECT = '@@app/DISCONNECT',
    REQUEST_NOTIFICATIONS = '@@app/REQUEST_NOTIFICATIONS',
    UPDATE_NOTIFICATIONS = '@@app/UPDATE_NOTIFICATIONS',
}

export interface ServerInfo {
    apiVersion: string;
    apiRelease: string;
    apiDate: string;
    apiSha: string;
}

export interface PushSubscription {
    endpoint: string;
    auth: string;
    p256dh: string;
}

export interface AppState {
    serverInfo: ServerInfo | undefined;
    notifications: boolean | undefined;
    notificationSubscription: PushSubscription | undefined;
}