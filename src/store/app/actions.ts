/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { action } from 'typesafe-actions';

import { AppActionTypes } from './types';

export const wake = () => action(AppActionTypes.WAKE);
export const connectAsUser = () => action(AppActionTypes.CONNECT_AS_USER);
export const connectAsPlayer = () => action(AppActionTypes.CONNECT_AS_PLAYER);
export const connectAsKiosk = () => action(AppActionTypes.CONNECT_AS_KIOSK);
export const connectAsEmcee = () => action(AppActionTypes.CONNECT_AS_EMCEE);
export const disconnect = () => action(AppActionTypes.DISCONNECT);
export const requestNotifications = () => action(AppActionTypes.REQUEST_NOTIFICATIONS);