/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';
import { UserProfileActionTypes } from '../userProfile';
import { AppActionTypes, AppState, ServerInfo } from './types';

export const initialState: AppState = {
    serverInfo: {
        apiVersion: "",
        apiRelease: "",
        apiSha: "",
        apiDate: "",
    },
    notifications: undefined,
    notificationSubscription: undefined,
}

const reducer: Reducer<AppState> = (state = initialState, action) => {
    switch (action.type) {
        case AppActionTypes.SERVER_INFO: {
            const serverInfo = (action.payload as ServerInfo);
            return { ...state, serverInfo };
        }
        case AppActionTypes.UPDATE_NOTIFICATIONS: {
            const notifications = action.payload;
            return { ...state, notifications };
        }
        case UserProfileActionTypes.SUBSCRIBE_PUSH: {
            const notificationSubscription = action.payload;
            return { ...state, notificationSubscription };
        }
        default: {
            return state;
        }
    }

}

export { reducer as appReducer }