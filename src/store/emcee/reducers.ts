/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';
import { firstBy } from 'thenby';

import { ConnectionStatus } from 'store/signalR';
import { EmceeActionTypes, EmceeState } from './types';

const initialState: EmceeState = {
    connectionStatus: ConnectionStatus.Unknown,
    users: [],
    played: [],
    microphones: [],
    microphoneLevels: [],
    iconResults: [],
}

const reducer: Reducer<EmceeState> = (state = initialState, action) => {
    switch (action.type) {
        case EmceeActionTypes.UPDATE_USERS: {
            const users = action.payload;
            return { ...state, users };
        }
        case EmceeActionTypes.SELECT_USER: {
            const selectedUserId = action.payload;
            return { ...state, selectedUserId };
        }
        case EmceeActionTypes.FIND_ICON_SUCCESS: {
            const results = action.payload;
            return { ...state, iconResults: results };
        }
        case EmceeActionTypes.FIND_ICON_ERROR: {
            return { ...state, iconResults: [] };
        }
        case EmceeActionTypes.UPDATE_PLAYED: {
            const played = action.payload;
            return { ...state, played };
        }
        case EmceeActionTypes.SET_CONNECTION_STATUS: {
            return { ...state, connectionStatus: action.payload }
        }
        case EmceeActionTypes.MICROPHONE_RECEIVED: {
            const mic = action.payload;
            const allMicrophones = state.microphones
                .filter(m => m.name !== mic.name)
                .concat([ mic ])
                .sort(firstBy<any>(m => m.name));

            const microphones = allMicrophones.map((m: any) => {
                const { rfLevel, afLevel, afPeak, ...mic } = m;
                return mic;
            });
            const microphoneLevels = allMicrophones.map((m: any) => {
                const { color, battery, rfAntenna, rfPilot, mute, ...mic } = m;
                return mic;
            });

            return { ...state, microphones, microphoneLevels }
        }
        default: {
            return state;
        }
    }

}

export { reducer as emceeReducer }