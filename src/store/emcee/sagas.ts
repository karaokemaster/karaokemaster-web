/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as Sentry from '@sentry/browser';
import { all, fork, put, select, takeEvery } from 'redux-saga/effects';
import * as serialize from 'serialize-javascript';

import { authHeaderSelector, loginComplete, loginError, loginSuccess, updateUserError, updateUserSuccess, UserProfileState } from 'store/userProfile';
import { EmceeActionTypes, findIconError, findIconSuccess, IconResult } from './';

import { runtimeConfig } from 'runtimeConfig';

function* handleLoginEmcee(action: any) {
    try {
        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/emcee/login`, {
            method: 'POST',
            cache: 'no-cache',
        });
        const results: UserProfileState = yield response.json();

        if (!response.ok) {
            yield put(loginError(yield response.text()));
        } else {
            yield put(loginSuccess(results));
            yield put(loginComplete());
        }
    } catch (err) {
        Sentry.captureException(err);
        yield put(loginError('An unknown error occurred!'));
    }
}

function* handleUpdateUser(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const userProfile: UserProfileState = action.payload;
        const { authenticating, token, updateError, ...profile } = userProfile;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user`, {
            method: 'POST',
            headers,
            body: serialize({ ...profile, }),
        });

        if (!response.ok) {
            yield put(updateUserError('all', yield response.text()));
        }
        else {
            yield put(updateUserSuccess('all'));
        }
    }
    catch (err) {
        yield put(updateUserError('all', 'An unknown error occurred!'));
    }
}

function* handleFindIcon(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const query: string = action.payload;

        headers.append('Query', query);

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user/searchIcons`, {
            method: 'POST',
            headers,
        });
        const results: string[] = yield response.json();
        const iconResults: IconResult[] = results
            .map(r => {
                const result = JSON.parse(r);
                return { ...result, icon: JSON.parse(result.icon) };
            });

        if (!response.ok) {
            yield put(findIconError(yield response.text()));
        }
        else {
            yield put(findIconSuccess(iconResults));
        }
    }
    catch (err) {
        yield put(findIconError('An unknown error occurred!'));
    }
}

function* emceeSaga() {
    yield all([
        fork(function* () { yield takeEvery(EmceeActionTypes.LOGIN_EMCEE, handleLoginEmcee); }),
        fork(function* () { yield takeEvery(EmceeActionTypes.SAVE_USER, handleUpdateUser); }),
        fork(function* () { yield takeEvery(EmceeActionTypes.FIND_ICON, handleFindIcon); }),
    ]);
}

export default emceeSaga;