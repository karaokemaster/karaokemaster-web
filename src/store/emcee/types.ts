/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { IconLookup } from '@fortawesome/fontawesome-svg-core';

import { QueueEntry } from 'store/queue';
import { ConnectionStatus } from 'store/signalR';
import { User } from 'store/user';

export enum EmceeActionTypes {
    LOGIN_EMCEE = '@@emcee/LOGIN_EMCEE',

    REQUEST_USERS = '@@emcee/REQUEST_USERS',
    SELECT_USER = '@@emcee/SELECT_USER',
    SAVE_USER = '@@emcee/SAVE_USER',

    FIND_ICON = '@@emcee/FIND_ICON',
    FIND_ICON_SUCCESS = '@@emcee/FIND_ICON_SUCCESS',
    FIND_ICON_ERROR = '@@emcee/FIND_ICON_ERROR',

    UPDATE_USERS = '@@emcee/UPDATE_USERS',
    UPDATE_PLAYED = '@@emcee/UPDATE_PLAYED',
    SET_CONNECTION_STATUS = '@@emcee/SET_CONNECTION_STATUS',
    RECONNECT = '@@emcee/RECONNECT',
    DISCONNECT = '@@emcee/DISCONNECT',

    MICROPHONE_RECEIVED = '@@emcee/MICROPHONE_RECEIVED',
}

export interface IconResult {
    readonly icon: IconLookup
    readonly label: string
}

export interface Microphone {
    readonly name: string
    readonly color: string
    readonly battery?: number
    readonly rfAntenna?: number
    readonly rfPilot: boolean
    readonly mute: boolean
}

export interface MicrophoneLevels {
    readonly name: string
    readonly rfLevel?: number
    readonly afLevel?: number
    readonly afPeak?: number
}

export interface EmceeState {
    readonly connectionStatus: ConnectionStatus
    readonly users: User[]
    readonly played: QueueEntry[]
    readonly microphones: Microphone[]
    readonly microphoneLevels: MicrophoneLevels[]
    
    readonly selectedUserId?: number
    readonly iconResults: IconResult[]
}