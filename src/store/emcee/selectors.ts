/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import xorWith from 'lodash.xorwith';
import { createSelector, createSelectorCreator, defaultMemoize } from 'reselect';
import * as serialize from 'serialize-javascript';
import { firstBy } from 'thenby';

import { defaultColor } from 'components/user-profile/settings/colorPicker';
import { defaultIcon } from 'components/user-profile/settings/iconPicker';
import { ApplicationState } from 'store';

// TODO: Combine w/ queue
const isArrayEqual = (x: any[], y: any[]) => {
    return isEqual(x, y) || isEmpty(xorWith(x, y, isEqual));
};

const createArrayEqualSelector = createSelectorCreator(
    defaultMemoize,
    isArrayEqual
);

export const emceeConnectionStatusSelector = ({ emcee }: ApplicationState) => emcee.connectionStatus;

export const usersSelector = createArrayEqualSelector(
    ({ emcee: { users } }: ApplicationState) => users,
    (users) => users
        .map(u => ({
            ...u,
            icon: u.icon || serialize(defaultIcon(u.username)),
            color: u.color || defaultColor(u.username),
        }))
        .sort(firstBy(u => u.username, { ignoreCase:true }))
);

export const selectedUserIdSelector = ({ emcee: { selectedUserId } }: ApplicationState) => selectedUserId;

export const selectedUserSelector = createSelector(
    usersSelector,
    selectedUserIdSelector,
    (users, userId) => users.find(u => u.userId === userId),
);

export const microphonesSelector = createArrayEqualSelector(
    ({ emcee: { microphones }}: ApplicationState) => microphones,
    (microphones) => microphones,
)

export const microphoneLevelsSelector = createSelector(
    ({ emcee: { microphoneLevels }}: ApplicationState) => microphoneLevels,
    (microphoneLevels) => microphoneLevels,
)

export const microphoneLevelSelector = createSelector(
    microphoneLevelsSelector,
    (state: ApplicationState, name: string) => name,
    (microphoneLevels, name) => {
        return microphoneLevels.find(m => m.name === name);
    }
)

export const findIconResults = createSelector(
    ({ emcee: { iconResults }}: ApplicationState) => iconResults,
    (results) => results,
)
export const playedVideosSelector = createSelector(
    ({ emcee }: ApplicationState) => emcee.played,
    (videos) => videos
        .map(v => ({
            ...v,
            userIcon: v.userIcon || serialize(defaultIcon(v.user)),
            userColor: v.userColor || defaultColor(v.user),
        }))
);
