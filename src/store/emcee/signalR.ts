/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { HubConnectionState } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';
import * as Sentry from '@sentry/browser';

import { AppActionTypes } from 'store/app';
import { EmceeActionTypes } from 'store/emcee';
import { ConnectionStatus, retryPolicy, SignalRActionTypes } from 'store/signalR';
import { tokenSelector } from 'store/userProfile';

import { runtimeConfig } from 'runtimeConfig';
import { Microphone } from './types';

let connection: signalR.HubConnection;

let startConnection: () => Promise<void>;
let microphoneSubscription: signalR.ISubscription<Microphone>;

const invokeMiddleware = (store: any) => (next: any) => async (action: any) => {
    if ((!connection || connection.state !== HubConnectionState.Connected)
        && action.type !== AppActionTypes.WAKE
        && action.type !== SignalRActionTypes.RECONNECT
        && action.type !== AppActionTypes.CONNECT_AS_EMCEE)
        return next(action);

    switch (action.type) {
        case AppActionTypes.CONNECT_AS_EMCEE: {
            await connection?.stop();
            await startConnection();

            microphoneSubscription = connection
                .stream("SubscribeMicrophones")
                .subscribe({
                    next: (state) => {
                        store.dispatch({ type: EmceeActionTypes.MICROPHONE_RECEIVED, payload: { ...state } });
                    },
                    complete: () => {
                        // store.dispatch({
                        //     type: PlayerActionTypes.STATE_RECEIVED,
                        //     payload: { state: PlaybackState.Disconnected, videoTime: undefined, videoTimeHiRes: undefined, videoDuration: undefined, queueId: undefined }
                        // });
                    },
                    error: (error) => {
                        Sentry.captureException(error);
                        // store.dispatch({
                        //     type: PlayerActionTypes.STATE_RECEIVED,
                        //     payload: { state: PlaybackState.Disconnected, videoTime: undefined, videoTimeHiRes: undefined, videoDuration: undefined, queueId: undefined }
                        // });
                    },
                });

            break;
        }
        case AppActionTypes.DISCONNECT: {
            if (microphoneSubscription) {
                microphoneSubscription.dispose();
            }

            await connection.stop();
            break;
        }
        case AppActionTypes.WAKE: {
            await startConnection();
            break;
        }

        case EmceeActionTypes.REQUEST_USERS: {
            await connection.invoke("SendUserList");
            break;
        }
        case EmceeActionTypes.RECONNECT: {
            await startConnection();
            break;
        }
    }

    return next(action);
}

// tslint:disable-next-line:ban-types
const registerCommands = (store: any) => {
    connection = new signalR.HubConnectionBuilder()
        .withUrl(
            `${runtimeConfig().client.apiUrlBase}/hubs/emcee`,
            {
                accessTokenFactory: () => {
                    const state = store.getState();
                    const token = tokenSelector(state);
                    return token ?? '';
                }
            })
        .withAutomaticReconnect(retryPolicy)
        .build();

    connection.on("ServerInfo", data => {
        store.dispatch({ type: AppActionTypes.SERVER_INFO, payload: data });
    })

    connection.on("UpdateUsers", data => {
        store.dispatch({ type: EmceeActionTypes.UPDATE_USERS, payload: data });
    })

    connection.on("UpdatePlayedVideos", data => {
        store.dispatch({ type: EmceeActionTypes.UPDATE_PLAYED, payload: data });
    })

    connection.onclose(() => {
        store.dispatch({ type: EmceeActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Disconnected });
    });

    connection.onreconnecting(() => {
        store.dispatch({ type: EmceeActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Reconnecting });
    });

    connection.onreconnected(() => {
        store.dispatch({ type: EmceeActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connected });
    });

    startConnection = async() => {
        const currentStatus = store.getState().emcee.connectionStatus;
        if (currentStatus === ConnectionStatus.Connected || currentStatus === ConnectionStatus.Connecting) {
            return;
        }

        store.dispatch({ type: EmceeActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connecting });

        try {
            await connection.start();
            store.dispatch({ type: EmceeActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connected });
        } catch {
            store.dispatch({ type: EmceeActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Disconnected });
            setTimeout(() => {
                store.dispatch({ type: EmceeActionTypes.RECONNECT });
            }, 1000);
        }
    }
}

export { invokeMiddleware, registerCommands }