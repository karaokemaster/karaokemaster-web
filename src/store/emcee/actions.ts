/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { action } from 'typesafe-actions';

import { UserProfile } from 'store/userProfile';
import { EmceeActionTypes, IconResult } from './types';

export const loginEmcee = () => action(EmceeActionTypes.LOGIN_EMCEE);
export const selectUser = (userId: number) => action(EmceeActionTypes.SELECT_USER, userId);
export const saveUser = (user: UserProfile) => action(EmceeActionTypes.SAVE_USER, user);
export const requestUsers = () => action(EmceeActionTypes.REQUEST_USERS);

export const findIcon = (query: string) => action(EmceeActionTypes.FIND_ICON, query);
export const findIconSuccess = (results: IconResult[]) => action(EmceeActionTypes.FIND_ICON_SUCCESS, results);
export const findIconError = (message: string) => action(EmceeActionTypes.FIND_ICON_ERROR, message);

export const emceeActions = {
    loginEmcee,
    selectUser,
    saveUser,
    findIcon,
}