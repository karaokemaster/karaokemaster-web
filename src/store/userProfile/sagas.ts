/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { IconLookup } from "@fortawesome/fontawesome-svg-core";
import * as Sentry from '@sentry/browser';
import { all, fork, put, select, takeEvery } from 'redux-saga/effects';
import * as serialize from 'serialize-javascript';

import { authHeaderSelector, login, loginComplete, loginError, loginSuccess, updateUserError, updateUserSuccess, UserProfileActionTypes, userProfileSelector, UserProfileState } from './';

import { runtimeConfig } from 'runtimeConfig';

function* handleLogin(action: any) {
    try {
        const username: string = action.payload;

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user/login`, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'username': username,
            },
        });
        const results: UserProfileState = yield response.json();

        if (!response.ok) {
            yield put(loginError(yield response.text()));
        } else {
            yield put(loginSuccess(results));
            yield put(loginComplete());
        }
    } catch (err) {
        Sentry.captureException(err);
        yield put(loginError('An unknown error occurred!'));
    }
}

function* handleSetIcon(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const userProfile: UserProfileState = yield select(userProfileSelector);
        const { authenticating, token, updateError, ...profile } = userProfile;
        const icon: IconLookup | undefined = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user`, {
            method: 'POST',
            headers,
            body: serialize({ ...profile, icon: serialize(icon), }),
        });

        if (!response.ok) {
            yield put(updateUserError('icon', yield response.text()));
        }
        else {
            yield put(updateUserSuccess('icon'));
        }
    }
    catch (err) {
        yield put(updateUserError('icon', 'An unknown error occurred!'));
    }
}

function* handleSetColor(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const userProfile: UserProfileState = yield select(userProfileSelector);
        const { authenticating, token, updateError, ...profile } = userProfile;
        const color: string | undefined = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user`, {
            method: 'POST',
            headers,
            body: serialize({ ...profile, color, }),
        });

        if (!response.ok) {
            yield put(updateUserError('color', yield response.text()));
        }
        else {
            yield put(updateUserSuccess('color'));
        }
    }
    catch (err) {
        yield put(updateUserError('color', 'An unknown error occurred!'));
    }
}

function* handleChangeUsername(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const userProfile: UserProfileState = yield select(userProfileSelector);
        const { authenticating, token, updateError, ...profile } = userProfile;
        const username: string = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user/changeUsername?username=${encodeURIComponent(username)}`, {
            method: 'POST',
            headers,
            body: serialize({ ...profile, }),
        });

        if (response.status === 409) {
            yield put(updateUserError('username', "Requested username is in use"));
        }
        else if (!response.ok) {
            yield put(updateUserError('username', 'An unknown error has occurred'));
            const error = JSON.parse(yield response.text());
            Sentry.captureException(error);
        }
        else {
            yield put(updateUserSuccess('username'));
            yield handleLogin(login(username));
        }
    }
    catch (err) {
        yield put(updateUserError('username', 'An unknown error occurred!'));
    }
}

function* handleSubscribePush(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const subscription = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user/subscribePush`, {
            method: 'POST',
            headers,
            body: serialize({ ...subscription, }),
        });

        if (!response.ok) {
            // yield put(updateUserError('username', 'An unknown error has occurred'));
            const error = JSON.parse(yield response.text());
            Sentry.captureException(error);
        }
        else {
            // yield put(updateUserSuccess('username'));
            // yield handleLogin(login(username));
        }
    }
    catch (err) {
        // yield put(updateUserError('username', 'An unknown error occurred!'));
    }
}

function* userSaga() {
    yield all([
        fork(function* () { yield takeEvery(UserProfileActionTypes.LOGIN, handleLogin); }),
        fork(function* () { yield takeEvery(UserProfileActionTypes.SET_USER_ICON, handleSetIcon); }),
        fork(function* () { yield takeEvery(UserProfileActionTypes.SET_USER_COLOR, handleSetColor); }),
        fork(function* () { yield takeEvery(UserProfileActionTypes.CHANGE_USERNAME, handleChangeUsername); }),
        fork(function* () { yield takeEvery(UserProfileActionTypes.SUBSCRIBE_PUSH, handleSubscribePush); }),
    ]);
}

export default userSaga;