/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import * as Sentry from '@sentry/browser';
import { Reducer } from 'redux';

import { defaultRecommendationsFilter } from 'store/recommendations';
import { UserProfileActionTypes, UserProfileState } from './types';

const loginState: Partial<UserProfileState> = {
    authenticating: false,
}

export const initialState: UserProfileState = {
    ...loginState,
    recommendationsFilter: defaultRecommendationsFilter,
}

const reducer: Reducer<UserProfileState> = (state = initialState, action) => {
    switch (action.type) {
        case UserProfileActionTypes.LOGIN: {
            return { ...state, ...loginState, authenticating: true }
        }
        case UserProfileActionTypes.LOGIN_SUCCESS: {
            const user: UserProfileState = action.payload;
            Sentry.setUser({ id: user.userId?.toString(), username: user.username });

            return { ...state, ...user, authenticating: false }
        }
        case UserProfileActionTypes.LOGIN_ERROR: {
            return { ...initialState, authenticating: false }
        }
        case UserProfileActionTypes.LOGOUT: {
            Sentry.setUser(null);

            return initialState;
        }
        case UserProfileActionTypes.RECEIVE_USER_PROFILE: {
            const user = action.payload;
            Sentry.setUser({ id: user.userId?.toString(), username: user.username });

            return { ...state, ...user }
        }
        case UserProfileActionTypes.CHANGE_USERNAME:
        case UserProfileActionTypes.SET_USER_COLOR:
        case UserProfileActionTypes.SET_USER_ICON: {
            return { ...state, updateError: undefined }
        }
        case UserProfileActionTypes.UPDATE_USER_ERROR: {
            const updateError = action.payload;

            return { ...state, updateError }
        }
        case UserProfileActionTypes.SET_RECOMMENDATIONS_FILTER: {
            const recommendationsFilter = {
                ...state.recommendationsFilter,
                ...action.payload
            };

            return { ...state, recommendationsFilter }
        }
        default: {
            return state;
        }
    }

}

export { reducer as userProfileReducer }