/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { IconLookup } from '@fortawesome/fontawesome-svg-core';
import { action } from 'typesafe-actions';

import { PushSubscription } from 'store/app';
import { RecommendationsFilter } from 'store/recommendations';
import { UserProfileActionTypes, UserProfileState } from './types';

export const login = (username: string) => action(UserProfileActionTypes.LOGIN, username);
export const loginSuccess = (user: UserProfileState) => action(UserProfileActionTypes.LOGIN_SUCCESS, user);
export const loginError = (error: string) => action(UserProfileActionTypes.LOGIN_ERROR, error);
export const loginComplete = () => action(UserProfileActionTypes.LOGIN_COMPLETE);
export const logout = () => action(UserProfileActionTypes.LOGOUT);

export const setUserIcon = (icon?: IconLookup) => action(UserProfileActionTypes.SET_USER_ICON, icon);
export const setUserColor = (color?: string) => action(UserProfileActionTypes.SET_USER_COLOR, color);
export const changeUsername = (username: string) => action(UserProfileActionTypes.CHANGE_USERNAME, username);
export const updateUserSuccess = (prop: string) => action(UserProfileActionTypes.UPDATE_USER_SUCCESS, prop);
export const updateUserError = (prop: string, error: string) => action(UserProfileActionTypes.UPDATE_USER_ERROR, { prop, error });
export const setRecommendationsFilter = (filter: RecommendationsFilter) => action(UserProfileActionTypes.SET_RECOMMENDATIONS_FILTER, filter);
export const subscribePush = (pushSubscription: PushSubscription) => action(UserProfileActionTypes.SUBSCRIBE_PUSH, pushSubscription);