/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { RecommendationsFilter } from 'store/recommendations';

export enum UserProfileActionTypes {
    LOGIN = '@@userProfile/LOGIN',
    LOGIN_SUCCESS = '@@userProfile/LOGIN_SUCCESS',
    LOGIN_ERROR = '@@userProfile/LOGIN_ERROR',
    LOGIN_COMPLETE = '@@userProfile/LOGIN_COMPLETE',
    LOGOUT = '@@userProfile/LOGOUT',
    RECEIVE_USER_PROFILE = '@@userProfile/RECEIVE_USER_PROFILE',

    SET_USER_ICON = '@@userProfile/SET_USER_ICON',
    SET_USER_COLOR = '@@userProfile/SET_USER_COLOR',
    CHANGE_USERNAME = '@@userProfile/CHANGE_USERNAME',
    UPDATE_USER_ERROR = '@@userProfile/UPDATE_USER_ERROR',
    UPDATE_USER_SUCCESS = '@@userProfile/UPDATE_USER_SUCCESS',
    SET_RECOMMENDATIONS_FILTER = '@@userProfile/SET_RECOMMENDATIONS_FILTER',
    SUBSCRIBE_PUSH = '@@userProfile/SUBSCRIBE_PUSH',
}

export interface UserProfile {
    readonly token?: string;
    readonly userId?: number;
    readonly username?: string;
    readonly icon?: string;
    readonly color?: string;
}

export interface OtherProfileState {
    readonly authenticating?: boolean;

    readonly recommendationsFilter: RecommendationsFilter;

    readonly updateError?: { prop: string, error: string };
}

export type UserProfileState = UserProfile & OtherProfileState;