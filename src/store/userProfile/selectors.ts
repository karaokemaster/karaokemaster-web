/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { createSelector } from 'reselect';
import { ApplicationState } from 'store';

export const authenticatingSelector = ({ userProfile }: ApplicationState) => userProfile.authenticating;

export const usernameSelector = ({ userProfile }: ApplicationState) => userProfile.username;
export const userIdSelector = ({ userProfile }: ApplicationState) => userProfile.userId;
export const tokenSelector = ({ userProfile }: ApplicationState) => userProfile.token;
export const userColorSelector = ({ userProfile }: ApplicationState) => userProfile.color;
export const userIconSelector = ({ userProfile }: ApplicationState) => userProfile.icon;
export const recommendationsFilterSelector = ({ userProfile }: ApplicationState) => userProfile.recommendationsFilter;
export const userProfileSelector = ({ userProfile }: ApplicationState) => userProfile;

export const isCurrentUserSelector = (username?: string) => createSelector(
    usernameSelector,
    (currentUsername) => {
        if (username === currentUsername) return true;

        // localCompare === 0 -> strings are equivalent
        return username?.localeCompare(currentUsername ?? "", undefined, { sensitivity: 'base' }) === 0;
    }
)

export const authHeaderSelector = createSelector(
    tokenSelector,
    (token) => (new Headers({
        'Authorization': `Bearer ${token}`,
    }))
)

export const updateErrorSelector = (prop: string) => (
    createSelector(
        (state: ApplicationState) => state,
        ({ userProfile: { updateError } }: ApplicationState) => updateError?.prop === prop ? updateError.error : undefined,
    ));
