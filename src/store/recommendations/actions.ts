/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Video } from 'store/videos';
import { action } from 'typesafe-actions';
import { RecommendationsActionTypes } from './types';

// TODO: Consolidate fetch actions
export const fetchRequest = () => action(RecommendationsActionTypes.FETCH_REQUEST)
export const fetchSuccess = (results: Video[]) => action(RecommendationsActionTypes.FETCH_SUCCESS, results)
export const fetchError = (message: string) => action(RecommendationsActionTypes.FETCH_ERROR, message)

export const fetchTopVideosRequest = () => action(RecommendationsActionTypes.FETCH_TOP_VIDEOS_REQUEST)
export const fetchTopVideosSuccess = (results: Video[]) => action(RecommendationsActionTypes.FETCH_TOP_VIDEOS_SUCCESS, results)
export const fetchTopVideosError = (message: string) => action(RecommendationsActionTypes.FETCH_TOP_VIDEOS_ERROR, message)

export const updateRecommendations = (recommendations: Video[]) => action(RecommendationsActionTypes.UPDATE_RECOMMENDATIONS, recommendations);
export const updatePreviouslyPlayed = (previouslyPlayed: Video[]) => action(RecommendationsActionTypes.UPDATE_PREVIOUSLY_PLAYED, previouslyPlayed);
export const updateTopVideos = (topVideos: Video[]) => action(RecommendationsActionTypes.UPDATE_TOP_VIDEOS, topVideos);

export const recommendationsActions = {
    fetchError,
    fetchRequest,
    fetchSuccess,
    fetchTopVideosRequest,
    fetchTopVideosSuccess,
    fetchTopVideosError,
    updateRecommendations,
    updatePreviouslyPlayed,
    updateTopVideos,
}
