/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';
import { UserProfileActionTypes } from 'store/userProfile';
import { Video } from 'store/videos';
import { RecommendationsActionTypes, RecommendationsState } from './types';

const initialState: RecommendationsState = {
    loading: false,
    results: [],
    previouslyPlayed: [],
    topVideos: [],
}

const reducer: Reducer<RecommendationsState> = (state = initialState, action) => {
    switch (action.type) {
        case RecommendationsActionTypes.FETCH_REQUEST: {
            return { ...state, loading: true, results: [] }
        }
        case RecommendationsActionTypes.FETCH_SUCCESS: {
            const results = (action.payload as Video[]);
            return { ...state, results, loading: false }
        }
        case RecommendationsActionTypes.FETCH_ERROR: {
            return { ...state, loading: false, results: [] }
        }
        case RecommendationsActionTypes.FETCH_TOP_VIDEOS_REQUEST: {
            return { ...state, topVideos: [] }
        }
        case RecommendationsActionTypes.FETCH_TOP_VIDEOS_SUCCESS: {
            const topVideos = (action.payload as Video[]);
            return { ...state, topVideos }
        }
        case RecommendationsActionTypes.FETCH_TOP_VIDEOS_ERROR: {
            return { ...state, topVideos: [] }
        }
        case RecommendationsActionTypes.UPDATE_RECOMMENDATIONS: {
            const results = (action.payload as Video[]);
            return { ...state, loading: false, results }
        }
        case RecommendationsActionTypes.UPDATE_PREVIOUSLY_PLAYED: {
            const previouslyPlayed = (action.payload as Video[]);
            return { ...state, loading: false, previouslyPlayed }
        }
        case RecommendationsActionTypes.UPDATE_TOP_VIDEOS: {
            const topVideos = (action.payload as Video[]);
            return { ...state, topVideos }
        }
        case UserProfileActionTypes.LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }

}

export { reducer as recommendationsReducer }