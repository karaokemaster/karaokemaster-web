/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Video } from 'store/videos/types';

// TODO: Consolidate fetch actions
export enum RecommendationsActionTypes {
    FETCH_REQUEST = '@@recommendations/FETCH_REQUEST',
    FETCH_SUCCESS = '@@recommendations/FETCH_SUCCESS',
    FETCH_ERROR = '@@recommendations/FETCH_ERROR',
    FETCH_TOP_VIDEOS_REQUEST = '@@recommendations/FETCH_TOP_VIDEOS_REQUEST',
    FETCH_TOP_VIDEOS_SUCCESS = '@@recommendations/FETCH_TOP_VIDEOS_SUCCESS',
    FETCH_TOP_VIDEOS_ERROR = '@@recommendations/FETCH_TOP_VIDEOS_ERROR',
    UPDATE_RECOMMENDATIONS = '@@recommendations/UPDATE_RECS',
    UPDATE_PREVIOUSLY_PLAYED = '@@recommendations/UPDATE_PREVIOUSLY_PLAYED',
    UPDATE_TOP_VIDEOS = '@@recommendations/UPDATE_TOP_VIDEOS',
}

export interface RecommendationsFilter {
    readonly recommendations: boolean;
    readonly previouslyPlayed: boolean;
    readonly topVideos: boolean;
    readonly hidePlayed: boolean;
}

export const defaultRecommendationsFilter: RecommendationsFilter = {
    recommendations: true,
    previouslyPlayed: true,
    topVideos: true,
    hidePlayed: false,
}

export interface RecommendationsState {
    readonly loading: boolean;
    readonly results: Video[];
    readonly previouslyPlayed: Video[];
    readonly topVideos: Video[];
}