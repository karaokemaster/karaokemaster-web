/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { createSelector } from 'reselect';

import { ApplicationState } from 'store';

export const recommendationsLoadingSelector = ({ recommendations }: ApplicationState) => recommendations.loading;
export const recommendationsSelector = createSelector(
    ({ recommendations }: ApplicationState) => recommendations.results,
    (results) => results
);
export const previouslyPlayedSelector = createSelector(
    ({ recommendations }: ApplicationState) => recommendations.previouslyPlayed,
    (previouslyPlayed) => previouslyPlayed
);
export const topVideosSelector = createSelector(
    ({ recommendations }: ApplicationState) => recommendations.topVideos,
    (topVideos) => topVideos
);
