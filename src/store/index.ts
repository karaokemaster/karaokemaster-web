/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { combineReducers, Reducer } from 'redux';
import { RouterState } from 'redux-first-history';
import { all, fork } from 'redux-saga/effects';

import { appReducer, AppState } from 'store/app';
import * as appSelectors from 'store/app/selectors';
import { cuesReducer, CuesState } from 'store/cues';
import * as cuesSelectors from 'store/cues';
import { emceeReducer, EmceeState } from 'store/emcee';
import emceeSaga from 'store/emcee/sagas';
import * as emceeSelectors from 'store/emcee/selectors';
import { playerReducer, PlayerState } from 'store/player';
import * as playerSelectors from 'store/player/selectors';
import { queueReducer, QueueState } from 'store/queue';
import queueSaga from 'store/queue/sagas';
import * as queueSelectors from 'store/queue/selectors';
import { recommendationsReducer, RecommendationsState } from 'store/recommendations';
import recommendationsSaga from 'store/recommendations/sagas';
import * as recommendationsSelectors from 'store/recommendations/selectors';
import { searchReducer, SearchState } from 'store/search';
import searchSaga from 'store/search/sagas';
import * as searchSelectors from 'store/search/selectors';
import { signalRReducer, SignalRState } from 'store/signalR';
import * as signalRSelectors from 'store/signalR/selectors';
import { userReducer, UserState } from 'store/user';
import userSaga from 'store/user/sagas';
import * as userSelectors from 'store/user/selectors';
import { userProfileReducer, UserProfileState } from 'store/userProfile';
import userProfileSaga from 'store/userProfile/sagas';
import * as userProfileSelectors from 'store/userProfile/selectors';

// The top-level state object
export interface ApplicationState {
    router: RouterState,
    user: UserState,
    userProfile: UserProfileState,
    queue: QueueState,
    player: PlayerState,
    emcee: EmceeState,
    recommendations: RecommendationsState,
    signalR: SignalRState,
    search: SearchState,
    cues: CuesState,
    app: AppState,
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
export const createRootReducer = (routerReducer: Reducer) => combineReducers<ApplicationState>({
    player: playerReducer,
    queue: queueReducer,
    emcee: emceeReducer,
    recommendations: recommendationsReducer,
    router: routerReducer,
    search: searchReducer,
    signalR: signalRReducer,
    user: userReducer,
    userProfile: userProfileReducer,
    cues: cuesReducer,
    app: appReducer,
})

export const selectors = {
    ...queueSelectors,
    ...playerSelectors,
    ...emceeSelectors,
    ...signalRSelectors,
    ...userSelectors,
    ...userProfileSelectors,
    ...appSelectors,
    ...cuesSelectors,
    ...recommendationsSelectors,
    ...searchSelectors,
}

export function* rootSaga() {
    yield all([
        fork(searchSaga),
        fork(recommendationsSaga),
        fork(userSaga),
        fork(userProfileSaga),
        fork(queueSaga),
        fork(emceeSaga),
    ]);
}