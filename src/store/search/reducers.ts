/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';
import { UserProfileActionTypes } from 'store/userProfile';
import { Video } from 'store/videos';
import { SearchActionTypes, SearchState } from './types';

const initialState: SearchState = {
    results: [],
    autocomplete: [],
    searching: false,
}

const reducer: Reducer<SearchState> = (state = initialState, action) => {
    switch (action.type) {
        case SearchActionTypes.FETCH_REQUEST: {
            return { ...state, searching: true, results: [] }
        }
        case SearchActionTypes.FETCH_SUCCESS: {
            const results = (action.payload as Video[]);
            return { ...state, results, autocomplete: [], searching: false }
        }
        case SearchActionTypes.FETCH_ERROR: {
            return { ...state, searching: false, results: [] }
        }
        case SearchActionTypes.AUTOCOMPLETE_QUERY: {
            if (action.payload.query !== '')
                return state;

            return { ...state, autocomplete: [] }
        }
        case SearchActionTypes.AUTOCOMPLETE_RESULTS: {
            const results = (action.payload as Video[]);
            return { ...state, autocomplete: results, results: [] };
        }
        case SearchActionTypes.AUTOCOMPLETE_CLEAR: {
            return { ...state, autocomplete: [] }
        }
        case UserProfileActionTypes.LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }

}

export { reducer as searchReducer }