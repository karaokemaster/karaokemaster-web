/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Video } from 'store/videos';
import { action } from 'typesafe-actions';
import { SearchActionTypes } from './types';

export const fetchRequest = (query: string) => action(SearchActionTypes.FETCH_REQUEST, { query })
export const fetchSuccess = (results: Video[]) => action(SearchActionTypes.FETCH_SUCCESS, results)
export const fetchError = (message: string) => action(SearchActionTypes.FETCH_ERROR, message)
export const autocompleteQuery = (query: string) => action(SearchActionTypes.AUTOCOMPLETE_QUERY, { query })
export const autocompleteResults = (results: Video[]) => action(SearchActionTypes.AUTOCOMPLETE_RESULTS, results)
export const autocompleteClear = () => action(SearchActionTypes.AUTOCOMPLETE_CLEAR)

export const searchActions = {
    fetchError,
    fetchRequest,
    fetchSuccess,
    autocompleteQuery,
    autocompleteResults,
    autocompleteClear,
}
