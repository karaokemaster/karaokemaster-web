/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { all, fork, put, select, takeEvery } from 'redux-saga/effects';

import { authHeaderSelector } from 'store/userProfile';
import { Video } from 'store/videos/types';
import { fetchError, fetchSuccess } from './actions';
import { SearchActionTypes } from './types';

import { runtimeConfig } from 'runtimeConfig';

function* handleFetch(action: any) {
    try {
        const { query } = action.payload;
        const headers: Headers = new Headers(yield select(authHeaderSelector));

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/video/search?query=${encodeURIComponent(query)}`, { headers });
        const results: Video[] = yield response.json();
    
        if (!response.ok) {
            yield put(fetchError(yield response.text()));
        }
        else {
            yield put(fetchSuccess(results));
        }
    }
    catch (err) {
        yield put(fetchError('An unknown error occurred!'));
    }
}

function* watchFetchRequest() {
    yield takeEvery(SearchActionTypes.FETCH_REQUEST, handleFetch);
}

function* searchSaga() {
    yield all([
        fork(watchFetchRequest),
    ]);
}

export default searchSaga;