/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

export enum CueState {
    Unknown = -1,
    Off = 0,
    On = 1,
}

export interface Cue {
    index: number;
    name: string;
    theme: boolean;
    color: string;
    state: CueState;
}

export enum CueActionTypes {
    SEND_CUE = '@@cues/SEND_CUE',
    CUE_SENT = '@@cues/CUE_SENT',
    REQUEST_CUES = '@@cues/REQUEST_CUES',
    UPDATE_CUES = '@@cues/UPDATE_CUES',
}

export interface CuesState {
    readonly cues: Cue[];
    readonly pendingCue?: Cue;
}