/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import createCachedSelector from 're-reselect';
import { createSelector } from 'reselect';

import { ApplicationState } from 'store';
import { nowPlayingSelector } from 'store/queue';
import { Cue, CueState } from './types';

export const lightingCuesSelector = ({ cues }: ApplicationState) => cues.cues;
export const pendingCueSelector = ({ cues }: ApplicationState) => cues.pendingCue;

export const lightingCueThemesSelector = createSelector(
    lightingCuesSelector,
    (cues) => cues.filter(c => c.theme)
);

export const lightingCueByNameSelector = createCachedSelector(
    lightingCuesSelector,
    (state: ApplicationState, name: string) => name,
    (cues, name) => cues.find(c => c.name === name)
)(
    (state, name) => name
);

export const isCueSavedToCurrentTrack = createCachedSelector(
    nowPlayingSelector,
    (state: ApplicationState, cue: Cue) => cue,
    (nowPlaying, cue) => {
        return nowPlaying?.lightingCue === cue.name;
    }
)(
    (state, cue) => cue.name
);

export const currentTrackCueSelector = createSelector(
    nowPlayingSelector,
    lightingCuesSelector,
    (nowPlaying, cues) => cues.find(c => c.name === nowPlaying?.lightingCue)
)

export const activeCueSelector = createSelector(
    lightingCuesSelector,
    (cues) => cues.find(c => c.state === CueState.On)
)

export const isCuePendingSelector = createCachedSelector(
    pendingCueSelector,
    (state: ApplicationState, cue: Cue) => cue,
    (pendingCue, cue) => {
        return pendingCue?.index === cue.index;
    }
)(
    (state, cue) => cue.index
);
