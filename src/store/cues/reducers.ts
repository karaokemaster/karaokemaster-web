/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';
import { firstBy } from 'thenby';
import { Cue, CueActionTypes, CuesState, CueState } from './types';

const initialState: CuesState = {
    cues: []
}

const reducer: Reducer<CuesState> = (state = initialState, action) => {
    switch (action.type) {
        case CueActionTypes.UPDATE_CUES: {
            const cues = (action.payload as Cue[]).sort(firstBy<any>(c => c.theme).thenBy(c => c.name));
            return { ...state, cues, pendingCue: undefined };
        }
        case CueActionTypes.SEND_CUE: {
            const cueIndex = action.payload;
            const cue = state.cues.find(c => c.index === cueIndex);
            if (cue?.state === CueState.On)
                return state;

            return { ...state, pendingCue: cue };
        }
        case CueActionTypes.CUE_SENT: {
            const sentCue = action.payload;
            const cue = state.cues.find(c => c.index === sentCue.index);
            if (cue?.state === CueState.On)
                return state;

            return { ...state, pendingCue: sentCue };
        }
        default: {
            return state;
        }
    }

}

export { reducer as cuesReducer }