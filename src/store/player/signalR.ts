/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import * as signalR from '@microsoft/signalr';
import { HubConnectionState } from '@microsoft/signalr';
import * as Sentry from '@sentry/browser';

import { runtimeConfig } from 'runtimeConfig';
import { AppActionTypes } from 'store/app';
import { PlaybackState, PlayerActionTypes, PlayerStateMessage, queueIdSelector, Reaction, videoTimeSelector } from 'store/player';
import { ConnectionStatus, retryPolicy } from 'store/signalR';
import { userIdSelector } from 'store/userProfile';

const connection = new signalR.HubConnectionBuilder()
    .withUrl(`${runtimeConfig().client.apiUrlBase}/hubs/player`)
    .withAutomaticReconnect(retryPolicy)
    .build();

let startConnection: () => Promise<void>;
let playerStateSubject: signalR.Subject<PlayerStateMessage>;
let playerStateSubscription: signalR.ISubscription<PlayerStateMessage>;
let reactionSubscription: signalR.ISubscription<Reaction>;

const delay = (ms: number) => {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

const invokeMiddleware = (store: any) => (next: any) => async (action: any) => {
    switch (action.type) {
        case AppActionTypes.CONNECT_AS_PLAYER: {
            // TODO: This is a major hack
            // TODO: Reconnect if connection is lost
            while (connection.state !== signalR.HubConnectionState.Connected) {
                await delay(500);
            }

            playerStateSubject = new signalR.Subject<PlayerStateMessage>();
            await connection.send("PublishPlayerState", playerStateSubject);

            reactionSubscription = connection
                .stream("SubscribeReactions")
                .subscribe({
                    next: (state) => {
                        const queueId = queueIdSelector(store.getState());
                        if (state.queueId !== queueId) return;

                        store.dispatch({ type: PlayerActionTypes.REACTION, payload: { ...state } });
                    },
                    complete: () => {
                        // Nothing
                    },
                    error: (error) => {
                        Sentry.captureException(error);
                    },
                });

            break;
        }
        case AppActionTypes.CONNECT_AS_KIOSK: {
            // TODO: Deduplicate code
            // TODO: This is a major hack
            // TODO: Reconnect if connection is lost
            while (connection.state !== signalR.HubConnectionState.Connected) {
                await delay(500);
            }

            playerStateSubscription = connection
                .stream("SubscribePlayerState")
                .subscribe({
                    next: (state) => {
                        const { videoTime: videoTimeHiRes, videoDuration, queueId } = state;
                        const videoTime = videoTimeHiRes ? parseInt(videoTimeHiRes.toFixed(0), 10) : undefined;
                        const videoTimeLoRes = videoTime ? Math.floor(videoTime / 10) * 10 : undefined;

                        store.dispatch({ type: PlayerActionTypes.STATE_RECEIVED, payload: { state: state.state, videoTime, videoTimeHiRes, videoTimeLoRes, videoDuration, queueId } });
                    },
                    complete: () => {
                        store.dispatch({
                            type: PlayerActionTypes.STATE_RECEIVED,
                            payload: { state: PlaybackState.Disconnected, videoTime: undefined, videoTimeHiRes: undefined, videoDuration: undefined, queueId: undefined }
                        });
                    },
                    error: (error) => {
                        Sentry.captureException(error);
                        store.dispatch({
                            type: PlayerActionTypes.STATE_RECEIVED,
                            payload: { state: PlaybackState.Disconnected, videoTime: undefined, videoTimeHiRes: undefined, videoDuration: undefined, queueId: undefined }
                        });
                    },
                });

            // TODO: Only connect for kiosk queue
            reactionSubscription = connection
                .stream("SubscribeReactions")
                .subscribe({
                    next: (state) => {
                        const queueId = queueIdSelector(store.getState());
                        if (state.queueId !== queueId) return;

                        store.dispatch({ type: PlayerActionTypes.REACTION, payload: { ...state } });
                    },
                    complete: () => {
                        // Nothing
                    },
                    error: (error) => {
                        Sentry.captureException(error);
                    },
                });

            break;

        }
        case AppActionTypes.CONNECT_AS_USER:
        case AppActionTypes.CONNECT_AS_EMCEE: {
            // TODO: This is a major hack
            // TODO: Reconnect if connection is lost
            while (connection.state !== signalR.HubConnectionState.Connected) {
                await delay(500);
            }

            playerStateSubscription = connection
                .stream("SubscribePlayerState")
                .subscribe({
                    next: (state) => {
                        const { videoTime: videoTimeHiRes, videoDuration, queueId } = state;
                        const videoTime = videoTimeHiRes ? parseInt(videoTimeHiRes.toFixed(0), 10) : undefined;
                        const videoTimeLoRes = videoTime ? Math.floor(videoTime / 10) * 10 : undefined;

                        store.dispatch({ type: PlayerActionTypes.STATE_RECEIVED, payload: { state: state.state, videoTime, videoTimeHiRes, videoTimeLoRes, videoDuration, queueId } });
                    },
                    complete: () => {
                        store.dispatch({
                            type: PlayerActionTypes.STATE_RECEIVED,
                            payload: { state: PlaybackState.Disconnected, videoTime: undefined, videoTimeHiRes: undefined, videoDuration: undefined, queueId: undefined }
                        });
                    },
                    error: (error) => {
                        Sentry.captureException(error);
                        store.dispatch({
                            type: PlayerActionTypes.STATE_RECEIVED,
                            payload: { state: PlaybackState.Disconnected, videoTime: undefined, videoTimeHiRes: undefined, videoDuration: undefined, queueId: undefined }
                        });
                    },
                });

            break;
        }
        case PlayerActionTypes.SET_STATE: {
            // TODO: Should throw an exception, or at least log something
            if (!playerStateSubject) break;

            const state: PlayerStateMessage = action.payload;

            playerStateSubject.next(state);
            break;
        }
        case PlayerActionTypes.RECONNECT:
        case AppActionTypes.WAKE: {
            await startConnection();
            break;
        }
        case AppActionTypes.DISCONNECT: {
            if (playerStateSubject) {
                playerStateSubject.complete();
            }

            if (playerStateSubscription) {
                playerStateSubscription.dispose();
            }

            if (reactionSubscription) {
                reactionSubscription.dispose();
            }

            break;
        }
        case PlayerActionTypes.SEND_PLAYER_CONTROL: {
            await connection.invoke("SendPlayerControl", action.payload);
            break;
        }
        case PlayerActionTypes.SEND_KEYDOWN: {
            if (connection.state === HubConnectionState.Connected)
                await connection.invoke("SendKeyDown", action.payload);
            break;
        }
        case PlayerActionTypes.SEND_REACTION: {
            if (connection.state === HubConnectionState.Connected) {
                const state = store.getState();

                const reaction = {
                    queueId: queueIdSelector(state),
                    userId: userIdSelector(state),
                    videoTime: videoTimeSelector(state),
                    reaction: action.payload,
                };

                await connection.invoke("SendReaction", reaction);
            }
            break;
        }
    }

    return next(action);
}

// tslint:disable-next-line:ban-types
const registerCommands = (store: any) => {
    connection.on("PlayerKeyDown", data => {
        // TODO: Come up with a better way to detect player vs. user mode
        if (playerStateSubject) {
            const keyEvent = new KeyboardEvent("keydown", { code: data } as KeyboardEventInit)
            document.dispatchEvent(keyEvent);
        }
        else {
            const keyEvent = new KeyboardEvent("km_keydown", { code: data } as KeyboardEventInit)
            document.dispatchEvent(keyEvent);

        }
        store.dispatch({ type: PlayerActionTypes.KEYDOWN, payload: data });
    })

    connection.on("ControlPlayer", data => {
        store.dispatch({ type: PlayerActionTypes.CONTROL_PLAYER, payload: data });
    });

    connection.onclose(() => {
        store.dispatch({ type: PlayerActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Disconnected });
    });

    connection.onreconnecting(() => {
        store.dispatch({ type: PlayerActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Reconnecting });
    });

    connection.onreconnected(() => {
        store.dispatch({ type: PlayerActionTypes.RECONNECTED });
        if (playerStateSubject)
            store.dispatch({ type: AppActionTypes.CONNECT_AS_PLAYER });
        if (playerStateSubscription)
            store.dispatch({ type: AppActionTypes.CONNECT_AS_USER });
    });

    startConnection = async() => {
        const currentStatus = store.getState().player.connectionStatus;
        if (currentStatus === ConnectionStatus.Connected || currentStatus === ConnectionStatus.Connecting) { 
            return;
        }

        store.dispatch({ type: PlayerActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connecting });

        try {
            await connection.start();
            store.dispatch({ type: PlayerActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connected });

            // TODO: This is rill gross, so fix it...
            if (playerStateSubject)
                store.dispatch({ type: AppActionTypes.CONNECT_AS_PLAYER });
            if (playerStateSubscription)
                store.dispatch({ type: AppActionTypes.CONNECT_AS_USER });
        } catch {
            store.dispatch({ type: PlayerActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Disconnected });
            setTimeout(() => {
                store.dispatch({ type: PlayerActionTypes.RECONNECT });
            }, 1000);
        }
    }

    startConnection();
}

export { invokeMiddleware, registerCommands }