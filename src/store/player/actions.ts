/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { action } from 'typesafe-actions';
import { ConnectionStatus } from '../signalR';
import { PlayerActionTypes, PlayerControl, PlayerStateMessage, Reaction } from './types';

export const setState = (state: PlayerStateMessage) => action(PlayerActionTypes.SET_STATE, state);
export const sendPlayerControl = (control: PlayerControl) => action(PlayerActionTypes.SEND_PLAYER_CONTROL, control);
export const controlPlayer = (control: PlayerControl) => action(PlayerActionTypes.CONTROL_PLAYER, control);
export const completePlayerControl = (control: PlayerControl) => action(PlayerActionTypes.CONTROL_COMPLETE, control);
export const startPlayer = () => action(PlayerActionTypes.START_PLAYER);
export const unstartPlayer = () => action(PlayerActionTypes.UNSTART_PLAYER);
export const setConnectionStatus = (connectionStatus: ConnectionStatus) => action(PlayerActionTypes.SET_CONNECTION_STATUS, connectionStatus);
export const reconnect = () => action(PlayerActionTypes.RECONNECT);
export const sendKeyDown = (code: string) => action(PlayerActionTypes.SEND_KEYDOWN, code);
export const sendReaction = (reaction: string) => action(PlayerActionTypes.SEND_REACTION, reaction);
export const reactionComplete = (reaction: Reaction) => action(PlayerActionTypes.REACTION_COMPLETE, reaction);
