/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';

import { QueueActionTypes, QueueEntry } from 'store/queue';
import { ConnectionStatus } from 'store/signalR';
import { UserProfileActionTypes } from 'store/userProfile';
import { PlaybackState, PlayerActionTypes, PlayerControl, PlayerState } from './types';

const initialState: PlayerState = {
    pendingControls: [],
    started: false,
    state: PlaybackState.Disconnected,
    videoDuration: 0,
    videoTime: 0,
    videoTimeHiRes: 0,
    videoTimeLoRes: 0,
    connectionStatus: ConnectionStatus.Unknown,
    bpm: 0,
    bpmTimestamp: 0,
    reactions: [],
}

const reducer: Reducer<PlayerState> = (state = initialState, action) => {
    switch (action.type) {
        case PlayerActionTypes.START_PLAYER: {
            return { ...state, started: true };
        }
        case PlayerActionTypes.UNSTART_PLAYER: {
            return { ...state, started: false };
        }
        case QueueActionTypes.UPDATE_QUEUE: {
            const entries = (action.payload as QueueEntry[]);

            if (!entries || entries.length < 1)
                return { ...state, started: false };

            return { ...state };
        }
        case PlayerActionTypes.CONTROL_PLAYER: {
            const pendingControls = [...state.pendingControls].concat([action.payload]);
            const started = state.started || action.payload === PlayerControl.Play;
            return { ...state, pendingControls, started };
        }
        case PlayerActionTypes.BEAT: {
            const bpm = (action.payload as number).toFixed(0);
            const bpmTimestamp = new Date().getTime();
            return { ...state, bpm, bpmTimestamp };
        }
        case PlayerActionTypes.CONTROL_COMPLETE: {
            let pendingControls = [...state.pendingControls];
            if (pendingControls.length < 1 || pendingControls[0] !== action.payload) {
                return state;
            }
            pendingControls = pendingControls.slice(1);

            return { ...state, pendingControls }
        }
        case PlayerActionTypes.SET_STATE:
        case PlayerActionTypes.STATE_RECEIVED: {
            let playerState = action.payload;
            if (!playerState.videoTime)
                delete playerState.videoTime;
            if (!playerState.videoTimeHiRes)
                delete playerState.videoTimeHiRes;
            if (!playerState.videoTimeLoRes)
                delete playerState.videoTimeLoRes;
            if (!playerState.videoDuration)
                delete playerState.videoDuration;

            if (state.queueId !== playerState.queueId) {
                playerState = {
                    ...playerState,
                    videoTime: 0,
                    videoTimeHiRes: 0,
                    videoTimeLoRes: 0,
                    videoDuration: 0,
                };
            }

            return { ...state, ...playerState }
        }
        case PlayerActionTypes.SET_CONNECTION_STATUS: {
            return { ...state, connectionStatus: action.payload }
        }
        case PlayerActionTypes.RECONNECTED: {
            return { ...state, connectionStatus: ConnectionStatus.Connected }
        }
        case PlayerActionTypes.REACTION: {
            const reaction = {
                ...action.payload,
                key: new Date().getTime(),
            };
            // TODO: Will need to purge old reactions on track change
            const reactions = state.reactions.slice(-50).concat([reaction]);
            // const reactions = [...state.reactions.filter(r => r.queueId !== reaction.queueId && r.userId !== reaction.userId && r.videoTime !== reaction.videoTime)].concat([reaction]);
            return { ...state, reactions };
        }
        case PlayerActionTypes.REACTION_COMPLETE: {
            const reactions = [...state.reactions.filter(r => r.key !== action.payload.key)];
            return { ...state, reactions };
        }
        case UserProfileActionTypes.LOGOUT: {
            return { ...initialState, connectionStatus: state.connectionStatus };
        }
        default: {
            return state;
        }
    }

}

export { reducer as playerReducer }