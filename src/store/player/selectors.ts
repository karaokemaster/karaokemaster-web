/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import isEqual from 'lodash.isequal';
import { createSelector, createSelectorCreator, defaultMemoize } from 'reselect'
import { firstBy } from 'thenby';

import { ApplicationState } from 'store';
import { PlaybackState, PlayerStateMessage } from './types';

// create a "selector creator" that uses lodash.isequal instead of ===
const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    isEqual
);

export const playerSelector = ({ player }: ApplicationState) => player;

export const playerConnectionStatusSelector = ({ player }: ApplicationState) => player.connectionStatus;
export const playerTempoSelector = ({ player }: ApplicationState) => player.bpm;
export const playerTempoBeatSelector = ({ player }: ApplicationState) => player.bpmTimestamp;
export const playerStartedSelector = ({ player }: ApplicationState) => player.started;
export const playerPendingControlsSelector = ({ player }: ApplicationState) => player.pendingControls;
export const playerReactionsSelector = ({ player }: ApplicationState) => player.reactions;

export const videoTimeSelector = createDeepEqualSelector(
    playerSelector,
    (player) => player.videoTime,
);
export const videoTimeHiResSelector = createDeepEqualSelector(
    playerSelector,
    (player) => player.videoTimeHiRes,
);
export const videoTimeLoResSelector = createDeepEqualSelector(
    playerSelector,
    (player) => player.videoTimeLoRes,
);
export const videoDurationSelector = createDeepEqualSelector(
    playerSelector,
    (player) => player.videoDuration,
);
export const queueIdSelector = createDeepEqualSelector(
    playerSelector,
    (player) => player.queueId,
);
export const playbackStateSelector = createDeepEqualSelector(
    playerSelector,
    (player) => player.state,
);

export const playerStateSelector = createSelector(
    videoTimeSelector,
    videoDurationSelector,
    queueIdSelector,
    playbackStateSelector,
    (videoTime, videoDuration, queueId, state): PlayerStateMessage => ({
        videoTime,
        videoDuration,
        queueId,
        state,
    }),
)

export const playbackStatePlayingSelector = createSelector(
    playbackStateSelector,
    (state) => state === PlaybackState.Playing || state === PlaybackState.StoppingAfter,
);

export const playbackStateStartingSelector = createSelector(
    playbackStateSelector,
    (state) => state === PlaybackState.Starting
);

export const playerCurrentQueueEntrySelector = createSelector(
    ({ queue }: ApplicationState) => queue.entries,
    (entries) => {
        const sorted = entries.filter(e => e.order >= 0).sort(firstBy(e => e.order))
        if (sorted.length < 1) return undefined;
        return sorted[0];
    }
)

export const playerNextQueueEntrySelector = createSelector(
    ({ queue }: ApplicationState) => queue.entries,
    (entries) => {
        const sorted = entries.filter(e => e.order >= 0).sort(firstBy(e => e.order))
        if (sorted.length < 2) return undefined;
        return sorted[1];
    }
)