/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { FontAwesomeIconProps } from "@fortawesome/react-fontawesome";
import { ConnectionStatus } from "../signalR";

export enum PlayerActionTypes {
    SET_STATE = '@@player/SET_STATE',
    STATE_RECEIVED = '@@player/STATE_RECEIVED',
    START_PLAYER = '@@player/START_PLAYER',
    UNSTART_PLAYER = '@@player/UNSTART_PLAYER',
    SEND_PLAYER_CONTROL = '@@signalr/SEND_PLAYER_CONTROL',
    CONTROL_PLAYER = '@@player/CONTROL_PLAYER',
    CONTROL_COMPLETE = '@@player/CONTROL_COMPLETE',
    SET_CONNECTION_STATUS = '@@player/SET_CONNECTION_STATUS',
    RECONNECT = '@@player/RECONNECT',
    RECONNECTED = '@@player/RECONNECTED',
    BEAT = '@@player/BEAT',
    SEND_KEYDOWN = '@@player/SEND_KEYDOWN',
    KEYDOWN = '@@player/KEYDOWN',
    SEND_REACTION = '@@player/SEND_REACTION',
    REACTION = '@@player/REACTION',
    REACTION_COMPLETE = '@@player/REACTION_COMPLETE',
}

export enum PlaybackState {
    Stopped = 0,
    Playing = 1,
    Starting = 2,
    StoppingAfter = 3,
    Failed = -2,
    Disconnected = -1,
}

export enum PlayerControl {
    Play = "PLAY_TRACK",
    Pause = "PAUSE_TRACK",
    Next = "NEXT_TRACK",
    Back = "RESTART_TRACK",
}

export interface PlayerStateMessage {
    readonly state: PlaybackState
    readonly queueId?: number
    readonly videoTime?: number
    readonly videoDuration?: number
}

export interface Reaction {
    readonly key?: number
    readonly queueId: number
    readonly userId: number
    readonly videoTime: number
    readonly reaction: string
}

export interface PlayerState {
    readonly started: boolean
    readonly state: PlaybackState
    readonly queueId?: number
    readonly videoTime: number
    readonly videoTimeHiRes: number
    readonly videoTimeLoRes: number
    readonly videoDuration: number
    readonly bpm: number
    readonly bpmTimestamp: number
    readonly pendingControls: PlayerControl[]
    readonly connectionStatus: ConnectionStatus
    readonly reactions: Reaction[]
}
