/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { IRetryPolicy } from '@microsoft/signalr';

export enum SignalRActionTypes {
    SET_STATUS = '@@signalr/SET_STATUS',
    RECONNECT = '@@signalr/RECONNECT',
}

export enum ConnectionStatus {
    Unknown,
    Connecting,
    Connected,
    Disconnected,
    Reconnecting,
}

export interface SignalRState {
    readonly connectionStatus: ConnectionStatus
}

export const retryPolicy: IRetryPolicy = {
    nextRetryDelayInMilliseconds: retryContext => {
        if (retryContext.elapsedMilliseconds < 10000) {
            return 1000;
        }
        if (retryContext.elapsedMilliseconds < 60000) {
            // If we've been reconnecting for less than 60 seconds so far,
            // wait between 1 and 5 seconds before the next reconnect attempt.
            return 1000 + Math.random() * 4000;
        } else {
            // If we've been reconnecting for more than 60 seconds so far, stop reconnecting.
            return null;
        }
    }
};