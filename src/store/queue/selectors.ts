/*

    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import isEqual from 'lodash.isequal';
import { createCachedSelector } from 're-reselect';
import { createSelector, createSelectorCreator, defaultMemoize } from 'reselect';
import serialize from 'serialize-javascript';
import { firstBy } from 'thenby';

import { defaultColor } from 'components/user-profile/settings/colorPicker';
import { defaultIcon } from 'components/user-profile/settings/iconPicker';
import { ApplicationState } from 'store';
import { queueIdSelector, videoDurationSelector, videoTimeLoResSelector, videoTimeSelector } from 'store/player';
import { isCurrentUserSelector, usernameSelector } from 'store/userProfile';
import { PlayingQueueEntry, QueueEntry, TimedQueueEntry } from './types';

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    isEqual
);

export const queueTimestampSelector = (state: ApplicationState) => state.queue.timestamp;
export const queueConnectionStatusSelector = ({ queue }: ApplicationState) => queue.connectionStatus;

export const queueEntriesSelector = createDeepEqualSelector(
    ({ queue }: ApplicationState) => queue.entries,
    (state: ApplicationState) => state,
    (entries, state) => entries
        .filter(e => e.order >= 0)
        .map(e => ({
            ...e,
            rating: isCurrentUserSelector(e.user)(state) ? e.rating : 0,
        })),
);

export const queueEntriesCountSelector = createSelector(
    queueEntriesSelector,
    (entries) => entries.length,
);

export const playedEntriesSelector = createSelector(
    ({ queue }: ApplicationState) => queue.entries,
    (state: ApplicationState) => state,
    (entries, state) => entries
        .filter(e => e.order < 0)
        .map(e => ({
            ...e,
            rating: isCurrentUserSelector(e.user)(state) ? e.rating : 0,
        })),
);

export const sortedQueueSelector = createDeepEqualSelector(
    queueEntriesSelector,
    (entries) => entries
        .map(e => ({
            ...e,
            userIcon: e.userIcon || serialize(defaultIcon(e.user)),
            userColor: e.userColor || defaultColor(e.user)
        }))
        .sort(firstBy<QueueEntry>(e => e.order).thenBy(e => e.queueId)),
);

export const sortedTimedQueueSelector = createSelector(
    sortedQueueSelector,
    (queue) => {
        const timedQueue: TimedQueueEntry[] = new Array();

        queue.forEach((e, i) => {
            const timeUntil = i > 0
                ? timedQueue[i - 1].timeUntil + timedQueue[i - 1].duration
                : 0;

            timedQueue.push({
                ...e,
                timeUntil,
            })
        })

        return timedQueue;
    }
);

export const nowPlayingEntrySelector = createDeepEqualSelector(
    sortedQueueSelector,
    (entries): QueueEntry | undefined  => {
        const entry = entries.find(e => e.order === 0);

        return entry;
    },
);

export const nowPlayingSelector = createDeepEqualSelector(
    nowPlayingEntrySelector,
    videoTimeSelector,
    videoDurationSelector,
    queueIdSelector,
    (entry, videoTime, videoDuration, queueId): PlayingQueueEntry | undefined  => {
        if (entry && entry.queueId === queueId) {
            return {
                ...entry,
                timeUntil: videoDuration > 0 ? -videoTime : 0,
                videoTime,
                videoDuration,
            }
        }
        else if (entry) {
            return {
                ...entry,
                timeUntil: 0,
                videoTime: 0,
                videoDuration: 0,
            }
        }

        return undefined;
    },
);

export const upNextSelector = createSelector(
    sortedTimedQueueSelector,
    (entries) => entries.filter(e=> e.order > 0),
);

export const timedQueueSelector = createSelector(
    nowPlayingSelector,
    upNextSelector,
    (nowPlaying, upNext) => {
        const timedQueue: TimedQueueEntry[] = new Array();
        const offset = nowPlaying ? 0 : -1;

        if (nowPlaying)
            timedQueue.push(nowPlaying);

        upNext.forEach((e, i) => {
            const timeUntil = (i + offset) >= 0
                ? timedQueue[i + offset].timeUntil + timedQueue[i + offset].duration
                : 0;

            timedQueue.push({
                ...e,
                timeUntil,
            })
        })

        return timedQueue;
    }
);

export const nextTrackForUserSelector = createSelector(
    usernameSelector,
    nowPlayingEntrySelector,
    sortedTimedQueueSelector,
    videoTimeLoResSelector,
    queueIdSelector,
    (username, nowPlaying, upNext, videoTime, queueId) => {
        if (nowPlaying && nowPlaying.user === username) {
            return {
                timeUntil: 0,
                tracksUntil: 0,
            }
        }

        const nextIndex = upNext.findIndex(e => e.user === username);

        if (nextIndex < 0)
            return undefined;

        return {
            tracksUntil: nextIndex,
            timeUntil: upNext[nextIndex].timeUntil - (nowPlaying?.queueId === queueId ? videoTime : 0),
        }
    }
);

export const nextTrackForUsernameSelector = (username: string) => createSelector(
    sortedTimedQueueSelector,
    nowPlayingEntrySelector,
    videoTimeLoResSelector,
    queueIdSelector,
    (upNext, nowPlaying, videoTime, queueId) => {
        const nextIndex = upNext.findIndex(e => e.order > 0 && e.user === username);

        if (nextIndex < 0)
            return undefined;

        return {
            tracksUntil: nextIndex,
            timeUntil: upNext[nextIndex].timeUntil - (nowPlaying?.queueId === queueId ? videoTime : 0),
        }
    }
);

export const userHasTrackInQueueSelector = createSelector(
    nextTrackForUserSelector,
    (track) => track !== undefined,
);

export const isTrackInQueueSelector = createCachedSelector(
    queueEntriesSelector,
    (state: ApplicationState, videoId: string) => videoId,
    (entries, videoId) => {
        return entries.some(e => e.videoId === videoId);
    }
)(
    (state, videoId) => videoId
);

export const isTrackPlayingSelector = createCachedSelector(
    nowPlayingSelector,
    (state: ApplicationState, videoId: string) => videoId,
    (entry, videoId) => {
        return entry?.videoId === videoId;
    }
)(
    (state, videoId) => videoId
);

export const hasTrackBeenPlayedSelector = createCachedSelector(
    playedEntriesSelector,
    (state: ApplicationState, videoId: string) => videoId,
    (entries, videoId) => entries
        .some(e => e.videoId === videoId),
)(
    (state, videoId) => videoId
)