/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Video } from 'store/videos';
import { action } from 'typesafe-actions';
import { QueueEntry } from './types';
import { QueueActionTypes } from './types';

export const enqueue = (video: Video) => action(QueueActionTypes.ENQUEUE_VIDEO, video);
export const enqueueForUser = (user: string, video: Video) => action(QueueActionTypes.ENQUEUE_VIDEO_FOR_USER, { user, video });
export const enqueueSuccess = () => action(QueueActionTypes.ENQUEUE_SUCCESS);
export const enqueueError = (error: string) => action(QueueActionTypes.ENQUEUE_ERROR, error);

export const dequeue = (entry: QueueEntry) => action(QueueActionTypes.DEQUEUE_VIDEO, entry);
export const dequeueSuccess = () => action(QueueActionTypes.DEQUEUE_SUCCESS);
export const dequeueError = (error: string) => action(QueueActionTypes.DEQUEUE_ERROR, error);

export const move = (entry: QueueEntry, afterId?: number, beforeId?: number) => action(QueueActionTypes.MOVE_VIDEO, { entry, afterId, beforeId });
export const moveSuccess = () => action(QueueActionTypes.MOVE_SUCCESS);
export const moveError = (error: string) => action(QueueActionTypes.MOVE_ERROR, error);

export const setEntryLyrics = (entry: QueueEntry, lyrics: string) => action(QueueActionTypes.SET_ENTRY_LYRICS, { entry, lyrics });
export const setRating = (entry: QueueEntry, rating: number) => action(QueueActionTypes.SET_ENTRY_RATING, { entry, rating });
export const setEntryUser = (entry: QueueEntry, user: string) => action(QueueActionTypes.SET_ENTRY_USER, { entry, user });
export const setEntryLightingCue = (entry: QueueEntry, cue: string) => action(QueueActionTypes.SET_ENTRY_LIGHTING_CUE, { entry, cue });
export const setEntrySuccess = (prop: string) => action(QueueActionTypes.SET_ENTRY_SUCCESS, prop);
export const setEntryError = (prop: string, error: string) => action(QueueActionTypes.SET_ENTRY_ERROR, { prop, error });

export const setEntryPlayState = (queueId: number, playState: number, startNextQueueId?: number) => action(QueueActionTypes.UPDATE_VIDEO_STATE, { queueId, playState, startNextQueueId });
export const updateQueue = (entries: QueueEntry[]) => action(QueueActionTypes.UPDATE_QUEUE, entries);
