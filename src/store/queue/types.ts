/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { ConnectionStatus } from 'store/signalR';

export interface QueueEntry {
    queueId: number
    videoId: string
    title: string
    duration: number
    thumbnailUrl: string
    order: number
    user: string
    userIcon: string
    userColor: string
    lightingCue: string
    rating: number
    lyrics: string
}

export type TimedQueueEntry = QueueEntry & { timeUntil: number };
export type PlayingQueueEntry = TimedQueueEntry & { videoTime: number, videoDuration: number };

export enum QueueActionTypes {
    ENQUEUE_VIDEO = '@@queue/ENQUEUE_VIDEO',
    ENQUEUE_VIDEO_FOR_USER = '@@queue/ENQUEUE_VIDEO_FOR_USER',
    ENQUEUE_SUCCESS = '@@queue/ENQUEUE_SUCCESS',
    ENQUEUE_ERROR = '@@queue/ENQUEUE_ERROR',
    DEQUEUE_VIDEO = '@@queue/DEQUEUE_VIDEO',
    DEQUEUE_SUCCESS = '@@queue/DEQUEUE_SUCCESS',
    DEQUEUE_ERROR = '@@queue/DEQUEUE_ERROR',
    MOVE_VIDEO = '@@queue/MOVE_VIDEO',
    MOVE_SUCCESS = '@@queue/MOVE_SUCCESS',
    MOVE_ERROR = '@@queue/MOVE_ERROR',

    SET_ENTRY_LYRICS = '@@queue/SET_ENTRY_LYRICS',
    SET_ENTRY_RATING = '@@queue/SET_ENTRY_RATING',
    SET_ENTRY_LIGHTING_CUE = '@@queue/SET_ENTRY_LIGHTING_CUE',
    SET_ENTRY_USER = '@@queue/SET_ENTRY_USER',
    SET_ENTRY_SUCCESS = '@@queue/SET_ENTRY_SUCCESS',
    SET_ENTRY_ERROR = '@@queue/SET_ENTRY_ERROR',

    UPDATE_VIDEO_STATE = '@@queue/UPDATE_VIDEO_STATE',
    UPDATE_QUEUE = '@@queue/UPDATE_QUEUE',
    SET_CONNECTION_STATUS = '@@queue/SET_CONNECTION_STATUS',
    RECONNECT = '@@queue/RECONNECT',
    DISCONNECT = '@@queue/DISCONNECT',
}

export interface QueueState {
    readonly connectionStatus: ConnectionStatus
    readonly entries: QueueEntry[]
    readonly timestamp: number
}