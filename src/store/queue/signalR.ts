/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { HubConnectionState } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';

import { AppActionTypes } from 'store/app';
import { QueueActionTypes } from 'store/queue';
import { RecommendationsActionTypes } from 'store/recommendations';
import { SearchActionTypes } from 'store/search';
import { ConnectionStatus, retryPolicy, SignalRActionTypes } from 'store/signalR';
import { UserActionTypes } from 'store/user';
import { tokenSelector, UserProfileActionTypes } from 'store/userProfile';

import { runtimeConfig } from 'runtimeConfig';

let connection: signalR.HubConnection;

let startConnection: () => Promise<void>;

const invokeMiddleware = (store: any) => (next: any) => async (action: any) => {
    if ((!connection || connection.state !== HubConnectionState.Connected)
        && action.type !== UserProfileActionTypes.LOGIN_COMPLETE
        && action.type !== AppActionTypes.WAKE
        && action.type !== SignalRActionTypes.RECONNECT
        && action.type !== AppActionTypes.CONNECT_AS_PLAYER
        && action.type !== AppActionTypes.CONNECT_AS_KIOSK
        && action.type !== AppActionTypes.CONNECT_AS_EMCEE)
        return next(action);

    switch (action.type) {
        case UserProfileActionTypes.LOGIN_COMPLETE:
        case AppActionTypes.CONNECT_AS_PLAYER:
        case AppActionTypes.CONNECT_AS_KIOSK:
        case AppActionTypes.CONNECT_AS_EMCEE: {
            await connection?.stop();
            await startConnection();
            break;
        }
        case UserProfileActionTypes.LOGOUT:
        case AppActionTypes.DISCONNECT: {
            await connection.stop();
            break;
        }
        case AppActionTypes.WAKE: {
            await startConnection();
            break;
        }

        case QueueActionTypes.UPDATE_VIDEO_STATE: {
            const { queueId, playState, startNextQueueId } = action.payload;

            await connection.invoke("UpdateQueueState", queueId, playState, startNextQueueId);
            break;
        }
        case SearchActionTypes.AUTOCOMPLETE_QUERY: {
            const { query } = action.payload;

            await connection.invoke("SearchAutocomplete", query);
            break;
        }
        case QueueActionTypes.RECONNECT: {
            await startConnection();
            break;
        }
    }

    return next(action);
}

// tslint:disable-next-line:ban-types
const registerCommands = (store: any) => {
    connection = new signalR.HubConnectionBuilder()
        .withUrl(
            `${runtimeConfig().client.apiUrlBase}/hubs/queue`,
            {
                accessTokenFactory: () => {
                    const state = store.getState();
                    const token = tokenSelector(state);
                    return token ?? '';
                }
            })
        .withAutomaticReconnect(retryPolicy)
        .build();

    connection.on("ServerInfo", data => {
        store.dispatch({ type: AppActionTypes.SERVER_INFO, payload: data });
    })

    connection.on("UpdateQueue", data => {
        store.dispatch({ type: QueueActionTypes.UPDATE_QUEUE, payload: data });
    })

    connection.on("UpdateRecommendations", data => {
        store.dispatch({ type: RecommendationsActionTypes.UPDATE_RECOMMENDATIONS, payload: data });
    })

    connection.on("UpdatePreviouslyPlayed", data => {
        store.dispatch({ type: RecommendationsActionTypes.UPDATE_PREVIOUSLY_PLAYED, payload: data });
    })

    connection.on("UpdateSessions", data => {
        store.dispatch({ type: UserActionTypes.UPDATE_SESSIONS, payload: data });
    })

    // TODO: Relocate?
    connection.on("UpdateAutocomplete", data => {
        store.dispatch({ type: SearchActionTypes.AUTOCOMPLETE_RESULTS, payload: data });
    })

    connection.onclose(() => {
        store.dispatch({ type: QueueActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Disconnected });
    });

    connection.onreconnecting(() => {
        store.dispatch({ type: QueueActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Reconnecting });
    });

    connection.onreconnected(() => {
        store.dispatch({ type: QueueActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connected });
    });

    startConnection = async() => {
        const currentStatus = store.getState().queue.connectionStatus;
        if (currentStatus === ConnectionStatus.Connected || currentStatus === ConnectionStatus.Connecting) {
            return;
        }

        store.dispatch({ type: QueueActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connecting });

        try {
            await connection.start();
            store.dispatch({ type: QueueActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Connected });
        } catch {
            store.dispatch({ type: QueueActionTypes.SET_CONNECTION_STATUS, payload: ConnectionStatus.Disconnected });
            setTimeout(() => {
                store.dispatch({ type: QueueActionTypes.RECONNECT });
            }, 1000);
        }
    }
}

export { invokeMiddleware, registerCommands }