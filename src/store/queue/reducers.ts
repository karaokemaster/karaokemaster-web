/*
    
    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';
import { UserProfileActionTypes } from 'store/userProfile';
import { firstBy } from 'thenby';
import { ConnectionStatus } from "../signalR";
import { QueueActionTypes, QueueEntry, QueueState } from './types';

const initialState: QueueState = {
    connectionStatus: ConnectionStatus.Unknown,
    entries: [],
    timestamp: 0,
}

const reducer: Reducer<QueueState> = (state = initialState, action) => {
    switch (action.type) {
        case QueueActionTypes.UPDATE_QUEUE: {
            const entries = (action.payload as QueueEntry[]).sort(firstBy<QueueEntry>(e => e.order).thenBy(e => e.queueId));

            return { ...state, entries, timestamp: new Date().getTime() };
        }
        case QueueActionTypes.SET_CONNECTION_STATUS: {
            return { ...state, connectionStatus: action.payload }
        }
        case UserProfileActionTypes.LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }

}

export { reducer as queueReducer }