/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { all, fork, put, select, takeEvery } from 'redux-saga/effects';
import * as serialize from 'serialize-javascript';

import { authHeaderSelector } from 'store/userProfile';
import { dequeueError, dequeueSuccess, enqueueError, enqueueSuccess, moveError, moveSuccess, setEntryError, setEntrySuccess } from './actions';
import { QueueActionTypes, QueueEntry } from './types';

import { runtimeConfig } from 'runtimeConfig';

function* handleEnqueue(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue`, {
            method: 'POST',
            headers,
            body: serialize(action.payload),
        });

        if (!response.ok) {
            yield put(enqueueError(yield response.text()));
        }
        else {
            yield put(enqueueSuccess());
        }
    }
    catch (err) {
        yield put(enqueueError('An unknown error occurred!'));
    }
}

function* handleEnqueueForUser(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));

        headers.append('Content-Type', 'application/json');

        const { video, user } = action.payload;

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue?username=${encodeURIComponent(user)}`, {
            method: 'POST',
            headers,
            body: serialize(video),
        });

        if (!response.ok) {
            yield put(enqueueError(yield response.text()));
        }
        else {
            yield put(enqueueSuccess());
        }
    }
    catch (err) {
        yield put(enqueueError('An unknown error occurred!'));
    }
}

function* handleDequeue(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const entry: QueueEntry = action.payload;

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}`, {
            method: 'DELETE',
            headers,
        });

        if (!response.ok) {
            yield put(dequeueError(yield response.text()));
        }
        else {
            yield put(dequeueSuccess());
        }
    }
    catch (err) {
        yield put(dequeueError('An unknown error occurred!'));
    }
}

function* handleMove(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const { entry, beforeId, afterId } = action.payload;

        let uri = `${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/move/between/${afterId}/${beforeId}`;

        if (!afterId)
            uri = `${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/move/before/${beforeId}`;
        else if (!beforeId)
            uri = `${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/move/after/${afterId}`;

        const response: Response = yield fetch(uri, {
            method: 'POST',
            headers,
        });

        if (!response.ok) {
            yield put(moveError(yield response.text()));
        }
        else {
            yield put(moveSuccess());
        }
    }
    catch (err) {
        yield put(moveError('An unknown error occurred!'));
    }
}

function* handleSetLyrics(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const { entry, lyrics } = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/lyrics`, {
            method: 'POST',
            headers,
            body: serialize(lyrics),
        });

        if (!response.ok) {
            yield put(setEntryError('lyrics', yield response.text()));
        }
        else {
            yield put(setEntrySuccess('lyrics'));
        }
    }
    catch (err) {
        yield put(setEntryError('lyrics','An unknown error occurred!'));
    }
}

function* handleSetRating(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const { entry, rating } = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/rating/${rating}`, {
            method: 'POST',
            headers,
        });

        if (!response.ok) {
            yield put(setEntryError('rating', yield response.text()));
        }
        else {
            yield put(setEntrySuccess('rating'));
        }
    }
    catch (err) {
        yield put(setEntryError('rating','An unknown error occurred!'));
    }
}

function* handleSetUser(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const { entry, user } = action.payload;

        headers.append('Content-Type', 'application/json');

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/user/${encodeURIComponent(user)}`, {
            method: 'POST',
            headers,
        });

        if (!response.ok) {
            yield put(setEntryError('user', yield response.text()));
        }
        else {
            yield put(setEntrySuccess('user'));
        }
    }
    catch (err) {
        yield put(setEntryError('user','An unknown error occurred!'));
    }
}

function* handleSetCue(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const { entry, cue } = action.payload;

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/queue/${entry.queueId}/lightingCue/${encodeURIComponent(cue)}`, {
            method: 'POST',
            headers,
        });

        if (!response.ok) {
            yield put(setEntryError('cue', yield response.text()));
        }
        else {
            yield put(setEntrySuccess('cue'));
        }
    }
    catch (err) {
        yield put(setEntryError('cue','An unknown error occurred!'));
    }
}

function* queueSaga() {
    yield all([
        fork(function* () { yield takeEvery(QueueActionTypes.ENQUEUE_VIDEO, handleEnqueue); }),
        fork(function* () { yield takeEvery(QueueActionTypes.ENQUEUE_VIDEO_FOR_USER, handleEnqueueForUser); }),
        fork(function* () { yield takeEvery(QueueActionTypes.DEQUEUE_VIDEO, handleDequeue); }),
        fork(function* () { yield takeEvery(QueueActionTypes.MOVE_VIDEO, handleMove); }),
        fork(function* () { yield takeEvery(QueueActionTypes.SET_ENTRY_LYRICS, handleSetLyrics); }),
        fork(function* () { yield takeEvery(QueueActionTypes.SET_ENTRY_RATING, handleSetRating); }),
        fork(function* () { yield takeEvery(QueueActionTypes.SET_ENTRY_USER, handleSetUser); }),
        fork(function* () { yield takeEvery(QueueActionTypes.SET_ENTRY_LIGHTING_CUE, handleSetCue); }),
    ]);
}

export default queueSaga;