/*
    
    Copyright 2019-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { HubConnectionState } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';
import { action as typesafeAction } from 'typesafe-actions';

import { AppActionTypes } from 'store/app';
import { CueActionTypes } from 'store/cues';
import { PlayerActionTypes } from 'store/player';
import { ConnectionStatus, retryPolicy, SignalRActionTypes } from 'store/signalR';
import { tokenSelector, UserProfileActionTypes } from 'store/userProfile';

import { runtimeConfig } from 'runtimeConfig';

let connection: signalR.HubConnection;

let startConnection: () => Promise<void>;

const signalRInvokeMiddleware = (store: any) => (next: any) => async (action: any) => {
    if (connection.state !== HubConnectionState.Connected
        && action.type !== UserProfileActionTypes.LOGIN_COMPLETE
        && action.type !== AppActionTypes.WAKE
        && action.type !== SignalRActionTypes.RECONNECT
        && action.type !== AppActionTypes.CONNECT_AS_PLAYER
        && action.type !== AppActionTypes.CONNECT_AS_KIOSK)
        return next(action);

    switch (action.type) {
        case UserProfileActionTypes.LOGIN_COMPLETE:
        case AppActionTypes.CONNECT_AS_PLAYER:
        case AppActionTypes.CONNECT_AS_KIOSK: {
            await connection?.stop();
            await startConnection();
            break;
        }
        case UserProfileActionTypes.LOGOUT:
        case AppActionTypes.DISCONNECT: {
            await connection.stop();
            break;
        }
        case AppActionTypes.WAKE: {
            await startConnection();
            break;
        }

        case CueActionTypes.REQUEST_CUES: {
            await connection.invoke("RequestCues");
            break;
        }
        case CueActionTypes.SEND_CUE: {
            const index = action.payload;

            await connection.invoke("SendCue", index);
            store.dispatch(typesafeAction(CueActionTypes.CUE_SENT, index));

            break;
        }
        case SignalRActionTypes.RECONNECT: {
            await startConnection();
            break;
        }
    }

    return next(action);
}

// tslint:disable-next-line:ban-types
const signalRRegisterCommands = (store: any) => {
    connection = new signalR.HubConnectionBuilder()
        .withUrl(
            `${runtimeConfig().client.apiUrlBase}/hub`,
            {
                accessTokenFactory: () => {
                    const state = store.getState();
                    const token = tokenSelector(state);
                    return token ?? '';
                }
            })
        .withAutomaticReconnect(retryPolicy)
        .build();

    connection.on("ServerInfo", data => {
        store.dispatch({ type: AppActionTypes.SERVER_INFO, payload: data });
    })

    connection.on("UserProfile", data => {
        store.dispatch({ type: UserProfileActionTypes.RECEIVE_USER_PROFILE, payload: data });
    })

    connection.on("UpdateCues", data => {
        store.dispatch({ type: CueActionTypes.UPDATE_CUES, payload: data });
    })

    connection.on("SendCue", data => {
        store.dispatch({ type: CueActionTypes.CUE_SENT, payload: data });
    })

    connection.on("Beat", data => {
        store.dispatch({ type: PlayerActionTypes.BEAT, payload: data });
    })

    connection.onclose(() => {
        store.dispatch({ type: SignalRActionTypes.SET_STATUS, payload: ConnectionStatus.Disconnected });
    });

    connection.onreconnecting(() => {
        store.dispatch({ type: SignalRActionTypes.SET_STATUS, payload: ConnectionStatus.Reconnecting });
    });

    connection.onreconnected(() => {
        store.dispatch({ type: SignalRActionTypes.SET_STATUS, payload: ConnectionStatus.Connected });
    });

    startConnection = async() => {
        const currentStatus = store.getState().signalR.connectionStatus;
        if (currentStatus === ConnectionStatus.Connected || currentStatus === ConnectionStatus.Connecting) { 
            return;
        }

        store.dispatch({ type: SignalRActionTypes.SET_STATUS, payload: ConnectionStatus.Connecting });

        try {
            await connection.start();
            store.dispatch({ type: SignalRActionTypes.SET_STATUS, payload: ConnectionStatus.Connected });

            // TODO: This should be better integrated into the sign-in process
            await connection.invoke("RequestCues");
        } catch {
            store.dispatch({ type: SignalRActionTypes.SET_STATUS, payload: ConnectionStatus.Disconnected });
            setTimeout(() => {
                store.dispatch({ type: SignalRActionTypes.RECONNECT });
            }, 1000);
        }
    }
}

export { signalRInvokeMiddleware, signalRRegisterCommands, retryPolicy }