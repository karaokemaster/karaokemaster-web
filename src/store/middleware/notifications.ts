/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import { action as typesafeAction } from 'typesafe-actions';

import { AppActionTypes, notificationsEnabledSelector, notificationSubscriptionSelector } from 'store/app';
import { QueueActionTypes, queueEntriesCountSelector } from 'store/queue';
import { UserProfileActionTypes } from 'store/userProfile';

const notificationsMiddleware = (store: any) => (next: any) => async (action: any) => {
    switch (action.type) {
        case UserProfileActionTypes.SUBSCRIBE_PUSH:
        case AppActionTypes.SERVER_INFO:
        case AppActionTypes.WAKE: {
            let status: boolean | undefined;

            if (false && !window.matchMedia('(display-mode: standalone)').matches) {
                // Only use notifications in PWA
                status = false;
            }
            else if (!("Notification" in window)) {
                // Notifications not supported
                status = false;
            }
            else if (Notification.permission === "granted") {
                status = true;
            }
            else if (Notification.permission === "denied") {
                status = false;
            }
            else {
                status = undefined;
            }

            store.dispatch(typesafeAction(AppActionTypes.UPDATE_NOTIFICATIONS, status));

            // Falls through on purpose...
        }
        case QueueActionTypes.UPDATE_QUEUE: {
            const state = store.getState();
            const status = notificationsEnabledSelector(state);

            const supportsAppBadge = 'setAppBadge' in navigator;

            if (status !== true || !supportsAppBadge) break;

            const badge = queueEntriesCountSelector(state);

            if (badge > 0) {
                navigator.setAppBadge(badge);
            } else {
                navigator.clearAppBadge();
            }

            break;
        }
        case UserProfileActionTypes.LOGIN_SUCCESS: {
            const state = store.getState();
            const notificationSubscription = notificationSubscriptionSelector(state);

            if (notificationSubscription === undefined) break;

            store.dispatch(typesafeAction(UserProfileActionTypes.SUBSCRIBE_PUSH, notificationSubscription));

            break;
        }
    }

    return next(action);
}

export { notificationsMiddleware }