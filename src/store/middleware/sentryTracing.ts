/*

    Copyright 2021 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as Sentry from '@sentry/browser';
import { Span } from '@sentry/tracing';
import { SeverityLevel } from '@sentry/types';

import { actionsBlacklist } from 'configureStore';

const sentryTracingMiddleware = (store: any) => (next: any) => async (action: any) => {
    const context = { name: action.type, description: action.type };
    let result: any;

    if (!actionsBlacklist.some(a => a === action.type)) {
        Sentry.addBreadcrumb({
            category: 'redux',
            message: action.type,
            level: 'info' as SeverityLevel,
            data: action.payload,
        });
    }

    Sentry.withScope(s => {
        let transaction: Span | undefined;

        try {
            const currentTransaction = s.getTransaction();
            transaction = (currentTransaction && currentTransaction.startChild({...context, op: 'redux'}))
                || Sentry.startTransaction(context);
            transaction.setData('payload', action.payload);
            s.setSpan(transaction);

            result = next(action);
        } catch (error) {
            Sentry.captureException(error);
        } finally {
            transaction?.finish();
        }
    });

    return result;
}

export { sentryTracingMiddleware };
