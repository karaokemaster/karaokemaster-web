/*

    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import createCachedSelector from "re-reselect";
import { createSelector } from 'reselect';
import { firstBy } from 'thenby';

import { ApplicationState } from 'store';
import { playedEntriesSelector } from 'store/queue';
import { Session } from './types';

export const todaySessionKey = new Date(1900, 1, 1);

export const ratedVideosSelector = ({ user }: ApplicationState) => user.rated;
export const ratedVideosLoadingSelector = ({ user }: ApplicationState) => user.loadingRated;

export const usersLoadingSelector = ({ user }: ApplicationState) => user.loadingUsers;
export const allUsersSelector = createSelector(
    ({ user }: ApplicationState) => user.users,
    (users) => users.sort(firstBy(u => u.username))
);

export const activeUsersSelector = createSelector(
    allUsersSelector,
    (users) => users.filter(u => u.isActive)
);

export const sessionLoadingSelector = createCachedSelector(
    ({ user }: ApplicationState) => user.loadingSession,
    (state: ApplicationState, session: Date) => session,
    (loadingSession, session) => {
        if (!loadingSession) return false;

        return new Date(loadingSession).getTime() === new Date(session).getTime()
    }
)(
    (state, session) => new Date(session).toISOString()
);

export const sessionsSelector = createSelector(
    ({ user }: ApplicationState) => user.sessions,
    playedEntriesSelector,
    activeUsersSelector,
    (sessions, played, users) => {
        const today: Session = {
            date: todaySessionKey,
            videoCount: played.length,
            videos: played.map(p => ({
                ...p,
                hasLyrics: p.lyrics?.length > 0,
                playCount: 0,
            })),
            users: users.map(u => u.username),
        };

        return [ today, ...sessions ];
    }
);

export const selectedSessionSelector = createSelector(
    sessionsSelector,
    ({ user: { selectedSession }}: ApplicationState) => selectedSession,
    (sessions, selectedSession) => {
        if (selectedSession === undefined || selectedSession === todaySessionKey) return sessions[0];

        const session = sessions.find(s => new Date(s.date).getTime() === new Date(selectedSession).getTime());

        return session;
    }
);

export const scrollPositionSelector = (tab: string) => (
    createSelector(
    (state: ApplicationState) => state,
    ({ user }: ApplicationState) => user.scrollPositions[tab] ?? 0,
));

export const selectedTabSelector = (area: string) => (
    createSelector(
        (state: ApplicationState) => state,
        ({ user }: ApplicationState) => user.areaTabs[area] ?? undefined,
    ));
