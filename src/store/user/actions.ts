/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { action } from 'typesafe-actions';

import { Video } from 'store/videos/types';
import { Session, User, UserActionTypes } from './types';

export const fetchActiveUsersRequest = ()                 => action(UserActionTypes.FETCH_ACTIVE_USERS_REQUEST)
export const fetchActiveUsersSuccess = (results: User[])  => action(UserActionTypes.FETCH_ACTIVE_USERS_SUCCESS, results)
export const fetchActiveUsersError   = (message: string)  => action(UserActionTypes.FETCH_ACTIVE_USERS_ERROR, message)
export const fetchRatedVideosRequest = ()                 => action(UserActionTypes.FETCH_RATED_VIDEOS_REQUEST)
export const fetchRatedVideosSuccess = (results: Video[]) => action(UserActionTypes.FETCH_RATED_VIDEOS_SUCCESS, results)
export const fetchRatedVideosError   = (message: string)  => action(UserActionTypes.FETCH_RATED_VIDEOS_ERROR, message)

export const selectSession = (session?: Date) => action(UserActionTypes.SELECT_SESSION, session);
export const fetchSessionSuccess = (session: Session) => action(UserActionTypes.FETCH_SESSION_SUCCESS, session);
export const fetchSessionError = (message: string) => action(UserActionTypes.FETCH_SESSION_ERROR, message);

export const setScrollPosition = (tab: string, position: number) => action(UserActionTypes.SET_SCROLL_POSITION, { tab, position });
export const setAreaTab = (area: string, tab: string) => action(UserActionTypes.SET_AREA_TAB, { area, tab });
