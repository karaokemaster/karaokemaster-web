/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { all, fork, put, select, takeEvery } from 'redux-saga/effects';

import { Session, User } from 'store/user/types';
import { authHeaderSelector } from 'store/userProfile';
import { Video } from 'store/videos/types';
import {
    fetchActiveUsersError,
    fetchActiveUsersSuccess,
    fetchRatedVideosError,
    fetchRatedVideosSuccess,
    fetchSessionError,
    fetchSessionSuccess
} from './actions';
import { todaySessionKey } from './selectors';
import { UserActionTypes } from './types';

import { runtimeConfig } from 'runtimeConfig';

function* handleFetchActiveUsers(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/User/getActive`, { headers });
        const results: User[] = yield response.json();
    
        if (!response.ok) {
            yield put(fetchActiveUsersError(yield response.text()));
        }
        else {
            yield put(fetchActiveUsersSuccess(results));
        }
    }
    catch (err) {
        yield put(fetchActiveUsersError('An unknown error occurred!'));
    }
}

function* handleFetchRatedVideos(action: any) {
    try {
        const headers: Headers = new Headers(yield select(authHeaderSelector));

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/user/ratedVideos`, { headers });
        const results: Video[] = yield response.json();

        if (!response.ok) {
            yield put(fetchRatedVideosError(yield response.text()));
        }
        else {
            yield put(fetchRatedVideosSuccess(results));
        }
    }
    catch (err) {
        yield put(fetchRatedVideosError('An unknown error occurred!'));
    }
}

function* handleSelectSession(action: any) {
    try {
        if (new Date(action.payload ?? todaySessionKey).getTime() === todaySessionKey.getTime()) return;

        const headers: Headers = new Headers(yield select(authHeaderSelector));
        const sessionKey = new Date(action.payload).toISOString();

        const response: Response = yield fetch(`${runtimeConfig().client.apiUrlBase}/v1/session/${sessionKey}`, { headers });
        const session: Session = yield response.json();

        if (!response.ok) {
            yield put(fetchSessionError(yield response.text()));
        }
        else {
            yield put(fetchSessionSuccess(session));
        }
    }
    catch (err) {
        yield put(fetchSessionError('An unknown error occurred!'));
    }
}

function* userSaga() {
    yield all([
        fork(function* () { yield takeEvery(UserActionTypes.FETCH_ACTIVE_USERS_REQUEST, handleFetchActiveUsers); }),
        fork(function* () { yield takeEvery(UserActionTypes.FETCH_RATED_VIDEOS_REQUEST, handleFetchRatedVideos); }),
        fork(function* () { yield takeEvery(UserActionTypes.SELECT_SESSION, handleSelectSession); }),
    ]);
}

export default userSaga;