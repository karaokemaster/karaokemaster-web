/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Video } from 'store/videos/types';

export enum UserActionTypes {
    FETCH_ACTIVE_USERS_REQUEST = '@@user/FETCH_ACTIVE_USERS_REQUEST',
    FETCH_ACTIVE_USERS_SUCCESS = '@@user/FETCH_ACTIVE_USERS_SUCCESS',
    FETCH_ACTIVE_USERS_ERROR   = '@@user/FETCH_ACTIVE_USERS_ERROR',
    FETCH_RATED_VIDEOS_REQUEST = '@@user/FETCH_RATED_VIDEOS_REQUEST',
    FETCH_RATED_VIDEOS_SUCCESS = '@@user/FETCH_RATED_VIDEOS_SUCCESS',
    FETCH_RATED_VIDEOS_ERROR   = '@@user/FETCH_RATED_VIDEOS_ERROR',
    UPDATE_SESSIONS = '@@user/SET_SESSIONS',
    SELECT_SESSION = '@@user/SELECT_SESSION',
    FETCH_SESSION_SUCCESS = '@@user/FETCH_SESSION_SUCCESS',
    FETCH_SESSION_ERROR = '@@user/FETCH_SESSION_ERROR',
    SET_SCROLL_POSITION = '@@user/SET_SCROLL_POSITION',
    SET_AREA_TAB = '@@user/SET_AREA_TAB',
}

export interface Session {
    readonly date: Date
    readonly videoCount: number
    readonly videos: Video[]
    readonly users: string[]
}

export interface ScrollPositions {
    [index: string]: number
}
export interface AreaTabs {
    [index: string]: string
}

export interface UserState {
    readonly selectedSession?: Date
    readonly sessions: Session[]
    readonly loadingSession?: Date
    readonly rated: Video[]
    readonly loadingRated: boolean
    readonly scrollPositions: ScrollPositions
    readonly areaTabs: AreaTabs
    readonly users: User[]
    readonly loadingUsers: boolean
}

export interface User {
    readonly userId: number
    readonly username: string
    readonly icon: string
    readonly color: string
    readonly isActive: boolean
}