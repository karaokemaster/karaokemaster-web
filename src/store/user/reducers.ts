/*
    
    Copyright 2019-2022 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

import { Reducer } from 'redux';

import { UserProfileActionTypes } from 'store/userProfile';
import { Video } from 'store/videos/types';
import { todaySessionKey } from './selectors';
import { AreaTabs, ScrollPositions, Session, User, UserActionTypes, UserState } from './types';

const initialState: UserState = {
    sessions: [],
    loadingSession: undefined,
    rated: [],
    loadingRated: false,
    scrollPositions: {} as ScrollPositions,
    areaTabs: {} as AreaTabs,
    users: [],
    loadingUsers: false,
}

const reducer: Reducer<UserState> = (state = initialState, action) => {
    switch (action.type) {
        case UserActionTypes.FETCH_ACTIVE_USERS_REQUEST: {
            return { ...state, loadingUsers: true, users: [] }
        }
        case UserActionTypes.FETCH_ACTIVE_USERS_SUCCESS: {
            const users = (action.payload as User[]);
            return { ...state, users, loadingUsers: false }
        }
        case UserActionTypes.FETCH_ACTIVE_USERS_ERROR: {
            return { ...state, loadingUsers: false, users: [] }
        }
        case UserActionTypes.FETCH_RATED_VIDEOS_REQUEST: {
            return { ...state, loadingRated: true, rated: [] }
        }
        case UserActionTypes.FETCH_RATED_VIDEOS_SUCCESS: {
            const rated = (action.payload as Video[]);
            return { ...state, rated, loadingRated: false }
        }
        case UserActionTypes.FETCH_RATED_VIDEOS_ERROR: {
            return { ...state, loadingRated: false, rated: [] }
        }
        case UserActionTypes.UPDATE_SESSIONS: {
            return { ...state, sessions: action.payload }
        }
        case UserActionTypes.FETCH_SESSION_SUCCESS: {
            const session: Session = action.payload;
            const sessions = [
                ...state.sessions.filter(s => s.date !== session.date),
                session
            ].sort((a, b) => (new Date(b.date).getTime() - new Date(a.date).getTime()));

            return { ...state, sessions, loadingSession: undefined }
        }
        case UserActionTypes.FETCH_SESSION_ERROR: {
            // TODO: Report status to user
            return { ...state, loadingSession: undefined }
        }
        case UserActionTypes.SELECT_SESSION: {
            const session = (action.payload as Date);
            if (new Date(session ?? todaySessionKey).getTime() === todaySessionKey.getTime())
                return { ...state, selectedSession: action.payload , loadingSession: undefined }

            return { ...state, selectedSession: action.payload , loadingSession: session }
        }
        case UserActionTypes.SET_SCROLL_POSITION: {
            const { tab, position } = action.payload;
            const scrollPositions: ScrollPositions = state.scrollPositions;
            scrollPositions[tab] = position;

            return { ...state, scrollPositions }
        }
        case UserActionTypes.SET_AREA_TAB: {
            const { area, tab } = action.payload;
            const areaTabs: AreaTabs = state.areaTabs;
            areaTabs[area] = tab;

            return { ...state, areaTabs }
        }
        case UserProfileActionTypes.LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }

}

export { reducer as userReducer }