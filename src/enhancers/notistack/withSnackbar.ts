import {EnhancerResult} from '@truefit/bach';
import {useSnackbar} from 'notistack';
import {CLOSE_SNACKBAR, ENQUEUE_SNACKBAR} from './constants';

export default (enqueueSnackbar: string = ENQUEUE_SNACKBAR, closeSnackbar: string = CLOSE_SNACKBAR) =>
    (): EnhancerResult => {
        return {
            dependencies: {
                useSnackbar,
            },
            initialize: `const {${enqueueSnackbar}, ${closeSnackbar}} = useSnackbar();`,
            props: [enqueueSnackbar, closeSnackbar],
        };
    };