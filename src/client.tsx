/*

    Copyright 2019-2023 Mike Peschka

    This file is part of KaraokeMaster.

    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

 */

import * as Sentry from '@sentry/browser';
import { Integrations } from '@sentry/tracing';
import { SamplingContext } from '@sentry/types';
import { createBrowserHistory } from 'history';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createReduxHistoryContext } from 'redux-first-history';

import App from 'components/App';
import configureStore from 'configureStore';
import { runtimeConfig } from 'runtimeConfig';
import { PlayerActionTypes } from 'store/player';

import gitVersion from 'gitversion.json';

let reg: ServiceWorkerRegistration | undefined;

window.addEventListener("load", async () => {
    if ("serviceWorker" in navigator) {
        reg = await navigator.serviceWorker.register(new URL('./worker.ts', import.meta.url), { scope: '/' });
        // await navigator.serviceWorker.register('/service-worker.js');
    }
});

Sentry.init({
    dsn: runtimeConfig().client.sentryDsn,
    release: `kmweb-${gitVersion.SemVer}`,
    environment: runtimeConfig().environment,
    integrations: [
        new Sentry.Integrations.GlobalHandlers({ onerror: false, onunhandledrejection: true }),
        new Sentry.Integrations.TryCatch({ setTimeout: false, setInterval: false, requestAnimationFrame: false, XMLHttpRequest: false, eventTarget: false }),
        new Integrations.BrowserTracing({
            tracingOrigins: ["localhost", /^\//, runtimeConfig().client.displayUrl, ...runtimeConfig().client.sentryTracingOrigins],
            // ... other options
        }),
    ],
    beforeSend: (event, hint) => {
        try {
            // @ts-ignore
            if (event.exception.values[0].mechanism.handled) return null;
        } catch {}

        return event;
    },

    // tracesSampleRate: 1.0,
    tracesSampler: (samplingContext: SamplingContext) => {
        // TODO: Sample less often
        const name = samplingContext.transactionContext.name;
        if (name === PlayerActionTypes.SET_STATE || name === PlayerActionTypes.STATE_RECEIVED || name === PlayerActionTypes.BEAT)
            return false;

        return true;
    },
});

// TODO: Ensure correct URL
const { createReduxHistory, routerMiddleware, routerReducer } = createReduxHistoryContext({
    history: createBrowserHistory(),
});

const initialState = (window as any).initialReduxState;
const store = configureStore(routerMiddleware, routerReducer, initialState);
const history = createReduxHistory(store);

const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;
renderMethod(
    <Provider store={store}>
        <App history={history} />
    </Provider>,
    document.getElementById("root"),
);

if ((module as any).hot) {
    (module as any).hot.accept();
}

export { reg }