import * as http from 'http';
import { loadConfig } from "./serverConfig";

/* tslint:disable:no-console */

// tslint:disable-next-line:no-var-requires
let app = require('./server').default;

const server: any = http.createServer(app);

let currentApp = app;

loadConfig().then((config) => {
  server.listen(config.port, (error: any) => {
    if (error) {
      console.error(`Startup error: ${error}`);
    }

    console.log('🚀 started');
    console.log(`👂 listening on port ${config.port}!`);
  });
});

if ((module as any).hot) {
  console.log('✅  Server-side HMR Enabled!');

  (module as any).hot.accept('./server', () => {
    console.log('🔁  HMR Reloading `./server`...');

    try {
      app = require('./server').default;
      server.removeListener('request', currentApp);
      server.on('request', app);
      currentApp = app;
    } catch (error) {
      console.error(error);
    }
  });
}

/* tslint:enable:no-console */
