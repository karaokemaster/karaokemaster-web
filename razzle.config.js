//./razzle.config.js

const path = require('path');

module.exports = {
    plugins: [
        'scss',
    ],
    modifyWebpackConfig({
                            env: {
                                target, // the target 'node' or 'web'
                                dev, // is this a development build? true or false
                            },
                            webpackConfig, // the created webpack config
                            webpackObject, // the imported webpack node module
                            options: {
                                razzleOptions, // the modified options passed to Razzle in the `options` key in `razzle.config.js` (options: { key: 'value'})
                                webpackOptions, // the modified options that was used to configure webpack/ webpack loaders and plugins
                            },
                            paths, // the modified paths that will be used by Razzle.
                        }) {
        const appConfig = webpackConfig; // stay immutable here

        const srcPath = path.resolve("./")
        appConfig.resolve.modules.push(srcPath);

        if (!appConfig.module.rules) {
            appConfig.module.rules = [];
        }
        appConfig.module.rules.push({
            test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            type: 'asset/resource',
        });

        if (!dev) {
            appConfig.performance = Object.assign({}, {
                maxAssetSize: 100000,
                maxEntrypointSize: 300000,
                hints: false
            });
        }

        appConfig.entry = {
            ...appConfig.entry,
            // 'service-worker': {
            //     import: './src/worker.ts',
            //     filename: '[name].js',
            // },
        };

        if (dev) {
            appConfig.devServer = {
                ...appConfig.devServer,
                allowedHosts: 'all',
            };
        }

        return appConfig;
    },
};