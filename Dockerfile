FROM --platform=$BUILDPLATFORM node:16 AS build-env

# Create app directory
WORKDIR /usr/src/karaokemaster-web

COPY . ./

RUN npx update-browserslist-db@latest
RUN yarn install --frozen-lockfile --ignore-engines
RUN yarn build --noninteractive

RUN \
    mv gitversion.json ./src && \
    mv .sentryclirc ~/ && \
    export SENTRY_RELEASE="kmweb-$(node -e "const gvt = $(cat src/gitversion.json); console.log(gvt.SemVer);")" && \
    export RAZZLE_SENTRY_RELEASE=$SENTRY_RELEASE && \
    curl -sL https://sentry.io/get-cli/ | bash && \
    sentry-cli releases new $SENTRY_RELEASE && \
    sentry-cli releases set-commits $SENTRY_RELEASE --auto && \
    sentry-cli releases files $SENTRY_RELEASE upload-sourcemaps ./build/public/ && \
    sentry-cli releases finalize $SENTRY_RELEASE

# Runtime

FROM node:16

# Create app directory
WORKDIR /usr/src/karaokemaster-web

# Install app dependencies
COPY ./package.json ./
COPY ./yarn.lock ./
COPY ./.npmrc ./
# yarn will try to restore dev dependencies, even with the production flag, which fails in QEMU builds...
# renaming devDependencies fixes that.
RUN sed -i -e 's/"devDependencies"/"DISABLED_devDependencies"/g' package.json
RUN yarn install --production --frozen-lockfile --ignore-engines --network-timeout 600000

# Bundle app source
COPY --from=build-env /usr/src/karaokemaster-web/build .
COPY ./entrypoint.sh .
RUN chmod +x ./entrypoint.sh

# Pick the correct port
EXPOSE 3000

CMD [ "node", "server.js" ]
ENTRYPOINT [ "/usr/src/karaokemaster-web/entrypoint.sh" ]