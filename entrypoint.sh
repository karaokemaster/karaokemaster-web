#!/bin/sh

cp -r . /var/opt/karaokemaster-web
rm -f /var/opt/karaokemaster-web/entrypoint.sh

# This will exec the CMD from your Dockerfile, i.e. "npm start"
exec "$@"